# CLOUD CLOVER EDITOR (CCE)

   ![](style/img/cce-logo-alt.svg)
   
   **CCE** allows you to manage **OpenCore**, **CloverEFI**, **Ozmosis** and **Chameleon** configs everywhere!

   [Open CCE](https://cloudclovereditor.altervista.org)  

   [Official CCE Thread](http://www.insanelymac.com/forum/topic/313284-cloud-clover-editor-cce/)

   [CCE Wiki](https://bitbucket.org/kylon/cloud-clover-editor-cce/wiki/Home)
   
### How does it work?

  Config files are stored in a PHP session variable as arrays.  
  Each config array is updated in real-time and stored again in your current session.

   This means that:

   * CCE does not work with files, thus it does not require a place to save them
   * CCE does not need to handle files creation, files deletion, etc...
   * You **can** safely reload the page, manually, or by mistake, with no data loss

### CCE requirements

    * HTTP Server (Apache, Nginx, ...)
    * PHP 7.3.0+ (latest version is always recommended for better performance/security)
    * Sqlite3 PHP extension
    * npm
    * composer
    * Any OS

### How to setup CCE at home
   >1. Download CCE locally with the following command:  
   `$ git clone --recurse-submodules https://bitbucket.org/kylon/cloud-clover-editor-cce`
   >2. Run `install-deps.sh` to install the required packages   
   >3. Rename 'cce/data/res/cce_configs_sample.db' to cce_configs.db if you want to enable CCE Bank. [Optional]  
   >4. Implement CRSF token functions in **cce/data/user_fn.php** [Optional]
   >5. You are done!

### Maintenance Mode
    Create a new empty file with no extension named "maintenance",
    without quotes, in the root folder of CCE.
    
    Delete this file to restore CCE.

### FAQs

   >Q: Looks interesting, how can i help?  
   >A: Fork CCE, make your changes and open a Pull Request.

   >Q: I found a bug! / I have an issue!  
   >A: Open a new issue [here](https://bitbucket.org/kylon/cloud-clover-editor-cce/issues?status=new&status=open) or reply [here](http://www.insanelymac.com/forum/topic/313284-cloud-clover-editor-cce/)  


### Sources

  [CloudCloverEditor](https://bitbucket.org/kylon/cloud-clover-editor-cce)  
  [CFPropertyList-CCE](https://bitbucket.org/kylon/cfpropertylist-cce)  
  [Jquery](https://github.com/jquery/jquery)  
  [Jquery UI](https://github.com/jquery/jquery-ui)  
  [Jquery UI Punch](http://touchpunch.furf.com/)  
  [Jquery Paging](https://github.com/infusion/jQuery-Paging)  
  [Boostrap](https://github.com/twbs/bootstrap)  
  [Font Awesome](https://github.com/FortAwesome/Font-Awesome)  
  [PsScrollbar](https://bitbucket.org/kylon/perfect-scrollbar)  
  [macserial](https://github.com/acidanthera/OpenCorePkg)    
  [codeMirror](https://github.com/codemirror/CodeMirror)

### Credits

  mackie100 - took some ideas from its app  
  Clover EFI dev team  
  Eric Slivka - new serial number  
  Virtual1 - new serial number  
  cecekpawon - PHP 5.3.3 patch, help with the ACPI Loader Mode flag and more  
  Micky1979 - Clover flying editor (Discontinued)  
  crusher - Help with the ACPI Loader Mode flag  
  Download-Fritz - Help with the ACPI Loader Mode flag  
  Pavo - Ozmosis fields and values  
  stehor - Ozmosis fields and values  
  Sherlocks - General help and support  
  gujiangjiang - General help and support  
  vit9696 - OpenCore data values and more
  Freepik from www.flaticon.com - a few svg icons

  *Please let me know if i forgot you!*