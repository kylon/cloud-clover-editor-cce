<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE;

use CFPropertyList\CFArray;
use CFPropertyList\CFBoolean;
use CFPropertyList\CFData;
use CFPropertyList\CFDictionary;
use CFPropertyList\CFNumber;
use CFPropertyList\CFString;
use CFPropertyList\CFType;
use CFPropertyList\CFTypeDetector;

require_once __DIR__.'/../modules/cfpropertylist-cce/vendor/autoload.php';
require_once __DIR__.'/../utils.php';

abstract class abstractPlist {
    /**
     * Instance of CFTypeDetector
     *
     * @var ?CFTypeDetector
     */
    protected ?CFTypeDetector $cfTypeDetector = null;

    /**
     * Parsed plist
     *
     * @var array
     */
    protected array $plist = [];

    /**
     * Parsed plist as string
     *
     * @var string
     */
    protected string $plist_string = '';

    /**
     * Flag that is set when an error occurred
     *
     * @var bool
     */
    protected bool $hasError = false;

    /**
     * Last exception (if any)
     *
     * @var mixed
     */
    protected mixed $error = null;

    /**
     * Return true if the error flag is set
     *
     * @return bool
     */
    public function hasError(): bool {
        return $this->hasError;
    }

    /**
     * Generate and return the plist as file
     *
     * @param bool
     *
     * @return string
     */
    public function getPlistFile(bool $indent = true): string {
        return $this->genPlist($this->plist, $indent);
    }

    /**
     * Return the plist string, if set
     *
     * @return string
     */
    public function getPlistString(): string {
        return $this->plist_string;
    }

    /**
     * Return the plist as array
     *
     * @return array
     */
    public function getPlist(): array {
        return $this->plist;
    }

    /**
     * Set a new parsed plist
     *
     * @param array
     */
    public function setPlist(array $newPlist = []): void {
        unset($this->plist);
        $this->resetErrorFlag();

        $this->plist = $newPlist;
    }

    /**
     * Set the plist string
     */
    public function setPlistString(): void {
        $this->plist_string = $this->getPlistFile();
    }

    /**
     * Update the plist string
     *
     * @param string
     *
     * @note You will need to parse plist_string to sync
     * @note the plist array and do all the operations
     * @note on this file
     */
    public function updatePlistString(string $string): void {
        $this->plist_string = $string;
    }

    /**
     * Parse a plist from file
     *
     * @param string|null file content
     */
    public function parsePlist(?string $file = ''): void {
        $this->resetErrorFlag();
        $this->plist_string = '';

        try {
            if ($file === '')
                $file = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><dict></dict></plist>';
            else if ($file === null)
                throw new \Exception();

            $cfObj = new \CFPropertyList\CFPropertyList();

            $cfObj->parse($file);
            $this->plist = $cfObj->toArray();

            unset($cfObj);

        } catch (\Throwable $e ) {
            $this->setErrorFlag($e);
            $this->plist = [];
        }
    }

    /**
     * Check if a value exists
     *
     * @param string $path
     *
     * @return bool
     */
    public function hasValue(string $path): bool {
        return $this->cd($path) !== null;
    }

    /**
     * Return the value of an option as CFType objects
     *
     * @param string $path
     *
     * @return array|CFType|null
     */
    public function getVals(string $path): array|CFType|null {
        return $this->cd($path);
    }

    /**
     * Return the raw value of an option
     *
     * @param string
     *
     * @return mixed
     */
    public function getRawVals(string $path): mixed {
        $data = $this->cd($path);

        if (is_array($data)) {
            $this->arrayToRawVals($data);
            return $data;

        } else if ($data === null) {
            return null;
        }

        $v = $data->getValue();

        return gettype($v) === 'string' ? \sanitizeString($v) : $v;
    }

    /**
     * Set a value
     *
     * @param string $path
     * @param string $key
     * @param mixed $val
     * @param string|boolean $dataType
     *
     * @throws \CFPropertyList\PListInvalidDataConversionException
     */
    public function setVal(string $path, string $key, mixed $val, string|bool $dataType=false): void {
        $tmp =& $this->cd($path);

        if ($tmp === null)
            $tmp =& $this->mkPath($path);

        if (!$dataType)
            $tmp[$key] = $this->cfTypeDetector->toCFTypeSimple($val);
        else
            $tmp[$key] = $this->getCFTypeObj($dataType, $val);

        if (!is_numeric($key))
            ksort($tmp);
    }

    /**
     * Change the CFType of the value in the given path
     *
     * @param string $path
     * @param string $dataType
     *
     * @throws \CFPropertyList\PListInvalidDataConversionException
     */
    public function setValType(string $path, string $dataType): void {
        $tmp =& $this->cd($path);

        if (!($tmp instanceof CFType) || $tmp instanceof CFArray || $tmp instanceof CFDictionary)
            return;

        switch ($dataType) {
            case 'string':
                $tmp = new CFString(strval($tmp->getValue()));
                break;
            case 'data':
                $tmp = new CFData($tmp->getValue());
                break;
            case 'bool':
                $tmp = new CFBoolean($tmp->getValue());
                break;
            case 'integer':
            case 'real':
                $tmp = new CFNumber($tmp->getValue());
                break;
            default:
                break;
        }
    }

    /**
     * Unset a value
     *
     * @param string
     * @param string
     */
    public function unsetVal(string $path, string $key): void {
        $tmp =& $this->cd($path);

        if ($path === '/') { // root key
            unset($tmp[$key]);
            return;
        }

        if ($tmp === null)
            return;

        unset($tmp[$key]);

        if (is_numeric($key))
            $tmp = array_values($tmp);
    }

    /**
     * Update a key in the given path
     *
     * @param string $path
     * @param int|string $oldKey
     * @param int|string $newKey
     */
    public function updateKey(string $path, int|string $oldKey, int|string $newKey): void {
        $tmp =& $this->cd($path);

        if ($tmp === null || !isset($tmp[$oldKey]))
            return;

        $tmp[$newKey] = $tmp[$oldKey];
        unset($tmp[$oldKey]);
    }

    /**
     * Swap two elements position
     *
     * @param string $path
     * @param int $oldKey
     * @param int $newKey
     *
     * @note Associative arrays not supported
     */
    public function swapElems(string $path, int $oldKey, int $newKey): void {
        $tmp =& $this->cd($path);

        if (isset($tmp[$oldKey]) && isset($tmp[$newKey])) {
            $oldKeyVal = $tmp[$oldKey];

            unset($tmp[$oldKey]);
            array_splice($tmp, $newKey, 0, [$oldKeyVal]);

            $tmp = array_values($tmp);
        }
    }

    /**
     * Return true if the plist is empty, false otherwise
     *
     * @return bool
     */
    public function isEmpty(): bool {
        return empty($this->plist);
    }

    /**
     * Return last error
     *
     * @return mixed
     */
    public function getError(): mixed {
        return $this->error;
    }

    /**
     * Create a download dialog.
     *
     * @param string
     *
     * @return boolean
     */
    public function export(string $fileName='cce-config'): bool {
        if ($this->plist_string === '')
            $file = $this->genPlist($this->plist);
        else
            $file = $this->plist_string;

        if (!$file)
            return false;

        header("Cache-Control: no-cache, must-revalidate");
        header("Content-Type: text/xml");
        header('Content-Disposition: attachment; filename="'.$fileName.'.plist"');

        echo $file;
        exit();
    }

    /**
     * Navigate the config array
     *
     * @param string
     *
     * @return array|CFType|null
     */
    protected function &cd(string $path): array|CFType|null {
        if ($path === '/')
            return $this->plist;

        $cPath = explode('/', $path);
        $tmp =& $this->plist;
        $err = null;

        for ($i=0, $len=count($cPath); $i<$len; ++$i) {
            if ($cPath[$i] == null)
                continue;

            $nPath = str_replace('%', '/', $cPath[$i]);

            if (!is_array($tmp) || !array_key_exists($nPath, $tmp))
                return $err;

            $tmp =& $tmp[$nPath];
        }

        return $tmp;
    }

    /**
     * Create all the missing levels of a given path.
     *
     * @param string
     *
     * @return array
     */
    protected function &mkPath(string $path): array {
        $tmp =& $this->plist;
        $pathEl = explode('/', $path);

        if ($tmp === null)
            $tmp = [];

        for ($i=0, $len=count($pathEl); $i<$len; ++$i) {
            if ($pathEl[$i] == null)
                continue;

            if (!array_key_exists($pathEl[$i], $tmp)) {
                $tmp += [$pathEl[$i] => []];
                ksort($tmp);
            }

            $tmp =& $tmp[$pathEl[$i]];
        }

        return $tmp;
    }

    /**
     * @param string $type
     * @param mixed $val
     *
     * @return CFType
     * @throws \CFPropertyList\PListInvalidDataConversionException
     */
    protected function getCFTypeObj(string $type, mixed $val): CFType {
        return match ($type) {
            'bool' => new CFBoolean($val),
            'data' => new CFData($val),
            'integer', 'real' => new CFNumber($val),
            default => new CFString($val),
        };
    }

    /**
     * Get the raw values from a CFType objects array
     *
     * @param array $data
     */
    private function arrayToRawVals(array &$data): void {
        foreach ($data as $key => &$val) {
            if (is_array($val)) {
                $this->arrayToRawVals($val);
                continue;
            }

            $v = $val->getValue();
            $val = gettype($v) === 'string' ? \sanitizeString($v) : $v;
        }
    }

    /**
     * Generate a plist file
     *
     * @param array
     * @param bool
     *
     * @return string|boolean
     */
    protected function genPlist(array $config, bool $indent = true): bool|string {
        $ret = false;

        $this->resetErrorFlag();

        try {
            $cfObj = new \CFPropertyList\CFPropertyList();

            $cfObj->add( $this->cfTypeDetector->toCFType($config) );
            $ret = $cfObj->toXML($indent);

            unset($cfObj);

        } catch (\Throwable $e) {
            $this->setErrorFlag($e);
        }

        return $ret;
    }

    private function resetErrorFlag(): void {
        $this->hasError = false;
        $this->error = null;
    }

    private function setErrorFlag(\Throwable $e): void {
        $this->error = $e;
        $this->hasError = true;
    }
}
