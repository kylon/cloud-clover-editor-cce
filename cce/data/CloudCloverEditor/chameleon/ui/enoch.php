<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\chameleon_UI;

require_once __DIR__.'/../../include/data/graphics.php';

function enoch(): string {
    try {
        global $text, $config, $fbNamesCombo, $igPlatIDCombo, $dropTb, $acpiMisc, $cpuOp,
               $audioOp, $gfxOp, $guiOp, $sysOp, $bootArgs, $kernelArch, $sipFOpts;

        $scrResOp = '1024x600x32;1024x768x32;1280x768x32;1280x800x32;1280x960x32;1280x1024x32;'.
                    '1366x768x32;1440x900x32;1600x900x32;1680x1050x32;1920x1080x32;1920x1200x32;'.
                    '2048x1252x32;2048x1536x32;2560x1600x32;2560x2048x32';

        $curCSR = $config->getRawVals('/CsrActiveConfig') ?? '';

        $dsdtInpt = drawSimpleInput('string', '/', 'DSDT', 'DSDT', '', '', '/Extra/DSDT.aml');
        $hpetInpt = drawSimpleInput('string', '/', 'HPET', 'HPET', '', '', '/Extra/HPET.aml');
        $sbstInpt = drawSimpleInput('string', '/', 'SBST', 'SBST', '', '', '/Extra/SBST.aml');
        $ecdtInpt = drawSimpleInput('string', '/', 'ECDT', 'ECDT', '', '', '/Extra/ECDT.aml');
        $asftInpt = drawSimpleInput('string', '/', 'ASFT', 'ASFT', '', '', '/Extra/ASFT.aml');
        $dmarInpt = drawSimpleInput('string', '/', 'DMAR', 'DMAR', '', '', '/Extra/DMAR.aml');
        $apicInpt = drawSimpleInput('string', '/', 'APIC', 'APIC', '', '', '/Extra/APIC.aml');
        $mcfgInpt = drawSimpleInput('string', '/', 'MCFG', 'MCFG', '', '', '/Extra/MCFG.aml');
        $fadtInpt = drawSimpleInput('string', '/', 'FADT', 'FADT', '', '', '/Extra/FADT.aml');
        $bgrtInpt = drawSimpleInput('string', '/', 'BGRT', 'BGRT', '', '', '/Extra/BGRT.aml');
        $dropOptions = drawSimpleInlineCheckOpts($dropTb, '/');
        $wakeImgInpt = drawSimpleInput('string', '/', 'WakeImage', 'wake_img', '', '', '/private/var/vm/sleepimage');
        $acpiMiscOptions = drawSimpleInlineCheckOpts($acpiMisc, '/');
        $busRatioInpt = drawSimpleInput('integer', '/', 'busratio', 'cpu_bus_ratio', '', '', '', true);
        $cpuOptions = drawSimpleInlineCheckOpts($cpuOp, '/');
        $hdefLayoutIdInpt = drawSimpleInput('string', '/', 'HDEFLayoutID', 'hdef_id', '', '', 'Hex');
        $hdaLayoutIdInpt = drawSimpleInput('string', '/', 'HDAULayoutID', 'hdau_id', '', '', 'Hex');
        $audioOptions = drawSimpleInlineCheckOpts($audioOp, '/');
        $gfxOptions = drawSimpleInlineCheckOpts($gfxOp, '/');
        $capriFbInpt = drawSimpleNumericSelect('/', 'IntelCapriFB', range(0, 11), 'capri_fb', PHP_INT_MIN, 'caprifb', '', '', true);
        $azulFbInpt = drawSimpleNumericSelect('/', 'IntelAzulFB', range(0, 16), 'azul_fb', PHP_INT_MIN, 'azulfb', '', '',  true);
        $bdwFbInpt = drawSimpleNumericSelect('/', 'IntelBdwFB', range(0, 19), 'bdw_fb', PHP_INT_MIN, 'broadfb', '', '',  true);
        $sklFbInpt = drawSimpleNumericSelect('/', 'IntelSklFB', range(0, 12), 'skl_fb', PHP_INT_MIN, 'sklfb', '', '',  true);
        $igPlatIdInpt = drawComboboxSimpleInput('string', '/', 'InjectIntel-ig', 'ig_platform_id', $igPlatIDCombo, '', 'injig');
        $atiFbInpt = drawComboboxSimpleInput('string', '/', 'AtiConfig', 'ati_config', $fbNamesCombo, '', 'atic');
        $atiPortsInpt = drawSimpleInput('integer', '/', 'AtiPorts', 'ati_ports', '', '', '', true);
        $nvDisplay0Inpt = drawSimpleInput('string', '/', 'display_0', 'nv_display0', '', 'nv0', 'Hex');
        $nvDisplay1Inpt = drawSimpleInput('string', '/', 'display_1', 'nv_display1', '', 'nv1', 'Hex');
        $guiOptions = drawSimpleInlineCheckOpts($guiOp, '/');
        $defPartitionInpt = drawSimpleTextArea('string', '/', 'Default Partition', 'def_partition', 2);
        $hidePartitionInpt = drawSimpleTextArea('string', '/', 'Hide Partition', 'hide_partition', 2);
        $renamePartitionInpt = drawSimpleTextArea('string', '/', 'Rename Partition', 'ren_partition', 2);
        $tmoutInpt = drawSimpleInput('integer', '/', 'Timeout', 'timeout', '', '', '', true);
        $keyLayoutInpt = drawSimpleInput('string', '/', 'KeyLayout', 'key_layout');
        $graphicsModeInpt = drawComboboxSimpleInput('string', '/', 'Graphics Mode', 'gfx_mode', $scrResOp, '', 'gres');
        $textModeInpt = drawSimpleInput('string', '/', 'Text Mode', 'text_mode');
        $pciRootInpt = drawSimpleInput('string', '/', 'PciRoot', 'pci_root');
        $csrInpt = drawCSRInput($curCSR, '/', 'CsrActiveConfig');
        $csrModal = drawSimpleFlagModal($sipFOpts, $curCSR, 'csrf', true);
        $sysOptions = drawSimpleInlineCheckOpts($sysOp, '/');
        $bootArgsInpt = drawBootArgsSelect($bootArgs);
        $kernFlagsInpt = drawTextAreaNoLabel('string', '/', 'Kernel Flags', 3, '', '', '', 'b-args-tx');
        $kernNameInpt = drawSimpleInput('string', '/', 'Kernel', 'kernel_name');
        $kernCacheInpt = drawSimpleInput('string', '/', 'Kernel Cache', 'kernel_cache');
        $kernArchInpt = drawSimpleSelect('/', 'Kernel Architecture', $kernelArch, 'kernel_arch', '', 'karch');
        $kextPlistInpt = drawSimpleInput('string', '/', 'KEXTPlist', 'Kext Plist', '', '', '/Extra/Kext.plist');
        $kernPlistInpt = drawSimpleInput('string', '/', 'KERNELPlist', 'Kernel Plist', '', '', '/Extra/Kernel.plist');
        $smbiosPlistInpt = drawSimpleInput('string', '/', 'SMBIOS', 'SMBIOS Plist', '', '', '/Extra/smbios.plist');
        $md0RamdiskInpt = drawSimpleInput('string', '/', 'md0', 'md0 Ramdisk', '', '', '/Extra/Postboot.img');

        return "<div class=\"row\"><div class=\"col-12 title\">ACPI</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-2\">{$dsdtInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$hpetInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$sbstInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$ecdtInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$asftInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$dmarInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-2\">{$apicInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$mcfgInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$fadtInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$bgrtInpt}</div>
                    <!-- col-lg-4 here -->
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['drop_tables']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$dropOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 col-lg-5\">{$wakeImgInpt}</div>
                    <div class=\"col-12 col-lg-7\">{$acpiMiscOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">CPU</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$busRatioInpt}</div>
                    <div class=\"col-12 col-md-8 col-lg-10\">{$cpuOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">Audio</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-3 col-lg-2\">{$hdefLayoutIdInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$hdaLayoutIdInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-8\">{$audioOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['graphics']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12\">{$gfxOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">Intel</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$capriFbInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$azulFbInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$bdwFbInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$sklFbInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$igPlatIdInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">ATI</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12 col-md-8 col-lg-8\">{$atiFbInpt}</div>
                            <div class=\"col-12 col-md-4 col-lg-4\">{$atiPortsInpt}</div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-lg-6\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">Nvidia</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12 col-md-6 col-lg-6\">{$nvDisplay0Inpt}</div>
                            <div class=\"col-12 col-md-6 col-lg-6\">{$nvDisplay1Inpt}</div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">GUI</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12\">{$guiOptions}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 col-lg-4\">{$defPartitionInpt}</div>
                    <div class=\"col-12 col-lg-4\">{$hidePartitionInpt}</div>
                    <div class=\"col-12 col-lg-4\">{$renamePartitionInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-2 col-lg-2\">{$tmoutInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$keyLayoutInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$graphicsModeInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$textModeInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['system']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-8 col-lg-4\">{$pciRootInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$csrInpt}</div>
                    <div class=\"col-12 col-lg-6\">{$sysOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">Kernel Flags</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-4\">
                        <div class=\"row\">
                            <div class=\"col-12\">{$bootArgsInpt}</div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-12\">{$kernFlagsInpt}</div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-md-6 col-lg-3\">{$kernNameInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$kernCacheInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$kernArchInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">Files</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-3\">{$kextPlistInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$kernPlistInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$smbiosPlistInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$md0RamdiskInpt}</div>
                </div>
            
                <!-- CSR Modal -->
                {$csrModal}";
    } catch (\Throwable) {}

    return "";
}