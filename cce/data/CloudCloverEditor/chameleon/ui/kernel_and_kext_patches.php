<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\chameleon_UI;

require_once __DIR__.'/../../include/fancypanel/fancypanel_main.php';

function kernel_and_kext_patches(): string {
    try {
        global $text, $config, $kernOp, $kextOp;

        $kextPfpdata = [
            'pdata' => [],
            'path' => '',
            'type' => 'chamKextP',
        ];

        $kernOptions = drawSimpleInlineCheckOpts($kernOp, '/');
        $kernPathPanel = draw_fancy_panel($config->getVals('/KernelPatches'), 'chamkernelP', 'KernelPatches', 'kernel_patch');
        $kextOptions = drawSimpleInlineCheckOpts($kextOp, '/');
        $kextPathTable = drawPatchTable('chamkextP', ['kext_name', 'comment'], $config->getVals('/KextsPatches'), ['edit']);
        $kextPathModal = drawModalBodySkel($kextPfpdata, -1, '', 'drawChamKextPatchModal');
        $forceKextTable = drawPatchTable('forcechamKx', ['name'], $config->getRawVals('/ForceToLoad'), ['cp']);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['kernel_patch']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12\">{$kernOptions}</div>
                </div>
            
                {$kernPathPanel}
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['kext_patch']}</div>
                </div>
            
                <div class=\"row mb-3\">
                    <div class=\"col-12\">{$kextOptions}</div>
                </div>
            
                {$kextPathTable}
            
                <div class=\"row mt-2\">
                    <div class=\"col-12 subtitle\">{$text['force_kext']}</div>
                </div>
            
                {$forceKextTable}
            
                <!-- Chameleon Kext Patch Modal -->
                {$kextPathModal}";
    } catch (\Throwable) {}

    return "";
}