<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\chameleon_UI;

require_once __DIR__.'/../../include/data/smbios.php';
require_once __DIR__.'/../../include/fn/smbios.php';

function smbios(): string {
    try {
        global $text, $config, $chassAssetTag, $boardT, $chassT;

        $selected = strtolower(str_replace([' ', ','], '', $config->getRawVals('/SMproductname') ?? ''));
        $chassTVal = getSelectedChassType($config->getRawVals('/SMchassistype'));

        $smbiosSelectInpt = draw_smbios_select($selected);
        $prodNameInpt = drawSimpleInput('string', '/', 'SMproductname', 'pd_name', '', 'smpdn');
        $familyInpt = drawSimpleInput('string', '/', 'SMfamily', 'pd_family', '', 'smpdf');
        $biosDateInpt = drawSimpleInput('string', '/', 'SMbiosdate', 'bios_rel_date', '', 'smbrd');
        $biosVerInpt = drawSimpleInput('string', '/', 'SMbiosversion', 'bios_ver', '', 'smbv');
        $biosVendorInpt = drawSimpleInput('string', '/', 'SMbiosvendor', 'bios_vendor', '', 'smbvn');
        $boardProductInpt = drawSimpleInput('string', '/', 'SMboardproduct', 'board_prodct', '', 'smbid');
        $boardVerInpt = drawSimpleInput('string', '/', 'SMboardversion', 'board_ver', '', 'smbvr');
        $boardTypeInpt = drawSimpleSelect('/', 'SMboardtype', $boardT, 'board_type', '', 'smbt', '', '', true);
        $boardSerialNoInpt = drawSimpleInput('string', '/', 'SMboardserial', 'board_sn', '', '', '', false, 'mlbfield');
        $chassAssTagInpt = drawComboboxSimpleInput('string', '/', 'SMchassisassettag', 'chass_asset', $chassAssetTag, '', 'smcha');
        $chassVerInpt = drawSimpleInput('string', '/', 'SMchassisversion', 'chass_version', '', 'smchmver');
        $chassTypeInpt = drawSimpleSelect('/', 'SMchassistype', $chassT, 'chass_type', '', 'smcht', '', '', false, false, $chassTVal);
        $chassManufInpt = drawSimpleInput('string', '/', 'SMchassismanufacturer', 'chass_manufacturer', '', 'smchm');
        $chassSerialNoInpt = drawSimpleInput('string', '/', 'SMchassisserial', 'chass_serialno');
        $extClockInpt = drawSimpleInput('integer', '/', 'SMexternalclock', 'external_clock', '', '', '', true);
        $maximalClockInpt = drawSimpleInput('integer', '/', 'SMmaximalclock', 'maximal_clock', '', '', '', true);
        $boardLocInpt = drawSimpleInput('string', '/', 'SMboardlocation', 'board_location', '', 'smcl');
        $boardManufInpt = drawSimpleInput('string', '/', 'SMboardmanufacturer', 'board_manufacturer', '', 'smbm');
        $serialNoInpt = drawSmbiosSerialNumberInput('/', 'SMserial');
        $fwFeatInpt = drawSimpleInput('string', '/', 'SMfirmwarefeatures', 'fw_features', '', 'smff');
        $fwFeatMask = drawSimpleInput('string', '/', 'SMfirmwarefeaturesmask', 'fw_feature_mask', '', 'smffm');
        $manufInpt = drawSimpleInput('string', '/', 'SMmanufacturer', 'manufacturer', '', 'smmnf');
        $sysUuidInpt = drawSimpleInput('string', '/', 'SMsystemuuid', 'sm_uuid', '', 'smuui');
        $sysVerInpt = drawSimpleInput('string', '/', 'SMsystemversion', 'version', '', 'smver');
        $smbiosDetailModal = draw_smbios_more_modal();

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['smbios']}</div></div>
    
                {$smbiosSelectInpt}
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$prodNameInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$familyInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$biosDateInpt}</div>
                    <div class=\"col-12 col-sm-8 col-lg-3\">{$biosVerInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$biosVendorInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$boardProductInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$boardVerInpt}</div>
                    <div class=\"col-12 col-sm-5 col-lg-3 \">{$boardTypeInpt}</div>
                    <div class=\"col-12 col-sm-7 col-lg-4\">{$boardSerialNoInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassAssTagInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassVerInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassTypeInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassManufInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassSerialNoInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$extClockInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$maximalClockInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$boardLocInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$boardManufInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$serialNoInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fwFeatInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fwFeatMask}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$manufInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-10\">{$sysUuidInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$sysVerInpt}</div>
                </div>
            
                {$smbiosDetailModal}";
    } catch (\Throwable) {}

    return "";
}