<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

function acpi(): string {
    try {
        global $text, $config, $dsdtFixesList, $miscDsdt, $acpiOpt, $ssdtGen, $ssdtOpt, $ssel;

        $generate = getCheckAttr($config->getRawVals('ACPI/SSDT/Generate'));
        $isGenerateTrue = $generate !== 'unchecked';
        $noFixMask = false;

        $dsdtPatchPanel = draw_fancy_panel($config->getVals('ACPI/DSDT/Patches'), 'dsdtP', 'ACPI/DSDT/Patches', 'patches');
        $dsdtNameInpt = drawSimpleInput('string', 'ACPI/DSDT', 'Name', 'dsdt_name', '', '', 'DSDT.aml');
        $fixMaskInpt = drawSimpleInput('string', 'ACPI/DSDT', 'FixMask', 'fix_mask', '', 'manual_fixmask', '', false, '', '', $noFixMask);
        $pnlfInpt = drawSimpleInput('string', 'ACPI/DSDT', 'PNLF_UID', 'pnlf_uid', '', 'pnlfuid');
        $dsdtOptions = drawSimpleInlineCheckOpts($miscDsdt, 'ACPI/DSDT');
        $ssdtGenCheckbox = drawCheckbox('', 'ACPI/SSDT', 'Generate', $generate, 'generate', false, '', 'data-change="ssdtg"');
        $ssdtOptions = drawSimpleInlineCheckOpts($ssdtOpt, 'ACPI/SSDT');
        $minMultInpt = drawSimpleInput('integer', 'ACPI/SSDT', 'MinMultiplier', 'min_mult', '', 'minmul', '', true);
        $maxMultInpt = drawSimpleInput('integer', 'ACPI/SSDT', 'MaxMultiplier', 'max_mult', '', 'maxmul', '', true);
        $c3LatencyInpt = drawSimpleInput('string', 'ACPI/SSDT', 'C3Latency', 'c3_latency', '', '', 'Hex');
        $resetAdrInpt = drawSimpleInput('string', 'ACPI', 'ResetAddress', 'ResetAddress', '', '', 'Hex');
        $resetValInpt = drawSimpleInput('string', 'ACPI', 'ResetValue', 'ResetValue', '', '', 'Hex');
        $acpiOptions = drawSimpleInlineCheckOpts($acpiOpt, 'ACPI');
        $ssdtOrderTable = drawPatchTable('ssdtOr', ['name', ''], $config->getRawVals('ACPI/SortedOrder'), ['cp']);
        $disableAmlTable = drawPatchTable('disaAml', ['name'], $config->getRawVals('ACPI/DisabledAML'), ['cp']);
        $dropSsdtTable = drawPatchTable('ssdtTb', ['signature', 'table_id', 'table_length', 'drop_for_all'], $config->getRawVals('ACPI/DropTables'), ['cp']);
        $renDevsTable = drawPatchTable('renDevs', ['ren_old_d', 'ren_new_d'], $config->getRawVals('ACPI/RenameDevices'), ['cp']);
        $dsdtFixOptions = '';
        $ssdtGenOptions = '';
        $pLimitInpts = '';

        foreach ($dsdtFixesList as $fix) {
            $fixStr = substr($fix, 5);
            $checked = getCheckAttr($config->getRawVals('ACPI/DSDT/Fixes/'.$fixStr));

            if (!$noFixMask && $checked === 'checked')
                $noFixMask = true;

            $dsdtFixOptions .= drawCheckbox('form-check-inline', 'ACPI/DSDT/Fixes', $fixStr, $checked, $fix, false, '', 'data-change="dsdtf"');
        }

        foreach ($ssdtGen as $genOp) {
            $ssdtVal = substr($genOp, 5);
            $checked = $isGenerateTrue ? '':getCheckAttr($config->getRawVals('ACPI/SSDT/Generate/'.$ssdtVal));

            $ssdtGenOptions .= drawCheckbox('form-check-inline', 'ACPI/SSDT/Generate', $ssdtVal, $checked, $genOp, $isGenerateTrue, 'ssdtGenOp');
        }

        foreach ($ssel as $sel) {
            $selVal = substr($sel, 5);
            $max = $selVal === 'PLimitDict' ? 2 : ($selVal === 'UnderVoltStep' ? 9:1);
            $select = drawSimpleNumericSelect('ACPI/SSDT', $selVal, range(0, $max), $sel, PHP_INT_MIN, '', '', '', true);

            $pLimitInpts .= "<div class=\"col-12 col-md-4\">{$select}</div>";
        }

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['dsdt']}</div></div>
    
                {$dsdtPatchPanel}
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-7\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['fixes']}</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12\">
                                <div class=\"tab-content\">
                                    <div id=\"dsdt-fixes\" class=\"active cce-tab-pane\">{$dsdtFixOptions}</div>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-lg-5\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['options']}</div>
                        </div>
            
                        <div class=\"row mt-2\">
                            <div class=\"col-12 col-sm-6\">{$dsdtNameInpt}</div>
                            <div class=\"col-12 col-sm-6\">{$fixMaskInpt}</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12 col-sm-6\">{$pnlfInpt}</div>
                            <div class=\"col-12 col-sm-6\">{$dsdtOptions}</div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['ssdt']}</div>
                </div>
            
                <div class=\"row ssdtOpt\">
                    <div class=\"col-12 col-lg-6\">
                        <ul class=\"nav nav-tabs\">
                            <li class=\"nav-item\">
                                <a class=\"nav-link cce-nav-link\">{$ssdtGenCheckbox}</a>
                            </li>
                        </ul>
                        <div class=\"tab-content\">
                            <div class=\"active cce-tab-pane single_ssdt\">
                                {$ssdtGenOptions}
                                {$ssdtOptions}
                            </div>
                        </div>
                    </div>
                    <div class=\"col-12 col-lg-6 mt-3\">
                        <div class=\"row\">
                            <div class=\"col-12\">
                                <div class=\"row\">{$pLimitInpts}</div>
                            </div>
                            <div class=\"col-12\">
                                <div class=\"row\">
                                    <div class=\"col-12 col-md-4\">{$minMultInpt}</div>
                                    <div class=\"col-12 col-md-4\">{$maxMultInpt}</div>
                                    <div class=\"col-12 col-md-4\">{$c3LatencyInpt}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['acpi']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-2\">{$resetAdrInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$resetValInpt}</div>
                    <div class=\"col-12 col-lg-8 mt-3\">{$acpiOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['sort_order']}</div>
                        </div>
                        {$ssdtOrderTable}
                    </div>
            
                    <div class=\"col-12 col-lg-6\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['dis_aml']}</div>
                        </div>
                        {$disableAmlTable}
                    </div>
                </div>
            
                <div class=\"row mt-2\">
                    <div class=\"col-12 col-lg-6\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['drop_tables']}</div>
                        </div>
                        {$dropSsdtTable}
                    </div>
            
                    <div class=\"col-12 col-lg-6\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['ren_dev_t']}</div>
                        </div>
                        {$renDevsTable}
                    </div>
                </div>";
    } catch (\Throwable) {}

    return "";
}
