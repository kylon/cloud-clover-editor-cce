<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

require_once __DIR__.'/../../include/data/boot.php';

function boot(): string {
    try {
        global $text, $config, $secPol, $bootO, $legacyBootO, $bootArgs, $cLogoCombo;

        $defBootVCombo = 'Last booted volume:LastBootedVolume';
        $xmpCombo = 'Yes;No;0;1;2';

        $customLogoV = boolToYesNo($config->getRawVals('Boot/CustomLogo'));
        $xmpValue = boolToYesNo($config->getRawVals('Boot/XMPDetection'));

        $bootArgsSelInpt = drawBootArgsSelect($bootArgs);
        $bootArgsInpt = drawTextAreaNoLabel('string', 'Boot', 'Arguments', 4, '', '', '', 'b-args-tx');
        $bootOptions = drawSimpleInlineCheckOpts($bootO, 'Boot');
        $volInpt = drawComboboxSimpleInput('string', 'Boot', 'DefaultVolume', 'def_b_vol', $defBootVCombo, '', 'bvol');
        $logoInpt = drawComboboxSimpleInput('string', 'Boot', 'CustomLogo', 'os_logo', $cLogoCombo, '', '', $text['c_logo_placeholder'], '', '', false, PHP_INT_MIN, PHP_INT_MIN, $customLogoV);
        $loaderInpt = drawSimpleInput('string', 'Boot', 'DefaultLoader', 'def_loader', '', '', 'boot.efi');
        $legacyBootInpt = drawSimpleSelect('Boot', 'Legacy', $legacyBootO, 'legacy', '', 'legc', '', '', true);
        $xmpInpt = drawComboboxSimpleInput('', 'Boot', 'XMPDetection', 'xmp_det', $xmpCombo, '', 'xmpd', '', '', '', false, PHP_INT_MIN, PHP_INT_MIN, $xmpValue);
        $tmoutInpt = drawSimpleInput('integer', 'Boot', 'Timeout', 'timeout', '', 'timo', '', true, '', '', false, -1);
        $bootBgColorInpt = drawSimpleInput('string', 'Boot', 'BootBgColor', 'boot_bg_color', '', 'bbgc');
        $bootWlTable = drawPatchTable('whiteB', ['name'], $config->getRawVals('Boot/WhiteList'), ['cp']);
        $bootBlTable = drawPatchTable('blackB', ['name'], $config->getRawVals('Boot/BlackList'), ['cp']);
        $secureOptions = drawCheckbox('', 'Boot', 'Secure', getCheckAttr($config->getRawVals('Boot/Secure')), 'secure');
        $policyOptions = drawSimpleSelect('Boot', 'Policy', $secPol, 'secure_policy', '', 'spol', '', '', true);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['boot_title']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-lg-5\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['boot_arg']}</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12\">{$bootArgsSelInpt}</div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-12\">{$bootArgsInpt}</div>
                        </div>
                    </div>
                    
                    <div class=\"col-12 col-lg-7\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['options']}</div>
                        </div>
                        
                        <div class=\"row\">
                            <div class=\"col-12 mt-3\">{$bootOptions}</div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-5\">{$volInpt}</div>
                    <div class=\"col-12 col-lg-7\">{$logoInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-3\">{$loaderInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$legacyBootInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$xmpInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$tmoutInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$bootBgColorInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['secure_boot']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-5\">
                        <div class=\"row\">
                            <div class=\"col-lg-12 subtitle\">{$text['whitelist']}</div>
                        </div>
                        {$bootWlTable}
                    </div>
            
                    <div class=\"col-12 col-lg-5\">
                        <div class=\"row\">
                            <div class=\"col-lg-12 subtitle\">{$text['blacklist']}</div>
                        </div>
                        {$bootBlTable}
                    </div>
            
                    <div class=\"col-12 col-lg-2 mt-3 mt-lg-0\">
                        <div class=\"row\">
                            <div class=\"col-12\">{$secureOptions}</div>
                            <div class=\"col-12 col-sm-6 col-lg-12 mt-3 mt-sm-0\">{$policyOptions}</div>
                        </div>
                    </div>
                </div>";
    } catch (\Throwable) {}

    return "";
}
