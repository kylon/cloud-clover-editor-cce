<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

function boot_graphics(): string {
    try {
        global $text;

        $bgColorInpt = drawSimpleInput('string', 'BootGraphics', 'DefaultBackgroundColor', 'def_bg_color', '', '', 'Hex');
        $uiScaleInpt = drawSimpleInput('integer', 'BootGraphics', 'UIScale', 'ui_scale', '', 'uisc', '', true);
        $efiLoginHdpiInpt = drawSimpleInput('integer', 'BootGraphics', 'EFILoginHiDPI', 'efi_login_hi_dpi', '', 'efihd', '', true);
        $flagStateInpt = drawSimpleInput('string', 'BootGraphics', 'flagstate', 'flagstate', '', 'flgst');

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['bootgfx_title']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$bgColorInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$uiScaleInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$efiLoginHdpiInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$flagStateInpt}</div>
                    <!-- col-lg-1 here -->
                </div>";
    } catch (\Throwable) {}

    return "";
}
