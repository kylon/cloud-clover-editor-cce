<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

function cpu(): string {
    try {
        global $text, $config, $cpuSetCheck;

        $hwpDisbl = !$config->getRawVals('CPU/HWPEnable') ?? true;

        $freqInpt = drawSimpleInput('integer', 'CPU', 'FrequencyMHz', 'frequency', '', 'fmhz', 'MHz', true);
        $busSpdInpt = drawSimpleInput('integer', 'CPU', 'BusSpeedkHz', 'bus_speed', '', 'bkhz', 'kHz', true);
        $qpiInpt = drawSimpleInput('integer', 'CPU', 'QPI', 'qpi', '', 'qpi', '', true);
        $cpuTypeInpt = drawSimpleInput('string', 'CPU', 'Type', 'type');
        $tdpInpt = drawSimpleInput('integer', 'CPU', 'TDP', 'tdp_value', '', 'tdp', '', true);
        $saveModeInpt = drawSimpleInput('integer', 'CPU', 'SavingMode', 'saving_mode', '255', 'savemd', '', true);
        $hwpInpt = drawSimpleInput('string', 'CPU', 'HWPValue', 'hwp_value', '', '', 'Hex', false, 'hwpval', '', $hwpDisbl);
        $cpuOptions = '';

        foreach ($cpuSetCheck as $check) {
            $checkStr = substr($check, 5);
            $checked = getCheckAttr($config->getRawVals('CPU/'.$checkStr));

            $cpuOptions .= drawCheckbox('form-check-inline', 'CPU', $checkStr, $checked, $check, false, '', "data-change=\"{$checkStr}\"");
        }

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['cpu_title']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$freqInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$busSpdInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-1\">{$qpiInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$cpuTypeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-1\">{$tdpInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$saveModeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$hwpInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 mt-3\">{$cpuOptions}</div>
                </div>";
    } catch (\Throwable) {}

    return "";
}
