<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

function devices(): string {
    try {
        global $text, $config, $usbO, $deviceMisc, $audOpts;

        $audioCombo = 'Detect;No';
        $intelBacklightCombo = '1808;2776;1295';

        $setIntelBacklight = $config->getRawVals('Devices/SetIntelBacklight');
        $setIntelMaxBacklight = $config->getRawVals('Devices/SetIntelMaxBacklight');
        $props = $config->getVals('Devices/Properties');

        $fakeGfxIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'IntelGFX', 'injo_Intel', '', '', 'Hex');
        $fakeAtiIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'ATI', 'injo_ATI', '', '', 'Hex');
        $fakeNvidiaIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'NVidia', 'injo_NVidia', '', '', 'Hex');
        $fakeLanIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'LAN', 'lan', '', '', 'Hex');
        $fakeWifiIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'WIFI', 'wifi', '', '', 'Hex');
        $fakeSataIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'SATA', 'sata', '', '', 'Hex');
        $fakeXhciIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'XHCI', 'xhci', '', '', 'Hex');
        $fakeImeiIdInpt = drawSimpleInput('string', 'Devices/FakeID', 'IMEI', 'imei', '', '', 'Hex');
        $usbOptions = drawSimpleInlineCheckOpts($usbO, 'Devices/USB');
        $audioInjInpt = drawComboboxSimpleInput('', 'Devices/Audio', 'Inject', 'inject', $audioCombo, '', '', $text['layout_id']);
        $audioOptions = drawSimpleInlineCheckOpts($audOpts, 'Devices/Audio');
        $intelMaxValInpt = drawComboboxSimpleInput('integer', 'Devices', 'IntelMaxValue', 'intel_max_val', $intelBacklightCombo);
        $intelBacklightCheckbox = drawCheckbox('form-check-inline', 'Devices', 'SetIntelBacklight', getCheckAttr($setIntelBacklight), 'SetIntelBacklight');
        $intelMaxBacklightCheckbox = drawCheckbox('form-check-inline', 'Devices', 'SetIntelMaxBacklight', getCheckAttr($setIntelMaxBacklight), 'SetIntelMaxBacklight', false, '', 'data-change="intlmaxbcklght"');
        $airportDevNameInpt = drawSimpleInput('string', 'Devices', 'AirportBridgeDeviceName', 'airport_bridge_name', '', 'airp');
        $disFuncsInpt = drawSimpleInput('string', 'Devices', 'DisableFunctions', 'disable_functions', '', 'dsfn');
        $devicesOptions = drawSimpleInlineCheckOpts($deviceMisc, 'Devices');
        $devicePropsMainTable = drawPatchTable('pPropM', ['device'], $props, ['cp']);
        $devicePropsValsTable = drawPatchTable('pPropS', ['key', 'value', 'data_type'], $props);
        $deviceAddMainTable = drawPatchTable('addProp', ['device', 'key', 'value', 'data_type', 'comment', 'disabled'], $config->getVals('Devices/AddProperties'), ['cp']);
        $arbProspMainTable = drawPatchTable('cPropM', ['comment', 'pci_addr'], $config->getRawVals('Devices/Arbitrary'), ['cp']);
        $arbPropsValsTable = drawPatchTable('cPropS', ['key', 'value', 'data_type', 'disabled'], $config->getVals('Devices/Arbitrary/0/CustomProperties'));

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['fake_id']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeGfxIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeAtiIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeNvidiaIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeLanIdInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeWifiIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeSataIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeXhciIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fakeImeiIdInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['devices']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-3\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['usb']}</div>
                        </div>
                        <div class=\"row mt-3\">
                            <div class=\"col-12\">{$usbOptions}</div>
                        </div>
                    </div>
                    <div class=\"col-12 col-lg-4\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['audio']}</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12 col-sm-4 col-lg-5\">{$audioInjInpt}</div>
                            <div class=\"col-12 col-sm-8 col-lg-7\">{$audioOptions}</div>
                        </div>
                    </div>
                    <div class=\"col-12 col-lg-5\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['intel_backlight']}</div>
                        </div>
            
                        <div class=\"row mt-3\">
                            <div class=\"col-12 col-sm-4 col-lg-6\">{$intelMaxValInpt}</div>
                            <div class=\"col-12 col-sm-8 col-lg-6\">
                                {$intelBacklightCheckbox}
                                {$intelMaxBacklightCheckbox}
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$airportDevNameInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$disFuncsInpt}</div>
                    <div class=\"col-12 col-lg-7\">{$devicesOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['properties']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$devicePropsMainTable}</div>
                    <div class=\"col-12 col-lg-6 mt-3 mt-lg-0\">{$devicePropsValsTable}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle mt-2\">{$text['add_prop']}</div>
                </div>
            
                {$deviceAddMainTable}
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['custom_props']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$arbProspMainTable}</div>
                    <div class=\"col-12 col-lg-6 mt-3 mt-lg-0\">{$arbPropsValsTable}</div>
                </div>";
    } catch (\Throwable) {}

    return "";
}
