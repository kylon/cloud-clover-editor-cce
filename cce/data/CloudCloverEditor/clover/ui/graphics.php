<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

require_once __DIR__.'/../../include/data/graphics.php';

function graphics(): string {
    try {
        global $text, $config, $igPlatIDCombo, $snbPlatIDCombo, $fbNamesCombo, $edidOpt, $toInj, $gfxOpt;

        $inject = getCheckAttr($config->getRawVals('Graphics/Inject'));
        $noGfxInject = $inject === 'unchecked';

        $edidOptions = drawSimpleInlineCheckOpts($edidOpt, 'Graphics/EDID');
        $edidVndIdInpt = drawSimpleInput('string', 'Graphics/EDID', 'VendorID', 'edid_vendor_id', '', '', '0x1006');
        $prodIdInpt = drawSimpleInput('string', 'Graphics/EDID', 'ProductID', 'edid_product_id', '', '', '0x9221');
        $horzPulseWdInpt = drawSimpleInput('string', 'Graphics/EDID', 'HorizontalSyncPulseWidth', 'sync_pulse_width', '', '', 'Hex');
        $videoInpSignlInpt = drawSimpleInput('string', 'Graphics/EDID', 'VideoInputSignal', 'video_inpt_signal', '', '', 'Hex');
        $edidInpt = drawComboSelectTextAreaNoLabel('Graphics/EDID', 'Custom', 'string;data', 3, '', '', '', '', false);
        $idPlatIdInpt = drawComboboxSimpleInput('string', 'Graphics', 'ig-platform-id', 'ig_platform_id', $igPlatIDCombo, '', 'ig-id');
        $snbPlatIdInpt = drawComboboxSimpleInput('string', 'Graphics', 'snb-platform-id', 'snb_platform_id', $snbPlatIDCombo, '', 'snb-id');
        $displCfgInpt = drawSimpleInput('string', 'Graphics', 'display-cfg', 'display_cfg', '', 'd-cfg');
        $nvcapInpt = drawSimpleInput('string', 'Graphics', 'NVCAP', 'nvcap', '', 'nvcap');
        $dualLinkInpt = drawSimpleNumericSelect('Graphics', 'DualLink', range(0, 1), 'dual_link', PHP_INT_MIN, 'dlink', '', '', true);
        $injCheckbox = drawCheckbox('', 'Graphics', 'Inject', $inject, 'inject', false, '', 'data-change="injgfx"');
        $vramInpt = drawSimpleInput('integer', 'Graphics', 'VRAM', 'vram', '', 'vram', '', true, '', '', false, 0);
        $vportsInpt = drawSimpleInput('integer', 'Graphics', 'VideoPorts', 'vports', '', 'vport', '', true, '', '', false, 0);
        $refClkInpt = drawSimpleInput('integer', 'Graphics', 'RefCLK', 'ref_clk', '', 'rclk', '', true);
        $bootDisplInpt = drawSimpleInput('integer', 'Graphics', 'BootDisplay', 'boot_display', '', 'bdisp', '', true);
        $fbNameInpt = drawComboboxSimpleInput('string', 'Graphics', 'FBName', 'fb_name', $fbNamesCombo, '', 'fbnm');
        $gfxOptions = drawSimpleInlineCheckOpts($gfxOpt, 'Graphics');
        $patchVbiosPanel = draw_fancy_panel($config->getVals('Graphics/PatchVBiosBytes'), 'vbiosP', 'Graphics/PatchVBiosBytes', 'vbios_patch');
        $nvidiaInjTable = drawPatchTable('mNvI', ['model', 'iopci_primary_match', 'iopci_sub_dev_id', 'vram', 'vports', 'gfco_LoadVBios'], $config->getRawVals('Graphics/NVIDIA'), ['cp']);
        $atiInjTable = drawPatchTable('mAI', ['model', 'iopci_primary_match', 'iopci_sub_dev_id', 'vram'], $config->getRawVals('Graphics/ATI'), ['cp']);
        $gfxInjOptions = '';

        foreach ($toInj as $inj) {
            $injStr = substr($inj, 5);
            $checked = $noGfxInject ? getCheckAttr($config->getRawVals('Graphics/Inject/'.$injStr)) : '';

            $gfxInjOptions .= drawCheckbox('form-check-inline', 'Graphics/Inject', $injStr, $checked, $inj, !$noGfxInject);
        }

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['edid_patch']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-lg-2\">{$edidOptions}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$edidVndIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$prodIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$horzPulseWdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$videoInpSignlInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['custom_edid']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12\">{$edidInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['gfx_patch']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$idPlatIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$snbPlatIdInpt}</div>
                    <div class=\"col-12 col-lg-4\">{$displCfgInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-10 col-lg-6\">{$nvcapInpt}</div>
                    <div class=\"col-12 col-sm-2 col-lg-2\">{$dualLinkInpt}</div>
                    <!-- col-lg-4 here -->
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-3\">
                        <ul class=\"nav nav-tabs\">
                            <li class=\"nav-item\">
                                <a class=\"nav-link cce-nav-link\">{$injCheckbox}</a>
                            </li>
                        </ul>
                        <div class=\"tab-content single_inj\">
                            <div class=\"active cce-tab-pane\">{$gfxInjOptions}</div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-lg-9 mt-3\">
                        <div class=\"row\">
                            <div class=\"col-12 col-lg-2\">{$vramInpt}</div>
                            <div class=\"col-12 col-lg-2\">{$vportsInpt}</div>
                            <div class=\"col-12 col-sm-3 col-lg-2\">{$refClkInpt}</div>
                            <div class=\"col-12 col-sm-4 col-lg-2\">{$bootDisplInpt}</div>
                            <div class=\"col-12 col-sm-5 col-lg-4\">{$fbNameInpt}</div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$gfxOptions}</div>
                </div>
            
                {$patchVbiosPanel}
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['mult_gfx_card_injection']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">NVidia</div>
                </div>
            
                {$nvidiaInjTable}
               
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">ATI</div>
                </div>
                
                {$atiInjTable}";
    } catch (\Throwable) {}

    return "";
}
