<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

require_once __DIR__.'/../../include/fancypanel/fancypanel_main.php';
require_once __DIR__.'/../../include/data/boot.php';

function gui(): string {
    try {
        global $text, $config, $scanLeg, $scanKern, $embThmOpt, $guiOpts, $langOpts;

        $scrResCOpt = '1024x600;1024x768;1152x864;1280x768;1280x800;1280x1024;'.
                        '1366x768;1400x1050;1440x900;1600x900;1600x1200;1680x1050;'.
                        '1920x1080;2048x1252;2048x1536;2560x1600;2560x2048';
        $consoleCOpt = '0;Min;Max';

        $scanVal = $config->getRawVals('GUI/Scan');
        $isScanDef = !is_array($scanVal);

        $scanEntriesCheckbox = drawCheckbox('form-check-inline', 'GUI/Scan', 'Entries', getCheckAttr($config->getRawVals('GUI/Scan/Entries')), 'scan_entries', $isScanDef, 'scanOp');
        $scanLinuxCheckbox = drawCheckbox('form-check-inline', 'GUI/Scan', 'Linux', getCheckAttr($config->getRawVals('GUI/Scan/Linux')), 'scan_linux', $isScanDef, 'scanOp');
        $scanToolsCheckbox = drawCheckbox('form-check-inline', 'GUI/Scan', 'Tool', getCheckAttr($config->getRawVals('GUI/Scan/Tool')), 'scan_tools', $isScanDef, 'scanOp');
        $scanKernInpt = drawSimpleSelect('GUI/Scan', 'Kernel', $scanKern, 'scan_kernel', '', 'scker', 'scanOp scanK', '', true, $isScanDef);
        $scanLegacyInpt = drawSimpleSelect('GUI/Scan', 'Legacy', $scanLeg, 'scan_legacy', '', 'scleg', 'scanOp scanL', '', true, $isScanDef);
        $mouseEnableCheckbox = drawCheckbox('form-check-inline', 'GUI/Mouse', 'Enabled', getCheckAttr($config->getRawVals('GUI/Mouse/Enabled')), 'enabled');
        $mouseMirrorCheckbox = drawCheckbox('form-check-inline', 'GUI/Mouse', 'Mirror', getCheckAttr($config->getRawVals('GUI/Mouse/Mirror')), 'mirror');
        $mouseDblClickInpt = drawSimpleInput('integer', 'GUI/Mouse', 'DoubleClick', 'dbl_clk', '', 'gmouse', '', true, '', '', false, 0);
        $mouseSpdInpt = drawSimpleInput('integer', 'GUI/Mouse', 'Speed', 'speed', '', 'msspeed', '', true, '', '', false, 0);
        $langInpt = drawSimpleSelect('GUI', 'Language', $langOpts, 'language', '', 'clng', '', '', true);
        $screenResInpt = drawComboboxSimpleInput('string', 'GUI', 'ScreenResolution', 'screen_res', $scrResCOpt, '', 'scres');
        $consoleModeInpt = drawComboboxSimpleInput('', 'GUI', 'ConsoleMode', 'console_mode', $consoleCOpt, '', 'cmode');
        $guiOptions = drawSimpleInlineCheckOpts($guiOpts, 'GUI');
        $themeInpt = drawSimpleInput('string', 'GUI', 'Theme', 'theme_name', '', 'gthm');
        $themeTypeInpt = drawSimpleSelect('GUI', 'EmbeddedThemeType', $embThmOpt, 'emb_thm_t', '', 'embthm', '', '', true);
        $timezoneInpt = drawSimpleInput('integer', 'GUI', 'Timezone', 'timezone', '', 'tmz', '', true, '', '', false, 0);
        $entriesPanel = draw_fancy_panel($config->getRawVals('GUI/Custom/Entries'), 'cEntry', 'GUI/Custom/Entries', 'custom_entr', true);
        $legacyPanel = draw_fancy_panel($config->getRawVals('GUI/Custom/Legacy'), 'cLegE', 'GUI/Custom/Legacy', 'custom_legacy_entr', true);
        $toolsPanel = draw_fancy_panel($config->getRawVals('GUI/Custom/Tool'), 'cToolsE', 'GUI/Custom/Tool', 'custom_tool_entr', true);
        $hideVolTable = drawPatchTable('hideVl', ['name'], $config->getRawVals('GUI/Hide'), ['cp']);
        $enableScanOptionSelected = isPropertyTrue($scanVal) || $scanVal === null ? 'selected':'';
        $customScanOptionSelected = !$isScanDef ? 'selected':'';
        $disableScanOptionSelected = isPropertyFalse($scanVal) ? 'selected':'';

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['bl_gui_sett']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-lg-5\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['scan_opt']}</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12 col-sm-4 col-lg-5\">
                                <div class=\"text-center mb-3\">
                                    <label class=\"form-label\" for=\"escan\">{$text['scan']}</label>
                                    <select id=\"escan\" class=\"form-select cce-sel\" data-sel=\"enbl-scan\" data-path=\"GUI\" data-field=\"Scan\">
                                        <option value=\"true\" {$enableScanOptionSelected}>{$text['scan_auto']}</option>
                                        <option value=\"custom\" {$customScanOptionSelected}>{$text['custom']}</option>
                                        <option value=\"false\" {$disableScanOptionSelected}>{$text['disabled']}</option>
                                    </select>
                                </div>
                            </div>
            
                            <div class=\"col-12 col-sm-8 col-lg-7\">
                                {$scanEntriesCheckbox}
                                {$scanLinuxCheckbox}
                                {$scanToolsCheckbox}
                            </div>
                        </div>
            
                        <div class=\"row mt-2 mt-sm-0\">
                            <div class=\"col-12 col-sm-6\">{$scanKernInpt}</div>
                            <div class=\"col-12 col-sm-6\">{$scanLegacyInpt}</div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-lg-3\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['mouse']}</div>
                        </div>
            
                        <div class=\"row mt-3\">
                            <div class=\"col-12 col-sm-6 col-lg-12 mb-3\">
                                {$mouseEnableCheckbox}
                                {$mouseMirrorCheckbox}
                            </div>
                            <div class=\"col-12 col-sm-3 col-lg-6\">{$mouseDblClickInpt}</div>
                            <div class=\"col-12 col-sm-3 col-lg-6\">{$mouseSpdInpt}</div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-lg-4\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['options']}</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12 col-md-4 col-lg-5 col-xl-3\">{$langInpt}</div>
                            <div class=\"col-12 col-md-4 col-lg-7 col-xl-5\">{$screenResInpt}</div>
                            <div class=\"col-12 col-md-4 col-lg-6 col-xl-4\">{$consoleModeInpt}</div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['gui_opts']}</div>
                        </div>
            
                        <div class=\"row mt-3\">
                            <div class=\"col-12\">{$guiOptions}</div>
                        </div>
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-8\">
                        <div class=\"row\">
                            <div class=\"col-12 subtitle\">{$text['theme_t']}</div>
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12 col-md-5 col-lg-5\">{$themeInpt}</div>
                            <div class=\"col-12 col-md-4 col-lg-4\">{$themeTypeInpt}</div>
                            <div class=\"col-12 col-md-3 col-lg-3\">{$timezoneInpt}</div>
                        </div>
                    </div>
                    <!-- col-lg-4 here -->
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['bl_entries_sett']}</div>
                </div>
            
                {$entriesPanel}
                {$legacyPanel}
                {$toolsPanel}
               
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['hide_vols']}</div>
                </div>
            
                {$hideVolTable}";
    } catch (\Throwable) {}

    return "";
}
