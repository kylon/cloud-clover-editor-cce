<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

require_once __DIR__.'/../../include/fancypanel/fancypanel_main.php';

function kernel_and_kext_patches(): string {
    try {
        global $text, $config, $fakeCpuIDCombo, $miscKK;

        $atiConnectorsInpt = drawSimpleTextArea('string', 'KernelAndKextPatches', 'ATIConnectorsData', 'ati_con_data', 3, '', 'katic');
        $atiConnectorsPatchInpt = drawSimpleTextArea('string', 'KernelAndKextPatches', 'ATIConnectorsPatch', 'ati_con_patch', 3, '', 'katicp');
        $atiConnectorsControllerInpt = drawSimpleInput('string', 'KernelAndKextPatches', 'ATIConnectorsController', 'ati_con_controller', '', 'katicc');
        $fakeIdInpt = drawComboboxSimpleInput('string', 'KernelAndKextPatches', 'FakeCPUID', 'fake_cpu_id', $fakeCpuIDCombo, '', 'kcpui');
        $kextKernOptions = drawSimpleInlineCheckOpts($miscKK, 'KernelAndKextPatches');
        $kextPatchPanel = draw_fancy_panel($config->getVals('KernelAndKextPatches/KextsToPatch'), 'kextP', 'KernelAndKextPatches/KextsToPatch', 'kext_patch');
        $kernPatchPanel = draw_fancy_panel($config->getVals('KernelAndKextPatches/KernelToPatch'), 'kernelP', 'KernelAndKextPatches/KernelToPatch', 'kernel_patch');
        $bootPatchPanel = draw_fancy_panel($config->getVals('KernelAndKextPatches/BootPatches'), 'bootefiP', 'KernelAndKextPatches/BootPatches', 'boot_efi_patches');
        $forceKextTable = drawPatchTable('forceKx', ['name'], $config->getRawVals('KernelAndKextPatches/ForceKextsToLoad'), ['cp']);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['kernel_kext_patch']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12\">{$atiConnectorsInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12\">{$atiConnectorsPatchInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-8 col-lg-4\">{$atiConnectorsControllerInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$fakeIdInpt}</div>
                    <div class=\"col-12 col-lg-6\">{$kextKernOptions}</div>
                </div>
            
                {$kextPatchPanel}
                {$kernPatchPanel}
                {$bootPatchPanel}
               
                <div class=\"row mt-2\">
                    <div class=\"col-12 subtitle\">{$text['force_kext']}</div>
                </div>
            
                {$forceKextTable}";
    } catch (\Throwable) {}

    return "";
}
