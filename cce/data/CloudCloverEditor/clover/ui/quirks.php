<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

function quirks(): string {
    try {
        global $text, $config, $ocQ, $kernCache, $clGpuBarsSzOpts;

        $mmioWhitelist = $config->getRawVals('Quirks/MmioWhitelist');

        $mmioWlTable = drawPatchTable('mmioWPCL', ['address', 'comment', 'enabled'], $mmioWhitelist, ['cp']);
        $quirks = drawSimpleInlineCheckOpts($ocQ, 'Quirks');
        $maxSlideInpt = drawSimpleInput('integer', 'Quirks', 'ProvideMaxSlide', 'provide_max_slide', '', 'maxslide', '', true, '', '', false, 0);
        $kernCacheInpt = drawSimpleSelect('Quirks', 'KernelCache', $kernCache, 'kernel_cache', '', 'kcc');
        $quirkResizeAppleGpuBarsInpt = drawSimpleSelect('Quirks', 'ResizeAppleGpuBars', $clGpuBarsSzOpts, 'resize_apple_gpub');

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['quirks']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['mmio_whitelist']}</div>
                </div>
            
                {$mmioWlTable}
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['quirks']}</div>
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$maxSlideInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$kernCacheInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$quirkResizeAppleGpuBarsInpt}</div>
                    <!-- col-lg-5 here -->
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$quirks}</div>
                </div>";
    } catch (\Throwable) {}

    return "";
}
