<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

function rt_variables(): string {
    try {
        global $text, $config, $btrFOpts, $sipFOpts;

        $romComboOpts = 'Use MAC address 0:UseMacAddr0;Use MAC address 1:UseMacAddr1';
        $btrOpts = '0x0;0x28';

        $romDataType = getValType($config->getVals('RtVariables'), 'ROM');
        $blockList = $config->getRawVals('RtVariables/Block');
        $curCSR = $config->getRawVals('RtVariables/CsrActiveConfig') ?? '';
        $curBtr = $config->getRawVals('RtVariables/BooterConfig') ?? '';

        $mlbInpt = drawSimpleInput('string', 'RtVariables', 'MLB', 'mlb', '', 'rtmlb');
        $romInpt = drawComboSelectSimpleCombobox('RtVariables', 'ROM', 'rom', 'string;data', $romDataType, $romComboOpts, '', 'rtrom');
        $btrConfigInpt = drawFlagInput($curBtr, 'RtVariables', 'BooterConfig', 'btrf', $btrOpts);
        $btrConfigModal = drawSimpleFlagModal($btrFOpts, $curBtr, 'btrf', true);
        $csrInpt = drawCSRInput($curCSR, 'RtVariables', 'CsrActiveConfig');
        $csrModal = drawSimpleFlagModal($sipFOpts, $curCSR, 'csrf', true);
        $rtBlockTable = drawPatchTable('rtBlock', ['guid','comment','name','disabled'], $blockList, ['cp']);
        $hwTargtInpt = drawSimpleInput('string', 'RtVariables', 'HWTarget', 'hw_targ', 'j160');

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['rt_var_title']}</div></div>

                <div class=\"row\">
                    <div class=\"col-12 col-lg-5\">{$mlbInpt}</div>
                    <div class=\"col-12 col-lg-5\">{$romInpt}</div>
                    <div class=\"col-12 col-lg-2\">{$hwTargtInpt}</div>
                </div>

                <div class=\"row\">
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$btrConfigInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$csrInpt}</div>
                    <!-- col-8 here -->
                </div>

                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['block_vars']}</div>
                </div>

                <div class=\"row\">
                    <div class=\"col-12\">{$rtBlockTable}</div>
                </div>

                <!-- CSR Modal -->
                {$csrModal}

                <!-- Booter Modal -->
                {$btrConfigModal}";
    } catch (\Throwable) {}

    return "";
}
