<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\clover_UI;

require_once __DIR__.'/../../include/data/smbios.php';
require_once __DIR__.'/../../include/fn/smbios.php';

function smbios(): string {
    try {
        global $text, $config, $chassAssetTag, $channels, $boardT, $chassT, $smbMOpt, $platFeatureOpts;

        $platformFeatFallback = '0xFFFFFFFF';
        $platformFeatCombo = "OEM specified on Apple hardware:{$platformFeatFallback}";
        $ramChVals = [1,2,3,4,6,8];

        $productName = $config->getRawVals('SMBIOS/ProductName') ?? '';
        $selected = strtolower(str_replace([' ', ','], '', $productName));
        $platformFeatFlag = $config->getRawVals('SMBIOS/PlatformFeature') ?? $platformFeatFallback;
        $chVal = $config->getRawVals('SMBIOS/Memory/Channels');
        $chassTVal = getSelectedChassType($config->getRawVals('SMBIOS/ChassisType'));

        $smbiosSelInpt = draw_smbios_select($selected);
        $prodNameInpt = drawSimpleInput('string', 'SMBIOS', 'ProductName', 'pd_name', '', 'smpdn');
        $familyInpt = drawSimpleInput('string', 'SMBIOS', 'Family', 'pd_family', '', 'smpdf');
        $biosRelDateInpt = drawSimpleInput('string', 'SMBIOS', 'BiosReleaseDate', 'bios_rel_date', '', 'smbrd');
        $biosVerInpt = drawSimpleInput('string', 'SMBIOS', 'BiosVersion', 'bios_ver', '', 'smbv');
        $biosVndInpt = drawSimpleInput('string', 'SMBIOS', 'BiosVendor', 'bios_vendor', '', 'smbvn');
        $boardIdInpt = drawSimpleInput('string', 'SMBIOS', 'Board-ID', 'board_id', '', 'smbid');
        $boardVerInpt = drawSimpleInput('string', 'SMBIOS', 'BoardVersion', 'board_ver', '', 'smbvr');
        $boardTypeInpt = drawSimpleSelect('SMBIOS', 'BoardType', $boardT, 'board_type', '', 'smbt', '', '', true);
        $boardSerialNoInpt = drawSimpleInput('string', 'SMBIOS', 'BoardSerialNumber', 'board_sn', '', '', '', false, 'mlbfield');
        $chassAssTagInpt = drawComboboxSimpleInput('string', 'SMBIOS', 'ChassisAssetTag', 'chass_asset', $chassAssetTag, '', 'smcha');
        $locInChassInpt = drawSimpleInput('string', 'SMBIOS', 'LocationInChassis', 'loc_in_chass', '', 'smcl');
        $chassTypeInpt = drawSimpleSelect('SMBIOS', 'ChassisType', $chassT, 'chass_type', '', 'smcht', '', '', true, false, $chassTVal);
        $chassManufInpt = drawSimpleInput('string', 'SMBIOS', 'ChassisManufacturer', 'chass_manufacturer', '', 'smchm');
        $serialNoInpt = drawSmbiosSerialNumberInput('SMBIOS', 'SerialNumber');
        $platFeatInpt = drawFlagInput($platformFeatFlag, 'SMBIOS', 'PlatformFeature', 'ocplf', $platformFeatCombo, null, 32);
        $platFeatModal = drawSimpleFlagModal($platFeatureOpts, $platformFeatFlag, 'ocplf', true);
        $fwFeatInpt = drawSimpleInput('string', 'SMBIOS', 'FirmwareFeatures', 'fw_features', '', 'smff');
        $fwFeatMaskInpt = drawSimpleInput('string', 'SMBIOS', 'FirmwareFeaturesMask', 'fw_feature_mask', '', 'smffm');
        $extfwFeatInpt = drawSimpleInput('string', 'SMBIOS', 'ExtendedFirmwareFeatures', 'ext_fw_feat', '', 'extsmff');
        $extfwFeatMaskInpt = drawSimpleInput('string', 'SMBIOS', 'ExtendedFirmwareFeaturesMask', 'ext_fw_feat_mask', '', 'extsmffm');
        $uuidInpt = drawSimpleInput('string', 'SMBIOS', 'SmUUID', 'sm_uuid', '', 'smuui');
        $boardManufInpt = drawSimpleInput('string', 'SMBIOS', 'BoardManufacturer', 'board_manufacturer', '', 'smbm');
        $manufInpt = drawSimpleInput('string', 'SMBIOS', 'Manufacturer', 'manufacturer', '', 'smmnf');
        $versionInpt = drawSimpleInput('string', 'SMBIOS', 'Version', 'version', '', 'smver');
        $efiVerInpt = drawSimpleInput('string', 'SMBIOS', 'EfiVersion', 'efi_version', '', 'efiver');
        $smbiosVerInpt = drawSimpleInput('string', 'SMBIOS', 'SmbiosVersion', 'smbios_ver', '', 'smbver');
        $memRankInpt = drawSimpleInput('integer', 'SMBIOS', 'MemoryRank', 'mem_rank', '', 'memrank', '', true, '', '', false, 1);
        $smbiosOptions = drawSimpleInlineCheckOpts($smbMOpt, 'SMBIOS');
        $ramPatchTable = drawPatchTable('SMram', ['slot', 'sizeM', 'frequency', 'type', 'vendor', 'serial', 'part'], $config->getRawVals('SMBIOS/Memory/Modules'), ['cp']);
        $ramSlotsInpt = drawSimpleInput('integer', 'SMBIOS/Memory', 'SlotCount', 'slot_count', '', 'smslc', '', true);
        $memSlotsTable = drawPatchTable('SMslots', ['device', 'id', 'name', 'type'], $config->getRawVals('SMBIOS/Slots'), ['cp']);
        $smbiosDetailModal = draw_smbios_more_modal();
        $ramChannelsOptions = '';

        for ($i=0, $l=count($channels); $i<$l; ++$i) {
            $selected = isSelected($ramChVals[$i], $chVal);

            $ramChannelsOptions .= "<option value=\"{$ramChVals[$i]}\" {$selected}>{$text[$channels[$i]]}</option>";
        }

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['smbios']}</div></div>
    
                {$smbiosSelInpt}
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$prodNameInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$familyInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$biosRelDateInpt}</div>
                    <div class=\"col-12 col-sm-8 col-lg-3\">{$biosVerInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$biosVndInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$boardIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$boardVerInpt}</div>
                    <div class=\"col-12 col-sm-5 col-lg-3 \">{$boardTypeInpt}</div>
                    <div class=\"col-12 col-sm-7 col-lg-4\">{$boardSerialNoInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassAssTagInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3 \">{$locInChassInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassTypeInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3 \">{$chassManufInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$serialNoInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3 \">{$platFeatInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fwFeatInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fwFeatMaskInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-md-6 col-lg-4\">{$extfwFeatInpt}</div>
                    <div class=\"col-12 col-sm-6 col-md-6 col-lg-4\">{$extfwFeatMaskInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-4\">{$memRankInpt}</div>
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$uuidInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$boardManufInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$manufInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-1\">{$versionInpt}</div>
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$efiVerInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$smbiosVerInpt}</div>
                    <!-- col-lg-6 here -->
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$smbiosOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['ram_modules']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['ram_inj']}</div>
                </div>
                
                {$ramPatchTable}
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 col-sm-4 col-lg-2\">
                        <div class=\"text-center mb-3\">
                            <label class=\"form-label\" for=\"smchn\">{$text['channels']}</label>
                            <select id=\"smchn\" class=\"form-select cce-sel\" data-path=\"SMBIOS/Memory\" data-field=\"Channels\">
                                <option value=\"\"></option>
                                {$ramChannelsOptions}
                            </select>
                        </div>
                    </div>
                    <div class=\"col-12 col-sm-3 col-lg-2\">{$ramSlotsInpt}</div>
                    <!-- col-lg-8 here -->
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['slots']}</div>
                </div>
            
                {$memSlotsTable}
                
                <!-- SMBIOS Details Modal -->
                {$smbiosDetailModal}
                
                <!-- PlatformFeatures Flag Modal -->
                {$platFeatModal}";
    } catch (\Throwable) {}

    return "";
}
