<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE;

abstract class abstractUI {
    /**
     * UI path
     *
     * @var string
     */
    protected string $path;

    /**
     * Menu entries.
     * icon => name
     *
     * @var array
     */
    protected array $menu;

    /**
     * UI function names list
     *
     * @var ?\ArrayObject
     */
    private ?\ArrayObject $funcArr = null;

    public function drawUI(string $fnName): string {
        $func = $this->path.$fnName;
        $fn = function_exists($func) ? $func : '\\drawWipPage';

        return $fn();
    }

    public function getMenu(): array {
        return $this->menu;
    }

    public function getFuncNamesList(): \ArrayObject {
        $this->prepareFuncArr();

        return $this->funcArr;
    }

    private function prepareFuncArr(): void {
        if ($this->funcArr !== null)
            return;

        $this->funcArr = new \ArrayObject();

        foreach ($this->menu as $txt)
            $this->funcArr->append(strtolower(str_replace(' ', '_', $txt)));
    }
}