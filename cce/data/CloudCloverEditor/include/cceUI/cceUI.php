<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE;

require_once __DIR__.'/abstractUI.php';
require_once __DIR__.'/cloverUI.php';
require_once __DIR__.'/ozmosisUI.php';
require_once __DIR__.'/chameleonUI.php';
require_once __DIR__.'/openCoreUI.php';

class cceUI {
    /**
     * cceUI Instance
     *
     * @var ?abstractUI
     */
    private static ?abstractUI $obj = null;

    /**
     * UI include path
     *
     * @var string
     */
    private static string $include = '';

    /**
     * Return the UI instance
     *
     * @param string $mode
     *
     * @return ?abstractUI
     */
    public static function getUIInstance(string $mode): ?abstractUI {
        if (self::$obj !== null)
            return self::$obj;

        switch ($mode) {
            case 'oz':
                self::$include = 'ozmosis.php';
                return (self::$obj = new ozmosisUI());
            case 'chm':
                self::$include = 'enoch.php';
                return (self::$obj = new chameleonUI());
            case 'oc':
                self::$include = 'opencore.php';
                return (self::$obj = new openCoreUI());
            default:
                self::$include = 'clover_efi.php';
                return (self::$obj = new cloverUI());
        }
    }

    public static function getIncludeFile(): string {
        return self::$include;
    }
}