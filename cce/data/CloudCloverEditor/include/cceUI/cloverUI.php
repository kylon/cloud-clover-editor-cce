<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE;

class cloverUI extends abstractUI {
    public function __construct() {
        $this->path = '\\CCE\\clover_UI\\';

        $this->menu = [
            'switch' => 'ACPI',
            'terminal22' => 'Boot',
            'cpu' => 'CPU',
            'hdd-o' => 'Devices',
            'blocked' => 'Disable Drivers',
            'columns2' => 'GUI',
            'image3' => 'Graphics',
            'wrench' => 'Kernel and Kext Patches',
            'superscript3' => 'RT Variables',
            'laptop3' => 'SMBIOS',
            'cog3' => 'System Parameters',
            'palette' => 'Boot Graphics',
            'properties' => 'Quirks'
        ];
    }
}