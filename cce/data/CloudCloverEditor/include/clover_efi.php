<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation either version 3 of the License or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not see <http://www.gnu.org/licenses/>.
 */

require_once __DIR__.'/../clover/ui/acpi.php';
require_once __DIR__.'/../clover/ui/boot.php';
require_once __DIR__.'/../clover/ui/cpu.php';
require_once __DIR__.'/../clover/ui/devices.php';
require_once __DIR__.'/../clover/ui/disable_drivers.php';
require_once __DIR__.'/../clover/ui/gui.php';
require_once __DIR__.'/../clover/ui/graphics.php';
require_once __DIR__.'/../clover/ui/kernel_and_kext_patches.php';
require_once __DIR__.'/../clover/ui/rt_variables.php';
require_once __DIR__.'/../clover/ui/smbios.php';
require_once __DIR__.'/../clover/ui/system_parameters.php';
require_once __DIR__.'/../clover/ui/boot_graphics.php';
require_once __DIR__.'/../clover/ui/quirks.php';