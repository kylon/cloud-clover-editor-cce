<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$fbNamesCombo =
    'Wormy [Old controller]:Wormy;'.
    'Alopias [Old controller]:Alopias;'.
    'Caretta [Old controller]:Caretta;'.
    'Kakapo [Old controller]:Kakapo;'.
    'Kipunji [Old controller]:Kipunji;'.
    'Peregrine [Old controller]:Peregrine;'.
    'Raven [Old controller]:Raven;'.
    'Sphyrna [Old controller]:Sphyrna;'.
    'Iago [AMD 2400]:Iago;'.
    'Hypoprion [AMD 2600]:Hypoprion;'.
    'Lamna [AMD 2600]:Lamna;'.
    'Megalodon [AMD 3800]:Megalodon;'.
    'Triakis [AMD 3800]:Triakis;'.
    'Flicker [AMD 4600]:Flicker;'.
    'Gliff [AMD 4600]:Gliff;'.
    'Shrike [AMD 4600]:Shrike;'.
    'Cardinal [AMD 4800]:Cardinal;'.
    'Motmot [AMD 4800]:Motmot;'.
    'Quail [AMD 4800]:Quail;'.
    'Douc [AMD 5000]:Douc;'.
    'Langur [AMD 5000]:Langur;'.
    'Uakari [AMD 5000]:Uakari;'.
    'Zonalis [AMD 5000]:Zonalis;'.
    'Alouatta [AMD 5000]:Alouatta;'.
    'Hoolock [AMD 5000]:Hoolock;'.
    'Vervet [AMD 5000]:Vervet;'.
    'Baboon [AMD 5000]:Baboon;'.
    'Eulemur [AMD 5000]:Eulemur;'.
    'Galago [AMD 5000]:Galago;'.
    'Colobus [AMD 5000]:Colobus;'.
    'Mangabey [AMD 5000]:Mangabey;'.
    'Nomascus [AMD 5000]:Nomascus;'.
    'Orangutan [AMD 5000]:Orangutan;'.
    'Pithecia [AMD 6000]:Pithecia;'.
    'Bulrushes [AMD 6000]:Bulrushes;'.
    'Cattail [AMD 6000]:Cattail;'.
    'Hydrilla [AMD 6000]:Hydrilla;'.
    'Duckweed [AMD 6000]:Duckweed;'.
    'Fanwort [AMD 6000]:Fanwort;'.
    'Elodea [AMD 6000]:Elodea;'.
    'Kudzu [AMD 6000]:Kudzu;'.
    'Gibba [AMD 6000]:Gibba;'.
    'Lotus [AMD 6000]:Lotus;'.
    'Ipomoea [AMD 6000]:Ipomoea;'.
    'Muskgrass [AMD 6000]:Muskgrass;'.
    'Juncus [AMD 6000]:Juncus;'.
    'Osmunda [AMD 6000]:Osmunda;'.
    'Pondweed [AMD 6000]:Pondweed;'.
    'Spikerush [AMD 6000]:Spikerush;'.
    'Typha [AMD 6000]:Typha;'.
    'Ramen [AMD 7000]:Ramen;'.
    'Tako [AMD 7000]:Tako;'.
    'Namako [AMD 7000]:Namako;'.
    'Aji [AMD 7000]:Aji;'.
    'Buri [AMD 7000]:Buri;'.
    'Chutoro [AMD 7000]:Chutoro;'.
    'Dashimaki [AMD 7000]:Dashimaki;'.
    'Ebi [AMD 7000]:Ebi;'.
    'Gari [AMD 7000]:Gari;'.
    'Futomaki [AMD 7000]:Futomaki;'.
    'Hamachi [AMD 7000]:Hamachi;'.
    'OPM [AMD 7000]:OPM;'.
    'Ikura [AMD 7000]:Ikura;'.
    'IkuraS [AMD 7000]:IkuraS;'.
    'Junsai [AMD 7000]:Junsai;'.
    'Kani [AMD 7000]:Kani;'.
    'KaniS [AMD 7000]:KaniS;'.
    'DashimakiS [AMD 7000]:DashimakiS;'.
    'Maguro [AMD 7000]:Maguro;'.
    'MaguroS [AMD 7000]:MaguroS;'.
    'Exmoor [AMD 8000]:Exmoor;'.
    'Baladi [AMD 8000]:Baladi;'.
    'MalteseS [AMD 9000]:MalteseS;'.
    'Lagotto [AMD 9000]:Lagotto;'.
    'GreyhoundS [AMD 9000]:GreyhoundS;'.
    'Maltese [AMD 9000]:Maltese;'.
    'Basset [AMD 9000]:Basset;'.
    'Greyhound [AMD 9000]:Greyhound;'.
    'Labrador [AMD 9000]:Labrador;'.
    'FlueveSWIP [AMD 9300]:FlueveSWIP;'.
    'Acre [AMD 9500]:Acre;'.
    'Dayman [AMD 9500]:Dayman;'.
    'Guariba [AMD 9500]:Guariba;'.
    'Huallaga [AMD 9500]:Huallaga;'.
    'Orinoco [AMD 9500]:Orinoco;'.
    'Berbice [AMD 9510]:Berbice;'.
    'Mazaruni [AMD 9515]:Mazaruni;'.
    'Longavi [AMD 9515]:Longavi;'.
    'Elqui [AMD 9520]:Elqui;'.
    'Caroni [AMD 9520]:Caroni;'.
    'Florin [AMD 9520]:Florin';

$snbPlatIDCombo =
    'SandyBridge [HD 3000 - 1 Ports]:0x00020000;'.
    'SandyBridge [HD 3000 - MBP8,x - 4 Ports]:0x00010000;'.
    'SandyBridge [HD 3000 - Macmini5,1/3 - 3 Ports]:0x00030010;'.
    'SandyBridge [HD 3000 - Macmini5,1/3 - 3 Ports]:0x00030020;'.
    'SandyBridge [HD 3000 - Macmini5,2 - 0 Ports]:0x00030030;'.
    'SandyBridge [HD 3000 - MBA4,1/2 - 3 Ports]:0x00040000;'.
    'SandyBridge [HD 3000 - iMac12,1/2 - 0 Ports]:0x00050000;';

$igPlatIDCombo =
    'IvyBridge [HD 4000 - 3 Ports]:0x01620005;'.
    'IvyBridge [HD 4000 - iMac13,1 - 0 Ports]:0x01620006;'.
    'IvyBridge [HD 4000 - iMac13,2 - 0 Ports]:0x01620007;'.
    'IvyBridge [HD 4000 - 4 Ports]:0x01660000;'.
    'IvyBridge [HD 4000 - MBP10,2 - 4 Ports]:0x01660001;'.
    'IvyBridge [HD 4000 - MBP10,1 - 1 Port]:0x01660002;'.
    'IvyBridge [HD 4000 - MBP9,2 - 4 Ports]:0x01660003;'.
    'IvyBridge [HD 4000 - MBP9,1 - 1 Port]:0x01660004;'.
    'IvyBridge [HD 4000 - MBA5,1 - 3 Ports]:0x01660008;'.
    'IvyBridge [HD 4000 - MBA5,2 - 3 Ports]:0x01660009;'.
    'IvyBridge [HD 4000 - Macmini6,1 - 3 Ports]:0x0166000A;'.
    'IvyBridge [HD 4000 - Macmini6,2 - 3 Ports]:0x0166000B;'.

    'Haswell [3 Ports]:0x04060000;'.
    'Haswell [HD 4600 - 0 Ports]:0x04120004;'.
    'Haswell [HD 4600 - iMac15,1 - 0 Ports]:0x0412000B;'.
    'Haswell [HD 4600 - 3 Ports]:0x04160000;'.
    'Haswell [HD 5000 - 3 Ports]:0x04260000;'.
    'Haswell [HD 4400 - 3 Ports]:0x0A160000;'.
    'Haswell [HD 4400 - 3 Ports]:0x0A16000C;'.
    'Haswell [HD 5000 - 3 Ports]:0x0A260000;'.
    'Haswell [HD 5000 - 3 Ports]:0x0A260005;'.
    'Haswell [HD 5000 - MBA6,1/2 - 3 Ports]:0x0A260006;'.
    'Haswell [HD 5000 - 3 Ports]:0x0A26000A;'.
    'Haswell [HD 5000 - 2 Ports]:0x0A26000D;'.
    'Haswell [HD 5100 - MBP11,1 - 3 Ports]:0x0A2E0008;'.
    'Haswell [HD 5100 - 3 Ports]:0x0A2E000A;'.
    'Haswell [HD 5100 - 2 Ports]:0x0A2E000D;'.
    'Haswell [3 Ports]:0x0C060000;'.
    'Haswell [3 Ports]:0x0C160000;'.
    'Haswell [3 Ports]:0x0C260000;'.
    'Haswell [HD 5200 - iMac14,1/4 - 3 Ports]:0x0D220003;'.
    'Haswell [HD 5200 - 3 Ports]:0x0D260000;'.
    'Haswell [HD 5200 - MBP11,2/3 - 4 Ports]:0x0D260007;'.
    'Haswell [HD 5200 - 1 Ports]:0x0D260009;'.
    'Haswell [HD 5200 - 4 Ports]:0x0D26000E;'.
    'Haswell [HD 5200 - 1 Ports]:0x0D26000F;'.

    'Broadwell [3 Ports]:0x16060000;'.
    'Broadwell [3 Ports]:0x16060002;'.
    'Broadwell [3 Ports]:0x160E0000;'.
    'Broadwell [3 Ports]:0x160E0001;'.
    'Broadwell [HD 5600 - 4 Ports]:0x16120003;'.
    'Broadwell [HD 5500 - 3 Ports]:0x16160000;'.
    'Broadwell [HD 5500 - 3 Ports]:0x16160002;'.
    'Broadwell [HD 5300 - 3 Ports]:0x161E0000;'.
    'Broadwell [HD 5300 - MB8,1 - 3 Ports]:0x161E0001;'.
    'Broadwell [HD 6200 - 3 Ports]:0x16220000;'.
    'Broadwell [HD 6200 - 3 Ports]:0x16220002;'.''.
    'Broadwell [HD 6200 - iMac16,2 - 3 Ports]:0x16220007;'.
    'Broadwell [HD 6000 - 3 Ports]:0x16260000;'.
    'Broadwell [HD 6000 - 3 Ports]:0x16260002;'.
    'Broadwell [HD 6000 - 3 Ports]:0x16260004;'.
    'Broadwell [HD 6000 - 3 Ports]:0x16260005;'.
    'Broadwell [HD 6000 - MBA7,1/2 - 3 Ports]:0x16260006;'.
    'Broadwell [HD 6000 - 2 Ports]:0x16260008;'.
    'Broadwell [HD 6100 - 3 Ports]:0x162b0000;'.''.
    'Broadwell [HD 6100 - MBP12,2 - 3 Ports]:0x162B0002;'.
    'Broadwell [HD 6100 - 3 Ports]:0x162B0004;'.
    'Broadwell [HD 6100 - 2 Ports]:0x162B0008;'.

    'Skylake [HD 510 - 0 Ports]:0x19020001;'.
    'Skylake [HD 530 - 3 Ports]:0x19120000;'.
    'Skylake [HD 530 - iMac17,1 - 0 Ports]:0x19120001;'.
    'Skylake [HD 520 - 3 Ports]:0x19160000;'.
    'Skylake [HD 520 - 3 Ports]:0x19160002;'.
    'Skylake [0 Ports]:0x19170001;'.
    'Skylake [HD 530 - MBP13,3 - 3 Ports]:0x191B0000;'.
    'Skylake [HD 530 - 1 Ports]:0x191B0006;'.
    'Skylake [HD 515 - 3 Ports]:0x191E0000;'.
    'Skylake [HD 515 - MB9,1 - 3 Ports]:0x191E0003;'.
    'Skylake [HD 540 - 3 Ports]:0x19260000;'.
    'Skylake [HD 540 - MBP13,1 - 3 Ports]:0x19260002;'.
    'Skylake [HD 540 - 3 Ports]:0x19260004;'.
    'Skylake [HD 540 - 3 Ports]:0x19260007;'.
    'Skylake [HD 550 - 3 Ports]:0x19270000;'.
    'Skylake [HD 550 - MBP13,2 - 3 Ports]:0x19270004;'.
    'Skylake [HD 580 - 0 Ports]:0x19320001;'.
    'Skylake [HD 580 - 3 Ports]:0x193B0000;'.
    'Skylake [HD 580 - 4 Ports]:0x193B0005;'.

    'Kabylake [HD 630 - 3 Ports]:0x59120000;'.
    'Kabylake [HD 630 - iMac18,2/3 - 0 Ports]:0x59120003;'.
    'Kabylake [HD 620 - 3 Ports]:0x59160000;'.
    'Kabylake [HD 620 - 3 Ports]:0x59160009;'.
    'Kabylake [0 Ports]:0x59180002;'.
    'Kabylake [HD 630 - MBP14,3 - 3 Ports]:0x591B0000;'.
    'Kabylake [HD 630 - 1 Ports]:0x591B0006;'.
    'Kabylake [3 Ports]:0x591C0005;'.
    'Kabylake [HD 615 - 3 Ports]:0x591E0000;'.
    'Kabylake [HD 615 - MB10,1 - 3 Ports]:0x591E0001;'.
    'Kabylake [HD 635 - 3 Ports]:0x59230000;'.
    'Kabylake [HD 640 - 3 Ports]:0x59260000;'.
    'Kabylake [HD 640 - MBP14,1 - 3 Ports]:0x59260002;'.
    'Kabylake [HD 640 - 3 Ports]:0x59260007;'.
    'Kabylake [HD 650 - 3 Ports]:0x59270000;'.
    'Kabylake [HD 650 - MBP14,2 - 3 Ports]:0x59270004;'.
    'Kabylake [HD 650 - 3 Ports]:0x59270009;'.
    'Kabylake [HD 615 - 3 Ports]:0x87C00000;'.
    'Kabylake [HD 615 - 3 Ports]:0x87C00005;'.

    'CoffeLake [UHD 630 - 3 Ports]:0x3E000000;'.
    'CoffeLake [UHD 630 - 0 Ports]:0x3E910003;'.
    'CoffeLake [UHD 630 - 3 Ports]:0x3E920000;'.
    'CoffeLake [UHD 630 - 0 Ports]:0x3E920003;'.
    'CoffeLake [UHD 630 - 3 Ports]:0x3E920009;'.
    'CoffeLake [UHD 630 - iMac19,1 - 0 Ports]:0x3E980003;'.
    'CoffeLake [UHD 630 - 3 Ports]:0x3E9B0000;'.
    'CoffeLake [UHD 630 - MBP15,1 - 1 Ports]:0x3E9B0006;'.
    'CoffeLake [UHD 630 - Macmini8,1 - 3 Ports]:0x3E9B0007;'.
    'CoffeLake [UHD 630 - 3 Ports]:0x3E9B0009;'.
    'CoffeLake [HD 655 - 3 Ports]:0x3EA50000;'.
    'CoffeLake [HD 655 - MBP15,2 - 3 Ports]:0x3EA50004;'.
    'CoffeLake [UHD 630 - 3 Ports]:0x3EA50005;'.
    'CoffeLake [HD 655 - 3 Ports]:0x3EA50009;'.
    'CoffeLake [3 Ports]:0x3EA60005;'.

    'CannonLake [1 Ports]:0x0A010000;'.
    'CannonLake [3 Ports]:0x5A400000;'.
    'CannonLake [3 Ports]:0x5A400009;'.
    'CannonLake [3 Ports]:0x5A410000;'.
    'CannonLake [3 Ports]:0x5A410009;'.
    'CannonLake [3 Ports]:0x5A490000;'.
    'CannonLake [3 Ports]:0x5A490009;'.
    'CannonLake [3 Ports]:0x5A500000;'.
    'CannonLake [3 Ports]:0x5A500009;'.
    'CannonLake [3 Ports]:0x5A510000;'.
    'CannonLake [3 Ports]:0x5A510009;'.
    'CannonLake [3 Ports]:0x5A520000;'.
    'CannonLake [3 Ports]:0x5A590000;'.
    'CannonLake [3 Ports]:0x5A590009;';
