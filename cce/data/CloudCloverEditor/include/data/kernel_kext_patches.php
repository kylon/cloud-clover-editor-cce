<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$osv = [
    'Mac OS X Snow Leopard', 10, 6, 8,
    'Mac OS X Lion', 10, 7, 5,
    'Mac OS X Mountain Lion', 10, 8, 5,
    'Mac OS X Mavericks', 10, 9, 5,
    'Mac OS X Yosemite', 10, 10, 5,
    'Mac OS X El Capitan', 10, 11, 6,
    'macOS Sierra', 10, 12, 6,
    'macOS High Sierra', 10, 13, 6,
    'macOS Mojave', 10, 14, 6,
    'macOS Catalina', 10, 15, 7,
    'macOS Big Sur', 10, 16, 0,
    'macOS Big Sur', 11, 0, 1,
    'macOS Big Sur', 11, 1, 2,
    'macOS Big Sur', 11, 2, 1,
    'macOS Big Sur', 11, 4, 0,
    'macOS Big Sur', 11, 5, 2,
    'macOS Big Sur', 11, 6, 0,
    ];

$fakeCpuIDCombo =
    'Yonah:0x0006E6;'.
    'Conroe:0x0006F2;'.
    'Penryn:0x010676;'.
    'Nehalem:0x0106A2;'.
    'Atom:0x0106C2;'.
    'XeonMP:0x0106D0;'.
    'Linnfield:0x0106E0;'.
    'Havendale:0x0106F0;'.
    'Clarkdale:0x020650;'.
    'AtomSandy:0x020660;'.
    'Lincroft:0x020670;'.
    'SandyBridge:0x0206A0;'.
    'Westmere:0x0206C0;'.
    'Jaketown:0x0206D0;'.
    'NehalemEx:0x0206E0;'.
    'WestmereEx:0x206F0;'.
    'Atom2000:0x030660;'.
    'IvyBridge:0x0306A0;'.
    'Haswell:0x0306C0;'.
    'IvyBridgeE5:0x0306E0;'.
    'HaswellMB:0x0306F0;'.
    'HaswellULT:0x040650;'.
    'CrystalWell:0x040660';
