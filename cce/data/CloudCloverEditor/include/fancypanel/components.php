<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/../data/kernel_kext_patches.php';

function drawMatchOS(array $fpdata, int $idx): string {
    global $text;

    $matchOSes = getPropVal($fpdata['p'], 'MatchOS');
    $matchBuild = getPropVal($fpdata['p'], 'MatchBuild');

    $matchOSField = drawTextAreaNoLabel('string', $fpdata['path'].'/'.$idx, 'MatchOS', 3, '', '', '', 'matchos-tx', false, $matchOSes);
    $matchBuildField = drawSimpleTextArea('string', $fpdata['path'].'/'.$idx, 'MatchBuild', 'match_build', 5, '', '', '', 'matchbuild-texarea', false, $matchBuild);
    $macOSVersList = genMacOSVersionsList();
    
    return "<div class=\"row mb-3\">
                <div class=\"col-12 col-lg-6\">
                    <div class=\"row\">
                        <div class=\"col-12\">
                            <div class=\"text-center mb-3\">
                                <label class=\"form-label\">{$text['match_oses']}</label>
                                <select class=\"form-select cce-sel\" data-sel=\"matchoses\">
                                    <option value=\"\"></option>
                                    <option value=\"del\">{$text['clear']}</option>
                                    <option value=\"All\">{$text['osv_All']}</option>
                                    {$macOSVersList}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-12\">{$matchOSField}</div>
                    </div>
                </div>
                <div class=\"col-12 col-lg-6\">{$matchBuildField}</div>
            </div>";
}