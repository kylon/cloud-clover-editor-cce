<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

/**
 * Draw custom entries elements (icon and modal)
 *
 * @param array $fpdata
 * @param string $hideCls
 * @param int $idx
 * @param bool $sortable
 *
 * @return string
 */
function drawCentryElems(array &$fpdata, string $hideCls, int $idx, bool $sortable): string {
    $html = '';

    foreach ($fpdata['pdata'] as $p) {
        $fullTitle = getPropVal($p, 'FullTitle');
        $title = getPropVal($p, 'Title');
        $hasFullTitle = $fullTitle !== '';
        $titleVal = $hasFullTitle || $title !== '' ? ($hasFullTitle ? $fullTitle : $title) : 'new';
        $fpdata['hasFullTitle'] = $hasFullTitle;
        $fpdata['titleV'] = $titleVal;
        $fpdata['p'] = &$p;

        $icon = drawIcon($fpdata, $idx, $titleVal, 'drive', $sortable);
        $modal = '';

        switch ($fpdata['type']) {
            case 'cEntry':
                $modal = drawModalBodySkel($fpdata, $idx, $titleVal, 'drawCentryModalBody');
                break;
            case 'cLegE':
                $modal = drawModalBodySkel($fpdata, $idx, $titleVal, 'drawLegacyCentryModalBody');
                break;
            case 'cToolsE':
                $modal = drawModalBodySkel($fpdata, $idx, $titleVal, 'drawToolsCentryModalBody');
                break;
            default:
                break;
        }

        $html .= "<div class=\"single-item-group{$hideCls}\">{$icon}{$modal}</div>";

        ++$idx;
        $hideCls = '';
    }

    return $html;
}

function drawCentryModalBody(array $fpdata, int $idx): string {
    global $config, $text, $centrySelType, $centryVolT, $centryChkOpt, $cLogoCombo;

    $path = $fpdata['path'].'/'.$idx;
    $titleDataField = $fpdata['hasFullTitle'] ? 'FullTitle':'Title';
    $fullTitleChecked = $fpdata['hasFullTitle'] ? 'checked':'';
    $entryOpts = '';

    $volume = getPropVal($fpdata['p'], 'Volume');
    $entryPath = getPropVal($fpdata['p'], 'Path');
    $hotkey = getPropVal($fpdata['p'], 'Hotkey');
    $volOsType = getPropVal($fpdata['p'], 'Type');
    $volType = getPropVal($fpdata['p'], 'VolumeType');
    $args = getPropVal($fpdata['p'], 'Arguments');
    $addArgs = getPropVal($fpdata['p'], 'AddArguments');
    $logo = boolToYesNo(getPropVal($fpdata['p'], 'CustomLogo'));
    $image = getPropVal($fpdata['p'], 'Image');
    $driveImage = getPropVal($fpdata['p'], 'DriveImage');
    $bgColor = getPropVal($fpdata['p'], 'BootBgColor');
    $hiddenVal = getPropVal($fpdata['p'], 'Hidden');
    $hiddenFalse = isSelected(false, $hiddenVal);
    $hiddenTrue = isSelected(true, $hiddenVal);
    $hiddenAlways = isSelected('Always', $hiddenVal);
    $injKextVal = getPropVal($fpdata['p'], 'InjectKexts');
    $injDetect = isSelected('Detect', $injKextVal);
    $injTrue = isSelected(true, $injKextVal);
    $injFalse = isSelected(false, $injKextVal);
    $subEntries = $config->getRawVals($path.'/SubEntries');

    $volumeField = drawSimpleInput('string', $path, 'Volume', 'volume', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $volume);
    $titleField = drawSimpleInput('string', $path, $titleDataField, 'title', '', '', '', false, 'cEntry-title', "data-subid=\"{$idx}\" data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleV']);
    $entryPathField = drawSimpleInput('string', $path, 'Path', 'path', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $entryPath);
    $hotkeyField = drawSimpleInput('string', $path, 'Hotkey', 'hotkey', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $hotkey);
    $argsField = drawSimpleTextArea('string', $path, 'Arguments', 'arguments', 4, '', '', '', '', false, $args);
    $addArgsField = drawSimpleTextArea('string', $path, 'AddArguments', 'add_arguments', 4, '', '', '', '', false, $addArgs);
    $logoField = drawComboboxSimpleInput('string', $path, 'CustomLogo', 'os_logo', $cLogoCombo, '', '', $text['c_logo_placeholder'], '', '', false, PHP_INT_MIN, PHP_INT_MIN, $logo);
    $imageField = drawSimpleInput('string', $path, 'Image', 'image', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $image);
    $driveImageField = drawSimpleInput('string', $path, 'DriveImage', 'drive_image', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $driveImage);
    $bgColorField = drawSimpleInput('string', $path, 'BootBgColor', 'boot_bg_color', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $bgColor);
    $fullTitleOpt = drawCheckbox('form-check-inline', $path, 'FullTitle', $fullTitleChecked, 'full_title', false, 'centry-ftitle', "data-subid=\"{$idx}\" data-change=\"fullTtl\" data-ftarget=\"cEntry\"", true, false);
    $subEntriesTable = drawPatchTable('subCEn', ['title', 'add_arguments', 'full_title', 'common_settings'], $subEntries, ['cp'], $idx);
    $centryTypeField = drawSimpleSelect($path, 'Type', $centrySelType, 'type', '', '', '', '', true, false, $volOsType);
    $centryVolumeTypeField = drawSimpleSelect($path, 'VolumeType', $centryVolT, 'volume_type', '', '', '', '', true, false, $volType);

    foreach ($centryChkOpt as $opt) {
        $subStr = substr($opt, 5);
        $entryOpts .= drawCheckbox('form-check-inline', $path, $subStr, getCheckAttr($fpdata['p'], $subStr), $opt);
    }
    
    return "<div class=\"row\">
                <div class=\"col-12 col-lg-7\">{$volumeField}</div>
                <div class=\"col-12 col-lg-5\">{$titleField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 col-xl-9\">{$entryPathField}</div>
                <div class=\"col-12 col-lg-4 col-xl-3\">{$hotkeyField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$centryTypeField}</div>
                <div class=\"col-12 col-lg-6\">{$centryVolumeTypeField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$argsField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$addArgsField}</div>
            </div>

            <div class=\"row\">
                <div class=\"col-12 subtitle\">{$text['theme_t']}</div>
            </div>

            <div class=\"row mt-3\">
                <div class=\"col-12 col-sm-12 col-lg-6\">{$logoField}</div>
                <div class=\"col-12 col-sm-12 col-lg-6\">{$imageField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-sm-12 col-lg-6\">{$driveImageField}</div>
                <div class=\"col-12 col-lg-3\">{$bgColorField}</div>
            </div>

            <div class=\"row\">
                <div class=\"col-12 subtitle\">{$text['options']}</div>
            </div>

            <div class=\"row\">
                <div class=\"col-12 col-lg-6 mt-3 mb-3\">
                     {$entryOpts}
                     {$fullTitleOpt}
                </div>
                <div class=\"col-12 col-lg-3\">
                    <div class=\"text-center mb-3\">
                        <label class=\"form-label\">{$text['hidden']}</label>
                        <select class=\"form-select cce-sel\" data-path=\"{$path}\" data-field=\"Hidden\">
                            <option value=\"false\" {$hiddenFalse}>{$text['no']}</option>
                            <option value=\"true\" {$hiddenTrue}>{$text['yes']}</option>
                            <option value=\"Always\" {$hiddenAlways}>{$text['always']}</option>
                        </select>
                    </div>
                </div>
                <div class=\"col-12 col-lg-3\">
                    <div class=\"text-center mb-3\">
                        <label class=\"form-label\">{$text['inject_kext']}</label>
                        <select class=\"form-select cce-sel\" data-path=\"{$path}\" data-field=\"InjectKexts\">
                            <option value=\"Detect\" {$injDetect}>{$text['detect']}</option>
                            <option value=\"true\" {$injTrue}>{$text['yes']}</option>
                            <option value=\"false\" {$injFalse}>{$text['no']}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-12 subtitle\">{$text['sub_entries']}</div>
            </div>

            <div data-subid=\"{$idx}\">{$subEntriesTable}</div>";
}

function drawLegacyCentryModalBody(array $fpdata, int $idx): string {
    global $lcentryChkOpt, $lcentryVolT;

    $path = $fpdata['path'].'/'.$idx;

    $volType = getPropVal($fpdata['p'], 'Type');
    $volume = getPropVal($fpdata['p'], 'Volume');
    $hotkey = getPropVal($fpdata['p'], 'Hotkey');
    $options = drawOpts($fpdata, $idx, $lcentryChkOpt);

    $volumeField = drawSimpleInput('string', $path, 'Volume', 'volume', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $volume);
    $titleField = drawSimpleInput('string', $path, 'Title', 'title', '', '', '', false, 'cLegE-title', "data-subid=\"{$idx}\" data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleV']);
    $hotkeyField = drawSimpleInput('string', $path, 'Hotkey', 'hotkey', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $hotkey);
    $lcentryVolTypeField = drawSimpleSelect($path, 'Type', $lcentryVolT, 'volume_type', '', '', '', '', true, false, $volType);

    return "<div class=\"row\">
                <div class=\"col-12 col-lg-3\">{$lcentryVolTypeField}</div>
                <div class=\"col-12 col-lg-3\">{$hotkeyField}</div>
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 col-xl-8\">{$volumeField}</div>
                <div class=\"col-12 col-lg-4 col-xl-4\">{$titleField}</div>
            </div>";
}

function drawToolsCentryModalBody(array $fpdata, int $idx): string {
    global $cToolsChkOpt;

    $path = $fpdata['path'].'/'.$idx;
    $titleDataField = $fpdata['hasFullTitle'] ? 'FullTitle':'Title';
    $fullTitleChecked = $fpdata['hasFullTitle'] ? 'checked':'';

    $volume = getPropVal($fpdata['p'], 'Volume');
    $entryPath = getPropVal($fpdata['p'], 'Path');
    $args = getPropVal($fpdata['p'], 'Arguments');
    $hotkey = getPropVal($fpdata['p'], 'Hotkey');
    $options = drawOpts($fpdata, $idx, $cToolsChkOpt, 12);

    $volumeField = drawSimpleInput('string', $path, 'Volume', 'volume', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $volume);
    $titleField = drawSimpleInput('string', $path, $titleDataField, 'title', '', '', '', false, 'cToolsE-title', "data-subid=\"{$idx}\" data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleV']);
    $entryPathField = drawSimpleInput('string', $path, 'Path', 'path', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $entryPath);
    $hotkeyField = drawSimpleInput('string', $path, 'Hotkey', 'hotkey', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $hotkey);
    $argsField = drawSimpleTextArea('string', $path, 'Arguments', 'arguments', 4, '', '', '', '', false, $args);
    $fullTitleOpt = drawCheckbox('form-check-inline', $path, 'FullTitle', $fullTitleChecked, 'full_title', false, 'centry-ftitle', "data-subid=\"{$idx}\" data-change=\"fullTtl\" data-ftarget=\"cToolsE\"", true, false);

    return "{$options}
            <div class=\"row\">
                <div class=\"col-12\">{$volumeField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-6 col-sm-4 col-lg-3 mt-4\">{$fullTitleOpt}</div>
                <div class=\"col-12 col-lg-9\">{$titleField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-8 col-xl-9\">{$entryPathField}</div>
                <div class=\"col-12 col-lg-4 col-xl-3\">{$hotkeyField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$argsField}</div>
            </div>";
}