<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/fancypanel_clover_gui.php';
require_once __DIR__.'/fancypanel_opencore_entries.php';
require_once __DIR__.'/fancypanel_patches.php';

function draw_fancy_panel(?array $pdata, string $type, string $path, string $title, bool $sortable = false, bool $hasDBSupport = true): string {
    global $text;

    $pdata = array_merge([''], $pdata ?? []); // add factory element (js)
    $fpdata = [
        'pdata' => &$pdata,
        'path' => $path,
        'type' => $type
    ];

    $dbBtn = $hasDBSupport && !$sortable ? "<div data-id=\"db-{$type}\" class=\"dbTbBtn single-item-dbPatch bg-info stretch-down\"><i class=\"fa icon-sphere\"></i></div>" : '';
    $roundCls = !$hasDBSupport || $sortable ? '':' kkp-btn';
    $sortCls = $sortable ? " fancypanel-sortable":'';
    $sortAttr = $sortable ? " data-sid=\"panel-{$type}\"":'';
    $hideCls = ' d-none';
    $startIdx = -1;
    $items = '';

    switch ($type) {
        case 'kextP':
        case 'ozkextP':
            $items = drawPatchElems($fpdata, 'Name', $hideCls, $startIdx, $sortable);
            break;
        case 'kernelP':
        case 'bootefiP':
        case 'ozkernelP':
        case 'ozBtrP':
        case 'ocAcpita':
        case 'ocBtrP':
        case 'ocKernPatch':
        case 'chamkernelP':
        case 'dsdtP':
        case 'ozAcpiP':
        case 'vbiosP':
            $items = drawPatchElems($fpdata, 'Comment', $hideCls, $startIdx, $sortable);
            break;
        case 'ocKernAdd':
            $items = drawPatchElems($fpdata, 'BundlePath', $hideCls, $startIdx, $sortable);
            break;
        case 'ocKernBlock':
        case 'ocForceProps':
            $items = drawPatchElems($fpdata, 'Identifier', $hideCls, $startIdx, $sortable);
            break;
        case 'cEntry':
        case 'cLegE':
        case 'cToolsE':
            $items = drawCentryElems($fpdata, $hideCls, $startIdx, $sortable);
            break;
        case 'ocEntry':
        case 'ocTools':
            $items = drawOCCentryElems($fpdata, 'Name', $hideCls, $startIdx, $sortable);
            break;
        case 'ocMemDevs':
            $items = drawOCCentryElems($fpdata, 'BankLocator', $hideCls, $startIdx, $sortable, 'Unknown');
            break;
        case 'ocUefiDrv':
            $items = drawOCCentryElems($fpdata, 'Path', $hideCls, $startIdx, $sortable);
            break;
        default:
            break;
    }

    return "<div class=\"row mt-2\">
                <div class=\"col-12 subtitle\">{$text[$title]}</div>
           </div>
            <div class=\"row\">
                <div class=\"col-12\">
                    <div class=\"row\">
                        <div class=\"col-12\">
                            <div data-id=\"add-{$type}\" class=\"addcEntryBtn noDelBtn bg-success single-item-add stretch-down{$roundCls}\">
                                <i class=\"fa icon-plus1\"></i>
                            </div>
                            {$dbBtn}
                        </div>
                    </div>
        
                    <div class=\"row\">
                        <div class=\"col-12\">
                            <div class=\"card no-round-upleft\">
                                <div class=\"card-body single-items-panel panel-{$type}{$sortCls}\"{$sortAttr}>
                                    {$items}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";
}

function drawIcon(array $fpdata, int $idx, string $title, string $icon, bool $sortable = false): string {
    $sortIcon = $sortable ? "<div class=\"{$fpdata['type']}-shandle centry-sort-handle bg-success rounded-circle\"><i class=\"fa icon-hand-paper-o2\"></i></div>" : '';

    return "<div class=\"single-item single-{$fpdata['type']} flip-in\">
                {$sortIcon}
                <div class=\"rounded-circle single-item-icon\"><i data-bs-toggle=\"modal\" data-bs-target=\".modal{$fpdata['type']}-{$idx}\" class=\"fa icon-{$icon}\"></i></div>
                <div class=\"single-item-title {$fpdata['type']}-list-title text-truncate\" data-subid=\"{$idx}\" title=\"{$title}\">
                   {$title} 
                </div>
                <div data-id=\"cp-{$fpdata['type']}\" data-path=\"{$fpdata['path']}\" data-index=\"{$idx}\" class=\"single-item-cp-btn bg-warning float-start copycEntry-btn rounded-circle\">
                    <i class=\"fa icon-copy2\"></i>
                </div>
                <div data-id=\"del-{$fpdata['type']}\" data-path=\"{$fpdata['path']}\" data-index=\"{$idx}\" class=\"single-item-del-btn float-end del-cEntry-btn bg-danger rounded-circle\">
                    <i class=\"fa icon-x\"></i>
                </div>
            </div>";
}

function drawModalBodySkel(array $fpdata, int $idx, string $title, callable $drawBodyFn): string {
    $body = $drawBodyFn($fpdata, $idx);

    return "<div class=\"modal fade modal{$fpdata['type']}-{$idx}\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
                <div class=\"modal-dialog modal-lg\" role=\"document\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <h5 class=\"modal-title modal-{$fpdata['type']}-title truncate-80 text-start\" data-subid=\"{$idx}\" title=\"{$title}\">
                                 {$title} 
                            </h5>
                            <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
                        </div>
                        <div class=\"modal-body\">{$body}</div>
                    </div>
                </div>
            </div>";
}

function drawOpts(array $fpdata, int $idx, array $opts, int $col = 6): string {
    global $text;

    $options = '';

    foreach ($opts as $opt) {
        $optSub = substr($opt, 5);
        $options .= drawCheckbox('form-check-inline', $fpdata['path'].'/'.$idx, $optSub, getCheckAttr($fpdata['p'], $optSub), $opt);
    }

    return "<div class=\"col-12 col-lg-{$col}\">
                <div class=\"row\">
                    <div class=\"col-12 subtitle text-center\">{$text['options']}</div>
                </div>
                <div class=\"row\">
                    <div class=\"col-12 text-start mt-3 mb-3\">{$options}</div>
                </div>
            </div>";
}