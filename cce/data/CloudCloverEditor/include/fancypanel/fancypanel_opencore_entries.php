<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

/**
 * Draw custom entries elements (icon and modal)
 *
 * @param array $fpdata
 * @param string $titleField
 * @param string $hideCls
 * @param int $idx
 * @param bool $sortable
 * @param int|string $fallbackValue
 *
 * @return string
 */
function drawOCCentryElems(array &$fpdata, string $titleField, string $hideCls, int $idx, bool $sortable, int|string $fallbackValue = 'new'): string {
    $html = '';

    foreach ($fpdata['pdata'] as $p) {
        $liveTitleField = getPropVal($p, $titleField, $fallbackValue);
        $fpdata['titleField'] = $liveTitleField;
        $fpdata['p'] = &$p;

        $icon = drawIcon($fpdata, $idx, $liveTitleField, 'drive', $sortable);
        $modal = '';

        switch ($fpdata['type']) {
            case 'ocEntry':
            case 'ocTools':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOCCentryModalBody');
                break;
            case 'ocMemDevs':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOCMemDevicesModalBody');
                break;
            case 'ocUefiDrv':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOCDriversProps');
                break;
            default:
                break;
        }

        $html .= "<div class=\"single-item-group{$hideCls}\">{$icon}{$modal}</div>";

        ++$idx;
        $hideCls = '';
    }

    return $html;
}

function drawOCCentryModalBody(array $fpdata, int $idx): string {
    global $cEntriesOpts;

    $path = $fpdata['path'].'/'.$idx;

    $args = getPropVal($fpdata['p'], 'Arguments');
    $comment = getPropVal($fpdata['p'], 'Comment');
    $entryPath = getPropVal($fpdata['p'], 'Path');
    $flavour = getPropVal($fpdata['p'], 'Flavour', 'Auto');

    $entryPathField = drawSimpleTextArea('string', $path, 'Path', 'path', 4, '', '', '', '', false, $entryPath);
    $argsField = drawSimpleTextArea('string', $path, 'Arguments', 'arguments', 4, '', '', '', '', false, $args);
    $commentField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, '', '', '', '', false, $comment);
    $nameField = drawSimpleInput('string', $path, 'Name', 'name', '', '', '', false, $fpdata['type'].'-name', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
    $flavourField = drawSimpleInput('string', $path, 'Flavour', 'flavour', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $flavour);
    $options = drawOpts($fpdata, $idx, $cEntriesOpts, 12);

    return "<div class=\"row\">{$options}</div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$nameField}</div>
                <div class=\"col-12 col-lg-6\">{$flavourField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$entryPathField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$argsField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commentField}</div>
            </div>";
}

function drawOCMemDevicesModalBody(array $fpdata, int $idx): string {
    global $ramSpeedOpts;

    $manufCombo = 'NO DIMM;';
    $path = $fpdata['path'].'/'.$idx;

    $assetTag = getPropVal($fpdata['p'], 'AssetTag', 'Unknown');
    $deviceLocator = getPropVal($fpdata['p'], 'DeviceLocator', 'Unknown');
    $manufacturer = getPropVal($fpdata['p'], 'Manufacturer', 'Unknown');
    $partNo = getPropVal($fpdata['p'], 'PartNumber', 'Unknown');
    $serialNo = getPropVal($fpdata['p'], 'SerialNumber', 'Unknown');
    $size = getPropVal($fpdata['p'], 'Size', '0');
    $speed = getPropVal($fpdata['p'], 'Speed', '0');

    $assetTagField = drawSimpleInput('string', $path, 'AssetTag', 'asset_tag', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $assetTag);
    $bankLocatorField = drawSimpleInput('string', $path, 'BankLocator', 'bank_locator', '', '', '', false, $fpdata['type'].'-name', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
    $deviceLocatorField = drawSimpleInput('string', $path, 'DeviceLocator', 'dev_locator', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $deviceLocator);
    $manufacturerField = drawComboboxSimpleInput('string', $path, 'Manufacturer', 'manufacturer', $manufCombo, '', '', '', '', '', false, PHP_INT_MIN, PHP_INT_MIN, $manufacturer);
    $partNoField = drawSimpleInput('string', $path, 'PartNumber', 'part_no', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $partNo);
    $serialNoField = drawSimpleInput('string', $path, 'SerialNumber', 'serial_numb', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $serialNo);
    $sizeField = drawSimpleInput('integer', $path, 'Size', 'size', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $size);
    $speedField = drawSimpleSelect($path, 'Speed', $ramSpeedOpts, 'speed', '', '', '', '', false, false, $speed);

    return "<div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-4\">{$assetTagField}</div>
                <div class=\"col-12 col-md-6 col-lg-4\">{$bankLocatorField}</div>
                <div class=\"col-12 col-md-6 col-lg-4\">{$deviceLocatorField}</div>
                <div class=\"col-12 col-md-6 col-lg-4\">{$manufacturerField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-4\">{$partNoField}</div>
                <div class=\"col-12 col-md-6 col-lg-4\">{$serialNoField}</div>
                <div class=\"col-12 col-md-6 col-lg-4\">{$sizeField}</div>
                <div class=\"col-12 col-md-6 col-lg-4\">{$speedField}</div>
            </div>";
}

function drawOCDriversProps(array $fpdata, int $idx): string {
    global $ocDriversOpts;

    $path = $fpdata['path'].'/'.$idx;

    $args = getPropVal($fpdata['p'], 'Arguments', '');

    $pathField = drawSimpleInput('string', $path, 'Path', 'path', '', '', '', false, '', 'data-triggerEv="itmLiveTitle"', false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
    $argsField = drawSimpleTextArea('string', $path, 'Arguments', 'arguments', 2, '', '', '', '', false, $args);
    $options = drawOpts($fpdata, $idx, $ocDriversOpts);

    return "{$options}
            <div class=\"row\">
                <div class=\"col-12\">{$pathField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$argsField}</div>
            </div>";
}