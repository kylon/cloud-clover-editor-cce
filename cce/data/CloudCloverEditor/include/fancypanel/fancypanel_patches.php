<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/../data/kernel_kext_patches.php';
require_once __DIR__.'/components.php';

/**
 * Draw custom patches elements (icon and modal)
 *
 * @param array $fpdata
 * @param string $titleField
 * @param string $hideCls
 * @param int $idx
 * @param bool $sortable
 * @param int|string $fallbackValue
 *
 * @return string
 */
function drawPatchElems(array &$fpdata, string $titleField, string $hideCls, int $idx, bool $sortable, $fallbackValue = 'new'): string {
    $html = '';

    foreach ($fpdata['pdata'] as $p) {
        $liveTitleField = trimSN(getPropVal($p, $titleField, $fallbackValue));
        $fpdata['titleField'] = $liveTitleField;
        $fpdata['p'] = &$p;

        $icon = drawIcon($fpdata, $idx, $liveTitleField, 'cog3', $sortable);
        $modal = '';

        switch ($fpdata['type']) {
            case 'kextP':
            case 'kernelP':
            case 'bootefiP':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawCloverPatchModal');
                break;
            case 'ozkextP':
            case 'ozkernelP':
            case 'ozBtrP':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOzmosisPatchModal');
                break;
            case 'ocAcpita':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOpenCoreACPIPatchModal');
                break;
            case 'chamkernelP':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawChamKernPatchModal');
                break;
            case 'dsdtP':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawCloverDSDTPatchModal');
                break;
            case 'ozAcpiP':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOzmosisACPIPatchModal');
                break;
            case 'vbiosP':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawCloverVBIOSPatchModal');
                break;
            case 'ocBtrP':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOpenCoreBooterPatchModal');
                break;
            case 'ocKernAdd':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOpenCoreKernAddPropModal');
                break;
            case 'ocKernBlock':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOpenCoreKernBlockPropModal');
                break;
            case 'ocForceProps':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOpenCoreForcePropModal');
                break;
            case 'ocKernPatch':
                $modal = drawModalBodySkel($fpdata, $idx, $liveTitleField, 'drawOpenCoreKernPatchModal');
                break;
            default:
                break;
        }

        $html .= "<div class=\"single-item-group{$hideCls}\">{$icon}{$modal}</div>";

        ++$idx;
        $hideCls = '';
    }

    return $html;
}

function drawOpenCoreACPIPatchModal(array $fpdata, int $idx): string {
    global $acpiPOpts;

    $path = $fpdata['path'].'/'.$idx;
    $oemTid = getPropVal($fpdata['p'], 'OemTableId');
    $base = getPropVal($fpdata['p'], 'Base');
    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $maskFind = trimSN(getPropVal($fpdata['p'], 'Mask'));
    $maskReplace = trimSN(getPropVal($fpdata['p'], 'ReplaceMask'));
    $count = getPropVal($fpdata['p'], 'Count');
    $limit = getPropVal($fpdata['p'], 'Limit');
    $skip = getPropVal($fpdata['p'], 'Skip');
    $baseSkip = getPropVal($fpdata['p'], 'BaseSkip');
    $tableSign = getPropVal($fpdata['p'], 'TableSignature');
    $tableLen = getPropVal($fpdata['p'], 'TableLength');

    $oemTidField = drawSimpleInput('data', $path, 'OemTableId', 'oem_table_id', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $oemTid);
    $baseField = drawSimpleInput('string', $path, 'Base', 'base', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $base);
    $findField = drawSimpleTextArea('data', $path, 'Find', 'find', 4, '', '', '', '', false, $find);
    $replField = drawSimpleTextArea('data', $path, 'Replace', 'replace', 4, '', '', '', '', false, $replace);
    $maskFField = drawSimpleTextArea('data', $path, 'Mask', 'find_mask', 4, '', '', '', '', false, $maskFind);
    $maskRField = drawSimpleTextArea('data', $path, 'ReplaceMask', 'replace_mask', 4, '', '', '', '', false, $maskReplace);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", '', '', '', false, $fpdata['titleField']);
    $countField = drawSimpleInput('integer', $path, 'Count', 'count', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $count);
    $limitField = drawSimpleInput('integer', $path, 'Limit', 'limit', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $limit);
    $skipField = drawSimpleInput('integer', $path, 'Skip', 'skip', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $skip);
    $baseSkipField = drawSimpleInput('integer', $path, 'BaseSkip', 'base_skip', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $baseSkip);
    $tableSignField = drawSimpleInput('data', $path, 'TableSignature', 'table_signature', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $tableSign);
    $tableLenField = drawSimpleInput('integer', $path, 'TableLength', 'table_length', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $tableLen);
    $options = drawOpts($fpdata, $idx, $acpiPOpts);

    return "<div class=\"row mb-2\">
                <!--col-md-6 here-->
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-6\">{$oemTidField}</div>
                <div class=\"col-12 col-md-6\">{$baseField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$maskFField}</div>
                <div class=\"col-12 col-lg-6\">{$maskRField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-2\">{$countField}</div>
                <div class=\"col-12 col-md-6 col-lg-2\">{$limitField}</div>
                <div class=\"col-12 col-md-6 col-lg-2\">{$skipField}</div>
                <div class=\"col-12 col-md-6 col-lg-3\">{$baseSkipField}</div>
                <div class=\"col-12 col-md-6 col-lg-3\">{$tableLenField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-4\">{$tableSignField}</div>
                <!--col-lg-8 here-->
            </div>";
}

function drawOpenCoreBooterPatchModal(array $fpdata, int $idx): string {
    global $ocKAddArchOpts, $ocKernAddOpts;

    $path = $fpdata['path'].'/'.$idx;
    $arch = getPropVal($fpdata['p'], 'Arch');
    $identifier = trimSN(getPropVal($fpdata['p'], 'Identifier'));
    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $maskFind = trimSN(getPropVal($fpdata['p'], 'Mask'));
    $maskReplace = trimSN(getPropVal($fpdata['p'], 'ReplaceMask'));
    $count = getPropVal($fpdata['p'], 'Count');
    $limit = getPropVal($fpdata['p'], 'Limit');
    $skip = getPropVal($fpdata['p'], 'Skip');

    $archField = drawSimpleSelect($path, 'Arch', $ocKAddArchOpts, 'arch', 'Any', '', '', '', false, false, $arch);
    $identifierField = drawSimpleInput('string', $path, 'Identifier', 'identifier', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $identifier);
    $findField = drawSimpleTextArea('data', $path, 'Find', 'find', 4, '', '', '', '', false, $find);
    $replField = drawSimpleTextArea('data', $path, 'Replace', 'replace', 4, '', '', '', '', false, $replace);
    $maskFField = drawSimpleTextArea('data', $path, 'Mask', 'find_mask', 4, '', '', '', '', false, $maskFind);
    $maskRField = drawSimpleTextArea('data', $path, 'ReplaceMask', 'replace_mask', 4, '', '', '', '', false, $maskReplace);
    $countField = drawSimpleInput('integer', $path, 'Count', 'count', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $count);
    $limitField = drawSimpleInput('integer', $path, 'Limit', 'limit', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $limit);
    $skipField = drawSimpleInput('integer', $path, 'Skip', 'skip', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $skip);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", '', '', '', false, $fpdata['titleField']);
    $options = drawOpts($fpdata, $idx, $ocKernAddOpts);

    return "<div class=\"row mb-2\">
                <div class=\"col-12 col-lg-6\">{$archField}</div>
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$maskFField}</div>
                <div class=\"col-12 col-lg-6\">{$maskRField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-xl-6\">{$identifierField}</div>
                <div class=\"col-12 col-md-6 col-lg-2\">{$countField}</div>
                <div class=\"col-12 col-md-6 col-lg-2\">{$limitField}</div>
                <div class=\"col-12 col-md-6 col-lg-2\">{$skipField}</div>
            </div>";
}

function drawOpenCoreKernAddPropModal(array $fpdata, int $idx): string {
    global $ocKAddArchOpts, $ocKernAddOpts;

    $path = $fpdata['path'].'/'.$idx;
    $comment = trimSN(getPropVal($fpdata['p'], 'Comment'));
    $arch = getPropVal($fpdata['p'], 'Arch');
    $exePath = getPropVal($fpdata['p'], 'ExecutablePath');
    $maxKern = getPropVal($fpdata['p'], 'MaxKernel');
    $minKern = getPropVal($fpdata['p'], 'MinKernel');
    $plistPath = getPropVal($fpdata['p'], 'PlistPath');

    $archField = drawSimpleSelect($path, 'Arch', $ocKAddArchOpts, 'arch', 'Any', '', '', '', false, false, $arch);
    $bundleField = drawSimpleInput('string', $path, 'BundlePath', 'bundle_path', '', '', '', false, '', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
    $exePathField = drawSimpleInput('string', $path, 'ExecutablePath', 'exe_path', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $exePath);
    $minKernField = drawSimpleInput('string', $path, 'MinKernel', 'min_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $minKern);
    $maxKernField = drawSimpleInput('string', $path, 'MaxKernel', 'max_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $maxKern);
    $plistPField = drawSimpleInput('string', $path, 'PlistPath', 'plist_path', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $plistPath);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, '', '', '', '', false, $comment);
    $options = drawOpts($fpdata, $idx, $ocKernAddOpts);

    return "<div class=\"row mb-2\">
                <div class=\"col-12 col-lg-6\">{$archField}</div>
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-xl-6\">{$bundleField}</div>
                <div class=\"col-12 col-xl-6\">{$exePathField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$minKernField}</div>
                <div class=\"col-12 col-lg-6\">{$maxKernField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$plistPField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>";
}

function drawOpenCoreKernBlockPropModal(array $fpdata, int $idx): string {
    global $ocKAddArchOpts, $ocKernAddOpts, $ocKBlkStrtgyOpts;

    $path = $fpdata['path'].'/'.$idx;
    $comment = trimSN(getPropVal($fpdata['p'], 'Comment'));
    $arch = getPropVal($fpdata['p'], 'Arch');
    $maxKern = getPropVal($fpdata['p'], 'MaxKernel');
    $minKern = getPropVal($fpdata['p'], 'MinKernel');
    $strategy = getPropVal($fpdata['p'], 'Strategy');

    $archField = drawSimpleSelect($path, 'Arch', $ocKAddArchOpts, 'arch', 'Any', '', '', '', false, false, $arch);
    $minKernField = drawSimpleInput('string', $path, 'MinKernel', 'min_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $minKern);
    $maxKernField = drawSimpleInput('string', $path, 'MaxKernel', 'max_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $maxKern);
    $strategyField = drawSimpleSelect($path, 'Strategy', $ocKBlkStrtgyOpts, 'strategy', 'Disable', '', '', '', false, false, $strategy);
    $identifierField = drawSimpleInput('string', $path, 'Identifier', 'identifier', '', '', '', false, '', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, '', '', '', '', false, $comment);
    $options = drawOpts($fpdata, $idx, $ocKernAddOpts);

    return "<div class=\"row mb-2\">
                <div class=\"col-12 col-lg-3\">{$archField}</div>
                <div class=\"col-12 col-lg-3\">{$strategyField}</div>
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$minKernField}</div>
                <div class=\"col-12 col-lg-6\">{$maxKernField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$identifierField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>";
}

function drawOpenCoreForcePropModal(array $fpdata, int $idx): string {
    global $ocKAddArchOpts, $ocKernAddOpts;

    $path = $fpdata['path'].'/'.$idx;
    $arch = getPropVal($fpdata['p'], 'Arch');
    $comment = trimSN(getPropVal($fpdata['p'], 'Comment'));
    $bundlePath = getPropVal($fpdata['p'], 'BundlePath');
    $exePath = getPropVal($fpdata['p'], 'ExecutablePath');
    $maxKern = getPropVal($fpdata['p'], 'MaxKernel');
    $minKern = getPropVal($fpdata['p'], 'MinKernel');
    $plistPath = getPropVal($fpdata['p'], 'PlistPath');

    $archField = drawSimpleSelect($path, 'Arch', $ocKAddArchOpts, 'arch', 'Any', '', '', '', false, false, $arch);
    $bundleField = drawSimpleInput('string', $path, 'BundlePath', 'bundle_path', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $bundlePath);
    $exePathField = drawSimpleInput('string', $path, 'ExecutablePath', 'exe_path', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $exePath);
    $minKernField = drawSimpleInput('string', $path, 'MinKernel', 'min_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $minKern);
    $maxKernField = drawSimpleInput('string', $path, 'MaxKernel', 'max_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $maxKern);
    $plistPField = drawSimpleInput('string', $path, 'PlistPath', 'plist_path', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $plistPath);
    $identifierField = drawSimpleInput('string', $path, 'Identifier', 'identifier', '', '', '', false, '', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, '', '', '', '', false, $comment);
    $options = drawOpts($fpdata, $idx, $ocKernAddOpts);

    return "<div class=\"row mb-2\">
                <div class=\"col-12 col-lg-6\">{$archField}</div>
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$bundleField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$exePathField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$identifierField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$plistPField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$minKernField}</div>
                <div class=\"col-12 col-lg-6\">{$maxKernField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>";
}

function drawOpenCoreKernPatchModal(array $fpdata, int $idx): string {
    global $ocKAddArchOpts, $ocKernAddOpts;

    $path = $fpdata['path'].'/'.$idx;
    $arch = getPropVal($fpdata['p'], 'Arch');
    $base = getPropVal($fpdata['p'], 'Base');
    $count = getPropVal($fpdata['p'], 'Count', 0);
    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $limit = getPropVal($fpdata['p'], 'Limit', 0);
    $mask = getPropVal($fpdata['p'], 'Mask');
    $maxKern = getPropVal($fpdata['p'], 'MaxKernel');
    $minKern = getPropVal($fpdata['p'], 'MinKernel');
    $repl = getPropVal($fpdata['p'], 'Replace');
    $replMask = getPropVal($fpdata['p'], 'ReplaceMask');
    $skip = getPropVal($fpdata['p'], 'Skip', 0);

    $archField = drawSimpleSelect($path, 'Arch', $ocKAddArchOpts, 'arch', 'Any', '', '', '', false, false, $arch);
    $baseField = drawSimpleInput('string', $path, 'Base', 'base', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $base);
    $countField = drawSimpleInput('integer', $path, 'Count', 'count', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $count);
    $findField = drawSimpleTextArea('data', $path, 'Find', 'find', 4, '', '', '', '', false, $find);
    $identifierField = drawSimpleInput('string', $path, 'Identifier', 'identifier', '', '', '', false, '', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
    $limitField = drawSimpleInput('integer', $path, 'Limit', 'limit', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $limit);
    $maskField = drawSimpleTextArea('data', $path, 'Mask', 'mask', 4, '', '', '', '', false, $mask);
    $minKernField = drawSimpleInput('string', $path, 'MinKernel', 'min_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $minKern);
    $maxKernField = drawSimpleInput('string', $path, 'MaxKernel', 'max_kernel', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $maxKern);
    $replField = drawSimpleTextArea('data', $path, 'Replace', 'replace', 4, '', '', '', '', false, $repl);
    $replMaskField = drawSimpleTextArea('data', $path, 'ReplaceMask', 'replace_mask', 4, '', '', '', '', false, $replMask);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", '', '', '', false, $fpdata['titleField']);
    $skipField = drawSimpleInput('integer', $path, 'Skip', 'skip', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $skip);
    $options = drawOpts($fpdata, $idx, $ocKernAddOpts);

    return "<div class=\"row mb-2\">
                <div class=\"col-12 col-lg-6\">{$archField}</div>
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$baseField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$identifierField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$maskField}</div>
                <div class=\"col-12 col-lg-6\">{$replMaskField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$minKernField}</div>
                <div class=\"col-12 col-lg-6\">{$maxKernField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-4 col-lg-2\">{$countField}</div>
                <div class=\"col-12 col-md-4 col-lg-2\">{$limitField}</div>
                <div class=\"col-12 col-md-4 col-lg-2\">{$skipField}</div>
            </div>";
}

function drawChamKernPatchModal(array $fpdata, int $idx): string {
    $path = $fpdata['path'].'/'.$idx;
    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $findT = getValType($fpdata['p'], 'Find');
    $replT = getValType($fpdata['p'], 'Replace');

    $findField = drawComboselectSimpleTextArea($path, 'Find', 'find', 'string;data', $findT, 4, '', '', '', '', false, $find);
    $replField = drawComboselectSimpleTextArea($path, 'Replace', 'replace', 'string;data', $replT, 4, '', '', '', '', false, $replace);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", '', '', '', false, $fpdata['titleField']);
    $matchOSFields = drawMatchOS($fpdata, $idx);

    return "<div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$matchOSFields}</div>
            </div>";
}

// special case, handled in chameleon kernel_and_kext_patches.php
function drawChamKextPatchModal(array $fpdata, int $idx): string {
    $path = $fpdata['path'].'/'.$idx;
    $fpdata['p'] = '';

    $findField = drawComboselectSimpleTextArea($path, 'Find', 'find', 'string;data', '', 4, '', '', '', '', false, '');
    $replField = drawComboselectSimpleTextArea($path, 'Replace', 'replace', 'string;data', '', 4, '', '', '', '', false, '');
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"chmKxCmm\"", '', '', '', false, '');
    $matchOSFields = drawMatchOS($fpdata, $idx);

    return "<div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$matchOSFields}</div>
            </div>";
}

function drawCloverPatchModal(array $fpdata, int $idx): string {
    global $kextPopt, $kernPopt, $bootEfiOpt;

    $path = $fpdata['path'].'/'.$idx;
    $procedureField = '';
    $nameField = '';
    $options = '';
    $commtEv = '';

    if ($fpdata['type'] === 'kextP') {
        $inpt = drawSimpleInput('string', $path, 'Name', 'name', '', '', '', false, $fpdata['type'].'-name', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
        $nameField = "<div class=\"col-12 col-lg-6\">{$inpt}</div>";
        $options = drawOpts($fpdata, $idx, $kextPopt);

    } else if ($fpdata['type'] === 'kernelP') {
        $procedure = getPropVal($fpdata['p'], 'Procedure');
        $inpt = drawSimpleInput('string', $path, 'Procedure', 'procedure', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $procedure);
        $procedureField = "<div class=\"col-12 col-md-6 col-lg-5\">{$inpt}</div>";
        $options = drawOpts($fpdata, $idx, $kernPopt);
        $commtEv = ' data-triggerEv="itmLiveTitle"';

    } else { // booter
        $options = drawOpts($fpdata, $idx, $bootEfiOpt);
        $commtEv = ' data-triggerEv="itmLiveTitle"';
    }

    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $startPattrn = trimSN(getPropVal($fpdata['p'], 'StartPattern'));
    $maskStart = trimSN(getPropVal($fpdata['p'], 'MaskStart'));
    $maskFind = trimSN(getPropVal($fpdata['p'], 'MaskFind'));
    $maskReplace = trimSN(getPropVal($fpdata['p'], 'MaskReplace'));
    $count = getPropVal($fpdata['p'], 'Count');
    $skip = getPropVal($fpdata['p'], 'Skip');
    $range = getPropVal($fpdata['p'], 'RangeFind');
    $findT = getValType($fpdata['p'], 'Find');
    $replT = getValType($fpdata['p'], 'Replace');
    $startPattrnT = getValType($fpdata['p'], 'StartPattern');
    $maskStartT = getValType($fpdata['p'], 'MaskStart');
    $maskFindT = getValType($fpdata['p'], 'MaskFind');
    $maskReplT = getValType($fpdata['p'], 'MaskReplace');

    $findField = drawComboselectSimpleTextArea($path, 'Find', 'find', 'string;data', $findT, 4, '', '', '', '', false, $find);
    $replField = drawComboselectSimpleTextArea($path, 'Replace', 'replace', 'string;data', $replT, 4, '', '', '', '', false, $replace);
    $startPField = drawComboselectSimpleTextArea($path, 'StartPattern', 'start_pattern', 'string;data', $startPattrnT, 4, '', '', '', '', false, $startPattrn);
    $maskPField = drawComboselectSimpleTextArea($path, 'MaskStart', 'mask_start', 'string;data', $maskStartT, 4, '', '', '', '', false, $maskStart);
    $maskFField = drawComboselectSimpleTextArea($path, 'MaskFind', 'find_mask', 'string;data', $maskFindT, 4, '', '', '', '', false, $maskFind);
    $maskRField = drawComboselectSimpleTextArea($path, 'MaskReplace', 'replace_mask', 'string;data', $maskReplT, 4, '', '', '', '', false, $maskReplace);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\"{$commtEv}", '', '', '', false, $fpdata['titleField']);
    $countField = drawSimpleInput('integer', $path, 'Count', 'count', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $count);
    $skipField = drawSimpleInput('integer', $path, 'Skip', 'skip', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $skip);
    $rangeField = drawSimpleInput('integer', $path, 'RangeFind', 'range_find', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $range);
    $matchOSFields = drawMatchOS($fpdata, $idx);

    return "<div class=\"row mb-2\">
                {$nameField}
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$startPField}</div>
                <div class=\"col-12 col-lg-6\">{$maskPField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$maskFField}</div>
                <div class=\"col-12 col-lg-6\">{$maskRField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$matchOSFields}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-2\">{$countField}</div>
                <div class=\"col-12 col-md-6 col-lg-2\">{$skipField}</div>
                <div class=\"col-12 col-md-6 col-lg-3\">{$rangeField}</div>
                {$procedureField}
            </div>";
}

function drawCloverDSDTPatchModal(array $fpdata, int $idx): string {
    $path = $fpdata['path'].'/'.$idx;
    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $tgt = getPropVal($fpdata['p'], 'TgtBridge');
    $findT = getValType($fpdata['p'], 'Find');
    $replT = getValType($fpdata['p'], 'Replace');
    $tgtT = getValType($fpdata['p'], 'TgtBridge');
    $opts = drawOpts($fpdata, $idx, ['dsdt_disabled' => 'Disabled']);

    $findField = drawComboselectSimpleTextArea($path, 'Find', 'find', 'string;data', $findT, 4, '', '', '', '', false, $find);
    $replField = drawComboselectSimpleTextArea($path, 'Replace', 'replace', 'string;data', $replT, 4, '', '', '', '', false, $replace);
    $tgtField = drawComboselectSimpleTextArea($path, 'TgtBridge', 'tgt_bridge', 'string;data', $tgtT, 4, '', '', '', '', false, $tgt);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", '', '', '', false, $fpdata['titleField']);

    return "<div class=\"row mb-2\">
                <div class=\"col-12 col-md-6\">{$tgtField}</div>
                {$opts}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>";
}

function drawCloverVBIOSPatchModal(array $fpdata, int $idx): string {
    $path = $fpdata['path'].'/'.$idx;
    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $findT = getValType($fpdata['p'], 'Find');
    $replT = getValType($fpdata['p'], 'Replace');

    $findField = drawComboselectSimpleTextArea($path, 'Find', 'find', 'string;data', $findT, 4, '', '', '', '', false, $find);
    $replField = drawComboselectSimpleTextArea($path, 'Replace', 'replace', 'string;data', $replT, 4, '', '', '', '', false, $replace);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", '', '', '', false, $fpdata['titleField']);

    return "<div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>";
}

function drawOzmosisACPIPatchModal(array $fpdata, int $idx): string {
    $path = $fpdata['path'].'/'.$idx;
    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $findT = getValType($fpdata['p'], 'Find');
    $replT = getValType($fpdata['p'], 'Replace');
    $opts = drawOpts($fpdata, $idx, ['ozac_disabled' => 'Disabled']);

    $findField = drawComboselectSimpleTextArea($path, 'Find', 'find', 'string;data', $findT, 4, '', '', '', '', false, $find);
    $replField = drawComboselectSimpleTextArea($path, 'Replace', 'replace', 'string;data', $replT, 4, '', '', '', '', false, $replace);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", '', '', '', false, $fpdata['titleField']);

    return "<div class=\"row mb-2\">
                {$opts}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>";
}

function drawOzmosisPatchModal(array $fpdata, int $idx): string {
    global $kextPopt, $kernPopt;

    $path = $fpdata['path'].'/'.$idx;
    $nameField = '';
    $options = '';
    $commtEv = '';

    if ($fpdata['type'] === 'ozkextP') {
        $inpt = drawSimpleInput('string', $path, 'Name', 'name', '', '', '', false, $fpdata['type'].'-name', "data-ptype=\"{$fpdata['type']}\" data-triggerEv=\"itmLiveTitle\"", false, PHP_INT_MIN, PHP_INT_MIN, $fpdata['titleField']);
        $nameField = "<div class=\"col-12 col-lg-6\">{$inpt}</div>";
        $options = drawOpts($fpdata, $idx, $kextPopt);

    } else if ($fpdata['type'] === 'ozkernelP') {
        $options = drawOpts($fpdata, $idx, $kernPopt);
        $commtEv = ' data-triggerEv="itmLiveTitle"';

    } else { // booter
        $options = drawOpts($fpdata, $idx, $kernPopt);
        $commtEv = ' data-triggerEv="itmLiveTitle"';
    }

    $find = trimSN(getPropVal($fpdata['p'], 'Find'));
    $replace = trimSN(getPropVal($fpdata['p'], 'Replace'));
    $count = getPropVal($fpdata['p'], 'Count');
    $wildcard = getPropVal($fpdata['p'], 'Wildcard');
    $findT = getValType($fpdata['p'], 'Find');
    $replT = getValType($fpdata['p'], 'Replace');

    $findField = drawComboselectSimpleTextArea($path, 'Find', 'find', 'string;data', $findT, 4, '', '', '', '', false, $find);
    $replField = drawComboselectSimpleTextArea($path, 'Replace', 'replace', 'string;data', $replT, 4, '', '', '', '', false, $replace);
    $commtField = drawSimpleTextArea('string', $path, 'Comment', 'comment', 4, "data-ptype=\"{$fpdata['type']}\"{$commtEv}", '', '', '', false, $fpdata['titleField']);
    $countField = drawSimpleInput('integer', $path, 'Count', 'count', '', '', '', true, '', '', false, 0, PHP_INT_MIN, $count);
    $wildcardField = drawSimpleInput('string', $path, 'Wildcard', 'Wildcard', '', '', '', false, '', '', false, PHP_INT_MIN, PHP_INT_MIN, $wildcard);
    $matchOSFields = drawMatchOS($fpdata, $idx);

    return "<div class=\"row mb-2\">
                {$nameField}
                {$options}
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-lg-6\">{$findField}</div>
                <div class=\"col-12 col-lg-6\">{$replField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$commtField}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12\">{$matchOSFields}</div>
            </div>
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-2\">{$countField}</div>
                <div class=\"col-12 col-md-6 col-lg-5\">{$wildcardField}</div>
                <!-- col-lg-5 here -->
            </div>";
}