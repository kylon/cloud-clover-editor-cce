<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function draw_smbios_select(string $selected, bool $disabled = false): string {
    global $text;

    $disCls = $disabled ? ' disabled':'';
    $updBtnDis = $disabled ? ' disabled':'';
    
    return "<div class=\"row mb-5\">
                <div class=\"col-12 col-lg-8 offset-lg-2 text-center\">
                    <label class=\"form-label\" for=\"smsel\">{$text['sel_model']}</label>
                    <div class=\"input-group\">
                        <button class=\"btn btn-secondary trigUpdSmbios\" type=\"button\" title=\"Update\"{$updBtnDis}><i class=\"fa icon-download3\"></i></button>
                        <select id=\"smsel\" class=\"form-select cce-sel\" data-sel=\"smbios-sel\" data-usrv=\"{$selected}\" aria-label=\"SMBIOS selection\"{$disCls}>
                            <option value=\"\"></option>
                        </select>
                        <button class=\"btn btn-secondary\" type=\"button\" data-bs-toggle=\"modal\" data-bs-target=\"#smbiosmore\">{$text['details']}</button>
                    </div>
                </div>
            </div>";
}

function draw_smbios_more_modal(): string {
    global $text;

    return "<!-- SMBIOS MORE INFO MODAL -->
            <div class=\"modal fade\" id=\"smbiosmore\" tabindex=\"-1\" role=\"dialog\">
                <div class=\"modal-dialog modal-lg\" role=\"document\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <h5 class=\"modal-title\">N/A</h5>
                            <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
                        </div>
                        <div class=\"modal-body smbios-info\">
                            <div class=\"row\">
                                <div class=\"col-12 col-lg-6\">
                                    <p><i class=\"fa icon-cpu me-2 font-xlarge\"></i> <span class=\"font-large\">CPU</span></p>
                                    <p class=\"cpu text-center font-italic\">N/A</p>
        
                                    <p><i class=\"fa icon-microchip me-2 font-xlarge\"></i> <span class=\"font-large\">RAM</span></p>
                                    <p class=\"ram text-center font-italic\">N/A</p>
        
                                    <p><i class=\"fa icon-cpu1 me-2 font-xlarge\"></i> <span class=\"font-large\">GPU</span></p>
                                    <p class=\"gpu text-center font-italic\">N/A</p>
        
                                    <p><i class=\"fa icon-info3 me-2 font-xlarge\"></i> <span class=\"font-large\">SMC Revision</span></p>
                                    <p class=\"smcr text-center font-italic\">N/A</p>
        
                                    <p><i class=\"fa icon-info3 me-2 font-xlarge\"></i> <span class=\"font-large\">Model Code</span></p>
                                    <p class=\"mid text-center font-italic\">N/A</p>
        
                                    <p><i class=\"fa icon-info3 me-2 font-xlarge\"></i> <span class=\"font-large\">{$text['year']}</span></p>
                                    <p class=\"yr text-center font-italic\">N/A</p>
        
                                    <p><i class=\"fa icon-info3 me-2 font-xlarge\"></i> <span class=\"font-large\">{$text['min_osv']}</span></p>
                                    <p class=\"minosv text-center font-italic\">N/A</p>
        
                                    <p><i class=\"fa icon-info3 me-2 font-xlarge\"></i> <span class=\"font-large\">{$text['max_osv']}</span></p>
                                    <p class=\"maxosv text-center font-italic\">N/A</p>
                                </div>
                                <div class=\"col-12 col-lg-6 text-center\">
                                    <i class=\"fa icon-question2 smbiosmodel-icon\"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";
}