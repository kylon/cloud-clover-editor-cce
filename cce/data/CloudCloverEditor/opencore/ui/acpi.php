<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

require_once __DIR__.'/../../include/fancypanel/fancypanel_main.php';

function acpi(): string {
    try {
        global $text, $config, $acpiQ;

        $addAcpiTable = drawPatchTable('octba', ['comment', 'path', 'enabled'], $config->getRawVals('ACPI/Add'), ['cp']);
        $delAcpiTable = drawPatchTable('octbb', ['all', 'comment', 'enabled', 'oem_table_id', 'table_length', 'table_signature'], $config->getRawVals('ACPI/Delete'), ['cp']);
        $acpiPatchPanel = draw_fancy_panel($config->getVals('ACPI/Patch'), 'ocAcpita', 'ACPI/Patch', 'acpi_patches');
        $acpiQuirks = drawSimpleInlineCheckOpts($acpiQ, 'ACPI/Quirks');

        return "<div class=\"row\"><div class=\"col-12 title\">ACPI</div></div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['add_tables']}</div>
                </div>
            
                {$addAcpiTable}
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['block_tables']}</div>
                </div>
            
                {$delAcpiTable}
                {$acpiPatchPanel}
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['quirks']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$acpiQuirks}</div>
                </div>";
    } catch (\Throwable) {}

    return "";
}
