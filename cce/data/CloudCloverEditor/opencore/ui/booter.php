<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

function booter(): string {
    try {
        global $text, $config, $btrQ;

        $resizeBarCombo = '[-1] Disabled:-1;[0] 1MB:0';

        $mmioWLTable = drawPatchTable('mmioWP', ['address', 'comment', 'enabled'], $config->getRawVals('Booter/MmioWhitelist'), ['cp']);
        $btrPathPanel = draw_fancy_panel($config->getRawVals('Booter/Patch'), 'ocBtrP', 'Booter/Patch', 'booter_patch');
        $btrQuirks = drawSimpleInlineCheckOpts($btrQ, 'Booter/Quirks');
        $maxSlideInpt = drawSimpleInput('integer', 'Booter', 'ProvideMaxSlide', 'provide_max_slide', 0, 'maxslide', '', true, '', '', false, 0);
        $quirkResizeAppleGpuBarsInpt = drawComboboxSimpleInput('integer', 'Booter/Quirks', 'ResizeAppleGpuBars', 'resize_apple_gpub', $resizeBarCombo, -1);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['booter']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">
                        {$text['mmio_whitelist']}
                    </div>
                </div>
            
                {$mmioWLTable}
                {$btrPathPanel}

                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['quirks']}</div>
                </div>

                <div class=\"row\">
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$maxSlideInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$quirkResizeAppleGpuBarsInpt}</div>
                    <!-- col-lg-10 here -->
                </div>
                
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$btrQuirks}</div>
                </div>";
    } catch (\Throwable) {}

    return "";
}