<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

function device_properties(): string {
    try {
        global $text, $config;

        $props = $config->getVals('DeviceProperties/Add');
        $blockProp = $config->getRawVals('DeviceProperties/Delete');

        $addPropsMainTable = drawPatchTable('ocpPropM', ['device'], $props, ['cp']);
        $addPropsValsTable = drawPatchTable('ocpPropS', ['key', 'value', 'data_type'], $props);
        $delPropsMainTable = drawPatchTable('socbPropM', ['device'], $blockProp, ['cp']);
        $delPropsValsTable = drawPatchTable('socbPropS', ['variable'], $blockProp);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['devprop_title']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['add']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$addPropsMainTable}</div>
                    <div class=\"col-12 col-lg-6 mt-3 mt-lg-0\">{$addPropsValsTable}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['delete']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$delPropsMainTable}</div>
                    <div class=\"col-12 col-lg-6 mt-3 mt-lg-0\">{$delPropsValsTable}</div>
                </div>";
    } catch (\Throwable) {}

    return "";
}
