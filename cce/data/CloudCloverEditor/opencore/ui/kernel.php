<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

function kernel(): string {
    try {
        global $text, $config, $ocKernQuirks, $ocKSchemeArchOpts, $kernCache, $ocEmuPropOpts, $ocFuzzyMOpts;

        $kernAddPanel = draw_fancy_panel($config->getVals('Kernel/Add'), 'ocKernAdd', 'Kernel/Add', 'add', true);
        $kernBlockPanel = draw_fancy_panel($config->getVals('Kernel/Block'), 'ocKernBlock', 'Kernel/Block', 'block', false, false);
        $kernForcePanel = draw_fancy_panel($config->getVals('Kernel/Force'), 'ocForceProps', 'Kernel/Force', 'force_props', false, false);
        $kernPatchPanel = draw_fancy_panel($config->getVals('Kernel/Patch'), 'ocKernPatch', 'Kernel/Patch', 'patch');
        $kernEmulateOpts = drawSimpleInlineCheckOpts($ocEmuPropOpts, 'Kernel/Emulate');
        $kernEmulateMinKInpt = drawSimpleInput('string', 'Kernel/Emulate', 'MinKernel', 'min_kernel');
        $kernEmulateMaxKInpt = drawSimpleInput('string', 'Kernel/Emulate', 'MaxKernel', 'max_kernel');
        $cpuId1DataInpt = drawSimpleInput('data', 'Kernel/Emulate', 'Cpuid1Data', 'cpu_id1_data', '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00');
        $cpuId1MaskInpt = drawSimpleInput('data', 'Kernel/Emulate', 'Cpuid1Mask', 'cpu_id1_mask', '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00');
        $kernSchemeOpts = drawSimpleInlineCheckOpts($ocFuzzyMOpts, 'Kernel/Scheme');
        $kernArchInpt = drawSimpleSelect('Kernel/Scheme', 'KernelArch', $ocKSchemeArchOpts, 'kernel_arch');
        $kernCacheInpt = drawSimpleSelect('Kernel/Scheme', 'KernelCache', $kernCache, 'kernel_cache');
        $kernQuirks = drawSimpleInlineCheckOpts($ocKernQuirks, 'Kernel/Quirks');
        $apfsTrimTimeInpt = drawSimpleInput('integer', 'Kernel/Quirks', 'SetApfsTrimTimeout', 'apfs_trim_tmout', -1, '', '', true, '', '', false, -1);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['kernel']}</div></div>
            
                {$kernAddPanel}
                {$kernBlockPanel}
                {$kernForcePanel}
                {$kernPatchPanel}
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['emulate_props']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 col-md-6 col-lg-4 col-xl-3 mb-3\">{$kernEmulateOpts}</div>
                    <div class=\"col-12 col-md-3 col-lg-3 col-xl-2\">{$kernEmulateMinKInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-3 col-xl-2\">{$kernEmulateMaxKInpt}</div>
                    <!-- col-lg-2 here -->
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$cpuId1DataInpt}</div>
                    <div class=\"col-12 col-lg-6\">{$cpuId1MaskInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['scheme']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 col-md-4 col-lg-3\">{$kernSchemeOpts}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$kernArchInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$kernCacheInpt}</div>
                    <!-- col-lg-5 here -->
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['quirks']}</div>
                </div>
            
                <div class=\"row mt-3 mb-3\">
                    <div class=\"col-12\">{$kernQuirks}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-2\">{$apfsTrimTimeInpt}</div>
                    <!--col-lg-8 here-->
                </div>";
    } catch (\Throwable) {}

    return "";
}