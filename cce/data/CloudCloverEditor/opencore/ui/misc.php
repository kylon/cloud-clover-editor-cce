<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

function misc(): string {
    try {
        global $text, $config, $hbrntModes, $pikerModes, $pickerAttrsOpts, $miscBootOpts, $ocDebugOpts,
               $dbgTargetOpts, $securityOpts, $dmgLoadOpts, $vaultOpts, $scanPolicyOpts, $exposeSDataOpts,
               $launcherOpts, $secBootModelOpts, $displayLvlOpts, $ocSerialOpts, $ocSerialCustOpts;

        $consoleAttrsFgM = 0x0F;
        $consoleAttrsBgM = 0x70;
        $consoleAttrsFg = [
            'BLACK', 'BLUE', 'GREEN', 'CYAN', 'RED', 'MAGENTA', 'BROWN', 'LIGHTGRAY', 'DARKGRAY', 'LIGHTBLUE',
            'LIGHTGREEN', 'LIGHTCYAN', 'LIGHTRED', 'LIGHTMAGENTA', 'YELLOW', 'WHITE'
        ];
        $consoleAttrsBg = ['BLACK', 'BLUE', 'GREEN', 'CYAN', 'RED', 'MAGENTA', 'BROWN', 'LIGHTGRAY'];
        $pickerAttrsCombo = "0;";
        $pickerVariantCombo = "Auto;Default;Acidanthera\GoldenGate;Acidanthera\Syrah;Acidanthera\Chardonnay";
        $targetCombo = '0;';
        $displayLvlCombo = '0;2147483650;';
        $scanPolicyCombo = '17760515;';
        $exposeSDataCombo = '0;6;';
        $launcherPathCombo = 'Default';

        $hasPwd = getCheckAttr($config->getRawVals('Misc/Security/EnablePassword'));
        $consoleAttrs = $config->getRawVals('Misc/Boot/ConsoleAttributes') ?? 0;
        $pickerAttrsF = $config->getRawVals('Misc/Boot/PickerAttributes') ?? 0;
        $displayLvF = $config->getRawVals('Misc/Debug/DisplayLevel') ?? 0;
        $logModulesV = $config->getRawVals('Misc/Debug/LogModules') ?? '*';
        $targetF = $config->getRawVals('Misc/Debug/Target') ?? 0;
        $haltLevelF = $config->getRawVals('Misc/Security/HaltLevel') ?? 2147483648;
        $scanPolicyF = $config->getRawVals('Misc/Security/ScanPolicy') ?? 17760515;
        $exposeSDataF = $config->getRawVals('Misc/Security/ExposeSensitiveData') ?? 6;
        $serialOverride = getCheckAttr($config->getRawVals('Misc/Serial/Override'));

        $isPwdEnabled = $hasPwd === 'checked';

        $hibernateModeInpt = drawSimpleSelect('Misc/Boot', 'HibernateMode', $hbrntModes, 'hibernate_mode');
        $pikerAttrInpt = drawFlagInput($pickerAttrsF, 'Misc/Boot', 'PickerAttributes', 'ocpka', $pickerAttrsCombo, 'integer');
        $pikerAttrModal = drawSimpleFlagModal($pickerAttrsOpts, $pickerAttrsF, 'ocpka');
        $pikerModeInpt = drawSimpleSelect('Misc/Boot', 'PickerMode', $pikerModes, 'picker_mode');
        $pikerVariantInpt = drawComboboxSimpleInput('string', 'Misc/Boot', 'PickerVariant', 'picker_variant', $pickerVariantCombo,'Auto');
        $timeoutInpt = drawSimpleInput('integer', 'Misc/Boot', 'Timeout', 'timeout', 0, '', '', true);
        $takeOffDelayInpt = drawSimpleInput('integer', 'Misc/Boot', 'TakeoffDelay', 'takeoff_delay', 0, '', '', true);
        $launcherOptsInpt = drawSimpleSelect('Misc/Boot', 'LauncherOption', $launcherOpts, 'launcher_opt');
        $launcherPathInpt = drawComboboxSimpleInput('string', 'Misc/Boot', 'LauncherPath', 'launcher_path', $launcherPathCombo,'Default');
        $miscBootOptions = drawSimpleInlineCheckOpts($miscBootOpts, 'Misc/Boot');
        $blessOverrideTable = drawPatchTable('ocBlessOvr', ['path'], $config->getRawVals('Misc/BlessOverride'), ['cp']);
        $logModulesInpt = drawSimpleTextArea('string', 'Misc/Debug', 'LogModules', 'log_modules', -1, '', '', '', '', false, $logModulesV);
        $displayDelayInpt = drawSimpleInput('integer', 'Misc/Debug', 'DisplayDelay', 'display_delay', 0, '', '', true, '', '', false, 0);
        $displayLvlInpt = drawFlagInput($displayLvF, 'Misc/Debug', 'DisplayLevel', 'ocdlv', $displayLvlCombo, 'integer');
        $displayLvlModal = drawAdvFlagModal($displayLvF, $displayLvlOpts, 'ocdlv', 'ocdlv_modal_title');
        $targetInpt = drawFlagInput($targetF, 'Misc/Debug', 'Target', 'ocdbgt', $targetCombo, 'integer');
        $targetModal = drawSimpleFlagModal($dbgTargetOpts, $targetF, 'ocdbgt');
        $debugOptions = drawSimpleInlineCheckOpts($ocDebugOpts, 'Misc/Debug');
        $apEcidInpt = drawSimpleInput('integer', 'Misc/Security', 'ApECID', 'apecid', 0, '', '', true);
        $haltLvlInpt = drawFlagInput($haltLevelF, 'Misc/Security', 'HaltLevel', 'ochlv', $displayLvlCombo, 'integer');
        $haltLvlModal = drawAdvFlagModal($haltLevelF, $displayLvlOpts, 'ochlv', 'ochlv_modal_title');
        $scanPolicyInpt = drawFlagInput($scanPolicyF, 'Misc/Security', 'ScanPolicy', 'ocscanplf', $scanPolicyCombo, 'integer');
        $scanPolicyModal = drawAdvFlagModal($scanPolicyF, $scanPolicyOpts, 'ocscanplf', 'scan_policy_title');
        $dmgLoadingInpt = drawSimpleSelect('Misc/Security', 'DmgLoading', $dmgLoadOpts, 'dmg_loading');
        $vaultInpt = drawSimpleSelect('Misc/Security', 'Vault', $vaultOpts, 'vault');
        $exposeSentiveDataInpt = drawFlagInput($exposeSDataF, 'Misc/Security', 'ExposeSensitiveData', 'ocexsdf', $exposeSDataCombo, 'integer');
        $exposeSensitiveDataModal = drawSimpleFlagModal($exposeSDataOpts, $exposeSDataF, 'ocexsdf');
        $secBootModelInpt = drawSimpleSelect('Misc/Security', 'SecureBootModel', $secBootModelOpts, 'secure_boot_m');
        $enablePwdCheckbox = drawCheckbox('', 'Misc/Security', 'EnablePassword', $hasPwd, 'enablePassword', false, '', 'data-change="ocenablepwd"');
        $pwdHashInpt = drawSimpleInput('data', 'Misc/Security', 'PasswordHash', 'pwd_hash', 0, '', '', false, 'ocpwd', '', !$isPwdEnabled);
        $pwdSaltInpt = drawSimpleInput('data', 'Misc/Security', 'PasswordSalt', 'pwd_salt', '', '', '', false, 'ocpwd', '', !$isPwdEnabled);
        $securityOptions = drawSimpleInlineCheckOpts($securityOpts, 'Misc/Security');
        $entriesPanel = draw_fancy_panel($config->getRawVals('Misc/Entries'), 'ocEntry', 'Misc/Entries', 'entries_t', true);
        $toolsPanel = draw_fancy_panel($config->getRawVals('Misc/Tools'), 'ocTools', 'Misc/Tools', 'tools_t', true);
        $serialOptions = drawSimpleInlineCheckOpts($ocSerialOpts, 'Misc/Serial');
        $serialOverriveCheckbox = drawCheckbox('', 'Misc/Serial', 'Override', $serialOverride, 'serial_override');
        $serialBaudRateInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'BaudRate', 'ser_brate', 115200, '', '', true);
        $serialClockRateInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'ClockRate', 'ser_clk_rt', 1843200, '', '', true);
        $serialTxFifoSzInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'ExtendedTxFifoSize', 'ser_tz_sz', 64, '', '', true);
        $serialFifoCtrlInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'FifoControl', 'ser_fifo_ctrl', 7, '', '', true);
        $serialLineCtrlInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'LineControl', 'ser_line_ctrl', 7, '', '', true);
        $serialPciDevInpt = drawSimpleInput('data', 'Misc/Serial/Custom', 'PciDeviceInfo', 'ser_pci_dev', '0xFF');
        $serialRegWidthInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'RegisterAccessWidth', 'ser_reg_width', 8, '', '', true);
        $serialRegBaseInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'RegisterBase', 'ser_reg_base', 0x03F8, '', '', true);
        $serialRegStrideInpt = drawSimpleInput('integer', 'Misc/Serial/Custom', 'RegisterStride', 'ser_reg_stride', 1, '', '', true);
        $serialCustomOptions = drawSimpleInlineCheckOpts($ocSerialCustOpts, 'Misc/Serial/Custom');
        $consoleAttrFgOpts = '';
        $consoleAttrsBgOpts = '';

        for ($i=0; $i<$consoleAttrsFgM; ++$i)
            $consoleAttrFgOpts .= drawRadio('cattrsfg-flag', '', '', (string)$i, $consoleAttrsFg[$i], 'cattrsocfg', hasFlag($consoleAttrs, $i), false, true, 'Misc/Boot', 'ConsoleAttributes', 'occattr');

        for ($i=0, $j=0; $i<$consoleAttrsBgM; $i+=16, ++$j)
            $consoleAttrsBgOpts .= drawRadio('cattrsbg-flag', '', '', (string)$i, $consoleAttrsBg[$j], 'cattrsocbg', hasFlag($consoleAttrs, $i), false, true, 'Misc/Boot', 'ConsoleAttributes', 'occattr');

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['boot']}</div></div>

                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-3\">
                        <div class=\"text-center mb-3\">
                            <label class=\"form-label\" for=\"occat\">{$text['console_attrs']}</label>
                            <div class=\"input-group\">
                                <input id=\"occat\" type=\"text\" class=\"form-control\" readonly value=\"{$consoleAttrs}\">
                                <button class=\"btn btn-secondary\" type=\"button\" data-bs-toggle=\"modal\" data-bs-target=\".consattrs-modal\"><i class=\"fa icon-cog3\"></i></button>
                            </div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-md-4 col-lg-2\">{$hibernateModeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$pikerAttrInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$pikerModeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$pikerVariantInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-1\">{$timeoutInpt}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$takeOffDelayInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$launcherOptsInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$launcherPathInpt}</div>
                    <!-- col-lg-6here -->
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
        
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$miscBootOptions}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['bless_override']}</div>
                </div>
        
                {$blessOverrideTable}
        
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['debug']}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 col-lg-5\">{$logModulesInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$displayDelayInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-3\">{$displayLvlInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$targetInpt}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
        
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$debugOptions}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['security']}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 col-lg-3\">{$apEcidInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$haltLvlInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3 col-xl-2\">{$scanPolicyInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$dmgLoadingInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$vaultInpt}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-4 col-xl-3\">{$exposeSentiveDataInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$secBootModelInpt}</div>
                    <!-- col-lg-5 -->
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['password']}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 col-lg-2 mt-3\">{$enablePwdCheckbox}</div>
                    <div class=\"col-12 col-lg-6\">{$pwdHashInpt}</div>
                    <div class=\"col-12 col-lg-4\">{$pwdSaltInpt}</div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
        
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$securityOptions}</div>
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['serial_t']}</div>
                </div>
                
                <div class=\"row mb-2\">
                    <div class=\"col-12\">{$serialOptions}</div>
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12\">
                        <ul class=\"nav nav-tabs\" id=\"serialtab\" role=\"tablist\">
                            <li class=\"nav-item\" role=\"presentation\">
                                <a class=\"nav-link cce-nav-link\" id=\"serialovrtab\" role=\"tab\" aria-controls=\"serialcustom\" aria-selected=\"true\">
                                {$serialOverriveCheckbox}
                                </a>
                            </li>
                        </ul>
                        <div class=\"tab-content\" id=\"serialcustomcont\">
                            <div class=\"cce-tab-pane active\" id=\"serialcustom\" role=\"tabpanel\" aria-labelledby=\"serialovrtab\">
                                <div class=\"row\">
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialBaudRateInpt}</div>
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialClockRateInpt}</div>
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialTxFifoSzInpt}</div>
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialFifoCtrlInpt}</div>
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialLineCtrlInpt}</div>
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialRegWidthInpt}</div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialRegBaseInpt}</div>
                                    <div class=\"col-12 col-md-4 col-lg-2\">{$serialRegStrideInpt}</div>
                                    <div class=\"col-12 col-md-8 col-lg-4\">{$serialPciDevInpt}</div>
                                    <!-- col-lg-4 here -->
                                </div>
                                <div class=\"row mt-2\">
                                    <div class=\"col-12\">{$serialCustomOptions}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['custom_entr']}</div>
                </div>
        
                {$entriesPanel}
                {$toolsPanel}
        
                <!-- Picker Attributes flag Modal -->
                {$pikerAttrModal}
        
                <!-- Display Level flag Modal -->
                {$displayLvlModal}
        
                <!-- Target flag Modal -->
                {$targetModal}
        
                <!-- Halt Level flag Modal -->
                {$haltLvlModal}
        
                <!-- Scan Policy flag Modal -->
                {$scanPolicyModal}
        
                <!-- Expose Sensitive Data flag Modal -->
                {$exposeSensitiveDataModal}
        
                <!-- Console attributes Modal -->
                <div class=\"modal fade consattrs-modal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
                    <div class=\"modal-dialog modal-sm\" role=\"document\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <h5 class=\"modal-title\">{$text['console_attrs']}</h5>
                                <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
                            </div>
                            <div class=\"modal-body\">
                                <div class=\"row\">
                                    <div class=\"col-12 text-center mb-2\">{$text['foreground_color']}</div>
                                </div>
        
                                <div class=\"row\">
                                    <div class=\"col-12\">{$consoleAttrFgOpts}</div>
                                </div>
        
                                <div class=\"row\">
                                    <div class=\"col-12 text-center mb-2 mt-3\">{$text['background_color']}</div>
                                </div>
        
                                <div class=\"row\">
                                    <div class=\"col-12\">{$consoleAttrsBgOpts}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    } catch (\Throwable) {}

    return "";
}