<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

function nvram(): string {
    try {
        global $text, $config, $nvrmOpts;

        $nvramAdd = $config->getVals('NVRAM/Add');
        $nvramBlock = $config->getRawVals('NVRAM/Delete');
        $legcySchema = $config->getRawVals('NVRAM/LegacySchema');

        $nvramOptions = drawSimpleInlineCheckOpts($nvrmOpts, 'NVRAM');
        $nvramAddMainTable = drawPatchTable('ocnaPropM', ['guid'], $nvramAdd, ['cp']);
        $nvramAddValsTable = drawPatchTable('ocnaPropS', ['key', 'value', 'data_type'], $nvramAdd);
        $nvramBlockMainTable = drawPatchTable('socnbPropM', ['guid'], $nvramBlock, ['cp']);
        $nvramBlockValsTable = drawPatchTable('socnbPropS', ['variable'], $nvramBlock);
        $legacySchemaMainTable = drawPatchTable('soclsPropM', ['guid'], $legcySchema, ['cp']);
        $legacySchemaValsTable = drawPatchTable('soclsPropS', ['variable'], $legcySchema);

        return "<div class=\"row\"><div class=\"col-12 title\">NVRAM</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$nvramOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['add']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$nvramAddMainTable}</div>
                    <div class=\"col-12 col-lg-6 mt-3 mt-lg-0\">{$nvramAddValsTable}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['delete']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$nvramBlockMainTable}</div>
                    <div class=\"col-12 col-lg-6 mt-3 mt-lg-0\">{$nvramBlockValsTable}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['legacy_schema']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$legacySchemaMainTable}</div>
                    <div class=\"col-12 col-lg-6 mt-3 mt-lg-0\">{$legacySchemaValsTable}</div>
                </div>";
    } catch (\Throwable) {}

    return "";
}