<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

require_once __DIR__.'/../../include/data/smbios.php';
require_once __DIR__.'/../../include/fn/smbios.php';

function platform_info(): string {
    try {
        global $text, $config, $ocPlatInfoOpts, $ocUpSMBModeOpts, $ocSMGenericOpts, $ocSysMemStatOpts,
                $ocMemErCorrOpts, $ocFormFacOpts, $ocMemTypeOpts, $ocBoardTypeOpts, $ocChassTypeOpts,
                $chassAssetTag, $platFeatureOpts;

        $platformFeatFallback = hexdec('0xFFFFFFFF');
        $procInfoCombo = 'Automatic:0;Core Solo [0x0201]:513;Core 2 Duo Type1 [0x0301]:769;Core 2 Duo Type2 [0x0302]:770;'.
                         'Xeon Penryn Type1 [0x0401]:1025;Xeon Penryn Type2 [0x0402]:1026;Xeon [0x0501]:1281;Xeon E5 [0x0A01]:2561;'.
                         'Core i5 Type1 [0x0601]:1537;Core i7 Type1 [0x0701]:1793;Core i3 Type1 [0x0901]:2305;Core i5 Type2 [0x0602]:1538;'.
                         'Core i7 Type2 [0x0702]:1794;Core i3 Type2 [0x0902]:2306;Core i5 Type3 [0x0603]:1539;Core i7 Type3 [0x0703]:1795;'.
                         'Core i3 Type3 [0x0903]:2307;Core i5 Type4 [0x0604]:1540;Core i7 Type4 [0x0704]:1796;Core i3 Type4 [0x0904]:2308;'.
                         'Core i5 Type5 [0x0605]:1541;Core i7 Type5 [0x0705]:1797;Core i3 Type5 [0x0905]:2309;Core i5 Type6 [0x0606]:1542;'.
                         'Core i7 Type6 [0x0706]:1798;Core i3 Type6 [0x0906]:2310;Core i7 Type7 [0x0707]:1799;Core i5 Type8 [0x0608]:1544;'.
                         'Core i5 Type9 [0x0609]:1545;Core i7 Type9 [0x0709]:1801;Core M Type1 [0x0B01]:2817;Core M Type6 [0x0B06]:2822;'.
                         'Core M3 Type1 [0x0C01]:3073;Core M3 Type7 [0x0C07]:3079;Core M5 Type1 [0x0D01]:3329;Core M5 Type7 [0x0D07]:3335;'.
                         'Core M7 Type1 [0x0E01]:3585;Core M7 Type7 [0x0E07]:3591;Xeon W [0x0F01]:3841;Core i9 Type1 [0x1001]:4097;'.
                         'Core i9 Type5 [0x1005]:4101;Core i9 Type9 [0x1009]:4105;';
        $platfNameCombo = 'platform:platform;';
        $platformFeatCombo = "OEM specified on Apple hardware:{$platformFeatFallback}";
        $devPathCombo = 'Not installed:0;Add SATA device paths:1;';

        $platInfoAutomaitc = getCheckAttr($config->getRawVals('PlatformInfo/Automatic'));
        $genericProductName = $config->getRawVals('PlatformInfo/Generic/SystemProductName');
        $productName = $config->getRawVals('PlatformInfo/SMBIOS/SystemProductName');
        $platformFeatFlag = $config->getRawVals('PlatformInfo/SMBIOS/PlatformFeature') ?? $platformFeatFallback;

        $automsgDisabled = $platInfoAutomaitc === 'checked' ? '':' d-none';
        $advPlatinfDisabled = $automsgDisabled === '' ? ' d-none':'';
        $selected = strtolower(str_replace([' ', ','], '', $productName ?? $genericProductName ?? ''));

        $smbiosSelectInpt = draw_smbios_select($selected);
        $updateSmbiosModeInpt = drawSimpleSelect('PlatformInfo', 'UpdateSMBIOSMode', $ocUpSMBModeOpts, 'up_smb_mode');
        $smbiosAutomaticCheckbox = drawCheckbox('form-check-inline', 'PlatformInfo', 'Automatic', $platInfoAutomaitc, 'automatic', false, 'autosmb', 'data-change="ocautoplatinf"');
        $platinfoOptions = drawSimpleInlineCheckOpts($ocPlatInfoOpts, 'PlatformInfo');
        $genericSerialNoInpt = drawSmbiosSerialNumberInput('PlatformInfo/Generic', 'SystemSerialNumber', 'sys_serial_no');
        $genericMlbInpt = drawSimpleInput('string', 'PlatformInfo/Generic', 'MLB', 'mlb', '', '', '', false, 'mlbfield');
        $genericMemStatusInpt = drawSimpleSelect('PlatformInfo/Generic', 'SystemMemoryStatus', $ocSysMemStatOpts, 'sys_mem_status');
        $genericProductNameInpt = drawSimpleInput('string', 'PlatformInfo/Generic', 'SystemProductName', 'sys_prod_name', '', 'ocgsysprodn');
        $genericProcTypeInpt = drawComboboxSimpleInput('integer', 'PlatformInfo/Generic', 'ProcessorType', 'cpu_type', $procInfoCombo, 0, 'ocgncput');
        $genericSysUuidInpt = drawSimpleInput('string', 'PlatformInfo/Generic', 'SystemUUID', 'sys_uuid');
        $genericRomInpt = drawSimpleInput('data', 'PlatformInfo/Generic', 'ROM', 'rom');
        $genericOptions = drawSimpleInlineCheckOpts($ocSMGenericOpts, 'PlatformInfo/Generic');
        $dataHubPlatfNameInpt = drawComboboxSimpleInput('string', 'PlatformInfo/DataHub', 'PlatformName', 'platf_name', $platfNameCombo);
        $dataHubProductNameInpt = drawSimpleInput('string', 'PlatformInfo/DataHub', 'SystemProductName', 'sys_prod_name', '', 'ocdhsyspdnm');
        $dataHubSerialNoInpt = drawSmbiosSerialNumberInput('PlatformInfo/DataHub', 'SystemSerialNumber', 'sys_serial_no');
        $dataHubBoardProdctInpt = drawSimpleInput('string', 'PlatformInfo/DataHub', 'BoardProduct', 'board_prodct', '', 'ocdhbdprod');
        $dataHubSysUiidInpt = drawSimpleInput('string', 'PlatformInfo/DataHub', 'SystemUUID', 'sys_uuid');
        $dataHubBoardRevInpt = drawSimpleInput('data', 'PlatformInfo/DataHub', 'BoardRevision', 'board_rev', '0', 'ocdhboardrev');
        $dataHubInitialTscInpt = drawSimpleInput('integer', 'PlatformInfo/DataHub', 'InitialTSC', 'initial_tsc', '0', '', '', true);
        $dataHubFsbFreqInpt = drawSimpleInput('integer', 'PlatformInfo/DataHub', 'FSBFrequency', 'fsb_freq', '0', '', '', true);
        $dataHubArtFreqInpt = drawSimpleInput('integer', 'PlatformInfo/DataHub', 'ARTFrequency', 'art_freq', '0', '', '', true);
        $dataHubDevicePathsInpt = drawComboboxSimpleInput('integer', 'PlatformInfo/DataHub', 'DevicePathsSupported', 'dev_path_supp', $devPathCombo, 0);
        $dataHubStartupPowerEvtInpt = drawSimpleInput('integer', 'PlatformInfo/DataHub', 'StartupPowerEvents', 'startup_pow_ev', '0', '', '', true);
        $dataHubSmcRevInpt = drawSimpleInput('data', 'PlatformInfo/DataHub', 'SmcRevision', 'smc_rev', '', 'ocdhsmcrev');
        $dataHubSmcBranchInpt = drawSimpleInput('data', 'PlatformInfo/DataHub', 'SmcBranch', 'smc_branch', '', 'ocdhsmcbrch');
        $dataHubSmcPlatInpt = drawSimpleInput('data', 'PlatformInfo/DataHub', 'SmcPlatform', 'smc_plat', '', 'ocdhsmcplat');
        $memMaxCapacityInpt = drawSimpleInput('integer', 'PlatformInfo/Memory', 'MaxCapacity', 'max_capacity', '0', '', '', true);
        $memDataWidthInpt = drawSimpleInput('integer', 'PlatformInfo/Memory', 'DataWidth', 'data_width', hexdec('0xFFFF'), '', '', true);
        $memTotWidthInpt = drawSimpleInput('integer', 'PlatformInfo/Memory', 'TotalWidth', 'tot_width', hexdec('0xFFFF'), '', '', true);
        $memErCorrectionInpt = drawSimpleSelect('PlatformInfo/Memory', 'ErrorCorrection', $ocMemErCorrOpts, 'err_correction', 3);
        $memFormFactorInpt = drawSimpleSelect('PlatformInfo/Memory', 'FormFactor', $ocFormFacOpts, 'form_factor', 2, 'ocmemffact');
        $memTypeInpt = drawSimpleSelect('PlatformInfo/Memory', 'Type', $ocMemTypeOpts, 'type', 2);
        $memTypeDetailInpt = drawSimpleInput('integer', 'PlatformInfo/Memory', 'TypeDetail', 'type_detail', '4', '', '', true);
        $memDevicesPanel = draw_fancy_panel($config->getVals('PlatformInfo/Memory/Devices'), 'ocMemDevs', 'PlatformInfo/Memory/Devices', 'devices', true, false);
        $nvramSysUuidInpt = drawSimpleInput('string', 'PlatformInfo/PlatformNVRAM', 'SystemUUID', 'sys_uuid');
        $nvramRomInpt = drawSimpleInput('data', 'PlatformInfo/PlatformNVRAM', 'ROM', 'rom');
        $nvramBidInpt = drawSimpleInput('string', 'PlatformInfo/PlatformNVRAM', 'BID', 'bid', '', 'ocnvrambid');
        $nvramFwFeatInpt = drawSimpleInput('data', 'PlatformInfo/PlatformNVRAM', 'FirmwareFeatures', 'fw_features', '', 'ocnvfwf');
        $nvramFwFeatMaskInpt = drawSimpleInput('data', 'PlatformInfo/PlatformNVRAM', 'FirmwareFeaturesMask', 'fw_feature_mask', '', 'ocnvfwfm');
        $nvramMlbInpt = drawSimpleInput('string', 'PlatformInfo/PlatformNVRAM', 'MLB', 'mlb', '', '', '', false, 'mlbfield');
        $smbiosBiosVndInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BIOSVendor', 'bios_vendor', '', 'ocbiosvnd');
        $smbiosBiosVerInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BIOSVersion', 'bios_ver', '', 'ocbiosv');
        $smbiosBiosRelDateInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BIOSReleaseDate', 'bios_rel_date', '', 'ocbiosreld');
        $smbiosSysManufInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'SystemManufacturer', 'sys_manufact', '', 'ocsysmanuf');
        $smbiosSysProdNameInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'SystemProductName', 'sys_prodct_name', '', 'smpdn');
        $smbiosSysVerInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'SystemVersion', 'sys_ver', '', 'ocsysver');
        $smbiosSysSerialNoInpt = drawSmbiosSerialNumberInput('PlatformInfo/SMBIOS', 'SystemSerialNumber', 'sys_serial_no');
        $smbiosSkuNoInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'SystemSKUNumber', 'sys_sku_no', '', 'ocsyssku');
        $smbiosSysFamilyInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'SystemFamily', 'sys_family', '', 'ocsysfaml');
        $smbiosBoardProductInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BoardProduct', 'board_prodct', '', 'ocboardprod');
        $smbiosBoardVerInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BoardVersion', 'board_ver', '', 'ocboardv');
        $smbiosBoardSerialNoInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BoardSerialNumber', 'board_sn', '', '', '', false, 'mlbfield');
        $smbiosSysUiidInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'SystemUUID', 'sys_uuid');
        $smbiosBoardManufInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BoardManufacturer', 'board_manufacturer', '', 'ocboardmanf');
        $smbiosBoardLocInChasInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BoardLocationInChassis', 'board_loc_in_chass', '', 'ocboardlocchas');
        $smbiosBoardAssTagInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'BoardAssetTag', 'board_asset_tag', '', 'ocboardasst');
        $smbiosBoardTypeInpt = drawSimpleSelect('PlatformInfo/SMBIOS', 'BoardType', $ocBoardTypeOpts, 'board_type', 0, 'ocboardtp');
        $smbiosChassManufInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'ChassisManufacturer', 'chass_manufacturer', '', 'occhassmanuf');
        $smbiosChassTypeInpt = drawSimpleSelect('PlatformInfo/SMBIOS', 'ChassisType', $ocChassTypeOpts, 'chass_type', 0, 'occhasstp');
        $smbiosChassVerInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'ChassisVersion', 'chass_version', '', 'occhassver');
        $smbiosChassAssTagInpt = drawComboboxSimpleInput('string', 'PlatformInfo/SMBIOS', 'ChassisAssetTag', 'chass_asset', $chassAssetTag, '', 'occhassasst');
        $smbiosChassSerialNoInpt = drawSimpleInput('string', 'PlatformInfo/SMBIOS', 'ChassisSerialNumber', 'chass_serial_no');
        $smbiosSmcVerInpt = drawSimpleInput('data', 'PlatformInfo/SMBIOS', 'SmcVersion', 'smc_ver', 0, 'ocsmcver');
        $smbiosPlatFeatInpt = drawFlagInput($platformFeatFlag, 'PlatformInfo/SMBIOS', 'PlatformFeature', 'ocplf', $platformFeatCombo, 'integer', 32);
        $smbiosPlatFeatModal = drawSimpleFlagModal($platFeatureOpts, $platformFeatFlag, 'ocplf');
        $smbiosFwFeatInpt = drawSimpleInput('data', 'PlatformInfo/SMBIOS', 'FirmwareFeatures', 'fw_features', 0, 'ocfwf');
        $smbiosFwFeatMaskInpt = drawSimpleInput('data', 'PlatformInfo/SMBIOS', 'FirmwareFeaturesMask', 'fw_feature_mask', 0, 'ocfwfm');
        $smbiosProcTypeInpt = drawComboboxSimpleInput('integer', 'PlatformInfo/SMBIOS', 'ProcessorType', 'cpu_type', $procInfoCombo, 0, 'ocproctp');
        $smbiosDetailModal = draw_smbios_more_modal();

    return "<div class=\"row\"><div class=\"col-12 title\">{$text['platform_info_t']}</div></div>

            {$smbiosSelectInpt}
        
            <div class=\"row\">
                <div class=\"col-12 subtitle\">{$text['options']}</div>
            </div>
        
            <div class=\"row mt-3 mb-3\">
                <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$updateSmbiosModeInpt}</div>
                <div class=\"col-12 col-md-8 col-lg-9 col-xl-10\">
                        {$smbiosAutomaticCheckbox}
                        {$platinfoOptions}
                </div>
            </div>
        
            <div class=\"row\">
                <div class=\"col-12 title\">{$text['generic_props']}</div>
            </div>
        
            <div class=\"row\">
                <div class=\"col-12 col-md-6 col-lg-3\">{$genericSerialNoInpt}</div>
                <div class=\"col-12 col-md-6 col-lg-3\">{$genericMlbInpt}</div>
                <div class=\"col-12 col-md-5 col-lg-3\">{$genericMemStatusInpt}</div>
                <div class=\"col-12 col-md-7 col-lg-3\">{$genericProductNameInpt}</div>
            </div>
        
            <div class=\"row mb-3\">
                <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$genericProcTypeInpt}</div>
                <div class=\"col-12 col-lg-6 col-xl-7\">{$genericSysUuidInpt}</div>
                <div class=\"col-12 col-lg-3 col-xl-3\">{$genericRomInpt}</div>
            </div>
        
            <div class=\"row\">
                <div class=\"col-12 subtitle\">{$text['options']}</div>
            </div>
        
            <div class=\"row mt-3 mb-3\">
                <div class=\"col-12\">{$genericOptions}</div>
            </div>
        
            <div class=\"row\">
                <div class=\"col-12 title\">{$text['data_hub']}</div>
            </div>
        
            <div class=\"row autoplatinfmsg{$automsgDisabled}\">
                <div class=\"col-12 fst-italic\">Automatic mode</div>
            </div>
        
            <div class=\"advplatinf{$advPlatinfDisabled}\">
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-2\">{$dataHubPlatfNameInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$dataHubProductNameInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$dataHubSerialNoInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-4\">{$dataHubBoardProdctInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$dataHubSysUiidInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$dataHubBoardRevInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$dataHubInitialTscInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$dataHubFsbFreqInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$dataHubArtFreqInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$dataHubDevicePathsInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$dataHubStartupPowerEvtInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-4 col-xl-2\">{$dataHubSmcRevInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-4 col-xl-2\">{$dataHubSmcBranchInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-4 col-xl-2\">{$dataHubSmcPlatInpt}</div>
                </div>
            </div>
    
            <div class=\"row\">
                <div class=\"col-12 title\">{$text['mem_props']}</div>
            </div>
    
            <div class=\"row autoplatinfmsg{$automsgDisabled}\">
                <div class=\"col-12 fst-italic\">Automatic mode</div>
            </div>
    
            <div class=\"advplatinf{$advPlatinfDisabled}\">
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-4\">{$memMaxCapacityInpt}</div>
                    <!-- col-lg-8 here-->
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-3 col-lg-2\">{$memDataWidthInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$memTotWidthInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$memErCorrectionInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$memFormFactorInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$memTypeInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$memTypeDetailInpt}</div>
                </div>
    
                {$memDevicesPanel}
            </div>
    
            <div class=\"row\">
                <div class=\"col-12 title\">{$text['plat_nvram_props']}</div>
            </div>
    
            <div class=\"row autoplatinfmsg{$automsgDisabled}\">
                <div class=\"col-12 fst-italic\">Automatic mode</div>
            </div>
    
            <div class=\"advplatinf{$advPlatinfDisabled}\">
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$nvramSysUuidInpt}</div>
                    <div class=\"col-12 col-lg-6\">{$nvramRomInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-4 col-xl-3\">{$nvramBidInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-4 col-xl-3\">{$nvramFwFeatInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-4 col-xl-3\">{$nvramFwFeatMaskInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-4 col-xl-3\">{$nvramMlbInpt}</div>
                </div>
            </div>
    
            <div class=\"row\">
                <div class=\"col-12 title\">{$text['smbios_props']}</div>
            </div>
    
            <div class=\"row autoplatinfmsg{$automsgDisabled}\">
                <div class=\"col-12 fst-italic\">Automatic mode</div>
            </div>
    
            <div class=\"advplatinf{$advPlatinfDisabled}\">
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$smbiosBiosVndInpt}</div>
                    <div class=\"col-12 col-md-8 col-lg-4\">{$smbiosBiosVerInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$smbiosBiosRelDateInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$smbiosSysManufInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$smbiosSysProdNameInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-3 col-lg-2\">{$smbiosSysVerInpt}</div>
                    <div class=\"col-12 col-md-5 col-lg-4\">{$smbiosSysSerialNoInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$smbiosSkuNoInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$smbiosSysFamilyInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-5 col-lg-5\">{$smbiosBoardProductInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-3\">{$smbiosBoardVerInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-4\">{$smbiosBoardSerialNoInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-lg-6\">{$smbiosSysUiidInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$smbiosBoardManufInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$smbiosBoardLocInChasInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-4 col-xl-2\">{$smbiosBoardAssTagInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-4 col-xl-2\">{$smbiosBoardTypeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-4 col-xl-2\">{$smbiosChassManufInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-4 col-xl-2\">{$smbiosChassTypeInpt}</div>
                    <div class=\"col-12 col-md-5 col-lg-4 col-xl-2\">{$smbiosChassVerInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-4 col-xl-2\">{$smbiosChassAssTagInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-3\">{$smbiosChassSerialNoInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$smbiosSmcVerInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$smbiosPlatFeatInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-3\">{$smbiosFwFeatInpt}</div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$smbiosFwFeatMaskInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3 col-xl-2\">{$smbiosProcTypeInpt}</div>
                    <!-- col-xl-8 here -->
                </div>
            </div>
            
            {$smbiosDetailModal}
            {$smbiosPlatFeatModal}";
    } catch (\Throwable) {}

    return "";
}