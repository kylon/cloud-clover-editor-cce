<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\openCore_UI;

function uefi(): string {
    try {
        global $text, $config, $keyOpts, $keyModes, $pntrSupMode, $prtclOpts, $uefiQ, $apfsOpts, $audOpts, $textRnrdr,
               $screenRes, $ocPlayChimeOpts, $ocInptAppleEvt, $ocAppleInptOpts, $ocGopPassThrOpts, $ocUIScaleOpts;

        $resizeBarCombo = '[-1] Disabled:-1;[0] 1MB:0';
        $consoleMCombo = 'Max';
        $consoleRCombo = 'Max';

        $connectDriversCheckbox = drawCheckbox('', 'UEFI', 'ConnectDrivers', getCheckAttr($config->getRawVals('UEFI/ConnectDrivers')), 'connect_drivers');
        $driversPanel = draw_fancy_panel($config->getRawVals('UEFI/Drivers'), 'ocUefiDrv', 'UEFI/Drivers', 'load_drivers');
        $apfsMinDateInpt = drawSimpleInput('integer', 'UEFI/APFS', 'MinDate', 'min_date', 0, '', '', true, '', '', false, -1);
        $apfsMinVerInpt = drawSimpleInput('integer', 'UEFI/APFS', 'MinVersion', 'min_version', 0, '', '', true, '', '', false, -1);
        $apfsOptions = drawSimpleInlineCheckOpts($apfsOpts, 'UEFI/APFS');
        $audioOptions = drawSimpleInlineCheckOpts($audOpts, 'UEFI/Audio');
        $audioDeviceInpt = drawSimpleInput('string', 'UEFI/Audio', 'AudioDevice', 'audio_dev');
        $audioCodecInpt = drawSimpleInput('integer', 'UEFI/Audio', 'AudioCodec', 'audio_codc', 0, '', '', true, '', '', false, 0);
        $audioOutInpt = drawSimpleInput('integer', 'UEFI/Audio', 'AudioOutMask', 'audio_out', -1, '', '', true, '', '', false, -1);
        $audioMinVolInpt = drawSimpleInput('integer', 'UEFI/Audio', 'MinimumVolume', 'min_volume', 0, '', '', true, '', '', false, 0, 100);
        $audioVolAmplfInpt = drawSimpleInput('integer', 'UEFI/Audio', 'VolumeAmplifier', 'volume_amplifier', 0, '', '', true, '', '', false, 0);
        $audioSetupDelayInpt = drawSimpleInput('integer', 'UEFI/Audio', 'SetupDelay', 'setup_delay', 0, '', '', true, '', '', false, 0);
        $audioPlayChimeInpt = drawSimpleSelect('UEFI/Audio', 'PlayChime', $ocPlayChimeOpts, 'play_chime');
        $maxGainInpt = drawSimpleInput('integer', 'UEFI/Audio', 'MaximumGain', 'max_gain', -15, '', '', true, '', '', false);
        $minAssistGainInpt = drawSimpleInput('integer', 'UEFI/Audio', 'MinimumAssistGain', 'min_assist_gain', -30, '', '', true, '', '', false);
        $minAudGainInpt = drawSimpleInput('integer', 'UEFI/Audio', 'MinimumAudibleGain', 'min_aud_gain', -128, '', '', true, '', '', false);
        $appleInputOptions = drawSimpleInlineCheckOpts($ocAppleInptOpts, 'UEFI/AppleInput');
        $inputAppleEvtInpt = drawSimpleSelect('UEFI/AppleInput', 'AppleEvent', $ocInptAppleEvt, 'apple_event');
        $inputKeyInitialDelayInpt = drawSimpleInput('integer', 'UEFI/AppleInput', 'KeyInitialDelay', 'key_initial_delay', 50, '', '', true, '', '', false, 0);
        $inputKeySubqDelayInpt = drawSimpleInput('integer', 'UEFI/AppleInput', 'KeySubsequentDelay', 'key_subq_delay', 5, '', '', true, '', '', false, 1);
        $inputPtrSpdDivInpt = drawSimpleInput('integer', 'UEFI/AppleInput', 'PointerSpeedDiv', 'pointer_speed_div', 1, '', '', true, '', '', false, 1);
        $inputPtrPollMaskInpt = drawSimpleInput('integer', 'UEFI/AppleInput', 'PointerPollMask', 'pointer_poll_mask', -1, '', '', true, '', '', false, -1);
        $inputPtrPollMaxInpt = drawSimpleInput('integer', 'UEFI/AppleInput', 'PointerPollMax', 'pointer_poll_max', 0, '', '', true, '', '', false, 0);
        $inputPtrPollMinInpt = drawSimpleInput('integer', 'UEFI/AppleInput', 'PointerPollMin', 'pointer_poll_min', 0, '', '', true, '', '', false, 0);
        $inputPtrSpdMulInpt = drawSimpleInput('integer', 'UEFI/AppleInput', 'PointerSpeedMul', 'pointer_speed_mul', 1, '', '', true, '', '', false, 0);
        $inputKeyModesInpt = drawSimpleSelect('UEFI/Input', 'KeySupportMode', $keyModes, 'key_supp_mode', '', 'ockm', '', '', true);
        $inputKeyForgThrsdInpt = drawSimpleInput('integer', 'UEFI/Input', 'KeyForgetThreshold', 'key_forg_thrsld', 0, '', '', true, '', '', false, 0);
        $inputPtrSupModeInpt = drawSimpleSelect('UEFI/Input', 'PointerSupportMode', $pntrSupMode, 'pointer_supp_mode', '', 'ocps', '', '', true);
        $inputTimerResolInpt = drawSimpleInput('integer', 'UEFI/Input', 'TimerResolution', 'timer_res', 0, '', '', true, '', '', false, 0);
        $inputOptions = drawSimpleInlineCheckOpts($keyOpts, 'UEFI/Input');
        $outputOptions = drawSimpleInlineCheckOpts($screenRes, 'UEFI/Output');
        $outputTextRenderInpt = drawSimpleSelect('UEFI/Output', 'TextRenderer', $textRnrdr, 'text_renderer', '', 'octxr');
        $outputUIScaleInpt = drawSimpleSelect('UEFI/Output', 'UIScale', $ocUIScaleOpts, 'ui_scale', -1);
        $outputConsoleModeInpt = drawComboboxSimpleInput('string', 'UEFI/Output', 'ConsoleMode', 'console_mode', $consoleMCombo);
        $outputResInpt = drawComboboxSimpleInput('string', 'UEFI/Output', 'Resolution', 'resolution', $consoleRCombo);
        $outputGopPassThrInpt = drawSimpleSelect('UEFI/Output', 'GopPassThrough', $ocGopPassThrOpts, 'gop_passt');
        $protocolOptions = drawSimpleInlineCheckOpts($prtclOpts, 'UEFI/ProtocolOverrides');
        $quirkExitBootServDelayInpt = drawSimpleInput('integer', 'UEFI/Quirks', 'ExitBootServicesDelay', 'exit_boot_serv_delay', 0, '', '', true, '', '', false, 0);
        $quirkTscSyncTmoutInpt = drawSimpleInput('integer', 'UEFI/Quirks', 'TscSyncTimeout', 'tsc_sync_tmout', 0, '', '', true, '', '', false, 0);
        $quirkResizeGpuBarsInpt = drawComboboxSimpleInput('integer', 'UEFI/Quirks', 'ResizeGpuBars', 'resize_gpub', $resizeBarCombo, -1);
        $uefiQuirks = drawSimpleInlineCheckOpts($uefiQ, 'UEFI/Quirks');
        $reservedMemTable = drawPatchTable('ocRsrvdMem', ['address', 'size', 'comment', 'type', 'enabled'], $config->getRawVals('UEFI/ReservedMemory'), ['cp']);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['uefi']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$connectDriversCheckbox}</div>
                </div>
            
                {$driversPanel}
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['apfs']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 col-md-3 col-lg-2\">{$apfsMinDateInpt}</div>
                    <div class=\"col-12 col-md-3 col-lg-2\">{$apfsMinVerInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-8\">{$apfsOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['audio']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 mb-3\">{$audioOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-12 col-lg-6\">{$audioDeviceInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$audioOutInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$audioCodecInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$audioMinVolInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$audioVolAmplfInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$audioSetupDelayInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$audioPlayChimeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$maxGainInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$minAudGainInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$minAssistGainInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title mb-2\">{$text['apple_inpt']}</div>
                </div>
                
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputAppleEvtInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputKeyInitialDelayInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputKeySubqDelayInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputPtrSpdDivInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputPtrSpdMulInpt}</div>
                    <!--col-lg-2 here-->
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputPtrPollMaskInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputPtrPollMaxInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputPtrPollMinInpt}</div>
                    <!--col-lg-6 here-->
                </div>
                
                <div class=\"row\">
                    <div class=\"col-12 col-mb-3 col-mt-3\">{$appleInputOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['input']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12 col-md-12 col-lg-4\">{$inputKeyModesInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$inputKeyForgThrsdInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$inputPtrSupModeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$inputTimerResolInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-mb-3 col-mt-3\">{$inputOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['output']}</div>
                </div>
            
                <div class=\"row mt-3 mb-3\">
                    <div class=\"col-12\">{$outputOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-4 col-lg-3\">{$outputTextRenderInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-3\">{$outputUIScaleInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$outputConsoleModeInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$outputResInpt}</div>
                    <div class=\"col-12 col-md-4 col-lg-2\">{$outputGopPassThrInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['protocol_overrides']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$protocolOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['quirks']}</div>
                </div>
            
                <div class=\"row mt-2\">
                    <div class=\"col-12 col-md-6 col-lg-3\">{$quirkExitBootServDelayInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$quirkTscSyncTmoutInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$quirkResizeGpuBarsInpt}</div>
                    <!-- col-lg-5 here -->
                </div>
                
                <div class=\"row mt-2\">
                    <div class=\"col-12\">{$uefiQuirks}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['reserved_mem']}</div>
                </div>
            
                {$reservedMemTable}";
    } catch (\Throwable) {}

    return "";
}