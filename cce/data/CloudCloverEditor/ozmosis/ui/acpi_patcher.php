<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\ozmosis_UI;

function acpi_patcher(): string {
    try {
        global $text, $config, $PrefOpts, $acpiMisc;

        $prefOptions = drawSimpleInlineCheckOpts($PrefOpts, 'Preferences');
        $cpuMinMultInpt = drawSimpleInput('integer', '/', 'CpuMinMultiplier', 'min_mult', '', '', '', true);
        $cpuMaxMultInpt = drawSimpleInput('integer', '/', 'CpuMaxMultiplier', 'max_mult', '', '', '', true);
        $acpiOptions = drawSimpleInlineCheckOpts($acpiMisc, '/');
        $ssdtDropTable = drawPatchTable('ozssdtTb', ['signature', 'table_id', 'data_type'], $config->getVals('/DropTables'), ['cp']);
        $acpiPatchPanel = draw_fancy_panel($config->getVals('/Patches'), 'ozAcpiP', '/Patches', 'patches');

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['preferences']}</div></div>
    
            <div class=\"row\">
                <div class=\"col-12\">{$prefOptions}</div>
            </div>
        
            <div class=\"row\">
                <div class=\"col-12 title\">{$text['acpi']}</div>
            </div>
        
            <div class=\"row\">
                <div class=\"col-12\">
                    <div class=\"row\">
                        <div class=\"col-12 col-md-6 col-lg-2\">{$cpuMinMultInpt}</div>
                        <div class=\"col-12 col-md-6 col-lg-2\">{$cpuMaxMultInpt}</div>
                        <div class=\"col-12 col-lg-8\">{$acpiOptions}</div>
                    </div>
                </div>
            </div>
        
            <div class=\"row\">
                <div class=\"col-12 subtitle\">{$text['drop_tables']}</div>
            </div>
        
            {$ssdtDropTable}
            {$acpiPatchPanel}";
    } catch (\Throwable) {}

    return "";
}