<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\ozmosis_UI;

function beepBeep(): string {
    try {
        global $text, $PrefOpts, $BBPlayOpts;

        $prefOptions = drawSimpleInlineCheckOpts($PrefOpts, 'Preferences');
        $audioFileInpt = drawSimpleInput('string', '/', 'AudioFileName', 'audio_fname');
        $audioIoPortInpt = drawSimpleInput('integer', '/', 'AudioIoPortIndex', 'audio_io_port', '', '', '', true, '', '', false, 0);
        $audioDevPathInpt = drawSimpleInput('string', '/', 'AudioIoDevicePath', 'audio_dev_path');
        $audioVolInpt = drawSimpleInput('integer', '/', 'AudioVolume', 'volume', '', '', '', true, '', '', false, 0, 100);
        $playEvtInpt = drawSimpleSelect('/', 'StartPlaybackOnEvent', $BBPlayOpts, 'start_play_ev', '', '', '', '', true);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['preferences']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12\">{$prefOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">BeepBeep</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-4\">{$audioFileInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-2\">{$audioIoPortInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-5\">{$audioDevPathInpt}</div>
                    <div class=\"col-12 col-md-6 col-lg-1\">{$audioVolInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-4\">{$playEvtInpt}</div>
                    <!-- col-lg-8 here -->
                </div>";
    } catch (\Throwable) {}

    return "";
}