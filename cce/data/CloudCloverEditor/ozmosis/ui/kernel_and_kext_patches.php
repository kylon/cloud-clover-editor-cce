<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\ozmosis_UI;

require_once __DIR__.'/../../include/fancypanel/fancypanel_main.php';

function kernel_and_kext_patches(): string {
    try {
        global $text, $config, $kkxPMisc, $PrefOpts;

        $prefOptions = drawSimpleInlineCheckOpts($PrefOpts, 'Preferences');
        $kextKernOptions = drawSimpleInlineCheckOpts($kkxPMisc, '/');
        $kextPatchPanel = draw_fancy_panel($config->getVals('KextsToPatch'), 'ozkextP', 'KextsToPatch', 'kext_patch');
        $kernelPatchPanel = draw_fancy_panel($config->getVals('KernelToPatch'), 'ozkernelP', 'KernelToPatch', 'kernel_patch');
        $btrPatchPanel = draw_fancy_panel($config->getVals('BooterToPatch'), 'ozBtrP', 'BooterToPatch', 'booter_patch');
        $blockKextTable = drawPatchTable('ozBlockKC', ['kext_id'], $config->getRawVals('BlockKextCaches'), ['cp']);

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['preferences']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12\">{$prefOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['options']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12\">{$kextKernOptions}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['kernel_kext_patch']}</div>
                </div>
            
                {$kextPatchPanel}
                {$kernelPatchPanel}
                {$btrPatchPanel}
               
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['block_kext_cache']}</div>
                </div>
            
                {$blockKextTable}";
    } catch (\Throwable) {}

    return "";
}