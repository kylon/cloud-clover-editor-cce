<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\ozmosis_UI;

require_once __DIR__.'/../../include/data/graphics.php';

function ozmosis(): string {
    try {
        global $text, $config, $fbNamesCombo, $snbPlatIDCombo, $igPlatIDCombo, $gfxOpts,
               $bootArgs, $diskOpts, $templtOpts, $sipFOpts, $acpiFlags;
    
        $guidSmbios = 'Defaults:4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102';
        $guidMisc = 'Defaults:1F8E0C02-58A9-4E34-AE22-2B63745FA101';
        $guidBoot = 'Defaults:7C436110-AB2A-4BBB-A880-FE41995C9F82';
        $acpiLdrOpts = '0x0;0x45;0x47';
    
        $voodoHdaChkd = $config->getRawVals($guidMisc.'/DisableVoodooHda');
        $voodoHdaSpidfChkd = $config->getRawVals($guidMisc.'/EnableVoodooHdaInternalSpdif');
        $atiDisabled = $config->getRawVals($guidMisc.'/DisableAtiInjection') ?? false;
        $intelDisabled = $config->getRawVals($guidMisc.'/DisableIntelInjection') ?? false;
        $curAcpiLdrFlag = $config->getRawVals($guidMisc.'/AcpiLoaderMode') ?? '0x45';
        $curCSR = strval($config->getRawVals($guidBoot.'/csr-active-config'));
        
        $skuInpt = drawSimpleInput('string', $guidSmbios, 'SystemSKU', 'oz_sys_sku', '', 'ssku');
        $fwRevInpt = drawSimpleInput('string', $guidSmbios, 'FirmwareRevision', 'oz_fw_rev', '', 'frev');
        $boardAssTagInpt = drawSimpleInput('string', $guidSmbios, 'BaseBoardAssetTag', 'oz_board_ass_tag', '', 'bbat');
        $hwAddrInpt = drawSimpleInput('string', $guidSmbios, 'HardwareAddress', 'oz_hw_addr', '', '', 'Your MAC address', false, '', 'data-triggerEv="ozMacAdr"');
        $procSerialInpt = drawSimpleInput('string', $guidSmbios, 'ProcessorSerial', 'oz_proc_serial', '', 'procs');
        $atiFbInpt = drawComboboxSimpleInput('string', $guidMisc, 'AtiFramebuffer', 'oz_ati_fb', $fbNamesCombo, '', 'atif', '', 'ozati-inj', '', $atiDisabled);
        $snbIdInpt = drawComboboxSimpleInput('string', $guidMisc, 'AAPL,snb_platform_id', 'oz_snb_pid', $snbPlatIDCombo, '', 'snbf', '', 'ozintel-inj', '', $intelDisabled);
        $igIdInpt = drawComboboxSimpleInput('string', $guidMisc, 'AAPL,ig-platform-id', 'oz_ig_pid', $igPlatIDCombo, '', 'igf', '', 'ozintel-inj', '', $intelDisabled);
        $acpiLoaderInpt = drawFlagInput($curAcpiLdrFlag, $guidMisc, 'AcpiLoaderMode', 'acpif', $acpiLdrOpts);
        $acpiLoaderModal = drawAdvFlagModal($curAcpiLdrFlag, $acpiFlags, 'acpif', 'acpif_modal_title', true, true);
        $tmoutInpt = drawSimpleInput('integer', $guidMisc, 'TimeOut', 'timeout', '', 'timeo', '', true);
        $diskOptInpt = drawSimpleSelectNoLabel('', '', $diskOpts, '', '', '', 'data-sel="oz-disk-type"', true, false, '');
        $ozTemplInpt = drawSimpleSelectNoLabel('', '', $templtOpts, '', '', 'oztempltype', 'data-sel="oz-templ-type"', true, true, '');
        $ozTemplDataInpt = drawSimpleTextAreaNoLabel('string', $guidMisc, '', 4, 'data-triggerEv="ozTemplateTx"', 'templ-txa', '', 'oz-templates-tx', true, '');
        $bootArgsSelInpt = drawBootArgsSelect($bootArgs);
        $bootArgsInpt = drawTextAreaNoLabel('string', $guidBoot, 'boot-args', 3, '', '', '', 'b-args-tx');
        $csrInpt = drawCSRInput($curCSR, $guidBoot, 'csr-active-config');
        $csrModal = drawSimpleFlagModal($sipFOpts, $curCSR, 'csrf', true);
        $gfxOptions = '';
    
        foreach ($gfxOpts as $ozOp) {
            $str = substr($ozOp, 5);
            $checked = getCheckAttr($config->getRawVals($guidMisc.'/'.$str));
            $disabled = ($str === 'DisableVoodooHda' && $voodoHdaSpidfChkd) || ($str === 'EnableVoodooHdaInternalSpdif' && $voodoHdaChkd);
    
            $gfxOptions .= drawCheckbox('form-check-inline', $guidMisc, $str, $checked, $ozOp, $disabled, '', "data-change=\"{$str}\"");
        }
        
        return "<div class=\"row\"><div class=\"col-12 title\">{$text['oz_smbios_sec']}</div></div>
    
                <div class=\"row\">
                    <div class=\"col-12 col-md-6 col-lg-4\">{$skuInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$fwRevInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$boardAssTagInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$hwAddrInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-4\">{$procSerialInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['oz_gfx_sec']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$atiFbInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$snbIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$igIdInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$acpiLoaderInpt}</div>
                    <div class=\"col-12 col-sm-2 col-lg-1\">{$tmoutInpt}</div>
                </div>
            
                <div class=\"row mt-2\">
                    <div class=\"col-12\">
                        <div class=\"row\">
                            <div class=\"col-12 col-sm-6 col-lg-3\">
                                <div class=\"row\">
                                    <div class=\"col-12\">{$text['oz_disk_title']}</div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-12\">{$diskOptInpt}</div>
                                </div>
                            </div>
            
                            <div class=\"col-12 col-sm-6 col-lg-3\">
                                <div class=\"row\">
                                    <div class=\"col-12\">{$text['oz_templ_title']}</div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-12\">{$ozTemplInpt}</div>
                                </div>
                            </div>
                            <!-- col-lg-6 here -->
                        </div>
            
                        <div class=\"row\">
                            <div class=\"col-12\">{$ozTemplDataInpt}</div>
                        </div>
            
                    </div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 subtitle\">{$text['options']}</div>
                </div>
            
                <div class=\"row mt-3\">
                    <div class=\"col-12\">{$gfxOptions}</div>
                </div>
            
            
                <div class=\"row\">
                    <div class=\"col-12 title\">{$text['oz_boot_arg_sec']}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-lg-4\">
                        <div class=\"row\">
                            <div class=\"col-12\">{$text['boot_arg']}</div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-12\">{$bootArgsSelInpt}</div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-12\">{$bootArgsInpt}</div>
                        </div>
                    </div>
            
                    <div class=\"col-12 col-sm-3 col-lg-2\">{$csrInpt}</div>
                    <!-- col-lg-6 here -->
                </div>
            
                <!-- ACPI Loader Mode Modal -->
                {$acpiLoaderModal}
            
                <!-- CSR Modal -->
                {$csrModal}";
    } catch (\Throwable) {}

    return "";
}
