<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\ozmosis_UI;

require_once __DIR__.'/../../include/data/smbios.php';
require_once __DIR__.'/../../include/fn/smbios.php';

function smbios(): string {
    try {
        global $text, $config, $chassAssetTag, $chassType;

        $dataPath = 'Defaults:4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102';
        $selected = strtolower(str_replace([' ', ','], '', $config->getRawVals($dataPath.'/ProductName') ?? ''));

        $smbiosSelInpt = draw_smbios_select($selected);
        $prodNameInpt = drawSimpleInput('string', $dataPath, 'ProductName', 'pd_name', '', 'smpdn');
        $familyInpt = drawSimpleInput('string', $dataPath, 'ProductFamily', 'pd_family', '', 'smpdf');
        $biosDateInpt = drawSimpleInput('string', $dataPath, 'BiosDate', 'bios_rel_date', '', 'smbrd');
        $biosVerInpt = drawSimpleInput('string', $dataPath, 'BiosVersion', 'bios_ver', '', 'smbv');
        $biosVendInpt = drawSimpleInput('string', $dataPath, 'FirmwareVendor', 'bios_vendor', '', 'smbvn');
        $prodIdInpt = drawSimpleInput('string', $dataPath, 'ProductId', 'board_id', '', 'smbid');
        $boardVerInpt = drawSimpleInput('string', $dataPath, 'BoardVersion', 'board_ver', '', 'smbvr');
        $boardSerialNoInpt = drawSimpleInput('string', $dataPath, 'BaseBoardSerial', 'board_sn', '', '', '', false, 'mlbfield');
        $chassAssTagInpt = drawComboboxSimpleInput('string', $dataPath, 'ChassisAssetTag', 'chass_asset', $chassAssetTag, '', 'smcha');
        $chassTypeInpt = drawSimpleSelect($dataPath, 'EnclosureType', $chassType, 'chass_type', '', 'smcht', '', '', true);
        $fwFeatInpt = drawSimpleInput('string', $dataPath, 'FirmwareFeatures', 'fw_features', '', 'smff');
        $fwFeatMaskInpt = drawSimpleInput('string', $dataPath, 'FirmwareFeaturesMask', 'fw_feature_mask', '', 'smffm');
        $manufInpt = drawSimpleInput('string', $dataPath, 'Manufacturer', 'manufacturer', '', 'smmnf');
        $sysVerInpt = drawSimpleInput('string', $dataPath, 'SystemVersion', 'version', '', 'smver');
        $serialNoInpt = drawSmbiosSerialNumberInput($dataPath, 'SystemSerial');
        $smbiosDetailModal = draw_smbios_more_modal();

        return "<div class=\"row\"><div class=\"col-12 title\">{$text['smbios']}</div></div>
    
                {$smbiosSelInpt}
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$prodNameInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$familyInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-3\">{$biosDateInpt}</div>
                    <div class=\"col-12 col-sm-8 col-lg-3\">{$biosVerInpt}</div>
                    <div class=\"col-12 col-sm-4 col-lg-2\">{$biosVendInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$prodIdInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-2\">{$boardVerInpt}</div>
                    <div class=\"col-12 col-sm-7 col-lg-4\">{$boardSerialNoInpt}</div>
                    <div class=\"col-12 col-sm-5 col-lg-3\">{$chassAssTagInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$chassTypeInpt}</div>
                    <div class=\"col-12 col-sm-6 col-lg-3\">{$fwFeatInpt}</div>
                    <div class=\"col-12 col-sm-5 col-lg-3\">{$fwFeatMaskInpt}</div>
                    <div class=\"col-12 col-sm-5 col-lg-2\">{$manufInpt}</div>
                    <div class=\"col-12 col-sm-2 col-lg-1\">{$sysVerInpt}</div>
                </div>
            
                <div class=\"row\">
                    <div class=\"col-12 col-sm-5 col-lg-3\">{$serialNoInpt}</div>
                    <!-- col-lg-9 here -->
                </div>
            
                {$smbiosDetailModal}";
    } catch (\Throwable) {}

    return "";
}
