<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE;

class plistContainer {
    /**
     * Plist container
     *
     * @var array
     */
    private array $container = [];

    /**
     * Active plist
     *
     * @var string
     */
    private string $activePlistIdx = '';

    /**
     * Generate a unique index for a plist
     *
     * @param string $fileName
     *
     * @return string
     */
    private function genIndex(string $fileName): string {
        return $fileName.'-'.substr(hash('sha256', strval(time())), rand(0, 45), 6);
    }

    /**
     * Get a list of all the plist indexes in the container
     *
     * @return array
     */
    public function getIndexesList(): array {
        return array_keys($this->container);
    }

    /**
     * Get active plist index
     *
     * @return string
     */
    public function getActiveIdx(): string {
        return $this->activePlistIdx;
    }

    /**
     * Set the active plist
     *
     * @param string $index
     */
    public function setActiveIdx(string $index): void {
        $this->activePlistIdx = $index;
    }

    /**
     * Get a plist from the container
     * If no index is specified, returns the active plist
     *
     * Returns an empty string if no plist is found
     *
     * @param string $index
     *
     * @return ?abstractPlist
     */
    public function get(string $index=''): ?abstractPlist
    {
        $idx = $index === '' ? $this->activePlistIdx : $index;

        return $this->container[$idx] ?? null;
    }

    /**
     * Return the plist as an object
     *
     * @param string $index
     *
     * @return ?abstractPlist
     */
    public function getPlistObj(string $index): ?abstractPlist
    {
        return $this->get($index);
    }

    /**
     * Add a new plist to the container
     *
     * @param string $fileName
     * @param abstractPlist $plist
     * @param bool $setActive
     */
    public function add(string $fileName, abstractPlist &$plist, bool $setActive=false): void {
        $index = $this->genIndex($fileName);

        while (array_key_exists($index, $this->container))
            $index = $this->genIndex($fileName);

        $this->container[$index] = $plist;

        if ($setActive)
            $this->activePlistIdx = $index;
    }

    /**
     * Remove a plist from the container
     *
     * @param string $index
     */
    public function remove(string $index): void {
        if (!array_key_exists($index, $this->container))
            return;
        else if ($index === $this->activePlistIdx)
            $this->activePlistIdx = '';

        unset($this->container[$index]);
    }

    /**
     * Update a plist
     *
     * @param string $index
     * @param abstractPlist $newContent
     */
    public function update(string $index, abstractPlist $newContent): void {
        if (array_key_exists($index, $this->container))
            $this->container[$index] = $newContent;
    }

    /**
     * Clear the container
     */
    public function clear(): void {
        unset($this->container);
        $this->container = [];
    }
}