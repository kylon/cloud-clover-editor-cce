<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE;

class settingsContainer {
    /**
     * CCE settings list (per plist)
     *
     * @var array
     *
     * @notes setting => default value
     */
    private array $settingsList = [
        'mode' => 'oc', // CCE mode
        'text' => false // text editor
    ];

    /**
     * Settings container
     *
     * @var array
     */
    private array $container = [];

    /**
     * Initialize settings for a given plist
     *
     * @param string $index
     */
    public function initSettings(string $index): void {
        foreach ($this->settingsList as $setting => $val)
            $this->container[$index][$setting] = $val;
    }

    /**
     * Get a setting
     *
     * @param string $index
     * @param string $setting
     *
     * @return mixed
     */
    public function get(string $index, string $setting): mixed {
        if (!array_key_exists($index, $this->container) || !array_key_exists($setting, $this->container[$index]))
            return '';

        return $this->container[$index][$setting];
    }

    /**
     * Set a setting
     *
     * @param string $index
     * @param string $setting
     * @param mixed $val
     *
     * @notes You must use initSettings first
     * @notes if $index does not exist
     */
    public function set(string $index, string $setting, mixed $val) {
        if (array_key_exists($index, $this->container) && array_key_exists($setting, $this->container[$index]))
            $this->container[$index][$setting] = $val;
    }

    /**
     * Remove all settings for a given plist
     *
     * @param string $index
     */
    public function remove(string $index): void {
        if (!array_key_exists($index, $this->container))
            return;

        unset($this->container[$index]);
    }

    /**
     * Replace an index
     *
     * @param string $old
     * @param string $new
     *
     * @return bool
     */
    public function replaceIndex(string $old, string $new): bool {
        if (!array_key_exists($old, $this->container))
            return false;

        $this->container[$new] = $this->container[$old];

        unset($this->container[$old]);
        return true;
    }

    /**
     * Clear the container
     */
    public function clear(): void {
        unset($this->container);
        $this->container = [];
    }
}