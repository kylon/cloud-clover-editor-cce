<?php
/**
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SimpleDB v. 1.2
 */
declare(strict_types = 1);

class simpleDB {
    /**
     * An instance of the PDO class.
     *
     * @var ?PDO
     */
    private ?PDO $db;

    /**
     * Tables prefixes (unique ID)
     *
     * @var array
     *
     * @note
     * format: xxxxxxx_
     *
     * Use an empty string '' for empty values
     */
    private array $prefx = [''];

    /**
     * Tables prefixes (Name)
     *
     * @var array
     *
     * @note
     * format: Aname_
     *
     * Use an empty string '' for empty values
     */
    private array $db_tab = [''];

    /**
     * Databases
     *
     * @var array
     *
     * @note
     * path to file or database name
     *
     * Use an empty string '' for empty values
     */
    private array $db_name = [__DIR__.'/res/cce_configs.db'];

    /**
     * Databases hosts
     *
     * @var array
     *
     * @note
     * Use an empty string '' for empty values
     */
    private array $host = [''];

    /**
     * Databases login data
     *
     * @var array
     *
     * @note
     * Structure: [ ['username_db1', 'pwd_db1'], ['username_db2', 'pwd_db2'] ...]
     *
     * Use an empty string '' for empty values
     */
    private array $login = [
        ['','']
    ];

    /**
     * Full selected table name
     *
     * @var string
     *
     * @note
     * format: xxxxxxx_Aname_tableName
     */
    private string $selected_table;

    /**
     * Current PDO driver
     *
     * @var string
     */
    private string $cur_driver;

    /**
     * Enable debug output
     *
     * @var bool
     */
    private bool $debug = false;

    /**
     * Last PDO error message (if $debug true)
     *
     * @var string
     */
    private string $error_msg = '';

    /**
     * PDO Options
     *
     * @var array
     */
    private array $options = [
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ];

    /**
     * Open a database connection.
     *
     * Suppoted drivers: mysql, sqlite3
     *
     * @param string  $driver         - The PDO Driver
     * @param integer $offset_db_name - Index of $db_name
     * @param integer $offset_login   - Index of $login
     * @param integer $offset_prefx   - Index of $prefx
     * @param integer $offset_db_tab  - Index of $db_tab
     * @param integer $offset_host    - Index of $host
     *
     * @throws PDOException
     *
     * @return PDO
     */
    public function __construct(string $driver='mysql', int $offset_db_name=0, int $offset_login=0, int $offset_prefx=0, int $offset_db_tab=0, int $offset_host=0) {
        try {
            $pdo_string = $driver.':';

            if ($driver === 'mysql')
                $pdo_string .= 'host='.$this->host[$offset_host].';charset=utf8;dbname=';

            $pdo_string .= $this->db_name[$offset_db_name];

            $this->db = new PDO($pdo_string, $this->login[$offset_login][0], $this->login[$offset_login][1], $this->options);

            $this->selected_table = $this->prefx[$offset_prefx].$this->db_tab[$offset_db_tab];
            $this->cur_driver = $driver;

        } catch (Throwable $e) {
            $this->db = null;

            $this->setErrorMsg($e);
        }

        return $this->db;
    }

    /**
     * Check database connection
     *
     * @return bool
     */
    public function isConnected(): bool {
        return $this->db !== null;
    }

    /**
     * Execute a query to write values.
     *
     * @param string $table - Table name
     * @param string $sql2  - Second part of the query
     * @param array  $vals  - Variables to bind
     * @param string $sql1  - First part of the query
     *
     * @return boolean
     * @throws PDOException
     *
     */
    public function write(string $table, string $sql2, array $vals, string $sql1='SELECT * FROM'): bool {
        $ret = false;

        try {
            $sql = $this->db->prepare("$sql1 `".$this->selected_table.$table."`$sql2");

            foreach ($vals as $param => $val)
                $sql->bindValue($param, $val, $this->getSQLVarType($val));

            $ret = $sql->execute();

        } catch (Throwable $e) {
            $this->setErrorMsg($e);
        }

        return $ret;
    }

    /**
     * Execute a query to count the rows.
     *
     * @param string $table - Table name
     * @param string $sql2  - Second part of the query
     * @param array  $vals  - Variables to bind
     * @param string $sql1  - First part of the query
     *
     * @throws PDOException
     *
     * @return integer
     */
    public function rows(string $table, string $sql2='', array $vals=[], string $sql1='SELECT COUNT(*) FROM'): int {
        $ret = -1;

        try {
            if ($this->cur_driver === 'mysql') {//TODO
                $ret = $this->db->query("$sql1 `".$this->selected_table.$table."`$sql2")->rowCount();

            } else {
                $sql = $this->fetch($table, $sql2, $vals, 'fetch', $sql1);

                if (!empty($sql))
                    $ret = intval($sql[0]);
            }

        } catch (Throwable $e) {
            $this->setErrorMsg($e);
        }

        return $ret;
    }

    /**
     * Execute a PDO fetch.
     *
     * @param string $table - Table name
     * @param string $sql2  - Second part of the query
     * @param array  $vals  - variables to bind
     * @param string $sql1  - First part of the query
     * @param string $type  - Fetch Type
     *
     * @throws PDOException
     *
     * @return array
     */
    public function fetch(string $table, string $sql2, array $vals, string $type='fetch', string $sql1='SELECT * FROM'): array {
        $ret = [];

        try {
            $sql = $this->db->prepare("$sql1 `".$this->selected_table.$table."`$sql2");

            foreach ($vals as $param => $val)
                $sql->bindValue($param, $val, $this->getSQLVarType($val));

            $sql->execute();

            $ret = match ($type) {
                'all' => $sql->fetchAll(),
                default => $sql->fetch(),
            };
        } catch (Throwable $e) {
            $this->setErrorMsg($e);
        }

        return $ret;
    }

    /**
     * Return all the columns of a table.
     *
     * @param string $table Table name
     *
     * @throws PDOException
     *
     * @return array|boolean

    public function mysql_get_columns($table) {//TODO
        if ($this->cur_driver !== 'mysql')
            return false;

        $re = [];

        try {
            $re = $this->db->prepare("SHOW COLUMNS FROM ".$this->selected_table.$table);

            $re->execute();

            while ($row = $re->fetch(PDO::FETCH_ASSOC))
                array_push($row['Field']);

        } catch (Throwable $e) {
            $this->setErrorMsg($e);
        }

        return $re;
    }*/

    /**
     * Execute Join query.
     *
     * @param string $table. tableName
     * @param string $cmd2. Second part of a query
     * @param array $arr. PHP variables to bind
     * @param array $tabToJ. Tables list to join
     * @param array $colToCmp. Colums list to compare
     * @param string $fetchType. Select the fetch type (fetch - all)
     * @param string $joinType. Type of the Join
     * @param string $cmd. First part of a query
     *
     * @throws PDOException
     *
     * @return array success, string error
     TODO
    public function join($table, $cmd2, $arr, $tabToJ, $colToCmp=['id','id'], $fetchType='fetch', $joinType = 'LEFT OUTER JOIN', $cmd = 'SELECT * FROM') {
        try {
            for ($i=0, $c=0, $len=count($tabToJ); $i<$len; $c+=2, ++$i)
                $sql_loop[] = $joinType.' `'.$this->selected_table.$tabToJ[$i]."` ON `".$this->selected_table.$table."`.`".$colToCmp[$c]."` = `".$this->selected_table.$tabToJ[$i]."`.`".$colToCmp[$c+1]."` ";

            $sql_loopf = implode("", $sql_loop);
            $sql = $this->db->prepare("$cmd `".$this->selected_table.$table."` $sql_loopf $cmd2");

            for ($i=0, $len=count($arr); $i<$len; ++$i) {
                $sqlType = $this->getSQLVarType($arr[$i]);

                $sql->bindParam('val'.$i, $arr[$i], $sqlType);
            }

            $sql->execute();

            if ($fetchType !== 'all')
                $var = $sql->fetch();
            else
                $var = $sql->fetchAll();

        } catch (Throwable $e) {
            $var = $this->setErrorMsg($e);
        }

        return $var;
    }*/

    /**
     * Return the last error message
     *
     * @return string
     */
    public function getErrorMsg(): string {
        $m = $this->error_msg;
        $this->error_msg = '';

        return $m;
    }

    /**
     * Close a database connection
     */
    public function kill(): void {
        unset($this->db);
        $this->db = null;
    }

    /**
     * Return SQL variable type of $value
     *
     * @param mixed $value
     *
     * @return integer
     */
    protected function getSQLVarType(mixed $value): int {
        return match (true) {
            (gettype($value) === 'string' && $this->is_blob($value)) => PDO::PARAM_LOB,
            is_int($value) => PDO::PARAM_INT,
            is_bool($value) => PDO::PARAM_BOOL,
            is_null($value) => PDO::PARAM_NULL,
            default => PDO::PARAM_STR,
        };
    }

    /**
     * Save error message
     *
     * @param Throwable $pdoException
     */
    protected function setErrorMsg(Throwable $pdoException): void {
        if (!$this->debug)
            return;

        $this->error_msg = $pdoException->getMessage().', Line:'.$pdoException->getLine();
    }

    /**
     * Check if a value is marked as BLOB
     *
     * @param string $v
     *
     * @return boolean
     */
    private function is_blob(string $v): bool {
        return substr_compare($v, 'b_', 0, 2) === 0;
    }
}
