<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/CloudCloverEditor/ozmosisPlist.php';
require_once __DIR__.'/CloudCloverEditor/chameleonPlist.php';
require_once __DIR__.'/CloudCloverEditor/openCorePlist.php';
require_once __DIR__.'/utils.php';

function getTableAdditionalButtons(array $btnList, string $type): string {
    $html = '';

    foreach ($btnList as $i => $btn) {
        switch ($btn) {
            case 'db':
                $html .= "<div data-id=\"db-{$type}\" class=\"dbTbBtn bg-info stretch-down\" title=\"Database\"><i class=\"fa icon-sphere\"></i></div>";
                break;
            case 'cp':
                $html .= "<div data-id=\"cp-{$type}\" class=\"cpTbBtn bg-warning stretch-down\" title=\"Copy\"><i class=\"fa icon-copy2\"></i></div>";
                break;
            case 'edit':
                $html .= "<div data-id=\"edit-{$type}\" class=\"editTbBtn bg-lightblue stretch-down\" title=\"Edit\"><i class=\"fa icon-edit2\"></i></div>";
                break;
            default:
                break;
        }
    }

    return $html;
}

/**
 * Draw a patch table
 *
 * @param string $type
 * @param array $lang - list of langauge keys
 * @param array|null $patches - list of raw patches
 * @param array $btnList - list of additional buttons to enable
 * @param array $vaList - extra parameters
 *
 * @return string
 */
function drawPatchTable(string $type, array $lang, ?array $patches, array $btnList = [], ...$vaList): string {
    global $text;

    $tableAdditionalButtons = getTableAdditionalButtons($btnList, $type);
    $tableContent = drawPatchTableCont($type, $patches, $vaList);
    $ths = '';

    foreach ($lang as $thead) {
        $thead = $text[$thead] ?? '';

        $ths .= "<th scope=\"col\">{$thead}</th>";
    }

    return "<div>
                <div class=\"row\">
                    <div class=\"col-12\">
                        <div data-id=\"del-{$type}\" class=\"delTbBtn bg-danger stretch-down\" title=\"Delete\"><i class=\"fa icon-trash22\"></i></div>
                        {$tableAdditionalButtons} 
                        <div data-id=\"add-{$type}\" class=\"addTbBtn bg-success stretch-down\" title=\"Add\"><i class=\"fa icon-plus1\"></i></div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-12\">
                        <div class=\"table-responsive\">
                            <table class=\"patch-table {$type}\">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>{$ths}</tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class=\"table-scrollbody border-top-0\">
                                            <table class=\"{$type}\">
                                                <tbody class=\"table-cont {$type}-tb-cont text-start\">{$tableContent}</tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>";
}

/**
 * Draw patch table content
 *
 * @param string $type
 * @param array|null $patches
 * @param array $vaList
 *
 * @return string
 */
function drawPatchTableCont(string $type, ?array $patches, ...$vaList): string {
    $patches = array_merge(['' => ''], $patches ?? []); // add factory element (js)
    $hideCls = 'd-none';
    $startIdx = -1;

    switch ($type) {
        case 'ozssdtTb':
        case 'ssdtTb':      return ssdtDrop_table($patches, $type, $startIdx, $hideCls);
        case 'ssdtOr':      return ssdtOrder_table($patches, $startIdx, $hideCls);
        case 'disaAml':     return singleCol_table($patches, 'ACPI/DisabledAML', $startIdx, $hideCls);
        case 'whiteB':      return singleCol_table($patches, 'Boot/WhiteList', $startIdx, $hideCls);
        case 'blackB':      return singleCol_table($patches, 'Boot/BlackList', $startIdx, $hideCls);
        case 'forceKx':     return singleCol_table($patches, 'KernelAndKextPatches/ForceKextsToLoad', $startIdx, $hideCls);
        case 'forcechamKx': return singleCol_table($patches, 'ForceToLoad', $startIdx, $hideCls);
        case 'ozBlockKC':   return singleCol_table($patches, 'BlockKextCaches', $startIdx, $hideCls);
        case 'hideVl':      return singleCol_table($patches, 'GUI/Hide', $startIdx, $hideCls);
        case 'ocBlessOvr':  return singleCol_table($patches, 'Misc/BlessOverride', $startIdx, $hideCls);
        case 'disaDrv':     return disableDrv_table($patches, $startIdx, $hideCls);
        case 'addProp':     return addProp_table($patches, $startIdx, $hideCls);
        case 'cPropM':      return customPropMaster_table($patches, $startIdx, $hideCls);
        case 'cPropS':      return customProp_table($patches, $startIdx, $hideCls);
        case 'ocpPropM':    return arbPropMaster_table($patches, 'DeviceProperties/Add', strtolower($type), $startIdx, $hideCls);
        case 'socbPropM':   return arbPropMaster_table($patches, 'DeviceProperties/Delete', strtolower($type), $startIdx, $hideCls);
        case 'ocnaPropM':   return arbPropMaster_table($patches, 'NVRAM/Add', strtolower($type), $startIdx, $hideCls);
        case 'socnbPropM':  return arbPropMaster_table($patches, 'NVRAM/Delete', strtolower($type), $startIdx, $hideCls);
        case 'soclsPropM':  return arbPropMaster_table($patches, 'NVRAM/LegacySchema', strtolower($type), $startIdx, $hideCls);
        case 'pPropM':      return arbPropMaster_table($patches, 'Devices/Properties', strtolower($type), $startIdx, $hideCls);
        case 'subCEn':      return customSubEntries_table($patches, $startIdx, $hideCls, $vaList);
        case 'mNvI':        return nvidiaInj_table($patches, $startIdx, $hideCls);
        case 'mAI':         return atiInj_table($patches, $startIdx, $hideCls);
        case 'SMram':       return smbiosRamInj_table($patches, $startIdx, $hideCls);
        case 'SMslots':     return smbiosSlotInj_table($patches, $startIdx, $hideCls);
        case 'rtBlock':     return rtvariablesBlock_table($patches, $startIdx, $hideCls);
        case 'octba':       return ocAddTable_table($patches, $startIdx, $hideCls);
        case 'octbb':       return ocBlockTable_table($patches, $startIdx, $hideCls);
        case 'mmioWP':      return ocMMIOWhitelst_table($patches, 'Booter/MmioWhitelist', $startIdx, $hideCls);
        case 'mmioWPCL':    return ocMMIOWhitelst_table($patches, 'Quirks/MmioWhitelist', $startIdx, $hideCls);
        case 'ocRsrvdMem':  return ocReservedMem_table($patches, $startIdx, $hideCls);
        case 'socbPropS':
        case 'ocpPropS':
        case 'ocnaPropS':
        case 'socnbPropS':
        case 'soclsPropS':
        case 'pPropS': {
            array_shift($patches);
            $patches = array_merge(['' => ['' => '']], $patches);

            if ($type === 'ocpPropS')
                return arbProp_table($patches, 'DeviceProperties/Add', strtolower($type), $hideCls);
            else if ($type === 'socbPropS')
                return arbPropSingle_table($patches, 'DeviceProperties/Delete', $startIdx, $hideCls);
            else if ($type === 'ocnaPropS')
                return arbProp_table($patches, 'NVRAM/Add', strtolower($type), $hideCls);
            else if ($type === 'socnbPropS')
                return arbPropSingle_table($patches, 'NVRAM/Delete', $startIdx, $hideCls);
            else if ($type === 'soclsPropS')
                return arbPropSingle_table($patches, 'NVRAM/LegacySchema', $startIdx, $hideCls);
            else
                return arbProp_table($patches, 'Devices/Properties', strtolower($type), $hideCls);
        }
        case 'chamkextP': {
            array_shift($patches);
            $patches = array_merge(['' => ['' => ['']]], $patches);

            return chameleonKextP_table($patches, $startIdx, $hideCls);
        }
        case 'renDevs': {
            array_shift($patches);

            $patches = array_merge(['' => ['' => '']], $patches);

            return renameDevs_table($patches, $startIdx, $hideCls);
        }
        default:
            break;
    }

    return '';
}

/**
 * Create a table row
 *
 * @param string $path
 * @param int|string $idx
 * @param array $types
 * @param array $fields
 * @param array $vals
 * @param array $dataTypes - array of data-vtype for text inputs
 * @param array $tdData
 * @param array $selData
 * @param string $trClass
 *
 * @return string
 */
function createTableRow(string $path, int|string $idx, array $types, array $fields, array $vals, array $dataTypes, array $tdData = [], array $selData = [], string $trClass = ''): string {
    $ret = "<tr class=\"{$trClass}\" data-path=\"{$path}\" data-index=\"{$idx}\">";

    for ($i=0, $j=0, $h=0, $f=0, $l=count($types); $i<$l; ++$i) {
        switch ($types[$i]) {
            case 'check':
                $ret .= "<td>{$vals[$i]}</td>";
                break;
            case 'sel': {
                $evt = isset($selData[$h]['evt']) ? " data-sel=\"{$selData[$h]['evt']}\"" : '';

                $ret .= "<td><select class=\"cce-sel select-inTable\" data-field=\"{$fields[$i]}\"{$evt}>{$vals[$i]}</select></td>";
                ++$h;
            }
                break;
            case 'sort':
                $ret .= $vals[$i];
                break;
            default: { // txt/numb
                $t = $cls = $data = $dtype = '';

                if ($types[$i] === 'numb') {
                    $t = ' inline-numb';
                    $dtype = ' data-vtype="integer"';

                } else if ($types[$i] === 'txt') {
                    $t = ' inline-text';
                    $dtype = isset($dataTypes[$f]) && $dataTypes[$f] !== '' ? " data-vtype=\"{$dataTypes[$f]}\"" : '';

                    $f++;
                }

                if (isset($tdData[$j])) {
                    $cls = $tdData[$j]['addCls'] ?? '';
                    $data = isset($tdData[$j]['data']) ? " data-{$tdData[$j]['data']}=\"{$tdData[$j]['dval']}\"":'';
                }

                $ret .= "<td class=\"{$cls}{$t} truncate-inTb\" data-field=\"{$fields[$i]}\" title=\"{$vals[$i]}\"{$data}{$dtype}>{$vals[$i]}</td>";
                ++$j;
            }
                break;
        }
    }

    return "{$ret}</tr>";
}

/**
 * @param array $types
 * @param string $userType
 *
 * @return string
 */
function genDataTypeSelect(array $types, string $userType): string {
    $ret = '';

    foreach ($types as $t) {
        $selected = isSelected($t, $userType);

        $ret .= "<option value=\"{$t}\" {$selected}>{$t}</option>";
    }

    return $ret;
}

function ssdtDrop_table(array $patches, string $type, int $i, string $hideCls): string {
    $isOz = substr_compare($type, 'oz', 0, 2) === 0;
    $signatures = [
        'APIC','BGRT','CSRT','DMAR','ECDT','FACP','FPDT','HPET',
        'LPIT','MCFG','MSDM','SLIC','SSDT','DSDT','TCPA','TPM2'
    ];
    $html = '';

    foreach ($patches as $p) {
        $selO = '<option value=""></option>';
        $signature = getPropVal($p, 'Signature');
        $tableId = getPropVal($p, 'TableId');

        foreach ($signatures as $sign) {
            $selected = isSelected($sign, $signature);

            $selO .= "<option value=\"{$sign}\" {$selected}>{$sign}</option>";
        }

        if ($isOz) {
            $dataTypes = ['string', 'data'];
            $selTTableId = genDataTypeSelect($dataTypes, getValType($p, 'TableId'));

            $html .= createTableRow('/DropTables', $i,
                            ['sel','txt','sel'],
                            ['Signature','TableId','TableId'],
                            [$selO, $tableId, $selTTableId],
                            [], [], [[], ['evt' => 'datatype']], $hideCls
                    );

        } else { // CloverEFI
            $chkb = drawCheckbox('', '', 'DropForAllOS', getCheckAttr($p, 'DropForAllOS'));
            $length = getPropVal($p, 'Length');

            $html .= createTableRow('ACPI/DropTables', $i,
                            ['sel', 'txt', 'numb', 'check'],
                            ['Signature', 'TableId', 'Length', ''],
                            [$selO, $tableId, $length, $chkb],
                            ['string'], [], [], $hideCls
                    );
        }

        ++$i;
        $hideCls = '';
    }

    return $html;
}

function ssdtOrder_table(array $patches, int $i, string $hideCls): string {
    $sortEl = '<td class="p-0"><div class="ssdt-sortable"><i class="fa icon-arrows-v"></i></div></td>';
    $html = '';

    foreach ($patches as $p) {
        $sp = sanitizeString($p);

        $html .= createTableRow('ACPI/SortedOrder', $i,
                        ['txt','sort'], [$i,''], [$sp, $sortEl],
                        ['string'], [], [], $hideCls
                );

        ++$i;
        $hideCls = '';
    }

    return $html;
}

function singleCol_table(array $patches, string $dataPath, int $i, string $hideCls): string {
    $html = '';

    foreach ($patches as $p) {
        $sp = sanitizeString($p);

        $html .= createTableRow($dataPath, $i, ['txt'], [$i], [$sp], ['string'], [], [], $hideCls);

        ++$i;
        $hideCls = '';
    }

    return $html;
}

function renameDevs_table(array $patches, int $i, string $hideCls): string {
    $html = '';

    foreach ($patches as $p) {
        if (!is_array($p))
            continue;

        $oldD = array_key_first($p);
        $newD = array_values($p)[0];
        $soldD = sanitizeString($oldD);
        $snewD = sanitizeString($newD);

        $html .= createTableRow('ACPI/RenameDevices', $i++,
                                ['txt','txt'],
                                [$oldD, $oldD],
                                [$soldD, $snewD],
                                ['string', 'string'],
                                [['data' => 'inlineeditEv', 'dval' => 'rendev'], ['data' => 'inlineeditEv', 'dval' => 'rendevVal']],
                                [], $hideCls
        );

        $hideCls = '';
    }

    return $html;
}

function disableDrv_table(array $patches, int $i, string $hideCls): string {
    $drivers = ['',
        'Ps2MouseDxe','NvmExpressDxe','AppleImageCodec','AppleImageLoader','SMCHelper',
        'UsbMouseDxe','UsbKbDxe','CsmVideoDxe','DataHubDxe','EmuVariableUefi','PartitionDxe',
        'PartitionDxe','AptioInputFix','AptioMemoryFix','OsxLowMemFixDrv','OsxAptioFixDrv',
        'OsxAptioFix2Drv','OsxAptioFix3Drv','VBoxExt2','VBoxExt4','VBoxHfs','Fat','HFSPlus',
        'OsxFatBinaryDrv','ApfsDriverLoader','AudioDxe','HashServiceFix'
    ];
    $html = '';

    foreach ($patches as $p) {
        $selO = '';

        foreach ($drivers as $el) {
            $selected = isSelected($el, $p);

            $selO .= "<option value=\"{$el}\" {$selected}>{$el}</option>";
        }

        $html .= createTableRow('DisableDrivers', $i, ['sel'], [$i], [$selO], [], [], [], $hideCls);

        ++$i;
        $hideCls = '';
    }

    return $html;
}

function addProp_table(array $patches, int $i, string $hideCls): string {
    $devices = ['', 'ATI','NVidia','IntelGFX','LAN','WIFI','Firewire','SATA','IDE','HDA','HDMI','LPC','SmBUS','USB'];
    $dataTypes = ['string', 'data', 'integer'];
    $html = '';

    foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'Disabled', getCheckAttr($p, 'Disabled'));
        $comment = getPropVal($p, 'Comment');
        $selDevice = getPropVal($p, 'Device');
        $dataTSel = genDataTypeSelect($dataTypes, getValType($p, 'Value'));
        $selO = '';

        foreach ($devices as $dev) {
            $selected = isSelected($dev, $selDevice);

            $selO .= "<option value=\"{$dev}\" {$selected}>{$dev}</option>";
        }

        $html .= createTableRow('Devices/AddProperties', $i++,
                        ['sel','txt','txt','sel','txt','check'],
                        ['Device','Key','Value','Value','Comment',''],
                        [$selO, getPropVal($p, 'Key'), getPropVal($p, 'Value'), $dataTSel, $comment, $chkb],
                        ['', '', 'string'],
                        [], [[], ['evt' => 'datatype']], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function customPropMaster_table(array $patches, int $i, string $hideCls): string {
    $html = '';

	foreach ($patches as $p) {
        $selected = $i === 0 ? 'entry-selected entry-active ':'';
        $comment = getPropVal($p, 'Comment');
        $pciAdr = getPropVal($p, 'PciAddr');

        $html .= createTableRow('Devices/Arbitrary', $i,
                        ['txt','txt'],
                        ['Comment','PciAddr'],
                        [$comment, $pciAdr],
                        ['string', 'string'], [], [],
                        $selected.'cpropm '.$hideCls
                );

        ++$i;
        $hideCls = '';
    }

    return $html;
}

function customProp_table(array $patches, int $i, string $hideCls): string {
    $dataTypes = ['string', 'data', 'integer'];
    $html = '';

	foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'Disabled', getCheckAttr($p, 'Disabled'));
        $dataTSel = genDataTypeSelect($dataTypes, getValType($p, 'Value'));

        $html .= createTableRow('Devices/Arbitrary/0/CustomProperties', $i++,
                        ['txt','txt','sel','check'],
                        ['Key','Value','Value',''],
                        [getPropVal($p, 'Key'), getPropVal($p, 'Value'), $dataTSel, $chkb],
                        ['', ''],
                        [], [['evt' => 'datatype']], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function arbPropMaster_table(array $patches, string $path, string $event, int $i, string $hideCls): string {
    $html = '';

	foreach ($patches as $key => $vals) {
        $selected = $i === 0 ? 'entry-selected entry-active ':'';
        $skey = sanitizeString($key);
        $kkey = str_replace('/', '%', $skey);

        $html .= createTableRow($path, $kkey,
                        ['txt'], [''], [$skey], [''], [['data' => 'inlineeditEv', 'dval' => $event]],
                        [], $selected.$event.' '.$hideCls
                );

        ++$i;
        $hideCls = '';
    }

    return $html;
}

function arbProp_table(array $patches, string $path, string $type, string $hideCls): string {
    $a = array_keys($patches);
    $pKeys = isset($a[1]) ? [$a[0], $a[1]]:[$a[0]]; // we only need the first 2 elems ( placeholder and first patch (if any) )
    $dataTypes = ['string', 'data', 'integer'];
    $html = '';

    foreach ($pKeys as $pKey) {
        $masterKey = sanitizeString(str_replace('/', '%', $pKey));

        foreach ($patches[$pKey] as $key => $val) {
            $hasData = $val instanceof \CFPropertyList\CFType;
            $v = $hasData ? sanitizeString($val->getValue()):'';
            $t = $hasData ? $val->getType():'';
            $sel = genDataTypeSelect($dataTypes, $t);
            $skey = sanitizeString($key);

            $html .= createTableRow($path.'/'.$masterKey, $skey,
                        ['txt','txt','sel'],
                        ['', $key, $key],
                        [$skey, $v, $sel],
                        ['',''],
                        [['data' => 'inlineeditEv', 'dval' => $type], ['data' => 'inlineeditEv', 'dval' => $type.'Val']],
                        [['evt' => 'datatype']], $hideCls
                    );

            $hideCls = '';
        }
    }

    return $html;
}

function arbPropSingle_table(array $patches, string $path, int $i, string $hideCls): string {
    $a = array_keys($patches);
    $pKeys = isset($a[1]) ? [$a[0], $a[1]]:[$a[0]]; // we only need the first 2 elems ( factory and first patch(if any) )
    $html = '';

    foreach ($pKeys as $pKey) {
        $masterKey = sanitizeString(str_replace('/', '%', $pKey));

        foreach ($patches[$pKey] as $key => $val) {
            $sval = sanitizeString($val);

            $html .= createTableRow($path.'/'.$masterKey, $i,
                        ['txt'], [$i], [$sval], ['string'], [], [], $hideCls
                    );

            ++$i;
            $hideCls = '';
        }
    }

    return $html;
}

function customSubEntries_table(array $patches, int $i, string $hideCls, ...$vaList): string {
    $vaArgs = $vaList[0][0];
    $html = '';

    foreach ($patches as $p) {
        $checked = getPropVal($p, 'FullTitle') !== '' ? 'checked':'';
        $chkb = drawCheckbox('', '', 'FullTitle', $checked, '', false, '', 'data-change="fullTtl"', true, false);
        $chkb2 = drawCheckbox('', '', 'CommonSettings', getCheckAttr($p, 'CommonSettings'));
        $title = getPropVal($p, 'Title');
        $fullTitle = getPropVal($p, 'FullTitle');
        $hasFullTitle = $fullTitle !== '';

        $html .= createTableRow('GUI/Custom/Entries/'.$vaArgs[0].'/SubEntries', $i,
                        ['txt','txt','check','check'],
                        [($hasFullTitle ? 'FullTitle':'Title'), 'AddArguments', '', ''],
                        [($hasFullTitle ? $fullTitle:$title), getPropVal($p, 'AddArguments'), $chkb, $chkb2],
                        ['string', 'string'],
                        [['addCls' => 'ftitle']], [], 'subcen-tr '.$hideCls
                );

        ++$i;
        $hideCls = '';
    }

    return $html;
}

function nvidiaInj_table(array $patches, int $i, string $hideCls): string {
    $html = '';

	foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'LoadVBios', getCheckAttr($p, 'LoadVBios'));

        $html .= createTableRow('Graphics/NVIDIA', $i++,
                        ['txt','txt','txt','numb','numb','check'],
                        ['Model', 'IOPCIPrimaryMatch', 'IOPCISubDevId', 'VRAM', 'VideoPorts', ''],
                        [
                            getPropVal($p, 'Model'), getPropVal($p, 'IOPCIPrimaryMatch'), getPropVal($p, 'IOPCISubDevId'),
                            getPropVal($p, 'VRAM'), getPropVal($p, 'VideoPorts'), $chkb
                        ],
                        ['string', 'string', 'string'],
                        [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function atiInj_table(array $patches, int $i, string $hideCls): string {
    $html = '';

	foreach ($patches as $p) {
        $html .= createTableRow('Graphics/ATI', $i++,
                        ['txt','txt','txt','numb'],
                        ['Model', 'IOPCIPrimaryMatch', 'IOPCISubDevId', 'VRAM'],
                        [
                            getPropVal($p, 'Model'), getPropVal($p, 'IOPCIPrimaryMatch'),
                            getPropVal($p, 'IOPCISubDevId'), getPropVal($p, 'VRAM')
                        ],
                        ['string', 'string', 'string'],
                        [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function smbiosRamInj_table(array $patches, int $i, string $hideCls): string {
    global $text, $ramSpeedOpts;

    $types = ['DDR','DDR2','DDR3','DDR4'];
    $html = '';

    foreach ($patches as $p) {
        $selO1 = $selO2 = $selO3 = $selO4 = '<option value=""></option>';
        $slot = getPropVal($p, 'Slot');
        $size = getPropVal($p, 'Size');
        $freq = getPropVal($p, 'Frequency');
        $type = getPropVal($p, 'Type');

        for ($f=0; $f<=23; ++$f) {
            $selected = isSelected($f, $slot);

            $selO1 .= "'<option value=\"{$f}\" {$selected}>{$f}</option>";
        }

        for ($ss=1024; $ss<16400; $ss*=2) {
            $selected = isSelected($ss, $size);

            $selO2 .= "<option value=\"{$ss}\" {$selected}>{$ss}</option>";
        }

        foreach ($ramSpeedOpts as $opt) {
            $speed = substr($opt, 5);
            $selected = isSelected($speed, $freq);

            $selO3 .= "<option value=\"{$speed}\" {$selected}>{$text[$opt]}</option>";
        }

        foreach ($types as $t) {
            $selected = isSelected($t, $type);

            $selO4 .= "<option value=\"{$t}\" {$selected}>{$t}</option>";
        }

        $html .= createTableRow('SMBIOS/Memory/Modules', $i++,
                        ['sel','sel','sel','sel','txt','txt','txt'],
                        ['Slot','Size','Frequency','Type','Vendor','Serial','Part'],
                        [
                            $selO1, $selO2, $selO3, $selO4, getPropVal($p, 'Vendor'),
                            getPropVal($p, 'Serial'), getPropVal($p, 'Part')
                        ],
                        ['string', 'string', 'string'],
                        [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function smbiosSlotInj_table(array $patches, int $i, string $hideCls): string {
    $devices = ['ATI','NVidia','IntelGFX','LAN','WIFI','Firewire','HDMI','USB','NVME'];
    $types = ['PCI' => 0,'PCIe X1' => 1,'PCIe X2' => 2, 'PCIe X4' => 4,'PCIe X8' => 8,'PCIe X16' => 16];
    $html = '';

    foreach ($patches as $p) {
        $devSelOp = $typeSelOp = '<option value=""></option>';
        $device = getPropVal($p, 'Device');
        $type = getPropVal($p, 'Type');

        foreach ($devices as $dev) {
            $selected = isSelected($dev, $device);

            $devSelOp .= "<option value=\"{$dev}\" {$selected}>{$dev}</option>";
        }

        foreach ($types as $k => $v) {
            $selected = isSelected($v, $type);

            $typeSelOp .= "<option value=\"{$v}\" {$selected}>{$k}</option>";
        }

        $html .= createTableRow('SMBIOS/Slots', $i++,
                        ['sel','txt','txt','sel'],
                        ['Device','ID','Name','Type'],
                        [$devSelOp, getPropVal($p, 'ID'), getPropVal($p, 'Name'), $typeSelOp],
                        ['string', 'string'],
                        [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function chameleonKextP_table(array $patches, int $i, string $hideCls): string {
    $html = '';

    foreach ($patches as $kext => $pathList) {
        $skext = sanitizeString($kext);

        foreach ($pathList as $p) {
            $comment = getPropVal($p, 'Comment');

            $html .= createTableRow('KextsPatches', $i++,
                        ['txt', ''], [$skext, ''], [$skext, $comment],
                        ['string'],
                        [
                            ['data' => 'inlineeditEv', 'dval' => 'chmkxkey'],
                            ['addCls' => 'comment']
                        ],
                        [], $hideCls
                    );

            $hideCls = '';
        }

        $i = 0;
    }

    return $html;
}

function ocAddTable_table(array $patches, int $i, string $hideCls): string {
    $html = '';

	foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'Enabled', getCheckAttr($p, 'Enabled'));
        $comment = getPropVal($p, 'Comment');
        $path = getPropVal($p, 'Path');

        $html .= createTableRow('ACPI/Add', $i++,
                        ['txt','txt','check'],
                        ['Comment', 'Path', ''],
                        [$comment, $path, $chkb],
                        ['string', 'string'],
                        [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function ocBlockTable_table(array $patches, int $i, string $hideCls): string {
    $html = '';

	foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'All', getCheckAttr($p, 'All'));
        $chkb2 = drawCheckbox('', '', 'Enabled', getCheckAttr($p, 'Enabled'));
        $comment = getPropVal($p, 'Comment');
        $oemTid = getPropVal($p, 'OemTableId');
        $tbLen = getPropVal($p, 'TableLength');
        $tbSign = getPropVal($p, 'TableSignature');

        $html .= createTableRow('ACPI/Delete', $i++,
                        ['check','txt','check','txt','numb','txt'],
                        ['', 'Comment', '', 'OemTableId', 'TableLength', 'TableSignature'],
                        [$chkb, $comment, $chkb2, $oemTid, $tbLen, $tbSign],
                        ['string', 'data', 'data'],
                        [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function ocMMIOWhitelst_table(array $patches, string $path, int $i, string $hideCls): string {
    $html = '';

	foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'Enabled', getCheckAttr($p, 'Enabled'));
        $adr = getPropVal($p, 'Address');
        $comment = getPropVal($p, 'Comment');

        $html .= createTableRow($path, $i++,
                        ['txt','txt','check'],
                        ['Address', 'Comment', ''],
                        [$adr, $comment, $chkb],
                        ['string', 'string'],
                        [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function ocReservedMem_table(array $patches, int $i, string $hideCls): string {
    $types = [
            'Reserved', 'LoaderCode', 'LoaderData', 'BootServiceCode', 'BootServiceData', 'RuntimeCode',
            'RuntimeData', 'Available', 'Persistent', 'UnusableMemory', 'ACPIReclaimMemory', 'ACPIMemoryNVS',
            'MemoryMappedIO', 'MemoryMappedIOPortSpace', 'PalCode'
    ];
    $html = '';

    foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'Enabled', getCheckAttr($p, 'Enabled'));
        $adr = getPropVal($p, 'Address');
        $sz = getPropVal($p, 'Size');
        $comment = getPropVal($p, 'Comment');
        $type = getPropVal($p, 'Type');
        $tsel = '';

        foreach ($types as $t) {
            $selected = isSelected($t, $type);

            $tsel .= "<option value=\"{$t}\" {$selected}>{$t}</option>";
        }

        $html .= createTableRow('UEFI/ReservedMemory', $i++,
                            ['numb','numb','txt','sel','check'],
                            ['Address', 'Size', 'Comment', 'Type', ''],
                            [$adr !== '' ? $adr:0, $sz !== '' ? $sz:0, $comment, $tsel, $chkb],
                            ['string'], [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}

function rtvariablesBlock_table(array $patches, int $i, string $hideCls): string {
    $html = '';

    foreach ($patches as $p) {
        $chkb = drawCheckbox('', '', 'Disabled', getCheckAttr($p, 'Disabled'));
        $name = getPropVal($p, 'Name');
        $comment = getPropVal($p, 'Comment');
        $uuid = getPropVal($p, 'Guid');

        $html .= createTableRow('RtVariables/Block', $i++,
                            ['txt','txt','txt','check'],
                            ['Guid', 'Comment', 'Name', ''],
                            [$uuid, $comment, $name, $chkb],
                            ['string', 'string', 'string'],
                            [], [], $hideCls
                );

        $hideCls = '';
    }

    return $html;
}
