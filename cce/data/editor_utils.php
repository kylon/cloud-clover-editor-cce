<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/CloudCloverEditor/include/configUpgrader.php';

/**
 * HTML Head tags
 *
 * @param string $pageName
 *
 * @return string
 */
function head(string $pageName = ''): string {
    $csrf = genCsrfToken();

    return "<meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,shrink-to-fit=no\">
            <meta name=\"description\" content=\"Cloud Clover Editor is a Web App that allows you to manage Clover EFI, Ozmosis, Chameleon and OpenCore configs everywhere!\">
            <meta name=\"keywords\" content=\"cce,clover efi,ozmosis,chameleon,opencore,cloud clover editor\">
            <meta name=\"csrf-token\" content=\"{$csrf}\">
            <meta name=\"theme-color\" content=\"#2649a7\">
            <meta name=\"author\" content=\"kylon\">
            <title>Cloud Clover Editor {$pageName}</title>";
}

function drawCCETabs(ArrayObject $tabsList, string $ccemode): void {
    $uiInstance = \CCE\cceUI::getUIInstance($ccemode);
    $tmp = fopen('php://temp/maxmemory:'.(10 * 1024 * 1024), 'rw');

    try {
        foreach ($tabsList as $fnName) {
            $active = $_SESSION['cce-tab'] === $fnName ? ' active':'';
            $page = $uiInstance->drawUI($fnName);

            if ($page === "")
                throw new Exception("{$fnName}: error");

            fwrite($tmp, "<div id=\"{$fnName}\" class=\"cce-tab{$active} fade-in\">
                            <div class=\"container-fluid\">{$page}</div>
                          </div>");
        }

        rewind($tmp);
        echo stream_get_contents($tmp);

    } catch (Throwable) {
        echo drawErrorPage();

    } finally {
        fclose($tmp);
    }
}

function drawWipPage(): void {
    global $text;

    echo "<div class=\"wip-page\">
                <img class=\"img-fluid mt-4 w-25 mb-4\" src=\"../style/img/cce-logo-alt.svg\" alt=\"cce logo\">
                <p>{$text['wip_page_text']}</p>
                <button class=\"btn btn-wiptxtmode\">Text Editor Mode</button>
            </div>";
}

function drawErrorPage(): string {
    return "<div class=\"container-fluid pb-5\">
                <div class=\"row fatal-error mt-5\">
                    <div class=\"col-12 text-center mb-3\">
                         <p>
                            An error occurred while parsing this configuration.
                            <br><br>
                            If you see this message, please open an issue <a href=\"https://bitbucket.org/kylon/cloud-clover-editor-cce/issues?status=new&status=open\" target=\"_blank\" rel=\"noopener\">here</a>, and attach the affected configuration file.
                            <br><br>
                            Two options are available:
                            <br><br>
                            <span class=\"fw-bold\">Reset CCE</span><br>
                            <span><i>(Save your files before doing so, all the changes will be lost)</i></span>
                            <br><br>
                            <span class=\"fw-bold\">Open the file in <i>Text Editor Mode</i></span>
                        </p>
                    </div>
                    <div class=\"col-12 text-center\">
                        <button type=\"submit\" form=\"rst\" class=\"btn btn-outline-primary\">Reset CCE</button> <button class=\"btn btn-outline-primary btn-switchfatxm\">Switch to Text Editor Mode</button>
                    </div>
                    
                    <form id=\"rst\" name=\"reset\" method=\"post\" action=\"data/xhr/upload.php\">
                        <input type=\"hidden\" name=\"ucmd\" value=\"reset\">
                    </form>
         </div></div>";
}

/**
 * Boostrap 5 toast HTML
 */
function bootstrapToastBase(): void { ?>
    <div class="toast-container position-fixed top-0 end-0 p-4">
        <div id="ccetoast" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-animation="true" data-bs-autohide="true" data-bs-delay="2300">
            <div class="toast-header">
                <strong class="me-auto toast-title">CCE</strong>
                <button type="button" class="btn-close bg-white" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body"></div>
        </div>
    </div>
<?php }

function drawOpenPlistErrModal(string $type): string {
    global $text;

    return "<!-- DETECT ERROR MODAL -->
            <div id=\"detecterrm\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
                <div class=\"modal-dialog\" role=\"document\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <h5 class=\"modal-title\">{$text['error']}</h5>
                            <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
                        </div>
                        <div class=\"modal-body\">
                            <div class=\"row\">
                                <div class=\"col-12 font-italic\">
                                    <p>{$text[$type.'_msg']}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";
}

function checkConfigUpgrades(string $cceMode, array &$upgrLog, string &$alertClass): void {
    global $config;

    $plistUpgrader = \CCE\Upgrader\abstractUpgrader::getUpgraderObj($cceMode, $config, true);

    if ($plistUpgrader === null)
        return;

    if ($plistUpgrader->checkUpgrade()) {
        $upgrLog = $plistUpgrader->getUpgradeLog();
        $alertClass = ' up-alert-active';
    }
}

function drawFilesNavbar(string $curIdx, \CCE\plistContainer $plistList, \CCE\settingsContainer $settings): string {
    $indexList = $plistList->getIndexesList();
    $configs = '';

    foreach ($indexList as $idx) {
        $icon = getFileIcon($settings->get($idx, 'mode'));
        $active = $idx === $curIdx ? ' fhandle-active':'';
        $fileIdx = substr($idx, 0, strlen($idx)-7);

        $configs .= "<div class=\"file-handle {$active}\" title=\"{$idx}\" data-idx=\"{$idx}\" data-click=\"switch-cfg\">
                        <i class=\"fa icon-file-{$icon}\"></i>
                        <div class=\"handle-name\">{$fileIdx}</div>
                        <i class=\"fa icon-close2 handle-close\" data-click=\"close-cfg\"></i>
    
                        <form class=\"session-switch-form\" method=\"post\" action=\"data/xhr/upload.php\">
                            <input type=\"hidden\" name=\"ucmd\" value=\"switchcfg\">
                            <input type=\"hidden\" name=\"idx\" value=\"{$idx}\">
                        </form>
                    </div>";
    }

    return "<div id=\"config-list\">{$configs}<div id=\"newfile\" data-click=\"config-add\"><i class=\"fa icon-plus1\"></i></div></div>";
}

function drawEditorNavbar(ArrayIterator $funcNamesIter, string $ccemode): string {
    $menu = \CCE\cceUI::getUIInstance($ccemode)->getMenu();
    $html = '';

    foreach ($menu as $icn => $tx) {
        $tab = $funcNamesIter->current();
        $active = $_SESSION['cce-tab'] === $tab ? 'active':'';
        $funcNamesIter->next();

        $html .= "<li>
                    <a class=\"{$active}\" href=\"#{$tab}\" aria-controls=\"{$tab}\">
                        <i class=\"fa icon-{$icn}\" aria-hidden=\"true\"></i> <span>{$tx}</span>
                    </a>
                </li>";
    }

    return "<ul class=\"sidebar-nav\">{$html}</ul>";
}

function drawSidebarToggleBtn(): string {
    return '<div class="sidebar-toggle-btn text-center">
                <i class="fa icon-pushpin pin-nav-btn d-none d-lg-block"></i>
                <i class="fa icon-angle-right vertical-center pin-arrow"></i>
            </div>';
}

function drawCceToolsPanel(): string {
    $op = ['from', 'to'];
    $ret = '';

    foreach ($op as $o) {
        $ret .= "<div class=\"mb-3\">
                    <label for=\"{$o}-encdec\" class=\"form-label\">{$o}</label>
                    <select id=\"{$o}-encdec\" class=\"form-select rounded-0 mb-2\">
                        <option selected value=\"b64\">Base64</option>
                        <option value=\"hex\">Hex</option>
                        <option value=\"ascii\">Ascii</option>
                    </select>
                </div>";
    }

    return "<div id=\"ccetools\" class=\"text-white mb-5 d-none flip-vertical-right\">
                <p class=\"text-center font-large fw-bold\">Encoder\Decoder</p>
                {$ret}
                <textarea id=\"encdec-val\" class=\"form-control rounded-0 mb-2\" placeholder=\"input\"></textarea>
                <textarea type=\"text\" class=\"encdec-tool-res w-100 rounded-0 form-control mb-3\" placeholder=\"result\" readonly></textarea>
                <button id=\"encdec-tool\" type=\"button\" class=\"btn-secondary rounded-0 border-0 w-100\">Encode\Decode</button>
            </div>";
}

function includeJsScripts(string $cceMode, bool $textMode): string {
    global $link;

    $requiredJs = [
        $link['jquery'],
        $link['bootstrapjs'],
        $link['psscrollbarjs'],
        $link['coderjs']
    ];
    $editorRequiredJs = [
        $link['dompurifyjs'],
        $link['utilsjs'],
        $link['flagsutilsjs'],
        $link['smbiosutilsjs'],
        $link['editorutilsjs'],
        $link['editorjs']
    ];
    $jsIncludes = '';
    $jsList = [];

    if ($textMode) {
        $textModeRequiredJs = [
            $link['codemirrorjs'],
            $link['cmactivelinejs'],
            $link['cmclosetagjs'],
            $link['cmsimplescrollbarjs'],
            $link['cmxmljs'],
            $link['cmdialogjs'],
            $link['cmsearchcursorjs'],
            $link['cmsearchjs'],
            $link['texteditorjs']
        ];

        $jsList = array_merge($requiredJs, $editorRequiredJs, $textModeRequiredJs);

    } else {
        $guiRequiredJs = [
            $link['jqueryui'],
            $link['jqueryuipunch'],
            $link['jcombobox'],
            $link['inlineedit'],
            $link['jquery-pagingjs'],
            $link['tritatecheckbox'],
            $link['smbioslistjs'],
            $link['comboselectjs'],
            $link['macserial']
        ];
        $guiEditorReqJs = [
            $link['baseguijs']
        ];

        switch ($cceMode) {
            case 'cce': array_push($guiEditorReqJs, $link['cloverguijs']);    break;
            case 'oc':  array_push($guiEditorReqJs, $link['opencoreguijs']);  break;
            case 'oz':  array_push($guiEditorReqJs, $link['ozmosisguijs']);   break;
            case 'chm': array_push($guiEditorReqJs, $link['chameleonguijs']); break;
            default: break;
        }

        array_push($guiEditorReqJs, $link['guieditorjs']);

        $jsList = array_merge($requiredJs, $guiRequiredJs, $editorRequiredJs, $guiEditorReqJs);
    }

    foreach ($jsList as $js)
        $jsIncludes .= "<script src=\"{$js}\"></script>\n\t";

    return $jsIncludes;
}

/**
 * HTML for flags input modals
 *
 * @param string $id
 * @param int|string $curFlag
 * @param string $title
 * @param string $bitsCheckboxes
 *
 * @return string
 */
function drawFlagModal(string $id, int|string $curFlag, string $title, string $bitsCheckboxes): string {
    global $text;

    return "<div class=\"modal fade {$id}-modal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-md\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\">{$text[$title]}<span class=\"{$id}-val\"> [ <span class=\"{$id}-fflag\">{$curFlag}</span> ]</span> </h5>
                    <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-12\">{$bitsCheckboxes}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>";
}

/**
 * @param array $flag
 * @param int|string $curFlag
 * @param string $id
 * @param bool $asHex
 *
 * @return string
 */
function drawSimpleFlagModal(array $flag, int|string $curFlag, string $id, bool $asHex = false): string {
    $flagC = count($flag);
    $iFlag = $asHex ? hexdec(strval($curFlag)) : $curFlag;
    $bits = getSimpleFlagBits($iFlag, $flagC);
    $bitChecks = '';

    for ($i=0; $i<$flagC; ++$i) {
        $val = 1 << $i;
        $checked = isBitSelected($val, $bits) ? 'checked':'';

        $bitChecks .= drawCheckbox($id.'-flag', '', '', $checked, $flag[$i], false, $id.'-flag-val', "value=\"{$val}\" data-change=\"{$id}\"", true, false);
    }

    return drawFlagModal($id, $curFlag, $id.'_modal_title', $bitChecks);
}

/**
 * @param int|string $curF
 * @param array $flags
 * @param string $id
 * @param string $title
 * @param bool $asHex
 * @param bool $withZero
 *
 * @return string
 */
function drawAdvFlagModal(int|string $curF, array $flags, string $id, string $title, bool $asHex = false, bool $withZero = false): string {
    global $text;

    $oflags = $flags;
    $iflag = $asHex ? hexdec(strval($curF)) : $curF;
    $extracted = [];

    if ($withZero)
        array_shift($flags);

    foreach ($flags as $v)
        array_push($extracted, intval(substr($v, 5)));

    $flagBits = getAdvFlagBits($iflag, $extracted);
    $bitChecks = '';

    foreach ($oflags as $flag) {
        $f = intval(substr($flag, 5));
        $checked = ($iflag === 0 && $f === 0) || isBitSelected($f, $flagBits) ? 'checked':'';

        $bitChecks .= drawCheckbox($id.'-flag', '', '', $checked, $text[$flag], false, $id.'-flag-val', "value=\"{$f}\" data-change=\"{$id}\"", true, false);
    }

    return drawFlagModal($id, $curF, $title, $bitChecks);
}

/**
 * @param int|string|null $curF
 * @param string $path
 * @param string $field
 * @param string $id
 * @param string $comboData
 * @param string|null $vtype
 * @param int|null $bits
 *
 * @return string
 */
function drawFlagInput(int|string|null $curF, string $path, string $field, string $id, string $comboData, ?string $vtype = null, ?int $bits = null): string {
    global $text;

    $valType = $vtype !== null ? " data-vtype=\"{$vtype}\"":'';
    $bitsN = $bits !== null ? " data-flag-bits=\"{$bits}\"":'';

    return "<div class=\"text-center mb-3\">
                <label class=\"form-label\" for=\"{$id}\">{$text[$id.'_config']}</label>
                <div class=\"input-group no-flexwrap\">
                    <input id=\"{$id}\" type=\"text\" disabled class=\"cce-combo combobox-input-group {$id}Conf\" value=\"{$curF}\"
                           data-path=\"{$path}\" data-field=\"{$field}\" data-combo=\"{$comboData}\"{$valType}{$bitsN}>
                    <button class=\"btn btn-secondary\" type=\"button\" data-bs-toggle=\"modal\" data-bs-target=\".{$id}-modal\"><i class=\"fa icon-cog3\"></i> </button>
                </div>
            </div>";
}

function drawCSRInput(?string $curCSR, string $path, string $field): string {
    $csrOpts = '0x0;0x3;0x14;0x3E7;0x67;0x127';

    return drawFlagInput($curCSR, $path, $field, 'csrf', $csrOpts);
}