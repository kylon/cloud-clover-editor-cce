<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$link = [
    // CSS
    'cce' => '../style/css/cce.css?v=20210309',
    'light_theme' => '../style/css/light_theme.css?v=20210504',
    'simplesidebar' => '../style/css/simplesidebar.min.css?v=20210225',
    'icomoon' => '../style/css/icomoon.min.css?v=20210131',

    // CSS - external
    'bootstrap' => '../node_modules/bootstrap/dist/css/bootstrap.min.css?v=522',
    'psscrollbar' => '../node_modules/perfect-scrollbar/css/perfect-scrollbar.css?v=155',
    'codemirror' => '../node_modules/codemirror-minified/lib/codemirror.css?v=5659',
    'cmbase16-dark_theme' => '../node_modules/codemirror-minified/theme/base16-dark.css?v=5659',
    'cmsimplescrollbar' => '../node_modules/codemirror-minified/addon/scroll/simplescrollbars.css?v=5659',
    'cmdialog' => '../node_modules/codemirror-minified/addon/dialog/dialog.css?v=5659',

    // Js
    'editorjs' => '../style/js/editor.js?v=20210504',
    'guieditorjs' => '../style/js/guieditor.js?v=20210506',
    'texteditorjs' => '../style/js/texteditor.js?v=20210330',
    'utilsjs' => '../style/js/utils/utils.js?v=20210504',
    'flagsutilsjs' => '../style/js/utils/flagsUtils.js?v=20210428',
    'smbiosutilsjs' => '../style/js/utils/smbiosUtils.js?v=20211103',
    'editorutilsjs' => '../style/js/utils/editorUtils.js?v=20210504',
    'coderjs' => '../style/js/utils/coder.js?v=20210309',
    'macserial' => 'data/macserial/macserial.js?v=20210608',

    // JS - GUI
    'baseguijs' => '../style/js/gui/base.js?v=20210506',
    'cloverguijs' => '../style/js/gui/clover.js?v=20210506',
    'opencoreguijs' => '../style/js/gui/opencore.js?v=20210506',
    'ozmosisguijs' => '../style/js/gui/ozmosis.js?v=20210506',
    'chameleonguijs' => '../style/js/gui/chameleon.js?v=20210506',

    // Js - Data
    'smbioslistjs' => '../style/js/data/smbios-data.js?v=20221020',

    // Js - CCE libs
    'jcombobox' => '../style/js/libs/jcombobox.js?v=20210306',
    'tritatecheckbox' => '../style/js/libs/tristatecheckbox.js?v=20210504',
    'inlineedit' => '../style/js/libs/inlineEdit.js?v=20201213',
    'jqueryui' => '../style/js/libs/jquery-ui.min.js?v=20161225',
    'jqueryuipunch' => '../style/js/libs/jquery-ui-punch.min.js?v=20160715',
    'jquery-pagingjs' => '../style/js/libs/jquery.paging.min.js?v=20190420',
    'comboselectjs' => '../style/js/libs/comboselect.js?v=20200304',

    // Js - external libs
    'jquery' => '../node_modules/jquery/dist/jquery.slim.min.js?v=361',
    'bootstrapjs' => '../node_modules/bootstrap/dist/js/bootstrap.min.js?v=522',
    'psscrollbarjs' => '../node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js?v=155',
    'dompurifyjs' => '../node_modules/dompurify/dist/purify.min.js?v=240',
    'codemirrorjs' => '../node_modules/codemirror-minified/lib/codemirror.js?v=5659',
    'cmxmljs' => '../node_modules/codemirror-minified/mode/xml/xml.js?v=5659',
    'cmsimplescrollbarjs' => '../node_modules/codemirror-minified/addon/scroll/simplescrollbars.js?v=5659',
    'cmactivelinejs' => '../node_modules/codemirror-minified/addon/selection/active-line.js?v=5659',
    'cmclosetagjs' => '../node_modules/codemirror-minified/addon/edit/closetag.js?v=5659',
    'cmdialogjs' => '../node_modules/codemirror-minified/addon/dialog/dialog.js?v=5659',
    'cmsearchcursorjs' => '../node_modules/codemirror-minified/addon/search/searchcursor.js?v=5659',
    'cmsearchjs' => '../node_modules/codemirror-minified/addon/search/search.js?v=5659'
];
