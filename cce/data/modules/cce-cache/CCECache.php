<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE;

class CCECache {
    private static ?CCECache $CCECacheObj = null;

    private string $cachePath = __DIR__.'/../../cache';

    private array $cacheTypes = ['cce', 'chm', 'oz', 'oc'];

    private array $cacheObjects = [];

    private function __construct() {}

    function __destruct() {
        self::$CCECacheObj = null;
    }

    public static function getInstance(): CCECache {
        if (self::$CCECacheObj === null)
            self::$CCECacheObj = new CCECache();

        return self::$CCECacheObj;
    }

    public function getCachePath(): string {
        return $this->cachePath;
    }

    public function generateCache(): void {
        if ($this->isValidCache())
            return;

        $this->clearCache();
        $this->generate();
        $this->write();
    }

    public function clearCache(): void {
        $this->_clearCache($this->cachePath);
    }

    private function _clearCache($path): void {
        $fileList = scandir($path);

        foreach ($fileList as $f) {
            if ($f === '.' || $f === '..')
                continue;

            $npath = $path.'/'.$f;

            if (is_dir($npath)) {
                $this->_clearCache($npath);
                rmdir($npath);

            } else {
                unlink($npath);
            }
        }
    }

    private function isValidCache(): bool {
        $idPath = $this->cachePath.'/cacheid.txt';

        if (!file_exists($idPath))
            return false;

        $langTimestamp = filemtime(__DIR__.'/../language/en'); // en is always up to date
        $cacheID = file_get_contents($idPath);

        return $cacheID && $langTimestamp === intval($cacheID);
    }

    private function generate(): void {
        $langPath = __DIR__.'/../language';
        $fileList = scandir($langPath);
        $noTranslate = require $langPath.'/no_translate.php';

        try {
            foreach ($fileList as $f) {
                $path = $langPath.'/'.$f;

                if ($f === '.' || $f === '..' || !is_dir($path))
                    continue;

                $common = require $path.'/common.php';
                $cacheObjComm = array_merge($noTranslate, $common);
                $toAdd = null;

                foreach ($this->cacheTypes as $t) {
                    $data = require "{$path}/{$t}.php";
                    $cacheObj = array_merge($cacheObjComm, $data);

                    $toAdd[$t] = [
                        'langData' => $cacheObj,
                        'cceData' => $this->genereateDataCache($t, $cacheObj)
                    ];
                }

                $this->cacheObjects[$f] = $toAdd;
            }
        } catch (\Exception $e) {
            die('Cache exception: '.$e->getMessage());
        }
    }

    private function genereateDataCache(string $type, array $langCacheObj): array {
        return match ($type) {
            'cce' => $this->generateCloverDataCache($langCacheObj),
            'chm' => $this->generateChameleonDataCache($langCacheObj),
            'oz' => $this->generateOzmosisDataCache($langCacheObj),
            'oc' => $this->generateOpenCoreCacheData($langCacheObj),
            default => [],
        };
    }

    private function generateCloverDataCache(array $langCacheObj): array {
        $data = [
            'dsdtFixesList'  => '', 'miscDsdt'  => '',
            'acpiOpt'  => '', 'ssdtGen'  => '', 'ssdtOpt'  => '', 'ssel'  => '',
            'secPol'  => '', 'bootO'  => '', 'legacyBootO'  => '', 'bootArgs'  => '',
            'cpuSetCheck'  => '', 'usbO'  => '', 'deviceMisc'  => '', 'audOpts'  => '',
            'scanLeg'  => '', 'scanKern'  => '', 'centrySelType'  => '', 'centryVolT'  => '',
            'lcentryVolT' => '', 'centryChkOpt'  => '', 'lcentryChkOpt' => '', 'cToolsChkOpt' => '',
            'embThmOpt'  => '', 'guiOpts'  => '', 'edidOpt'  => '', 'toInj'  => '', 'gfxOpt'  => '',
            'miscKK'  => '', 'kextPopt'  => '', 'kernPopt'  => '', 'bootEfiOpt'  => '', 'sipFOpts'  => '',
            'btrFOpts'  => '', 'channels'  => '', 'boardT'  => '', 'chassT'  => '', 'smbMOpt'  => '',
            'sysParam' => '', 'ocQ'  => '', 'kernCache' => '', 'langOpts' => '', 'ramSpeedOpts' => '',
            'platFeatureOpts' => '', 'kextInjOpts' => '', 'clGpuBarsSzOpts' => ''
        ];

        foreach ($langCacheObj as $k => $v) {
            $key5 = substr($k,0,5);
            $str = "\n\t\"{$k}\",";

            switch ($key5) {
                case 'dfix_': $data['dsdtFixesList']    .= $str; break;
                case 'mscd_': $data['miscDsdt']         .= $str; break;
                case 'acop_': $data['acpiOpt']          .= $str; break;
                case 'ssgn_': $data['ssdtGen']          .= $str; break;
                case 'ssop_': $data['ssdtOpt']          .= $str; break;
                case 'ssel_': $data['ssel']             .= $str; break;
                case 'secp_': $data['secPol']           .= $str; break;
                case 'bbop_': $data['bootO']            .= $str; break;
                case 'lbop_': $data['legacyBootO']      .= $str; break;
                case 'barg_': $data['bootArgs']         .= $str; break;
                case 'cpuC_': $data['cpuSetCheck']      .= $str; break;
                case 'usbo_': $data['usbO']             .= $str; break;
                case 'dvco_': $data['deviceMisc']       .= $str; break;
                case 'audo_': $data['audOpts']          .= $str; break;
                case 'eddo_': $data['edidOpt']          .= $str; break;
                case 'injo_': $data['toInj']            .= $str; break;
                case 'gfco_': $data['gfxOpt']           .= $str; break;
                case 'lscn_': $data['scanLeg']          .= $str; break;
                case 'kscn_': $data['scanKern']         .= $str; break;
                case 'cesl_': $data['centrySelType']    .= $str; break;
                case 'ceop_': $data['centryChkOpt']     .= $str; break;
                case 'lcop_': $data['lcentryChkOpt']    .= $str; break;
                case 'ctop_': $data['cToolsChkOpt']     .= $str; break;
                case 'volt_': $data['centryVolT']       .= $str; break;
                case 'lvlt_': $data['lcentryVolT']      .= $str; break;
                case 'ethm_': $data['embThmOpt']        .= $str; break;
                case 'gopt_': $data['guiOpts']          .= $str; break;
                case 'mkkp_': $data['miscKK']           .= $str; break;
                case 'kopt_': $data['kextPopt']         .= $str; break;
                case 'krno_': $data['kernPopt']         .= $str; break;
                case 'befo_': $data['bootEfiOpt']       .= $str; break;
                case 'sipf_': $data['sipFOpts']         .= $str; break;
                case 'botr_': $data['btrFOpts']         .= $str; break;
                case 'chro_': $data['channels']         .= $str; break;
                case 'bdtp_': $data['boardT']           .= $str; break;
                case 'chtp_': $data['chassT']           .= $str; break;
                case 'scho_': $data['smbMOpt']          .= $str; break;
                case 'sypo_': $data['sysParam']         .= $str; break;
                case 'cocq_': $data['ocQ']              .= $str; break;
                case 'kctp_': $data['kernCache']        .= $str; break;
                case 'clol_': $data['langOpts']         .= $str; break;
                case 'rspd_': $data['ramSpeedOpts']     .= $str; break;
                case 'oplf_': $data['platFeatureOpts']  .= $str; break;
                case 'kxij_': $data['kextInjOpts']      .= $str; break;
                case 'ocrg_': $data['clGpuBarsSzOpts']  .= $str; break;
                default:
                    break;
            }
        }

        return $data;
    }

    private function generateChameleonDataCache(array $langCacheObj): array {
        $data = [
            'dropTb'  => '', 'acpiMisc'  => '', 'cpuOp'  => '', 'audioOp'  => '',
            'gfxOp'  => '', 'guiOp'  => '', 'sysOp'  => '', 'sipFOpts'  => '',
            'bootArgs'  => '', 'kernOp'  => '', 'kextOp'  => '', 'boardT'  => '',
            'chassT' => '', 'kernelArch' => ''
        ];

        foreach ($langCacheObj as $k => $v) {
            $key5 = substr($k,0,5);
            $str = "\n\t\"{$k}\",";

            switch ($key5) {
                case 'chdp_': $data['dropTb']       .= $str; break;
                case 'chms_': $data['acpiMisc']     .= $str; break;
                case 'chss_': $data['cpuOp']        .= $str; break;
                case 'chau_': $data['audioOp']      .= $str; break;
                case 'chgx_': $data['gfxOp']        .= $str; break;
                case 'chgu_': $data['guiOp']        .= $str; break;
                case 'chsy_': $data['sysOp']        .= $str; break;
                case 'sipf_': $data['sipFOpts']     .= $str; break;
                case 'barg_': $data['bootArgs']     .= $str; break;
                case 'chkp_': $data['kernOp']       .= $str; break;
                case 'chkx_': $data['kextOp']       .= $str; break;
                case 'bdtp_': $data['boardT']       .= $str; break;
                case 'chtp_': $data['chassT']       .= $str; break;
                case 'chka_': $data['kernelArch']   .= $str; break;
                default:
                    break;
            }
        }

        return $data;
    }

    private function generateOzmosisDataCache(array $langCacheObj): array {
        $data = [
            'PrefOpts'  => '', 'acpiMisc'  => '', 'kextPopt'  => '', 'kernPopt'  => '',
            'kkxPMisc'  => '', 'gfxOpts'  => '', 'bootArgs'  => '', 'acpiFlags'  => '',
            'diskOpts'  => '', 'templtOpts'  => '', 'sipFOpts'  => '', 'chassType' => '',
            'BBPlayOpts' => ''
        ];

        foreach ($langCacheObj as $k => $v) {
            $key5 = substr($k, 0, 5);
            $str = "\n\t\"{$k}\",";

            switch ($key5) {
                case 'ozpf_': $data['PrefOpts']   .= $str; break;
                case 'ozao_': $data['acpiMisc']   .= $str; break;
                case 'kopt_': $data['kextPopt']   .= $str; break;
                case 'krno_': $data['kernPopt']   .= $str; break;
                case 'ozms_': $data['kkxPMisc']   .= $str; break;
                case 'ozgo_': $data['gfxOpts']    .= $str; break;
                case 'barg_': $data['bootArgs']   .= $str; break;
                case 'ozto_': $data['templtOpts'] .= $str; break;
                case 'ozdt_': $data['diskOpts']   .= $str; break;
                case 'sipf_': $data['sipFOpts']   .= $str; break;
                case 'chtp_': $data['chassType']  .= $str; break;
                case 'ozal_': $data['acpiFlags']  .= $str; break;
                case 'ozbp_': $data['BBPlayOpts'] .= $str; break;
                default:
                    break;
            }
        }

        return $data;
    }

    private function generateOpenCoreCacheData(array $langCacheObj): array {
        $data = [
            'acpiPOpts'  => '', 'acpiQ'  => '', 'btrQ'  => '', 'nvrmOpts' => '', 'ocPlayChimeOpts' => '',
            'keyOpts'  => '', 'keyModes'  => '', 'pntrSupMode'  => '', 'prtclOpts'  => '',
            'uefiQ' => '', 'apfsOpts' => '', 'audOpts' => '', 'textRnrdr' => '', 'screenRes' => '',
            'kernCache' => '', 'hbrntModes' => '', 'miscBootOpts' => '', 'pikerModes' => '',
            'ocDebugOpts' => '', 'dbgTargetOpts' => '', 'securityOpts' => '', 'launcherOpts' => '',
            'dmgLoadOpts' => '', 'vaultOpts' => '', 'scanPolicyOpts' => '', 'exposeSDataOpts' => '',
            'secBootModelOpts' => '', 'pickerAttrsOpts' => '', 'displayLvlOpts' => '', 'cEntriesOpts' => '',
            'ocKAddArchOpts' => '', 'ocKernAddOpts' => '', 'ocEmuPropOpts' => '', 'ocKernQuirks' => '',
            'ocKSchemeArchOpts' => '', 'ocFuzzyMOpts' => '', 'ocPlatInfoOpts' => '', 'ocUpSMBModeOpts' => '',
            'ocSMGenericOpts' => '', 'ocSysMemStatOpts' => '', 'ocMemErCorrOpts' => '', 'ocFormFacOpts' => '',
            'ocMemTypeOpts' => '', 'ocBoardTypeOpts' => '', 'ocChassTypeOpts' => '', 'ramSpeedOpts' => '',
            'platFeatureOpts' => '', 'ocInptAppleEvt' => '', 'ocAppleInptOpts' => '', 'ocGopPassThrOpts' => '',
            'ocDriversOpts' => '', 'ocUIScaleOpts' => '', 'ocKBlkStrtgyOpts' => '', 'ocSerialOpts' => '',
            'ocSerialCustOpts' => ''
        ];

        foreach ($langCacheObj as $k => $v) {
            $key5 = substr($k,0,5);
            $str = "\n\t\"{$k}\",";

            switch ($key5) {
                case 'apop_': $data['acpiPOpts']         .= $str; break;
                case 'acpo_': $data['acpiQ']             .= $str; break;
                case 'obtr_': $data['btrQ']              .= $str; break;
                case 'onvo_': $data['nvrmOpts']          .= $str; break;
                case 'ocko_': $data['keyOpts']           .= $str; break;
                case 'ockm_': $data['keyModes']          .= $str; break;
                case 'ocpm_': $data['pntrSupMode']       .= $str; break;
                case 'ocpt_': $data['prtclOpts']         .= $str; break;
                case 'oplc_': $data['ocPlayChimeOpts']   .= $str; break;
                case 'quef_': $data['uefiQ']             .= $str; break;
                case 'oaft_': $data['apfsOpts']          .= $str; break;
                case 'ocau_': $data['audOpts']           .= $str; break;
                case 'octr_': $data['textRnrdr']         .= $str; break;
                case 'ocrs_': $data['screenRes']         .= $str; break;
                case 'kctp_': $data['kernCache']         .= $str; break;
                case 'ochm_': $data['hbrntModes']        .= $str; break;
                case 'ocbo_': $data['miscBootOpts']      .= $str; break;
                case 'opkm_': $data['pikerModes']        .= $str; break;
                case 'ocdb_': $data['ocDebugOpts']       .= $str; break;
                case 'odtr_': $data['dbgTargetOpts']     .= $str; break;
                case 'ocsc_': $data['securityOpts']      .= $str; break;
                case 'olco_': $data['launcherOpts']      .= $str; break;
                case 'odmg_': $data['dmgLoadOpts']       .= $str; break;
                case 'ovlt_': $data['vaultOpts']         .= $str; break;
                case 'oscp_': $data['scanPolicyOpts']    .= $str; break;
                case 'ocxs_': $data['exposeSDataOpts']   .= $str; break;
                case 'osbm_': $data['secBootModelOpts']  .= $str; break;
                case 'ocpa_': $data['pickerAttrsOpts']   .= $str; break;
                case 'odlv_': $data['displayLvlOpts']    .= $str; break;
                case 'ocet_': $data['cEntriesOpts']      .= $str; break;
                case 'oaro_': $data['ocKAddArchOpts']    .= $str; break;
                case 'oknd_': $data['ocKernAddOpts']     .= $str; break;
                case 'oemp_': $data['ocEmuPropOpts']     .= $str; break;
                case 'okqk_': $data['ocKernQuirks']      .= $str; break;
                case 'oska_': $data['ocKSchemeArchOpts'] .= $str; break;
                case 'ocfz_': $data['ocFuzzyMOpts']      .= $str; break;
                case 'opli_': $data['ocPlatInfoOpts']    .= $str; break;
                case 'ousm_': $data['ocUpSMBModeOpts']   .= $str; break;
                case 'osgp_': $data['ocSMGenericOpts']   .= $str; break;
                case 'osms_': $data['ocSysMemStatOpts']  .= $str; break;
                case 'oerc_': $data['ocMemErCorrOpts']   .= $str; break;
                case 'ofmf_': $data['ocFormFacOpts']     .= $str; break;
                case 'omtp_': $data['ocMemTypeOpts']     .= $str; break;
                case 'osbt_': $data['ocBoardTypeOpts']   .= $str; break;
                case 'ocht_': $data['ocChassTypeOpts']   .= $str; break;
                case 'rspd_': $data['ramSpeedOpts']      .= $str; break;
                case 'oplf_': $data['platFeatureOpts']   .= $str; break;
                case 'ocae_': $data['ocInptAppleEvt']    .= $str; break;
                case 'ocai_': $data['ocAppleInptOpts']   .= $str; break;
                case 'ogpo_': $data['ocGopPassThrOpts']  .= $str; break;
                case 'oldo_': $data['ocDriversOpts']     .= $str; break;
                case 'qusl_': $data['ocUIScaleOpts']     .= $str; break;
                case 'ostk_': $data['ocKBlkStrtgyOpts']  .= $str; break;
                case 'ocsr_': $data['ocSerialOpts']      .= $str; break;
                case 'osro_': $data['ocSerialCustOpts']  .= $str; break;
                default:
                    break;
            }
        }

        return $data;
    }

    private function write(): void {
        $head = "// AUTOGENERATED FILE - DO NOT EDIT\n";

        try {
            foreach ($this->cacheObjects as $lang => $data) {
                $dir = $this->cachePath.'/'.$lang;

                if (!file_exists($dir))
                    mkdir($dir, 0755, true);

                foreach ($data as $type => $d) {
                    $langFile = "{$dir}/{$type}_lang.php";
                    $dataFile = "{$dir}/{$type}_data.php";
                    $fd1 = fopen($langFile, 'w');
                    $fd2 = fopen($dataFile, 'w');
                    $cont1 = "<?php\n{$head}\nreturn [";
                    $cont2 = "<?php\n{$head}";

                    foreach ($d['langData'] as $k => $v)
                        $cont1 .= "\n\t'{$k}' => '".str_replace("'", "\\'", $v)."',";

                    foreach ($d['cceData'] as $k => $v)
                        $cont2 .= "\n\${$k} = [".str_replace('$', '\\$', $v)."\n];";

                    fwrite($fd1, $cont1."\n];");
                    fclose($fd1);

                    fwrite($fd2, $cont2);
                    fclose($fd2);
                }
            }

            $this->writeCacheID();

        } catch (\Exception $e) {
            die('Cache Exception: '.$e->getMessage());
        }
    }

    private function writeCacheID(): void {
        try {
            $fd = fopen($this->cachePath.'/cacheid.txt', 'w');
            $cacheID = strval(filemtime(__DIR__.'/../language/en'));

            fwrite($fd, $cacheID);
            fclose($fd);

        } catch (\Exception $e) {
            die('Cache exception: '.$e->getMessage());
        }
    }
}