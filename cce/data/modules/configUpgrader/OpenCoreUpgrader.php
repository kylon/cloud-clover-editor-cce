<?php
/**
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\Upgrader;

use CCE\abstractPlist;
use CFPropertyList\CFBoolean;
use CFPropertyList\CFString;

class OpenCoreUpgrader extends abstractUpgrader {
    /**
     * OpenCoreUpgrader constructor
     *
     * @param abstractPlist $plistObj
     * @param boolean $enableLog
     */
    protected function __construct(abstractPlist $plistObj, bool $enableLog) {
        $this->plistObj = $plistObj;
        $this->enableUpgraderLog = $enableLog;
    }

    private function _hasMatchKernel(): bool {
        $configPaths = ['Kernel/Add', 'Kernel/Block', 'kernel/Patch'];

        foreach ($configPaths as $cp) {
            $data = $this->plistObj->getVals($cp);

            if ($data === null)
                continue;

            foreach ($data as $itm) {
                if (isset($itm['MatchKernel']))
                    return true;
            }
        }

        return false;
    }

    /**
     * Check for outdated keys
     *
     * @return bool
     */
    public function checkUpgrade(): bool {
        $this->resetUpgradables();

        // replace ExposeBootPath with ExposeSensitiveData [OC 0.0.2+]
        if ($this->plistObj->hasValue('Misc/Debug/ExposeBootPath')) {
            $this->addUpgradableItem('exbpath');
            $this->addToLog('Replace ExposeBootPath with ExposeSensitiveData [OpenCore 0.0.2+]');
        }

        // replace MatchKernel with MinKernel and MaxKernel [OC 0.5.1+]
        if ($this->_hasMatchKernel()) {
            $this->addUpgradableItem('matchkern');
            $this->addToLog('Replace MatchKernel with MinKernel and MaxKernel [OC 0.5.1+]');
        }

        // remove ThirdPartyTrim kernel quirk in favour of ThirdPartyDrives [OC 0.5.3+]
        if ($this->plistObj->hasValue('Kernel/Quirks/ThirdPartyTrim')) {
            $this->addUpgradableItem('3rdtrim');
            $this->addToLog('Remove ThirdPartyTrim kernel quirk in favour of ThirdPartyDrives [OpenCore 0.5.3+]');
        }

        // remove AvoidHighAlloc [OC 0.5.6+]
        if ($this->plistObj->hasValue('UEFI/Quirks/AvoidHighAlloc')) {
            $this->addUpgradableItem('highalloc');
            $this->addToLog('Remove AvoidHighAlloc [OpenCore 0.5.6+]');
        }

        // replace RequireVault and RequireSignature with Vault [OC 0.5.6+]
        if ($this->plistObj->hasValue('Misc/Security/RequireVault') || $this->plistObj->hasValue('Misc/Security/RequireSignature')) {
            $this->addUpgradableItem('reqsign');
            $this->addToLog('Replace RequireVault and RequireSignature with Vault [OpenCore 0.5.7+]');
        }

        // remove ConsoleBehaviourOs and ConsoleBehaviourUi [OC 0.5.6+]
        if ($this->plistObj->hasValue('Misc/Boot/ConsoleBehaviourUi') || $this->plistObj->hasValue('Misc/Boot/ConsoleBehaviourOs')) {
            $this->addUpgradableItem('consbehv');
            $this->addToLog('Remove ConsoleBehaviourOs and ConsoleBehaviourUi [OpenCore 0.5.6+]');
        }

        // move ConsoleMode and Resolution options from Misc to UEFI Output section [OC 0.5.6+]
        if ($this->plistObj->hasValue('Misc/Boot/ConsoleMode') || $this->plistObj->hasValue('Misc/Boot/Resolution')) {
            $this->addUpgradableItem('movtoout');
            $this->addToLog('Move ConsoleMode and Resolution options from Misc to UEFI Output section [OpenCore 0.5.6+]');
        }

        // replace ConsoleControl and BuiltinTextRenderer with TextRenderer [OC 0.5.6+]
        if ($this->plistObj->hasValue('Misc/Boot/BuiltinTextRenderer') || $this->plistObj->hasValue('UEFI/Protocols/ConsoleControl')) {
            $this->addUpgradableItem('txrender');
            $this->addToLog('Replace ConsoleControl and BuiltinTextRenderer with TextRenderer [OpenCore 0.5.6+]');
        }

        // move console-related UEFI quirks to Output section [OC 0.5.6+]
        if ($this->plistObj->hasValue('UEFI/Quirks/ClearScreenOnModeSwitch') || $this->plistObj->hasValue('UEFI/Quirks/IgnoreTextInGraphics') ||
                $this->plistObj->hasValue('UEFI/Quirks/ProvideConsoleGop') || $this->plistObj->hasValue('UEFI/Quirks/ReconnectOnResChange') ||
                $this->plistObj->hasValue('UEFI/Quirks/ReplaceTabWithSpace') || $this->plistObj->hasValue('UEFI/Quirks/SanitiseClearScreen')) {
            $this->addUpgradableItem('movout2');
            $this->addToLog('Move console-related UEFI quirks to Output section [OpenCore 0.5.6+]');
        }

        // remove ProvideEarlyConsole [OC 0.5.7+]
        if ($this->plistObj->hasValue('UEFI/Output/ProvideEarlyConsole')) {
            $this->addUpgradableItem('ueconsole');
            $this->addToLog('Remove ProvideEarlyConsole [OpenCore 0.5.7+]');
        }

        // remove Scale [OC 0.5.7+]
        if ($this->plistObj->hasValue('UEFI/Output/Scale')) {
            $this->addUpgradableItem('uscale');
            $this->addToLog('Remove Scale [OpenCore 0.5.7+]');
        }

        // replace ProtectCsmRegion with ProtectMemoryRegions [OC 0.5.7+]
        if ($this->plistObj->hasValue('Booter/Quirks/ProtectCsmRegion')) {
            $this->addUpgradableItem('csmreg');
            $this->addToLog('Replace ProtectCsmRegion with ProtectMemoryRegions [OpenCore 0.5.7+]');
        }

        // replace ShrinkMemoryMap with RebuildAppleMemoryMap [OC 0.5.7+]
        if ($this->plistObj->hasValue('Booter/Quirks/ShrinkMemoryMap')) {
            $this->addUpgradableItem('shrinkmem');
            $this->addToLog('Replace ShrinkMemoryMap with RebuildAppleMemoryMap [OpenCore 0.5.7+]');
        }

        // fix typo EnableJumpStart -> EnableJumpstart [OC 0.5.8+]
        if ($this->plistObj->hasValue('UEFI/APFS/EnableJumpStart')) {
            $this->addUpgradableItem('typojump');
            $this->addToLog('Fix typo EnableJumpStart -> EnableJumpstart [OpenCore 0.5.8+]');
        }

        // ren Protocols -> ProtocolOverrides [OC 0.5.8+]
        if ($this->plistObj->hasValue('UEFI/Protocols')) {
            $this->addUpgradableItem('protocols');
            $this->addToLog('Rename Protocols to ProtocolOverrides in UEFI [OpenCore 0.5.8+]');
        }

        // remove RequestBootVarFallback [OC 0.5.9+]
        if ($this->plistObj->hasValue('UEFI/Quirks/RequestBootVarFallback')) {
            $this->addUpgradableItem('reqbvarflbk');
            $this->addToLog('Remove RequestBootVarFallback [OpenCore 0.5.9+]');
        }

        // remove DirectGopCacheMode [OC 0.5.9+]
        if ($this->plistObj->hasValue('UEFI/Output/DirectGopCacheMode')) {
            $this->addUpgradableItem('dirgopmode');
            $this->addToLog('Remove DirectGopCacheMode [OpenCore 0.5.9+]');
        }

        // ren Block -> Delete [OC 0.5.9+]
        if ($this->plistObj->hasValue('ACPI/Block') || $this->plistObj->hasValue('DeviceProperties/Block') || $this->plistObj->hasValue('NVRAM/Block')) {
            $this->addUpgradableItem('blockdel');
            $this->addToLog('Rename Block to Delete in ACPI, DeviceProperties, and NVRAM [OpenCore 0.5.9+]');
        }

        // remove HideSelf [OC 0.5.9+]
        if ($this->plistObj->hasValue('Misc/Boot/HideSelf')) {
            $this->addUpgradableItem('hideself');
            $this->addToLog('Remove HideSelf [OpenCore 0.5.9+]');
        }

        // remove DeduplicateBootOrder [OC 0.6.5+]
        if ($this->plistObj->hasValue('UEFI/Quirks/DeduplicateBootOrder')) {
            $this->addUpgradableItem('dedupbord');
            $this->addToLog('Remove DeduplicateBootOrder [OpenCore 0.6.5+]');
        }

        // replace BootProtect with LauncherOption and LauncherPath [OC 0.6.6+]
        if ($this->plistObj->hasValue('Misc/Security/BootProtect')) {
            $this->addUpgradableItem('bootprot');
            $this->addToLog('Replace BootProtect with LauncherOption and LauncherPath [OpenCore 0.6.6+]');
        }

        // remove KeyMergeThreshold [OC 0.6.7+]
        if ($this->plistObj->hasValue('UEFI/Input/KeyMergeThreshold')) {
            $this->addUpgradableItem('keymergeth');
            $this->addToLog('Remove KeyMergeThreshold [OpenCore 0.6.7+]');
        }

        // remove AppleEvent from ProtocolOverrides [0.6.8+]
        if ($this->plistObj->hasValue('UEFI/ProtocolOverrides/AppleEvent')) {
            $this->addUpgradableItem('applevent');
            $this->addToLog('Remove AppleEvent from ProtocolOverrides [OpenCore 0.6.8+]');
        }

        // change CustomDelays from string to bool [0.6.9+]
        if ($this->plistObj->hasValue('UEFI/AppleInput/CustomDelays')) {
            $val = $this->plistObj->getVals('UEFI/AppleInput/CustomDelays');

            if ($val->getType() === 'string') {
                $this->addUpgradableItem('custdelay');
                $this->addToLog('Change CustomDelays from string to bool [OpenCore 0.6.9+]');
            }
        }

        // change GopPassThrough from bool to string [0.7.0+]
        if ($this->plistObj->hasValue('UEFI/Output/GopPassThrough')) {
            $val = $this->plistObj->getVals('UEFI/Output/GopPassThrough');

            if ($val->getType() === 'bool') {
                $this->addUpgradableItem('goppasst');
                $this->addToLog('Change GopPassThrough from bool to string [OpenCore 0.7.0+]');
            }
        }

        // replace AdviseWindows with AdviseFeatures [0.7.0+]
        if ($this->plistObj->hasValue('PlatformInfo/Generic/AdviseWindows')) {
            $this->addUpgradableItem('advisewin');
            $this->addToLog('Replace AdviseWindows with AdviseFeatures [OpenCore 0.7.0+]');
        }

        // update UEFI Drivers plist type [0.7.3+]
        if ($this->plistObj->hasValue('UEFI/Drivers')) {
            $drvList = $this->plistObj->getVals('UEFI/Drivers');

            if (isset($drvList[0]) && !is_array($drvList[0])) {
                $this->addUpgradableItem('udrv');
                $this->addToLog('Update UEFI Drivers plist type [0.7.3+]');
            }
        }

        // remove AudioOut [0.7.7+]
        if ($this->plistObj->hasValue('UEFI/Audio/AudioOut')) {
            $this->addUpgradableItem('audioout');
            $this->addToLog('Remove AudioOut. Please set your AudioOutMask manually. [0.7.7+]');
        }

        // remove MinimumVolume [0.7.7+]
        if ($this->plistObj->hasValue('UEFI/Audio/MinimumVolume')) {
            $this->addUpgradableItem('minvol');
            $this->addToLog('Remove MinimumVolume. Please set MinimumAudibleGain manually. [0.7.7+]');
        }

        // remove VolumeAmplifier [0.7.7+]
        if ($this->plistObj->hasValue('UEFI/Audio/VolumeAmplifier')) {
            $this->addUpgradableItem('volampl');
            $this->addToLog('Remove VolumeAmplifier. [0.7.7+]');
        }

        // remove serial init from Misc/Debug [0.8.0+]
        if ($this->plistObj->hasValue('Misc/Debug/SerialInit')) {
            $this->addUpgradableItem('serialin');
            $this->addToLog('Remove serial init from Misc/Debug [0.8.0+]');
        }

        // implement Reset NVRAM and Toggle SIP as drivers [0.8.1+]
        if ($this->plistObj->hasValue('Misc/Security/AllowToggleSip') || $this->plistObj->hasValue('Misc/Security/AllowNvramReset')) {
            $this->addUpgradableItem('sipdrivers');
            $this->addToLog('Remove AllowToggleSip and AllowNvramReset options (now drivers) [0.8.1+]');
        }

        return !empty($this->upgradables);
    }

    /**
     * Upgrade outdated keys
     *
     * @throws \CFPropertyList\PListInvalidDataConversionException
     */
    public function upgradePlist(): void {
        foreach ($this->upgradables as $upgrKey) {
            switch ($upgrKey) {
                case 'exbpath': {
                    $this->plistObj->unsetVal('Misc/Debug', 'ExposeBootPath');
                    $this->plistObj->setVal('Misc/Security', 'ExposeSensitiveData', 6);
                }
                    break;
                case 'matchkern': {
                    $configPaths = ['Kernel/Add', 'Kernel/Block', 'Kernel/Patch'];

                    foreach ($configPaths as $cp) {
                        $data = $this->plistObj->getVals($cp);
                        $i = -1;

                        if ($data === null)
                            continue;

                        foreach ($data as $itm) {
                            ++$i;

                            if (!isset($itm['MatchKernel']))
                                continue;

                            $path = "{$cp}/{$i}";

                            $this->plistObj->unsetVal($path, 'MatchKernel');
                            $this->plistObj->setVal($path, 'MinKernel', $itm['MatchKernel']);
                            $this->plistObj->setVal($path, 'MaxKernel', $itm['MatchKernel']);
                        }
                    }
                }
                    break;
                case '3rdtrim': {
                    $trim = $this->plistObj->getRawVals('Kernel/Quirks/ThirdPartyTrim');

                    $this->plistObj->unsetVal('Kernel/Quirks', 'ThirdPartyTrim');

                    if (!$this->plistObj->hasValue('Kernel/Quirks/ThirdPartyDrives'))
                        $this->plistObj->setVal('Kernel/Quirks', 'ThirdPartyDrives', $trim);
                }
                    break;
                case 'highalloc': {
                    $this->plistObj->unsetVal('UEFI/Quirks', 'AvoidHighAlloc');
                }
                    break;
                case 'reqsign': {
                    $reqsign = $this->plistObj->getRawVals('Misc/Security/RequireSignature');
                    $reqvault = $this->plistObj->getRawVals('Misc/Security/RequireVault');
                    $vault = $reqsign === true && $reqvault === true ? 'Secure':'';

                    $this->plistObj->unsetVal('Misc/Security', 'RequireSignature');
                    $this->plistObj->unsetVal('Misc/Security', 'RequireVault');

                    $this->plistObj->setVal('Misc/Security', 'Vault', $vault);
                }
                    break;
                case 'consbehv': {
                    $this->plistObj->unsetVal('Misc/Boot', 'ConsoleBehaviourOs');
                    $this->plistObj->unsetVal('Misc/Boot', 'ConsoleBehaviourUi');
                }
                    break;
                case 'movtoout': {
                    $cmode = $this->plistObj->getRawVals('Misc/Boot/ConsoleMode');
                    $res = $this->plistObj->getRawVals('Misc/Boot/Resolution');

                    $this->plistObj->unsetVal('Misc/Boot', 'ConsoleMode');
                    $this->plistObj->unsetVal('Misc/Boot', 'Resolution');

                    if (!$this->plistObj->hasValue('UEFI/Output/ConsoleMode'))
                        $this->plistObj->setVal('UEFI/Output', 'ConsoleMode', $cmode);

                    if (!$this->plistObj->hasValue('UEFI/Output/Resolution'))
                        $this->plistObj->setVal('UEFI/Output', 'Resolution', $res);
                }
                    break;
                case 'txrender': {
                    $this->plistObj->unsetVal('UEFI/Protocols', 'ConsoleControl');
                    $this->plistObj->unsetVal('Misc/Boot', 'BuiltinTextRenderer');

                    if (!$this->plistObj->hasValue('UEFI/Output/TextRenderer'))
                        $this->plistObj->setVal('UEFI/Output', 'TextRenderer', 'BuiltinGraphics');
                }
                    break;
                case 'movout2': {
                    $cscr = $this->plistObj->getRawVals('UEFI/Quirks/ClearScreenOnModeSwitch');
                    $ignTxG = $this->plistObj->getRawVals('UEFI/Quirks/IgnoreTextInGraphics');
                    $cgop = $this->plistObj->getRawVals('UEFI/Quirks/ProvideConsoleGop');
                    $resChng = $this->plistObj->getRawVals('UEFI/Quirks/ReconnectOnResChange');
                    $tabs = $this->plistObj->getRawVals('UEFI/Quirks/ReplaceTabWithSpace');
                    $clrScr = $this->plistObj->getRawVals('UEFI/Quirks/SanitiseClearScreen');

                    $this->plistObj->unsetVal('UEFI/Quirks', 'ClearScreenOnModeSwitch');
                    $this->plistObj->unsetVal('UEFI/Quirks', 'IgnoreTextInGraphics');
                    $this->plistObj->unsetVal('UEFI/Quirks', 'ProvideConsoleGop');
                    $this->plistObj->unsetVal('UEFI/Quirks', 'ReconnectOnResChange');
                    $this->plistObj->unsetVal('UEFI/Quirks', 'ReplaceTabWithSpace');
                    $this->plistObj->unsetVal('UEFI/Quirks', 'SanitiseClearScreen');

                    $this->plistObj->setVal('UEFI/Output', 'ClearScreenOnModeSwitch', $cscr ?? false);
                    $this->plistObj->setVal('UEFI/Output', 'IgnoreTextInGraphics', $ignTxG ?? false);
                    $this->plistObj->setVal('UEFI/Output', 'ProvideConsoleGop', $cgop ?? false);
                    $this->plistObj->setVal('UEFI/Output', 'ReconnectOnResChange', $resChng ?? false);
                    $this->plistObj->setVal('UEFI/Output', 'ReplaceTabWithSpace', $tabs ?? false);
                    $this->plistObj->setVal('UEFI/Output', 'SanitiseClearScreen', $clrScr ?? false);
                }
                    break;
                case 'ueconsole': {
                    $this->plistObj->unsetVal('UEFI/Output', 'ProvideEarlyConsole');
                }
                    break;
                case 'uscale': {
                    $this->plistObj->unsetVal('UEFI/Output', 'Scale');
                }
                    break;
                case 'csmreg': {
                    $csmRegV = $this->plistObj->getRawVals('Booter/Quirks/ProtectCsmRegion');

                    if ($csmRegV !== null) {
                        $this->plistObj->unsetVal('Booter/Quirks', 'ProtectCsmRegion');
                        $this->plistObj->setVal('Booter/Quirks', 'ProtectMemoryRegions', $csmRegV);
                    }
                }
                    break;
                case 'shrinkmem': {
                    $shrinkV = $this->plistObj->getRawVals('Booter/Quirks/ShrinkMemoryMap');

                    if ($shrinkV !== null) {
                        $this->plistObj->unsetVal('Booter/Quirks', 'ShrinkMemoryMap');
                        $this->plistObj->setVal('Booter/Quirks', 'RebuildAppleMemoryMap', $shrinkV);
                    }
                }
                    break;
                case 'typojump': {
                    $jumpVal = $this->plistObj->getRawVals('UEFI/APFS/EnableJumpStart');

                    if ($jumpVal !== null) {
                        $this->plistObj->unsetVal('UEFI/APFS', 'EnableJumpStart');
                        $this->plistObj->setVal('UEFI/APFS', 'EnableJumpstart', $jumpVal);
                    }
                }
                    break;
                case 'protocols': {
                    $protocols = $this->plistObj->getVals('UEFI/Protocols');

                    if ($protocols !== null) {
                        $this->plistObj->unsetVal('UEFI', 'Protocols');
                        $this->plistObj->setVal('UEFI', 'ProtocolOverrides', $protocols);
                    }
                }
                    break;
                case 'reqbvarflbk': {
                    $this->plistObj->unsetVal('UEFI/Quirks', 'RequestBootVarFallback');
                }
                    break;
                case 'dirgopmode': {
                    $this->plistObj->unsetVal('UEFI/Output', 'DirectGopCacheMode');
                }
                    break;
                case 'blockdel': {
                    $acpiBlk = $this->plistObj->getVals('ACPI/Block');
                    $devBlk = $this->plistObj->getVals('DeviceProperties/Block');
                    $nvramBlk = $this->plistObj->getVals('NVRAM/Block');

                    if ($acpiBlk !== null) {
                        $this->plistObj->unsetVal('ACPI', 'Block');
                        $this->plistObj->setVal('ACPI', 'Delete', $acpiBlk);
                    }

                    if ($devBlk !== null) {
                        $this->plistObj->unsetVal('DeviceProperties', 'Block');
                        $this->plistObj->setVal('DeviceProperties', 'Delete', $devBlk);
                    }

                    if ($nvramBlk !== null) {
                        $this->plistObj->unsetVal('NVRAM', 'Block');
                        $this->plistObj->setVal('NVRAM', 'Delete', $nvramBlk);
                    }
                }
                    break;
                case 'hideself': {
                    $this->plistObj->unsetVal('Misc/Boot', 'HideSelf');
                }
                    break;
                case 'dedupbord': {
                    $this->plistObj->unsetVal('UEFI/Quirks', 'DeduplicateBootOrder');
                }
                    break;
                case 'bootprot': {
                    $this->plistObj->unsetVal('Misc/Security', 'BootProtect');
                    $this->plistObj->setVal('Misc/Boot', 'LauncherOption', 'Disabled');
                    $this->plistObj->setVal('Misc/Boot', 'LauncherPath', 'Default');
                }
                    break;
                case 'keymergeth': {
                    $this->plistObj->unsetVal('UEFI/Input', 'KeyMergeThreshold');
                }
                    break;
                case 'applevent': {
                    $this->plistObj->unsetVal('UEFI/ProtocolOverrides', 'AppleEvent');
                }
                    break;
                case 'custdelay': {
                    $val = $this->plistObj->getRawVals('UEFI/AppleInput/CustomDelays');
                    $keySupport = $this->plistObj->getRawVals('UEFI/Input/KeySupport');

                    $this->plistObj->unsetVal('UEFI/AppleInput', 'CustomDelays');
                    $this->plistObj->setVal('UEFI/AppleInput', 'CustomDelays', ($val === 'Auto' && $keySupport === true) || $val === 'Enabled');
                }
                    break;
                case 'goppasst': {
                    $val = $this->plistObj->getRawVals('UEFI/Output/GopPassThrough');

                    $this->plistObj->unsetVal('UEFI/Output', 'GopPassThrough');
                    $this->plistObj->setVal('UEFI/Output', 'GopPassThrough', $val === true ? 'Enabled':'Disabled');
                }
                    break;
                case 'advisewin': {
                    $val = $this->plistObj->getRawVals('PlatformInfo/Generic/AdviseWindows');

                    $this->plistObj->unsetVal('PlatformInfo/Generic', 'AdviseWindows');
                    $this->plistObj->setVal('PlatformInfo/Generic', 'AdviseFeatures', $val === true);
                }
                    break;
                case 'udrv': {
                    $dlist = $this->plistObj->getVals('UEFI/Drivers');

                    $this->plistObj->unsetVal('UEFI', 'Drivers');

                    for ($i=0,$l=count($dlist); $i<$l; ++$i)
                        $this->plistObj->setVal('UEFI/Drivers', strval($i), ['Path' => $dlist[$i], 'Enabled' => new CFBoolean(), 'Arguments' => new CFString()]);
                }
                    break;
                case 'audioout': {
                    $this->plistObj->unsetVal('UEFI/Audio', 'AudioOut');
                }
                    break;
                case 'minvol': {
                    $this->plistObj->unsetVal('UEFI/Audio', 'MinimumVolume');
                }
                    break;
                case 'volampl': {
                    $this->plistObj->unsetVal('UEFI/Audio', 'VolumeAmplifier');
                }
                    break;
                case 'serialin': {
                    $serialInit = $this->plistObj->getRawVals('Misc/Debug/SerialInit');

                    $this->plistObj->unsetVal('Misc/Debug', 'SerialInit');
                    $this->plistObj->setVal('Misc/Serial', 'Init', $serialInit === true);
                }
                    break;
                case 'sipdrivers': {
                    $this->plistObj->unsetVal('Misc/Security', 'AllowNvramReset');
                    $this->plistObj->unsetVal('Misc/Security', 'AllowToggleSip');
                }
                    break;
                default:
                    break;
            }
        }
    }
}
