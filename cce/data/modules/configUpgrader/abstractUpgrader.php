<?php
/**
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\Upgrader;

use CCE\abstractPlist;

abstract class abstractUpgrader {
    /**
     * A reference to a config object
     *
     * @var ?abstractPlist $plistObj
     */
    protected ?abstractPlist $plistObj = null;

    /**
     * A list of outdated items
     *
     * @var array $upgradables
     */
    protected array $upgradables = [];

    /**
     * Flag to enable upgrader logging
     *
     * @var bool $enableUpgraderLog
     */
    protected bool $enableUpgraderLog = false;

    /**
     * Upgrade changelog
     *
     * @var array $upgradeLog
     */
    private array $upgradeLog = [];

    /**
     * Check for outdated keys
     *
     * @return bool
     */
    abstract function checkUpgrade(): bool;

    /**
     * Upgrade outdated keys
     */
    abstract function upgradePlist(): void;

    /**
     * Add a log message to $upgraderLog
     *
     * @param string $msg
     */
    protected function addToLog(string $msg): void {
        if ($this->enableUpgraderLog)
            array_push($this->upgradeLog, $msg);
    }

    /**
     * Add an item to $upgradables
     *
     * @param string $item
     */
    protected function addUpgradableItem(string $item): void {
        array_push($this->upgradables, $item);
    }

    /**
     * Reset upgradables array
     */
    protected function resetUpgradables(): void {
        $this->upgradables = [];
    }

    /**
     * Get the istance of an upgrader object
     *
     * @param string $configType
     * @param abstractPlist $plistObj
     * @param boolean $enableLog
     *
     * @return ?abstractUpgrader
     */
    public static function getUpgraderObj(string $configType, abstractPlist $plistObj, bool $enableLog=false): ?abstractUpgrader {
        return match ($configType) {
            'cce' => new cloverEFIUpgrader($plistObj, $enableLog),
            'oc' => new OpenCoreUpgrader($plistObj, $enableLog),
            default => null
        };
    }

    /**
     * Return the upgrade log
     *
     * @return array
     */
    public function getUpgradeLog(): array {
        return $this->upgradeLog;
    }
}
