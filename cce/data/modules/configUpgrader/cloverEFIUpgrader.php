<?php
/**
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace CCE\Upgrader;

use CCE\abstractPlist;

class cloverEFIUpgrader extends abstractUpgrader {
    /**
     * DSDT fixes upgrade array
     *
     * @var array $dsdtFixesList
     */
    private array $dsdtFixesList = [
        'AddDTGP_0001' => 'AddDTGP',
        'FIX_DARWIN_10000' => 'FixDarwin',
        'FixShutdown_0004' => 'FixShutdown',
        'AddMCHC_0008' => 'AddMCHC',
        'FixHPET_0010' => 'FixHPET',
        'FakeLPC_0020' => 'FakeLPC',
        'FixIPIC_0040' => 'FixIPIC',
        'FixSBUS_0080' => 'FixSBUS',
        'FixDisplay_0100' => 'FixDisplay',
        'FixIDE_0200' => 'FixIDE',
        'FixSATA_0400' => 'FixSATA',
        'FixFirewire_0800' => 'FixFirewire',
        'FixUSB_1000' => 'FixUSB',
        'FixLAN_2000' => 'FixLAN',
        'FixAirport_4000' => 'FixAirport',
        'FixHDA_8000' => 'FixHDA',
        'FixDarwin7_10000' => 'FixDarwin7',
        'FIX_RTC_20000' => 'FixRTC',
        'FIX_TMR_40000' => 'FixTMR',
        'AddIMEI_80000' => 'AddIMEI',
        'FIX_INTELGFX_100000' => 'FixIntelGfx',
        'FIX_WAK_200000' => 'FixWAK',
        'DeleteUnused_400000' => 'DeleteUnused',
        'FIX_ADP1_800000' => 'FixADP1',
        'AddPNLF_1000000' => 'AddPNLF',
        'FIX_S3D_2000000' => 'FixS3D',
        'FIX_ACST_4000000' => 'FixACST',
        'AddHDMI_8000000' => 'AddHDMI',
        'FixRegions_10000000' => 'FixRegions',
        'FixHeaders_20000000' => 'FixHeaders',
    ];

    /**
     * cloverEFIUpgrader constructor
     *
     * @param abstractPlist $plistObj
     * @param boolean $enableLog
     */
    protected function __construct(abstractPlist $plistObj, bool $enableLog) {
        $this->plistObj = $plistObj;
        $this->enableUpgraderLog = $enableLog;
    }

    /**
     * Check for outdated keys
     *
     * @return bool
     */
    public function checkUpgrade(): bool {
        $this->resetUpgradables();

        // EDID [cloverEFI 3737+]
        if ($this->plistObj->hasValue('Graphics/InjectEDID') || $this->plistObj->hasValue('Graphics/CustomEDID')) {
            $this->addUpgradableItem('EDID');
            $this->addToLog('Move InjectEDID and/or CustomEDID to Graphics/EDID [Clover EFI 3737+]');
        }

        // default boot bg color [cloverEFI 3830+]
        if ($this->plistObj->hasValue('SystemParameters/DefaultBackgroundColor')) {
            $this->addUpgradableItem('sysDefBgColor');
            $this->addToLog('Move SystemParameters/DefaultBackgroundColor to BootGraphics/DefaultBackgroundColor [Clover EFI 3830+]');
        }

        // new Way Flag [cloverEFI 4006+]
        if ($this->plistObj->hasValue('ACPI/DSDT/Fixes/NewWay_80000000') || $this->plistObj->hasValue('ACPI/DSDT/Fixes/FixDarwin_0002')) {
            $this->addUpgradableItem('newWayFix');
            $this->addToLog('Remove deprecated flag NewWay_80000000 [Clover EFI 4006+]');
        }

        // Haswell-E Patch [cloverEFI 4145+]
        if ($this->plistObj->hasValue('KernelAndKextPatches/KernelHaswellE')) {
            $this->addUpgradableItem('haswellEP');
            $this->addToLog('Remove deprecated flag KernelHaswellE [Clover EFI 4145+]');
        }

        // AsusAICPUPM key [cloverEFI 4152+]
        if ($this->plistObj->hasValue('KernelAndKextPatches/AsusAICPUPM')) {
            $this->addUpgradableItem('aicpupm');
            $this->addToLog('Rename AsusAICPUPM to AppleIntelCPUPM [Clover EFI 4152+]');
        }

        // KernelIvyXCPM key [cloverEFI 4250+]
        if ($this->plistObj->hasValue('KernelAndKextPatches/KernelIvyXCPM')) {
            $this->addUpgradableItem('ivyxcpm');
            $this->addToLog('Rename KernelIvyXCPM to KernelXCPM [Clover EFI 4250+]');
        }

        // new DSDT fixes keys [cloverEFI 4282+]
        foreach ($this->dsdtFixesList as $oldWay => $newWay) {
            if (!$this->plistObj->hasValue('ACPI/DSDT/Fixes/'.$oldWay))
                continue;

            $this->addUpgradableItem('prettyDFix');
            $this->addToLog('Prettify DSDT fixes names (FIX_YYYY_XXXX to FIXYYYY) [Clover EFI 4282+]');
            break;
        }

        // FixHeaders path [cloverEFI 4427+]
        if ($this->plistObj->hasValue('ACPI/DSDT/Fixes/FixHeaders')) {
            $this->addUpgradableItem('fixhead');
            $this->addToLog('Correct FixHeaders path [Clover EFI 4427+]');
        }

        // InjectLAN [cloverEFI 4507+]
        if ($this->plistObj->hasValue('Devices/InjectLAN')) {
            $this->addUpgradableItem('injlan');
            $this->addToLog('Rename InjectLAN to LANInjection [Clover EFI 4507+]');
        }

        // InjectHDMI [cloverEFI 4507+]
        if ($this->plistObj->hasValue('Devices/InjectHDMI')) {
            $this->addUpgradableItem('injhdmi');
            $this->addToLog('Rename InjectHDMI to HDMIInjection [Clover EFI 4507+]');
        }

        // DropOEM DSM [cloverEFI 5017+]
        if ($this->plistObj->hasValue('ACPI/DSDT/DropOEM_DSM')) {
            $this->addUpgradableItem('dropdsm');
            $this->addToLog('Remove DropOEM_DSM [Clover EFI 5017+]');
        }

        // KernelCPU [cloverEFI 5017+]
        if ($this->plistObj->hasValue('KernelAndKextPatches/KernelCpu')) {
            $this->addUpgradableItem('kernelcpu');
            $this->addToLog('Remove Kernel CPU patch [Clover EFI 5017+]');
        }

        // Remove obsolete PluginType [cloverEFI 5129+]
        if ($this->plistObj->hasValue('ACPI/SSDT/PluginType')) {
            $this->addUpgradableItem('plgtype');
            $this->addToLog('Remove obsolete PluginType [cloverEFI 5129+]');
        }

        // RenameDevices array [cloverEFI 5130+]
        if ($this->plistObj->hasValue('ACPI/RenameDevices')) {
            $obj = $this->plistObj->getRawVals('ACPI/RenameDevices') ?? [];
            $hasArray = count($obj) > 0 && key_exists(0, $obj) && is_array($obj[0]);

            if (!$hasArray) {
                $this->addUpgradableItem('rendevsarr');
                $this->addToLog('RenameDevices is now array [Clover EFI 5130+]');
            }
        }

        // Move ProvideConsoleGopEnable to GUI/ProvideConsoleGop [cloverEFI 5134+]
        if ($this->plistObj->hasValue('Quirks/ProvideConsoleGopEnable')) {
            $this->addUpgradableItem('provcgop');
            $this->addToLog('Move Quirks/ProvideConsoleGopEnable to GUI/ProvideConsoleGop [cloverEFI 5134+]');
        }

        // Remove BooterCfg [cloverEFI 5134+]
        if ($this->plistObj->hasValue('RtVariables/BooterCfg')) {
            $this->addUpgradableItem('btrcfgcmd');
            $this->addToLog('Remove BooterCfg [cloverEFI 5134+]');
        }

        return !empty($this->upgradables);
    }

    /**
     * Upgrade outdated keys
     *
     * @throws \CFPropertyList\PListInvalidDataConversionException
     */
    public function upgradePlist(): void {
        foreach ($this->upgradables as $upgrKey) {
            switch ($upgrKey) {
                case 'EDID': {
                    $inject = $this->plistObj->getRawVals('Graphics/InjectEDID');
                    $custom = $this->plistObj->getRawVals('Graphics/CustomEDID');

                    if ($inject !== null) {
                        $this->plistObj->unsetVal('Graphics', 'InjectEDID');
                        $this->plistObj->setVal('Graphics/EDID', 'Inject', $inject, 'bool');
                    }

                    if ($custom !== null) {
                        $this->plistObj->unsetVal('Graphics', 'CustomEDID');
                        $this->plistObj->setVal('Graphics/EDID', 'Custom', $custom);
                    }
                }
                    break;
                case 'sysDefBgColor': {
                    $sysDefBgColor = $this->plistObj->getRawVals('SystemParameters/DefaultBackgroundColor');

                    $this->plistObj->unsetVal('SystemParameters', 'DefaultBackgroundColor');
                    $this->plistObj->setVal('BootGraphics', 'DefaultBackgroundColor', $sysDefBgColor);
                }
                    break;
                case 'newWayFix': {
                    $darwinVal = $this->plistObj->getRawVals('ACPI/DSDT/Fixes/FixDarwin_0002');

                    $this->plistObj->unsetVal('ACPI/DSDT/Fixes', 'NewWay_80000000');

                    if ($darwinVal !== null) {
                        $this->plistObj->unsetVal('ACPI/DSDT/Fixes', 'FixDarwin_0002');
                        $this->plistObj->setVal('ACPI/DSDT/Fixes', 'FIX_DARWIN_10000', $darwinVal, 'bool');
                    }
                }
                    break;
                case 'haswellEP': {
                    $this->plistObj->unsetVal('KernelAndKextPatches', 'KernelHaswellE');
                }
                    break;
                case 'aicpupm': {
                    $aicpuVal = $this->plistObj->getRawVals('KernelAndKextPatches/AsusAICPUPM');

                    $this->plistObj->unsetVal('KernelAndKextPatches', 'AsusAICPUPM');
                    $this->plistObj->setVal('KernelAndKextPatches', 'AppleIntelCPUPM', $aicpuVal);
                }
                    break;
                case 'ivyxcpm': {
                    $xcpmVal = $this->plistObj->getRawVals('KernelAndKextPatches/KernelIvyXCPM');

                    $this->plistObj->unsetVal('KernelAndKextPatches', 'KernelIvyXCPM');
                    $this->plistObj->setVal('KernelAndKextPatches', 'KernelXCPM', $xcpmVal);
                }
                    break;
                case 'prettyDFix': {
                    foreach ($this->dsdtFixesList as $oldWay => $newWay) {
                        $curVal = $this->plistObj->getRawVals('ACPI/DSDT/Fixes/'.$oldWay);

                        if ($curVal !== null) {
                            $this->plistObj->unsetVal('ACPI/DSDT/Fixes', $oldWay);
                            $this->plistObj->setVal('ACPI/DSDT/Fixes', $newWay, $curVal, 'bool');
                        }
                    }
                }
                    break;
                case 'fixhead': {
                    $fixheadVal = $this->plistObj->getRawVals('ACPI/DSDT/Fixes/FixHeaders');

                    $this->plistObj->unsetVal('ACPI/DSDT/Fixes', 'FixHeaders');
                    $this->plistObj->setVal('ACPI', 'FixHeaders', $fixheadVal);
                }
                    break;
                case 'injlan': {
                    $injLanVal = $this->plistObj->getRawVals('Devices/InjectLAN');

                    $this->plistObj->unsetVal('Devices', 'InjectLAN');
                    $this->plistObj->setVal('Devices', 'LANInjection', $injLanVal);
                }
                    break;
                case 'injhdmi': {
                    $injHdmiVal = $this->plistObj->getRawVals('Devices/InjectHDMI');

                    $this->plistObj->unsetVal('Devices', 'InjectHDMI');
                    $this->plistObj->setVal('Devices', 'HDMIInjection', $injHdmiVal);
                }
                    break;
                case 'dropdsm': {
                    $this->plistObj->unsetVal('ACPI/DSDT', 'DropOEM_DSM');
                }
                    break;
                case 'kernelcpu': {
                    $this->plistObj->unsetVal('KernelAndKextPatches', 'KernelCpu');
                }
                    break;
                case 'plgtype': {
                    $this->plistObj->unsetVal('ACPI/SSDT', 'PluginType');
                }
                    break;
                case 'rendevsarr': {
                    $devices = $this->plistObj->getRawVals('ACPI/RenameDevices');
                    $i = 0;

                    $this->plistObj->unsetVal('ACPI', 'RenameDevices');

                    foreach ($devices as $oldD => $newD)
                        $this->plistObj->setVal('ACPI/RenameDevices/'.($i++), strval($oldD), $newD);
                }
                    break;
                case 'provcgop': {
                    $cgopVal = $this->plistObj->getRawVals('Quirks/ProvideConsoleGopEnable');

                    $this->plistObj->unsetVal('Quirks', 'ProvideConsoleGopEnable');
                    $this->plistObj->setVal('GUI', 'ProvideConsoleGop', $cgopVal);
                }
                    break;
                case 'btrcfgcmd': {
                    $this->plistObj->unsetVal('RtVariables', 'BooterCfg');
                }
                    break;
                default:
                    break;
            }
        }
    }
}
