<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/../cce-cache/CCECache.php';

class TextContainer implements ArrayAccess {
    private array $container = [];
    private string $language = '';

    private function getBrowserLanguage(): string {
        $browserLang = filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE');

        if (gettype($browserLang) !== 'string')
            return '';

        return substr($browserLang, 0, 2);
    }

    /**
     * Set language
     *
     * @throws Exception
     */
    private function setLanguage(): void {
        $prefLang = $this->getBrowserLanguage();
        $cachePath = \CCE\CCECache::getInstance()->getCachePath();

        if ($prefLang === '' || !file_exists("{$cachePath}/{$prefLang}")) {
            $prefLang = 'en';

            if (!file_exists($cachePath.'/en'))
                throw new Exception("Missing text files for language: {$prefLang}");
        }

        $this->language = $prefLang;
    }

    public function __construct($cceMode) {
        try {
            $this->setLanguage();

            $cachePath = \CCE\CCECache::getInstance()->getCachePath();
            $this->container = (require_once "{$cachePath}/{$this->language}/{$cceMode}_lang.php");

        } catch (Throwable $e) {
            fatalError($e);
        }
    }

    public function offsetUnset($offset): void {} // not allowed

    public function offsetSet($offset, $value): void {
        if (is_null($offset))
            $this->container[] = $value;
        else
            $this->container[$offset] = $value;
    }

    public function offsetExists($offset): bool {
        return isset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return $this->container[$offset] ?? null;
    }

    public function getLanguage(): string {
        return $this->language;
    }
}
