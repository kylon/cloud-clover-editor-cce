<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// DO NOT TRANSLATE => TRANSLATE
return [
    // Chameleon
    'chdp_DropSSDT' => 'Drop SSDT',
    'chdp_DropHPET' => 'Drop HPET',
    'chdp_DropSLIC' => 'Drop SLIC',
    'chdp_DropSBST' => 'Drop SBST',
    'chdp_DropECDT' => 'Drop ECDT',
    'chdp_DropASFT' => 'Drop ASFT',
    'chdp_DropDMAR' => 'Drop DMAR',
    'chdp_DropBGRT' => 'Drop BGRT',
    'chdp_DropMCFG' => 'Drop MCFG',
    'chdp_DropAPIC' => 'Drop APIC',
    'chms_ForceHPET' => 'Force HPET',
    'chms_EthernetBuiltIn' => 'Ethernet BuiltIn',
    'chms_EnableWifi' => 'Enable Wifi',
    'chms_USBBusFix' => 'USB Bus Fix',
    'chms_EHCIacquire' => 'EHCI Acquire',
    'chms_UHCIreset' => 'UHCI Reset',
    'chms_EHCIhard' => 'EHCI Hard Reset',
    'chms_USBLegacyOff' => 'USB Legacy Off',
    'chms_XHCILegacyOff' => 'XHCI Legacy Off',
    'chms_Wake' => 'Wake',
    'chms_ForceWake' => 'Force Wake',
    'wake_img' => 'Wake Image',
    'chss_GenerateCStates' => 'Generate C States',
    'chss_GeneratePStates' => 'Generate P States',
    'chss_CSTUsingSystemIO' => 'CST Using System IO',
    'chss_EnableC2State' => 'Enable C2 State',
    'chss_EnableC3State' => 'Enable C3 State',
    'chss_EnableC4State' => 'Enable C4 State',
    'chss_EnableC6State' => 'Enable C6 State',
    'chss_EnableC7State' => 'Enable C7 State',
    'cpu_bus_ratio' => 'Bus Ratio',
    'chau_HDAEnabler' => 'HDA Enabler',
    'chau_EnableHDMIAudio' => 'Enable HDMI Audio (Nvidia/ATI)',
    'chau_UseIntelHDMI' => 'Use Intel HDMI',
    'hdef_id' => 'HDEF Layout ID',
    'hdau_id' => 'HDAU Layout ID',
    'graphics' => 'Graphics',
    'chgx_GraphicsEnabler' => 'Graphics Enabler',
    'chgx_SkipIntelGfx' => 'Skip Intel Graphics',
    'chgx_SkipNvidiaGfx' => 'Skip Nvidia Graphics',
    'chgx_SkipAtiGfx' => 'Skip Ati Graphics',
    'chgx_UseAtiROM' => 'Use Alternate ATI ROM',
    'chgx_UseNvidiaROM' => 'Use Alternate Nvidia ROM',
    'chgx_EnableBacklight' => 'Enable Backlight Option (Nvidia/ATI)',
    'chgx_EnableDualLink' => 'Enable Dual Link Option (Nvidia/ATI)',
    'chgx_NvidiaGeneric' => 'Nvidia Generic',
    'chgx_NvidiaSingle' => 'Nvidia Single',
    'chgx_NvidiaNoEFI' => 'Nvidia No EFI',
    'chgx_VBIOS' => 'Inject VBIOS',
    'ati_config' => 'ATI Config',
    'ati_ports' => 'ATI Ports',
    'nv_display0' => 'Nvidia display-cfg 0',
    'nv_display1' => 'Nvidia display-cfg 1',
    'capri_fb' => 'Ivy Bridge FB',
    'azul_fb' => 'Haswell FB',
    'bdw_fb' => 'Broadwell FB',
    'skl_fb' => 'Skylake FB',
    'chgu_GUI' => 'Show GUI',
    'chgu_Boot Banner' => 'Show Boot Banner',
    'chgu_ShowInfo' => 'Show Info',
    'chgu_Legacy Logo' => 'Legacy Logo',
    'chgu_Wait' => 'Wait for key press',
    'chgu_Quiet Boot' => 'Quiet Boot',
    'chgu_Instant Menu' => 'Instant Menu',
    'chgu_EnableHiDPI' => 'Enable HiDPI',
    'chgu_BlackMode' => 'Black Mode',
    'chgu_Scan Single Drive' => 'Scan Single Drive',
    'chgu_Rescan' => 'CD-ROM Rescan Mode',
    'chgu_Rescan Prompt' => 'CD-ROM Rescan Prompt',
    'def_partition' => 'Default Partition',
    'hide_partition' => 'Hide Partition',
    'ren_partition' => 'Rename Partition',
    'key_layout' => 'Key Layout',
    'gfx_mode' => 'Graphics Mode',
    'text_mode' => 'Text Mode',
    'system' => 'System',
    'chsy_RebootOnPanic' => 'Reboot On Panic',
    'chsy_UseKernelCache' => 'Use Kernel Cache',
    'chsy_UseMemDetect' => 'Use Mem Detect',
    'chsy_PrivateData' => 'Private Data',
    'chsy_RestartFix' => 'Restart Fix',
    'chsy_PS2RestartFix' => 'PS2 Restart Fix',
    'chsy_ForceFullMemInfo' => 'Force Full Mem Info',
    'chsy_SMBIOSdefaults' => 'SMBIOS defaults',
    'kernel_name' => 'Kernel Name',
    'chka_auto' => 'Auto',
    'chka_32' => 'i386',
    'chka_64' => 'x86_64',
    'pci_root' => 'Alternate Pci Root',


    // Kernel and kext patches

    'chkp_SkipKP' => 'Skip Kernel Patcher',
    'chkp_KernelPm' => 'Kernel PM Patch',
    'chkp_KernelBooter_kexts' => 'Kernel Booter kexts',
    'chkp_KernelLapicError' => 'Kernel Lapic Error',
    'chkp_KernelLapicVersion' => 'Kernel Lapic Version',
    'chkp_KernelHaswell' => 'Kernel Haswell',
    'chkp_kKernelcpuFamily' => 'Kernel CPU Family',
    'chkp_KernelSSE3' => 'Kernel SSE3',
    'chkx_SkipKextsPatcher' => 'Skip Kexts Patcher',
    'chkx_AICPMPatch' => 'AppleIntelCPUPM Patch',
    'chkx_AppleRTCPatch' => 'AppleRTC Patch',
    'chkx_OrangeIconFixSata' => 'Orange Icon Fix Sata',
    'chkx_TrimEnablerSata' => 'Trim Enabler Sata',
    'chkx_NVIDIAWebDrv' => 'NVIDIA Web Driver',
    'chkx_AppleHDABinPatch' => 'AppleHDA Bin Patch',
    'kext_name' => 'Kext Name',


    // SMBIOS
    'board_location' => 'Board Location',
    'chass_serialno' => 'Chassis Serial Number',
    'external_clock' => 'External Clock',
    'maximal_clock' => 'Maximal Clock',
];