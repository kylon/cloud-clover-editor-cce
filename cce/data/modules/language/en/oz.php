<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// DO NOT TRANSLATE => TRANSLATE
return [
    // Common
    'preferences' => 'Preferences',
    'ozpf_Debug' => 'Debug',
    'ozpf_SaveLogToFile' => 'Save Log To File',
    'ozpf_SaveLogToDeviceTree' => 'Save Log To Device Tree',
    'ozpf_SaveLogToNvram' => 'Save Log To Nvram',
    'ozpf_Off' => 'Disable',


    // Ozmosis
    'oz_smbios_sec' => 'GUID: 4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102',
    'oz_sys_sku' => 'System SKU',
    'oz_fw_rev' => 'Firmware Revision',
    'oz_hw_addr' => 'Hardware Address',
    'oz_board_ass_tag' => 'Base Board Asset Tag',
    'oz_proc_serial' => 'Processor Serial',
    'oz_gfx_sec' => 'GUID: 1F8E0C02-58A9-4E34-AE22-2B63745FA101',
    'acpif_config' => 'Acpi Mode',
    'acpif_modal_title' => 'ACPI Loader Mode',
    'ozal_0' => 'ACPI_LOADER_MODE_DISABLE',
    'ozal_1' => 'ACPI_LOADER_MODE_ENABLE',
    'ozal_2' => 'ACPI_LOADER_MODE_DUMP',
    'ozal_4' => 'ACPI_LOADER_MODE_DARWIN',
    'ozal_8' => 'ACPI_LOADER_MODE_WINDOWS',
    'ozal_64' => 'ACPI_LOADER_MODE_UPDATE_LEGACY',
    'oz_ati_fb' => 'Ati Framebuffer',
    'oz_snb_pid' => 'AAPL,snb_platform_id',
    'oz_ig_pid' => 'AAPL,ig-platform-id',
    'oz_disk_title' => 'Disk type',
    'oz_templ_title' => 'Template type',
    'ozdt_BootEntryTemplate' => 'Boot entry',
    'ozdt_DarwinDiskTemplate' => 'Darwin disk',
    'ozdt_DarwinRecoveryDiskTemplate' => 'Darwin recovery disk',
    'ozdt_DarwinCoreStorageTemplate' => 'Darwin core storage',
    'ozdt_AndroidDiskTemplate' => 'Android disk',
    'ozdt_AndroidDiskOptionTemplate' => 'Android disk options',
    'ozdt_LinuxDiskTemplate' => 'Linux disk',
    'ozdt_LinuxDiskOptionTemplate' => 'Linux disk options',
    'ozdt_LinuxRescueDiskTemplate' => 'Linux rescue disk',
    'ozdt_LinuxRescueOptionTemplate' => 'Linux rescue disk options',
    'ozto_del' => 'Clear template',
    'ozto_$label' => '$label',
    'ozto_$guid' => '$guid',
    'ozto_$uuid' => '$uuid',
    'ozto_$platform' => '$platform',
    'ozto_$major' => '$major',
    'ozto_$minor' => '$minor',
    'ozto_$build' => '$build',
    'ozgo_DisableAtiInjection' => 'Disable Ati Injection',
    'ozgo_DisableNvidaInjection' => 'Disable Nvida Injection',
    'ozgo_DisableIntelInjection' => 'Disable Intel Injection',
    'ozgo_DisableVoodooHda' => 'Disable VoodooHda',
    'ozgo_EnableVoodooHdaInternalSpdif' => 'Enable VoodooHda Internal S/PIDF',
    'ozgo_SkipSmbusSpdScan' => 'Skip SMBUS Spd Scan',
    'ozgo_DisableBootEntriesFilter' => 'Disable Boot Entries Filter',
    'ozgo_UserInterface' => 'Display User Interface/GUI',
    'oz_boot_arg_sec' => 'GUID: 7C436110-AB2A-4BBB-A880-FE41995C9F82',


    // ACPI
    'ozao_GenerateCpuStates' => 'Generate CPU States',
    'ozao_PatchOemTableOnly' => 'Patch OEM Tables Only',
    'ozao_PatchSsdtTableOnly' => 'Patch SSDT Tables Only',
    'ozao_DropOemTableOnly' => 'Drop OEM Tables Only',


    // Kernel and kext patches
    'ozms_WholePrelinked' => 'Whole Prelinked',
    'block_kext_cache' => 'Block Kext Caches',
    'kext_id' => 'Kext ID',


    // BeepBeep
    'audio_fname' => 'Audio File Name',
    'audio_io_port' => 'Audio Port Index',
    'audio_dev_path' => 'Audio Device Path',
    'start_play_ev' => 'Start Playback On Event',
    'ozbp_0' => 'Start Playback On Boot',
    'ozbp_1' => 'Start Playback On Application',
    'ozbp_2' => 'Start Playback On Apple Boot',
];