<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/CloudCloverEditor/cloverPlist.php';
require_once __DIR__.'/CloudCloverEditor/settingsContainer.php';
require_once __DIR__.'/CloudCloverEditor/plistContainer.php';
require_once __DIR__.'/modules/language/TextContainer.php';
require_once __DIR__.'/user_fn.php';

function fatalError(Throwable $e) {
    $now = new Datetime();

    header('Location: fatal.php');
    file_put_contents(__DIR__.'/res/fatalLog.txt', $now->format('Y-m-d H:i').'  '.$e."\n\n", FILE_APPEND);
}

function initCCE(): void {
    $CCECache = CCE\CCECache::getInstance();

    $CCECache->generateCache();

    if (isset($_SESSION['plist-list']))
        return;

    $plistList = new CCE\plistContainer();
    $settings = new CCE\settingsContainer();
    $plist = new \CCE\openCorePlist();

    $plistList->add('cce-config', $plist, true);
    $settings->initSettings($plistList->getActiveIdx());

    $_SESSION['plist-list'] = serialize($plistList);
    $_SESSION['cce-sett'] = serialize($settings);
    $_SESSION['cce-tab'] = null;
}

/**
 * Helper: get file icon
 *
 * @param string
 *
 * @return string
 */
function getFileIcon(string $configType): string {
    return match ($configType) {
        'oz' => 'ozmosis',
        'chm' => 'chameleon',
        'oc' => 'opencore',
        default => 'clover-efi',
    };
}

/**
 * Translate a bool value to Yes/No
 *
 * @param mixed $val
 *
 * @return mixed
 *
 * @note Mainly used with comboboxes.
 */
function boolToYesNo(mixed $val): mixed {
    if (is_bool($val))
        $val = $val === true ? 'Yes':'No';

    return $val;
}

/**
 * Translate a bool value to the string 'true' or 'false'
 *
 * @param bool $value
 *
 * @return string
 */
function boolToText(bool $value): string {
    return ($value ? 'true':'false');
}

/**
 * Trim and remove white spaces and new lines
 *
 * @param $string
 *
 * @return string
 */
function trimSN($string): string {
    return str_replace([' ', '\n', '\t'], '', trim(strval($string)));
}

/**
 * Return the value of a property, if set
 *
 * @param mixed $obj
 * @param string $key
 * @param mixed $fallbackValue
 *
 * @return mixed
 */
function getPropVal(mixed $obj, string $key = '', mixed $fallbackValue = null): mixed {
    if (is_array($obj)) {
        if (!isset($obj[$key]))
            return $fallbackValue ?? '';

        if ($obj[$key] instanceof \CFPropertyList\CFType)
            $obj[$key] = $obj[$key]->getValue();

        return gettype($obj[$key]) === 'string' ? sanitizeString($obj[$key]) : $obj[$key];

    } else if ($obj instanceof \CFPropertyList\CFType) {
        $ret = $obj->getValue();

        return gettype($ret) === 'string' ? sanitizeString($ret) : $ret;
    }

    return $fallbackValue ?? '';
}

/**
 * Return the data type of a CFType object
 *
 * @param mixed $obj
 * @param string $key
 *
 * @return string
 */
function getValType(mixed $obj, string $key = ''): string {
    if (is_array($obj)) {
        if (!isset($obj[$key]) || !($obj[$key] instanceof \CFPropertyList\CFType))
            return '';

        return $obj[$key]->getType();
    }

    return $obj instanceof \CFPropertyList\CFType ? $obj->getType():'';
}

/**
 * Check if a property is true
 *
 * @param mixed
 *
 * @return bool
 */
function isPropertyTrue($prop): bool {
    if ($prop instanceof \CFPropertyList\CFType)
        $prop = $prop->getValue();

    $propT = gettype($prop);
    $isString = $propT === 'string';

    if ($propT !== 'boolean' && !$isString)
        return false;

    return ($isString ? (strcasecmp($prop, 'true') === 0 || strcasecmp($prop, 'yes') === 0) : $prop === true);
}

/**
 * Check if a property is false
 *
 * @param mixed
 *
 * @return bool
 */
function isPropertyFalse($prop): bool {
    if ($prop instanceof \CFPropertyList\CFType)
        $prop = $prop->getValue();

    $propT = gettype($prop);
    $isString = $propT === 'string';

    if ($propT !== 'boolean' && !$isString)
        return false;

    return ($isString ? (strcasecmp($prop, 'false') === 0 || strcasecmp($prop, 'no') === 0) : $prop === false);
}

/**
 * Get checkbox state
 *
 * @param mixed $toCheck
 * @param string|int|null $key
 *
 * @return string
 */
function getCheckAttr(mixed $toCheck, string|int|null $key=null): string {
    $hasKey = $key !== null;
    $ret = 'unchecked';

    if (($hasKey && !isset($toCheck[$key])) || !$hasKey && is_array($toCheck))
        return $ret;

    $val = $hasKey ? $toCheck[$key]:$toCheck;

    if (isPropertyTrue($val))
        $ret = 'checked';
    else if (isPropertyFalse($val))
        $ret = 'indeterminate';

    return $ret;
}

/**
 * Check if a select option is selected
 *
 * @param mixed
 * @param mixed
 *
 * @return string
 */
function isSelected(mixed $expected, mixed $userVal): string {
    if ($userVal instanceof \CFPropertyList\CFType)
        $userVal = $userVal->getValue();
    else if ($userVal === null || $expected === '')
        return '';

    $expected = is_bool($expected) ? boolToText($expected) : strval($expected);
    $userVal = is_bool($userVal) ? boolToText($userVal) : strval($userVal);
    $isFalse = strcasecmp($expected, 'no') === 0 && (strcasecmp($userVal, 'false') === 0 || strcasecmp($userVal, 'no') === 0);
    $isTrue = !$isFalse && strcasecmp($expected, 'yes') === 0 && (strcasecmp($userVal, 'true') === 0 || strcasecmp($userVal, 'yes') === 0);

    return ($isFalse || $isTrue || strcasecmp($expected, $userVal) === 0 ? 'selected':'');
}

/**
 * Helper to get selected chassis type
 *
 * @param mixed $userVal
 *
 * @return string
 */
function getSelectedChassType(mixed $userVal): string {
    if ($userVal === null || $userVal === '')
        return '';
    else if (is_numeric($userVal))
        return '0x'.dechex(intval($userVal));

    $i = preg_match('/^0[xX]/', $userVal) === 1 ? 2:0;

    // count leading zeroes
    while ($userVal[$i] === '0')
        ++$i;

    return '0x'.substr($userVal, $i);
}

/**
 * @param int|float $flag
 * @param array $bits
 *
 * @return array
 */
function getAdvFlagBits(int|float $flag, array $bits): array {
    $ret = [];

    if ($flag === 0)
        return [0];

    foreach ($bits as $bit) {
        if (hasFlag($flag, $bit))
            array_push($ret, $bit);
    }

    return $ret;
}

/**
 * @param int|float $flag
 * @param int $bitsN
 *
 * @return array
 */
function getSimpleFlagBits(int|float $flag, int $bitsN): array {
    $ret = [];

    if ($flag === 0)
        return $ret;

    for ($i=0; $i<$bitsN; ++$i) {
        $v = 1 << $i;

        if (hasFlag($flag, $v))
            array_push($ret, $v);
    }

    return $ret;
}

/**
 * Check if $bit is set
 *
 * @param int
 * @param array
 *
 * @return bool
 *
 * @note Use getFlagBits() or getAcpiLoaderFlagBits() to create $bitsArray.
 */
function isBitSelected(int $bit, array $bitsArray): bool {
    $set = false;

    for ($i=0, $len=count($bitsArray); $i<$len; ++$i) {
        if ($bit === $bitsArray[$i]) {
            $set = true;
            break;
        }
    }

    return $set;
}

/**
 * Draw a CCE checkbox
 *
 * @param string $parentClass
 * @param string $dpath
 * @param string $dfield
 * @param string $state
 * @param string $label
 * @param bool $disabled
 * @param string $inptClass
 * @param string $attr
 * @param bool $hasCCEClass
 * @param bool $tristate
 *
 * @return string
 */
function drawCheckbox(string $parentClass, string $dpath, string $dfield, string $state = '', string $label = '', bool $disabled = false,
                      string $inptClass = '', string $attr = '', bool $hasCCEClass = false, bool $tristate = true): string {
    global $text;

    $eventClass = $hasCCEClass ? ' cce-checkbox ':'';
    $classes = 'form-check-input '.($tristate ? 'tristate-checkbox ':'').$eventClass.$inptClass;
    $disabled = $disabled ? ' disabled':'';
    $ltext = $text[$label] ?? $label;

    if ($tristate)
        $state = $state === '' ? 'data-state="unchecked"':'data-state="'.$state.'"';
    else
        $state = $state === 'checked' ? $state:'';

    return "<div class=\"form-check {$parentClass}\" data-path=\"{$dpath}\">
                <input type=\"checkbox\" class=\"{$classes}\" {$state} data-field=\"{$dfield}\"{$disabled} {$attr}>
                <label class=\"form-check-label\">{$ltext}</label>
            </div>";
}

/**
 * Draw a CCE radio
 *
 * @param string $parentClass
 * @param string $inptAttr
 * @param string $inptClass
 * @param string $val
 * @param string $label
 * @param string $name
 * @param bool $checked
 * @param bool $disabled
 * @param bool $hasCCEClass
 * @param string $dataPath
 * @param string $dataField
 * @param string $changeEvt
 *
 * @return string
 */
function drawRadio(string $parentClass, string $inptAttr, string $inptClass, string $val, string $label,  string $name,
                   bool $checked = false, bool $disabled = false, bool $hasCCEClass = false, string $dataPath = '',
                   string $dataField = '', string $changeEvt = ''): string {
    global $text;

    $cceClass = $hasCCEClass ? 'cce-radio':'';
    $chkd = $checked ? ' checked':'';
    $disabled = $disabled ? ' disabled':'';
    $ltext = $text[$label] ?? $label;

    return "<div class=\"form-check {$cceClass} {$parentClass}\" data-path=\"{$dataPath}\">
                <input name=\"{$name}\" type=\"radio\" class=\"form-check-input {$inptClass}\" data-field=\"{$dataField}\" value=\"{$val}\" data-change=\"{$changeEvt}\"{$chkd}{$disabled} {$inptAttr}>
                <label class=\"form-check-label\">{$ltext}</label>
            </div>";
}

function drawSelect(string $dpath, string $dfield, string $htmlOptions, string $label, string $id = '', string $inptClass = '',
                    string $attributes = '', bool $disabled = false): string {
    global $text;

    $disable = $disabled ? ' disabled':'';
    $labelHtml = '';

    if ($label !== '') {
        $label = $text[$label] ?? $label;
        $ldisable = $disabled ? ' color-disabled':'';
        $labelHtml = "<label class=\"form-label{$ldisable}\" for=\"{$id}\">{$label}</label>";
    }

    return "<div class=\"text-center mb-3\">
                {$labelHtml}
                <select id=\"{$id}\" class=\"form-select cce-sel {$inptClass}\" data-path=\"{$dpath}\" data-field=\"{$dfield}\" {$attributes}{$disable}>{$htmlOptions}</select>
            </div>";
}

/**
 * Draw a simple select
 *
 * @param string $dpath
 * @param string $dfield
 * @param array $options
 * @param string $label
 * @param string|int $fallbackValue
 * @param string $id
 * @param string $inptClass
 * @param string $attributes
 * @param bool $hasEmpty
 * @param bool $disabled
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawSimpleSelect(string $dpath, string $dfield, array $options, string $label, mixed $fallbackValue = '',
                          string $id = '', string $inptClass = '', string $attributes = '', bool $hasEmpty = false,
                          bool $disabled = false, string|int|null $customValue = null): string {
    global $text, $config;

    $opts = $hasEmpty ? '<option value=""></option>':'';
    $sep = $dpath === '/' ? '':'/';
    $value = $customValue ?? $config->getRawVals($dpath.$sep.$dfield) ?? $fallbackValue;

    foreach ($options as $opt) {
        $str = substr($opt, 5);
        $selected = isSelected($str, $value);

        $opts .= "<option value=\"{$str}\" {$selected}>{$text[$opt]}</option>";
    }

    return drawSelect($dpath, $dfield, $opts, $label, $id, $inptClass, $attributes, $disabled);
}

/**
 * Draw a simple numeric select
 *
 * @param string $dpath
 * @param string $dfield
 * @param array $options
 * @param string $label
 * @param int $fallbackValue
 * @param string $id
 * @param string $inptClass
 * @param string $attributes
 * @param bool $hasEmpty
 * @param bool $disabled
 * @param int|null $customValue
 *
 * @return string
 */
function drawSimpleNumericSelect(string $dpath, string $dfield, array $options, string $label, int $fallbackValue = PHP_INT_MIN,
                                 string $id = '', string $inptClass = '', string $attributes = '', bool $hasEmpty = false,
                                 bool $disabled = false, ?int $customValue = null): string {
    global $config;

    $opts = $hasEmpty ? '<option value=""></option>':'';
    $sep = $dpath === '/' ? '':'/';
    $value = $customValue ?? $config->getRawVals($dpath.$sep.$dfield) ?? $fallbackValue;

    foreach ($options as $opt) {
        $selected = isSelected($opt, $value);

        $opts .= "<option value=\"{$opt}\" {$selected}>{$opt}</option>";
    }

    return drawSelect($dpath, $dfield, $opts, $label, $id, $inptClass, $attributes, $disabled);
}

/**
 * Draw a simple select with no label
 *
 * @param string $dpath
 * @param string $dfield
 * @param array $options
 * @param string $fallbackValue
 * @param string $id
 * @param string $inptClass
 * @param string $attributes
 * @param bool $hasEmpty
 * @param bool $disabled
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawSimpleSelectNoLabel(string $dpath, string $dfield, array $options, mixed $fallbackValue = '', string $id = '',
                                 string $inptClass = '', string $attributes = '', bool $hasEmpty = false,
                                 bool $disabled = false, string|int|null $customValue = null): string {

    return drawSimpleSelect($dpath, $dfield, $options, '', $fallbackValue, $id, $inptClass, $attributes, $hasEmpty, $disabled, $customValue);
}

function drawBootArgsSelect(array $bootArgs): string {
    global $text;

    $opts = "<option value=\"\"></option><option value=\"del\">{$text['clear']}</option>";

    foreach ($bootArgs as $arg) {
        $str = substr($arg, 5);

        $opts .= "<option value=\"{$str}\">{$text[$arg]}</option>";
    }

    return "<div class=\"text-center mb-3\">
                <select class=\"form-select cce-sel\" data-sel=\"b-args\">{$opts}</select>
            </div>";
}

/**
 * Draw a CCE input with label
 *
 * @param string $dataType
 * @param string $dpath
 * @param string $dfield
 * @param string $label
 * @param mixed $fallbackValue
 * @param string $id
 * @param string $placeholder
 * @param bool $numeric
 * @param string $inptClass
 * @param string $attributes
 * @param bool $disabled
 * @param int $minL
 * @param int $maxL
 * @param null $customValue
 * @param bool $noCCEClass
 *
 * @return string
 */
function drawSimpleInput(string $dataType, string $dpath, string $dfield, string $label, mixed $fallbackValue = '',
                         string $id = '', string $placeholder = '', bool $numeric = false, string $inptClass = '',
                         string $attributes = '', bool $disabled = false, int $minL = PHP_INT_MIN,
                         int $maxL = PHP_INT_MIN, $customValue = null, bool $noCCEClass = false): string {
    global $text, $config;

    $icls = '';
    $ltext = $text[$label] ?? $label;
    $lfor = $id !== '' ? "for=\"{$id}\"" : '';
    $iid = $id !== '' ? "id=\"{$id}\"" : '';
    $plh = $placeholder !== '' ? " placeholder=\"{$placeholder}\"" : '';
    $itp = $numeric ? 'number' : 'text';
    $min = $minL !== PHP_INT_MIN ? " min=\"{$minL}\"" : '';
    $max = $maxL !== PHP_INT_MIN ? " max=\"{$maxL}\"" : '';
    $vtype = $dataType !== '' ? " data-vtype=\"{$dataType}\"":'';
    $ldsbl = $disabled ? ' color-disabled' : '';
    $idsbl = $disabled ? ' disabled' : '';
    $sep = $dpath === '/' ? '':'/';
    $val = $customValue ?? $config->getRawVals($dpath.$sep.$dfield) ?? $fallbackValue;

    if (!$noCCEClass)
        $icls = $numeric ? ' cce-numb' : ' cce-text';

    return "<div class=\"text-center mb-3\">
                <label {$lfor} class=\"form-label{$ldsbl}\">{$ltext}</label>
                <input {$iid} type=\"{$itp}\" data-path=\"{$dpath}\" data-field=\"{$dfield}\" class=\"form-control{$icls} {$inptClass}\" value=\"{$val}\"{$plh}{$min}{$max}{$idsbl}{$vtype} {$attributes}>
            </div>";
}

/**
 * Draw a CCE combobox input with label
 *
 * @param string $dataType
 * @param string $dpath
 * @param string $dfield
 * @param string $label
 * @param string $comboData
 * @param mixed $fallbackValue
 * @param string $id
 * @param string $placeholder
 * @param string $inptClass
 * @param string $attributes
 * @param bool $disabled
 * @param int $minL
 * @param int $maxL
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawComboboxSimpleInput(string $dataType, string $dpath, string $dfield, string $label, string $comboData,
                                 mixed $fallbackValue = '', string $id = '', string $placeholder = '',
                                 string $inptClass = '', string $attributes = '', bool $disabled = false,
                                 int $minL = PHP_INT_MIN, int $maxL = PHP_INT_MIN,
                                 string|int|null $customValue = null): string {

    $iClass = $inptClass.' cce-combo'.($disabled ? ' jcb-disabled':'');
    $attrs = "{$attributes} data-combo=\"{$comboData}\"";

    return drawSimpleInput($dataType, $dpath, $dfield, $label, $fallbackValue, $id, $placeholder, false, $iClass, $attrs, $disabled, $minL, $maxL, $customValue, true);
}

/**
 * Draw a CCE combo-select input with label
 *
 * @param string $dpath
 * @param string $dfield
 * @param string $label
 * @param string $comboselData
 * @param string $comboSelValue
 * @param mixed $fallbackValue
 * @param string $id
 * @param string $placeholder
 * @param string $inptClass
 * @param string $attributes
 * @param bool $disabled
 * @param int $minL
 * @param int $maxL
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawComboSelectSimpleInput(string $dpath, string $dfield, string $label, string $comboselData, string $comboSelValue,
                                    mixed $fallbackValue = '', string $id = '', string $placeholder = '',
                                    string $inptClass = '', string $attributes = '', bool $disabled = false,
                                    int $minL = PHP_INT_MIN, int $maxL = PHP_INT_MIN,
                                    string|int|null $customValue = null): string {

    $iClass = "{$inptClass} comboselect-input";
    $attrs = "{$attributes} data-comboselect=\"{$comboselData}\" data-comboselect-opt=\"{$comboSelValue}\"";

    return drawSimpleInput('', $dpath, $dfield, $label, $fallbackValue, $id, $placeholder, false, $iClass, $attrs, $disabled, $minL, $maxL, $customValue);
}

/**
 * Draw a CCE combo-select combobox input with label
 *
 * @param string $dpath
 * @param string $dfield
 * @param string $label
 * @param string $comboselData
 * @param string $comboSelValue
 * @param string $comboData
 * @param mixed $fallbackValue
 * @param string $id
 * @param string $placeholder
 * @param string $inptClass
 * @param string $attributes
 * @param bool $disabled
 * @param int $minL
 * @param int $maxL
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawComboSelectSimpleCombobox(string $dpath, string $dfield, string $label, string $comboselData,
                                       string $comboSelValue, string $comboData, mixed $fallbackValue = '',
                                       string $id = '', string $placeholder = '', string $inptClass = '',
                                       string $attributes = '', bool $disabled = false, int $minL = PHP_INT_MIN,
                                       int $maxL = PHP_INT_MIN, string|int|null $customValue = null): string {

    $iClass = "{$inptClass} comboselect-input";
    $attrs = "{$attributes} data-comboselect=\"{$comboselData}\" data-comboselect-opt=\"{$comboSelValue}\"";

    return drawComboboxSimpleInput('', $dpath, $dfield, $label, $comboData, $fallbackValue, $id, $placeholder, $iClass, $attrs, $disabled, $minL, $maxL, $customValue);
}

/**
 * Draw a CCE textarea with no label
 *
 * @param string $dataType
 * @param string $dpath
 * @param string $dfield
 * @param int $rows
 * @param string $attributes
 * @param string $id
 * @param string $placeholder
 * @param string $areaClass
 * @param bool $disabled
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawTextAreaNoLabel(string $dataType, string $dpath, string $dfield, int $rows = -1, string $attributes = '',
                             string $id = '', string $placeholder = '', string $areaClass = '', bool $disabled = false,
                             string|int|null $customValue = null): string {
    global $config;

    $iid = $id !== '' ? "id=\"{$id}\"" : '';
    $plh = $placeholder !== '' ? " placeholder=\"{$placeholder}\"" : '';
    $rowsN = $rows !== -1 ? "rows=\"{$rows}\"" : '';
    $disbl = $disabled ? ' disabled' : '';
    $sep = $dpath === '/' ? '':'/';
    $val = $customValue ?? $config->getRawVals($dpath.$sep.$dfield) ?? '';
    $dtype = $dataType === '' ? '':" data-vtype=\"{$dataType}\"";

    return "<textarea {$iid} data-path=\"{$dpath}\" data-field=\"{$dfield}\" class=\"form-control cce-text {$areaClass}\"{$dtype}{$plh}{$rowsN}{$disbl} {$attributes}>{$val}</textarea>";
}

/**
 * @param string $dataType
 * @param string $dpath
 * @param string $dfield
 * @param int $rows
 * @param string $attributes
 * @param string $id
 * @param string $placeholder
 * @param string $areaClass
 * @param bool $disabled
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawSimpleTextAreaNoLabel(string $dataType, string $dpath, string $dfield, int $rows = -1,
                                   string $attributes = '', string $id = '', string $placeholder = '',
                                   string $areaClass = '', bool $disabled = false,
                                   string|int|null $customValue = null): string {
    $txArea = drawTextAreaNoLabel($dataType, $dpath, $dfield, $rows, $attributes, $id, $placeholder, $areaClass, $disabled, $customValue);

    return "<div class=\"text-center mb-3\">{$txArea}</div>";
}

/**
 * Draw a CCE combo-select textarea with no label
 *
 * @param string $dpath
 * @param string $dfield
 * @param string $comboData
 * @param int $rows
 * @param string $attributes
 * @param string $id
 * @param string $placeholder
 * @param string $areaClass
 * @param bool $disabled
 * @param \CFPropertyList\CFType|null $customValue
 * @param string|null $comboValue
 *
 * @return string
 */
function drawComboSelectTextAreaNoLabel(string $dpath, string $dfield, string $comboData, int $rows = -1, string $attributes = '',
                                        string $id = '', string $placeholder = '', string $areaClass = '', bool $disabled = false,
                                        ?\CFPropertyList\CFType $customValue = null, ?string $comboValue = null): string {
    global $config;

    $sep = $dpath === '/' ? '':'/';
    $val = $customValue ?? $config->getVals($dpath.$sep.$dfield);
    $valT = $comboValue ?? getValType($val);
    $aClass = $areaClass.' comboselect-txarea';
    $attrs = "{$attributes} data-comboselect=\"{$comboData}\" data-comboselect-opt=\"{$valT}\"";

    return drawTextAreaNoLabel('', $dpath, $dfield, $rows, $attrs, $id, $placeholder, $aClass, $disabled, getPropVal($val));
}

/**
 * Draw a CCE textarea with label
 *
 * @param string $dataType
 * @param string $dpath
 * @param string $dfield
 * @param string $label
 * @param int $rows
 * @param string $attributes
 * @param string $id
 * @param string $placeholder
 * @param string $areaClass
 * @param bool $disabled
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawSimpleTextArea(string $dataType, string $dpath, string $dfield, string $label, int $rows = -1,
                            string $attributes = '', string $id = '', string $placeholder = '',
                            string $areaClass = '', bool $disabled = false,
                            string|int|null $customValue = null): string {
    global $text;

    $ltext = $text[$label] ?? $label;
    $lfor = $id !== '' ? "for=\"{$id}\"" : '';
    $txArea = drawTextAreaNoLabel($dataType, $dpath, $dfield, $rows, $attributes, $id, $placeholder, $areaClass, $disabled, $customValue);

    return "<div class=\"text-center mb-3\">
                <label {$lfor} class=\"form-label\">{$ltext}</label>
                {$txArea}
            </div>";
}

/**
 * Draw a CCE combo-select textarea with label
 *
 * @param string $dpath
 * @param string $dfield
 * @param string $label
 * @param string $comboData
 * @param string $comboValue
 * @param int $rows
 * @param string $attributes
 * @param string $id
 * @param string $placeholder
 * @param string $areaClass
 * @param bool $disabled
 * @param string|int|null $customValue
 *
 * @return string
 */
function drawComboselectSimpleTextArea(string $dpath, string $dfield, string $label, string $comboData,
                                       string $comboValue, int $rows = -1, string $attributes = '',
                                       string $id = '', string $placeholder = '', string $areaClass = '',
                                       bool $disabled = false, string|int|null $customValue = null): string {

    $aClass = $areaClass.' comboselect-txarea';
    $attrs = "{$attributes} data-comboselect=\"{$comboData}\" data-comboselect-opt=\"{$comboValue}\"";

    return drawSimpleTextArea('', $dpath, $dfield, $label, $rows, $attrs, $id, $placeholder, $aClass, $disabled, $customValue);
}

function drawSimpleInlineCheckOpts(array $opts, string $dpath): string {
    global $config;

    $sep = $dpath === '/' ? '':'/';
    $ret = '';

    foreach ($opts as $opt) {
        $str = substr($opt, 5);
        $checked = getCheckAttr($config->getRawVals($dpath.$sep.$str));

        $ret .= drawCheckbox('form-check-inline', $dpath, $str, $checked, $opt);
    }

    return $ret;
}

function drawSmbiosSerialNumberInput(string $dpath, string $dfield, string $label = 'serial_numb'): string {
    global $text, $config;

    $sep = $dpath === '/' ? '':'/';
    $val = $config->getRawVals($dpath.$sep.$dfield) ?? '';

    return "<div class=\"text-center mb-3\">
                <label class=\"form-label\" for=\"smsn\">{$text[$label]}</label>
                <div class=\"input-group\">
                    <input type=\"text\" class=\"cce-text form-control macserialres\" data-path=\"{$dpath}\" data-field=\"{$dfield}\" value=\"{$val}\">
                    <button class=\"btn btn-secondary macserialbtn\" type=\"button\"><i class=\"fa icon-refresh\"></i> </button>
                </div>
            </div>";
}

/**
 * Generate a list of all macOS version string from 10.6 to current
 *
 * @return string
 */
function genMacOSVersionsList(): string {
    global $osv;

    $ret = '';

    for ($m=0, $sz=count($osv); $m<$sz; $m+=4) {
        $majVer = $osv[$m+1];

        $ret .= "<option value=\"{$majVer}.{$osv[$m+2]}\">{$osv[$m]} ({$majVer}.{$osv[$m+2]})</option>";

        for ($h = 0, $off = $m + 3; $h <= $osv[$off]; ++$h) {
            $n = $h === 0 ? 'x' : $h;
            $upStr = $h === 0 ? '' : ' Upd '.$h;

            $ret .= "<option value=\"{$majVer}.{$osv[$m+2]}.{$n}\">{$osv[$m]}{$upStr} ({$majVer}.{$osv[$m+2]}.{$n})</option>";
        }
    }

    return $ret;
}

/**
 * Check if a flag is set
 *
 * @param int|float $flag
 * @param int $bit
 *
 * @return bool
 */
function hasFlag(int|float $flag, int $bit): bool {
    return ($flag & $bit) === $bit;
}

/**
 * Sanitize a string
 * if the value is not a string, it will be converted to string
 *
 * @param mixed $str
 *
 * @return string
 */
function sanitizeString(mixed $str): string {
    if (gettype($str) !== 'string')
        $str = strval($str);

    return trim(str_replace(['<', '>', '"', "'"], ['&lt;', '&gt;', '&quot;', '&apos;'], $str));
}