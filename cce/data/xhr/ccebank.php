<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

session_start();

require_once __DIR__.'/../CloudCloverEditor/ozmosisPlist.php';
require_once __DIR__.'/../CloudCloverEditor/chameleonPlist.php';
require_once __DIR__.'/../CloudCloverEditor/openCorePlist.php';
require_once __DIR__.'/../SimpleDB.php';
require_once __DIR__.'/../utils.php';

$isValidSession = isset($_SESSION['cce-sett']) && isset($_SESSION['plist-list']);

if (!$isValidSession || !isValidToken())
    goto error;

$fdata = json_decode(file_get_contents('php://input'), true);

if ($fdata === null)
    goto error;

$wcmd = filter_var($fdata['type'], FILTER_SANITIZE_STRING);
$argC = count($fdata['data'] ?? []);
$db = new simpleDB('sqlite');
$re = false;

if (!$db->isConnected()) {
    $db->kill();
    goto error;
}

switch ($wcmd) {
    case 'bsearch': {
        if ($argC !== 1)
            break;

        $search = $fdata['data']['queryFilter'];
        $hasCid = isset($_SESSION['mybnkcid']);
        $sqlOp = $hasCid ? '=':'!=';
        $sqlCID = $hasCid ? 'cid = :cid AND':'';
        $sqlBind = [];

        $cmd = $search === 'listall' ?
            'WHERE '.$sqlCID.' locked '.$sqlOp.' "z" ORDER BY last_save DESC' :
            'WHERE '.$sqlCID.' name LIKE :search AND locked '.$sqlOp.' "z" ORDER BY last_save DESC';

        if ($hasCid)
            $sqlBind['cid'] = $_SESSION['mybnkcid'];

        if ($search !== 'listall')
            $sqlBind['search'] = '%'.$search.'%';

        $re = $db->fetch('config_list', $cmd, $sqlBind, 'all', 'SELECT id, name, locked, type FROM');
    }
        break;
    case 'chksvtobnk': {
        $sname = sanitizeString($fdata['data']['ConfigName']);

        if ($argC !== 1 || $sname === '')
            break;

        $sqlBind['name'] = $sname;
        $re = $db->rows('config_list', 'WHERE name = :name', $sqlBind) === 1 ? true:-1;
    }
        break;
    case 'getbnkcfglock': {
        if ($argC !== 1)
            break;

        $sname = sanitizeString($fdata['data']['ConfigName']);
        if ($sname === '')
            break;

        $sqlBind['name'] = $sname;
        $key = $db->fetch('config_list', 'WHERE name = :name', $sqlBind, 'fetch', 'SELECT locked FROM');
        $re = empty($key) ? false:$key['locked'];
    }
        break;
    case 'genbnkcid': {
        if ($argC !== 0)
            break;

        while (true) {
            $cid = substr(hash('sha256', strval(time())), rand(0, 45), 12);
            $sqlBind['cid'] = $cid;
            $getCid = $db->rows('config_list', 'WHERE cid = :cid', $sqlBind);

            if ($getCid > 0)
                continue;

            if ($getCid === 0 && strlen($cid) === 12)
                $re = $cid;

            break;
        }
    }
        break;
    case 'chkbnkname': {
        if ($argC !== 1)
            break;

        $sname = sanitizeString($fdata['data']['ConfigName']);
        $len = strlen($sname);
        $out = [];

        preg_match("#[^0-9a-zA-Z- ]+#", $sname, $out);

        if (!empty($out) || $len < 5 || $len > 40 || $sname === 'config' || substr_compare($sname, 'cce-config', 0, 10) === 0)
            break;

        $sqlBind['name'] = $sname;
        $re = $db->rows('config_list', 'WHERE name = :name', $sqlBind) === 0 ? true:-1;
    }
        break;
    case 'chkisbnkcfg': {
        if ($argC !== 0)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $name = substr($plistList->getActiveIdx(), 0, -7);
        $sqlBind['name'] = sanitizeString($name);
        $rows = $db->rows('config_list', 'WHERE name = :name', $sqlBind);
        $re = $rows === 1 ? true:-1;
    }
        break;
    case 'getbankoptscfgdata': {
        if ($argC !== 0)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $name = sanitizeString(substr($plistList->getActiveIdx(), 0, -7));
        $sqlBind['name'] = $name;
        $data = $db->fetch('config_list', 'WHERE name = :name', $sqlBind, 'fetch', 'SELECT name, locked FROM');
        $re = $data;
    }
        break;
    case 'svtobnk': {
        if ($argC !== 6)
            break;

        $sname = sanitizeString($fdata['data']['ConfigName']);
        $slen = strlen($sname);

        if ($sname === '' || $slen < 5 || $slen > 40)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $settings = unserialize($_SESSION['cce-sett']);
        $type = substr_compare($fdata['data']['CID'], 'u_', 0, 2) === 0 ? 'up':'mk';
        $cceMode = $settings->get($plistList->getActiveIdx(), 'mode');
        $plist = $plistList->getPlistObj('');
        $untouched = $plist; // save this for session vars, only delete on db

        if ($cceMode === 'cce' && $fdata['data']['SkipCentry']) {
            $plist->unsetVal('GUI/Custom', 'Entries');
            $plist->unsetVal('GUI/Custom', 'Legacy');
            $plist->unsetVal('GUI/Custom', 'Tool');
        }

        if ($fdata['data']['SkipSdata']) {
            switch ($cceMode) {
                case 'cce':
                    $plist->unsetVal('SMBIOS', 'SerialNumber');
                    $plist->unsetVal('SMBIOS', 'SmUUID');
                    $plist->unsetVal('SMBIOS', 'BoardSerialNumber');
                    $plist->unsetVal('RtVariables', 'MLB');
                    $plist->unsetVal('RtVariables', 'ROM');
                    $plist->unsetVal('SystemParameters', 'CustomUUID');
                    break;
                case 'oz':
                    $plist->unsetVal('Defaults:4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102', 'BoardSerialNumber');
                    $plist->unsetVal('Defaults:4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102', 'SerialNumber');
                    $plist->unsetVal('Defaults:4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102', 'HardwareAddress');
                    $plist->unsetVal('Defaults:4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102', 'ProcessorSerial');
                    break;
                case 'chm':
                    $plist->unsetVal('/', 'SMboardserial');
                    $plist->unsetVal('/', 'SMchassisserial');
                    $plist->unsetVal('/', 'SMserial');
                    $plist->unsetVal('/', 'SMsystemuuid');
                    break;
                case 'oc':
                    $plist->unsetVal('PlatformInfo/DataHub', 'ROM');
                    $plist->unsetVal('PlatformInfo/DataHub', 'SystemSerialNumber');
                    $plist->unsetVal('PlatformInfo/DataHub', 'SystemUUID');
                    $plist->unsetVal('PlatformInfo/Generic', 'MLB');
                    $plist->unsetVal('PlatformInfo/Generic', 'ROM');
                    $plist->unsetVal('PlatformInfo/Generic', 'SystemSerialNumber');
                    $plist->unsetVal('PlatformInfo/Generic', 'SystemUUID');
                    $plist->unsetVal('PlatformInfo/PlatformNVRAM', 'MLB');
                    $plist->unsetVal('PlatformInfo/PlatformNVRAM', 'ROM');
                    $plist->unsetVal('PlatformInfo/SMBIOS', 'BoardSerialNumber');
                    $plist->unsetVal('PlatformInfo/SMBIOS', 'ChassisSerialNumber');
                    $plist->unsetVal('PlatformInfo/SMBIOS', 'SystemUUID');
                    $plist->unsetVal('PlatformInfo/SMBIOS', 'SystemSerialNumber');
                    break;
                default:
                    break;
            }
        }

        $content = $plist->getPlistFile(false);

        if ($type === 'mk') {
            $sqlBind['cid'] = $fdata['data']['CID'];
            $isValidCid = $db->rows('config_list', 'WHERE cid = :cid', $sqlBind) >= 0;
            $cmd2 = '(cid, name, locked, content, is_empty, type) VALUES (:cid, :name, :locked, :content, :is_empty, :type)';
            $sqlBind = [
                'cid' => $fdata['data']['CID'],
                'name' => $sname,
                'locked' => $fdata['data']['LockFlag'],
                'content' => 'b_'.gzcompress($content, 4),
                'is_empty' => $plist->isEmpty(),
                'type' => $cceMode
            ];

            if ((!$fdata['data']['IsNewCID'] && !$isValidCid) || preg_match('[^0-9a-zA-Z- ]', $sname))
                break;

            $re = $db->write('config_list', $cmd2, $sqlBind, 'INSERT INTO');
            if ($re !== true)
                break;

            $plistList->add($sname, $untouched);

            $idxList = $plistList->getIndexesList();
            $idx = end($idxList);

            $settings->replaceIndex($plistList->getActiveIdx(), $idx);
            $plistList->remove($plistList->getActiveIdx());
            $plistList->setActiveIdx($idx);

            $_SESSION['cce-sett'] = serialize($settings);
            $_SESSION['plist-list'] = serialize($plistList);
            $re = $idx;

        } else {
            $isPublic = $fdata['data']['LockFlag'] === 'n';
            $cid = substr($fdata['data']['CID'], 2);
            $sqlBind = $isPublic ? ['name' => $sname] : ['cid' => $cid, 'name' => $sname];
            $cmd2 = $isPublic ? 'name = :name' : 'cid = :cid AND name = :name';
            $isValidData = $db->rows('config_list', 'WHERE '.$cmd2, $sqlBind) === 1;

            if (!$isValidData)
                break;

            $cmd1 = "SET content = :content, last_save = datetime('now'), is_empty = :is_empty, type = :type WHERE ".$cmd2;
            $sqlBind2 = ['content' => 'b_'.gzcompress($content, 4), 'is_empty' => $plist->isEmpty(), 'type' => $cceMode];
            $sqlBind = array_merge($sqlBind, $sqlBind2);
            $re = $db->write('config_list', $cmd1, $sqlBind, 'UPDATE');
        }
    }
        break;
    case 'delcfgfrombnk': {
        if ($argC !== 3)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $sname = sanitizeString(substr($plistList->getActiveIdx(), 0, -7));
        $sqlBind = ['cid' => $fdata['data']['CID'], 'name' => $sname];
        $isValid = $db->rows('config_list', 'WHERE cid = :cid AND name = :name', $sqlBind) === 1;

        if (!$isValid)
            break;

        $re = $db->write('config_list', 'WHERE cid = :cid AND name = :name', $sqlBind, 'DELETE FROM');
    }
        break;
    case 'updbnkcfg': {
        if ($argC !== 3)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $settings = unserialize($_SESSION['cce-sett']);
        $name = sanitizeString(substr($plistList->getActiveIdx(), 0, -7));
        $sname = sanitizeString($fdata['data']['ConfigName']);
        $isNameChanged = $name !== $sname;

        $sqlBind = ['cid' => $fdata['data']['CID'], 'name' => $name];
        $isValid = $db->rows('config_list', 'WHERE cid = :cid AND name = :name', $sqlBind) === 1;

        $sqlBind = ['name' => $sname];
        $isNameNotAvailable = $db->rows('config_list', 'WHERE name = :name', $sqlBind) === 1;

        if (!$isValid || ($isNameChanged && $isNameNotAvailable) || ($isNameChanged && $sname === '') || preg_match('[^0-9a-zA-Z- ]', $sname))
            break;

        $sqlBind = [
            'new_name' => $sname,
            'locked' => $fdata['data']['LockFlag'],
            'cid' => $fdata['data']['CID'],
            'cur_name' => $name
        ];
        $re = $db->write('config_list',
                         'SET name = :new_name, locked = :locked WHERE cid = :cid AND name = :cur_name',
                         $sqlBind, 'UPDATE');

        if (!$re)
            break;

        if ($isNameChanged) {
            $plist = $plistList->getPlistObj('');

            $plistList->add($sname, $plist);

            $idxList = $plistList->getIndexesList();
            $newIdx = end($idxList);

            $settings->replaceIndex($plistList->getActiveIdx(), $newIdx);

            $plistList->remove($plistList->getActiveIdx());
            $plistList->setActiveIdx($newIdx);

            $_SESSION['plist-list'] = serialize($plistList);
            $_SESSION['cce-sett'] = serialize($settings);
            $re = $newIdx;
        }
    }
        break;
    case 'getbnkviewmode': {
        if ($argC !== 0)
            break;

        $re = isset($_SESSION['mybnkcid']) ? 'myb':'cceb';
    }
        break;
    default:
        break;
}

$db->kill();

echo json_encode($re);
exit(0);

error:
    echo json_encode(false);
    exit(1);