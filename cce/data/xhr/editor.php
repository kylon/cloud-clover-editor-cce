<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

session_start();

require_once __DIR__.'/../CloudCloverEditor/ozmosisPlist.php';
require_once __DIR__.'/../CloudCloverEditor/chameleonPlist.php';
require_once __DIR__.'/../CloudCloverEditor/openCorePlist.php';
require_once __DIR__.'/../SimpleDB.php';
require_once __DIR__.'/../utils.php';

$isValidSession = isset($_SESSION['cce-sett']) && isset($_SESSION['plist-list']);

if (!$isValidSession || !isValidToken())
    goto error;

$fdata = json_decode(file_get_contents('php://input'), true);

if ($fdata === null)
    goto error;

$wcmd = filter_var($fdata['type'], FILTER_SANITIZE_STRING);
$argC = count($fdata['data'] ?? []);
$re = false;

switch ($wcmd) {
    case 'sncfg': {
        if ($argC !== 0)
            break;

        $settings = unserialize($_SESSION['cce-sett']);
        $plistList = unserialize($_SESSION['plist-list']);
        $plist = new \CCE\openCorePlist();

        $plistList->add('cce-config', $plist);

        $idxList = $plistList->getIndexesList();
        $idx = end($idxList);

        $settings->initSettings($idx);

        $_SESSION['plist-list'] = serialize($plistList);
        $_SESSION['cce-sett'] = serialize($settings);
        $re = $idx;
    }
        break;
    case 'dscfg': {
        if ($argC !== 2)
            break;

        if ($fdata['data']['NextIdx'] === 'x') { // last one, reset cce
            unset($_SESSION);
            session_destroy();

        } else {
            $plistList = unserialize($_SESSION['plist-list']);
            $settings = unserialize($_SESSION['cce-sett']);
            $isActive = $plistList->getActiveIdx() === $fdata['data']['RemovedIdx'];

            $plistList->remove($fdata['data']['RemovedIdx']);
            $settings->remove($fdata['data']['RemovedIdx']);

            if ($isActive) {
                $plistList->setActiveIdx($fdata['data']['NextIdx']);
                $_SESSION['cce-tab'] = null;
            }

            $_SESSION['cce-sett'] = serialize($settings);
            $_SESSION['plist-list'] = serialize($plistList);
        }

        $re = true;
    }
        break;
    default:
        break;
}

echo json_encode($re);
exit(0);

error:
    echo json_encode(false);
    exit(1);