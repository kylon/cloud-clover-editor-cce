<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

session_start();

require_once __DIR__.'/../CloudCloverEditor/ozmosisPlist.php';
require_once __DIR__.'/../CloudCloverEditor/chameleonPlist.php';
require_once __DIR__.'/../CloudCloverEditor/openCorePlist.php';
require_once __DIR__.'/../SimpleDB.php';
require_once __DIR__.'/../utils.php';

$isValidSession = isset($_SESSION['cce-sett']) && isset($_SESSION['plist-list']);

if (!$isValidSession || !isValidToken())
    goto error;

$fdata = json_decode(file_get_contents('php://input'), true);

if ($fdata === null)
    goto error;

$wcmd = filter_var($fdata['type'], FILTER_SANITIZE_STRING);
$argC = count($fdata['data'] ?? []);
$re = false;

switch ($wcmd) {
    case 'setval': {
        if ($argC !== 4)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');

        $plist->setVal($fdata['data']['Path'], $fdata['data']['Field'], $fdata['data']['Value'], $fdata['data']['DataType']);
        $plistList->update($plistList->getActiveIdx(), $plist);

        $_SESSION['plist-list'] = serialize($plistList);
        $re = true;
    }
        break;
    case 'setdatatype': {
        if ($argC !== 4)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');

        $plist->setValType($fdata['data']['Path'].'/'.$fdata['data']['Field'], $fdata['data']['DataType']);
        $plistList->update($plistList->getActiveIdx(), $plist);

        $_SESSION['plist-list'] = serialize($plistList);
        $re = true;
    }
        break;
    case 'unset': {
        if ($argC !== 4)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');

        if (is_array($fdata['data']['Field'])) {
            $isIntKey = is_numeric($fdata['data']['Field'][0]);

            for ($i=0, $len=count($fdata['data']['Field']); $i<$len; ++$i) {
                if ($isIntKey && $i !== 0)
                    $fdata['data']['Field'][$i] = intval($fdata['data']['Field'][$i]) - $i;

                $plist->unsetVal($fdata['data']['Path'], $fdata['data']['Field'][$i]);
            }

        } else {
            $plist->unsetVal($fdata['data']['Path'], $fdata['data']['Field']);
        }

        $plistList->update($plistList->getActiveIdx(), $plist);

        $_SESSION['plist-list'] = serialize($plistList);
        $re = true;
    }
        break;
    case 'updkey': {
        if ($argC !== 3)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');

        $plist->updateKey($fdata['data']['Path'], $fdata['data']['OldKey'], $fdata['data']['NewKey']);
        $plistList->update($plistList->getActiveIdx(), $plist);

        $_SESSION['plist-list'] = serialize($plistList);
        $re = true;
    }
        break;
    case 'sortval': {
        if ($argC !== 3)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');

        $plist->swapElems($fdata['data']['Path'], intval($fdata['data']['OldKey']), intval($fdata['data']['NewKey']));
        $plistList->update($plistList->getActiveIdx(), $plist);

        $_SESSION['plist-list'] = serialize($plistList);
        $re = true;
    }
        break;
    case 'isvalnull': {
        if ($argC !== 4)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');

        $re = !$plist->hasValue($fdata['data']['Path'].'/'.$fdata['data']['Field']);
    }
        break;
    case 'getpprops': {
        if ($argC !== 1)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');
        $re = $plist->getVals($fdata['data']['Path']) ?? [];
        $res = [];

        if ($re === [])
            break;

        foreach ($re as $k => $v) {
            $t['key'] = $k;
            $t['val'] = sanitizeString($v->getValue());
            $t['type'] = $v->getType();

            array_push($res, $t);
        }

        $re = $res;
    }
        break;
    case 'getcprops': {
        if ($argC !== 1)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');
        $re = $plist->getVals($fdata['data']['Path']) ?? [];
        $res = [];

        if ($re === [])
            break;

        foreach ($re as $patch) {
            $t = [];

            foreach ($patch as $k => $v) {
                $t[$k] = sanitizeString($v->getValue());
                $t[$k.'_type'] = $v->getType();
            }

            array_push($res, $t);
        }

        $re = $res;
    }
        break;
    case 'getsprops': {
        if ($argC !== 1)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');
        $re = $plist->getRawVals($fdata['data']['Path']);
    }
        break;
    case 'getoztempldata': {
        if ($argC !== 4)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');
        $re = $plist->getRawVals($fdata['data']['Path'].'/'.$fdata['data']['Field']);
    }
        break;
    case 'getchamKxPatchdata': {
        if ($argC !== 1)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $plist = $plistList->getPlistObj('');

        $data = $plist->getVals($fdata['data']['Path']);
        $ret = [];

        if ($data === null)
            break;

        foreach ($data as $k => $v) {
            $ret[$k] = sanitizeString($v->getValue());
            $ret[$k.'_type'] = $v->getType();
        }

        $re = $ret;
    }
        break;
    case 'copy': {
        if ($argC !== 2 || count($fdata['data']['Data']) === 0)
            break;

        $plistList = unserialize($_SESSION['plist-list']);
        $settings = unserialize($_SESSION['cce-sett']);

        if ($settings->get($plistList->getActiveIdx(), 'mode') !== $settings->get($fdata['data']['Data'][0], 'mode') ||
            $settings->get($fdata['data']['Data'][0], 'text'))
            break;

        $source = $plistList->getPlistObj('');
        $dest = $plistList->getPlistObj($fdata['data']['Data'][0]);
        $vals = $dest->getVals($fdata['data']['Path']);
        $isIntKey = is_numeric($fdata['data']['Data'][1]);
        $destIdx = !$isIntKey ? str_replace('%', '/', $fdata['data']['Data'][1]) : count($vals ?? []);

        for ($i=1, $len=count($fdata['data']['Data']); $i<$len; ++$i) {
            $content = $source->getVals($fdata['data']['Path'].'/'.$fdata['data']['Data'][$i]);

            if ($content === null)
                continue;

            $dest->setVal($fdata['data']['Path'], $destIdx, $content);
            ++$destIdx;
        }

        $plistList->update($fdata['data']['Data'][0], $dest);

        $_SESSION['plist-list'] = serialize($plistList);
        $re = true;
    }
        break;
    default:
        break;
}

echo json_encode($re);
exit(0);

error:
    echo json_encode(false);
    exit(1);
