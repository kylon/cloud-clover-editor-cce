<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

session_start();

require_once __DIR__.'/../SimpleDB.php';
require_once __DIR__.'/../utils.php';

$isValidSession = isset($_SESSION['cce-sett']) && isset($_SESSION['plist-list']);

if (!$isValidSession || !isValidToken())
    goto error;

$fdata = json_decode(file_get_contents('php://input'), true);

if ($fdata === null)
    goto error;

$wcmd = filter_var($fdata['type'], FILTER_SANITIZE_STRING);
$argC = count($fdata['data'] ?? []);
$db = new simpleDB('sqlite');
$re = false;

if (!$db->isConnected()) {
    $db->kill();
    goto error;
}

switch ($wcmd) {
    case 'getPatchesList':
    case 'getPatchesListCount': {
        $isCount = $wcmd === 'getPatchesListCount';
        $argN = $isCount ? 2:4;

        if ($argC !== $argN)
            break;

        $sqlBind = [];
        $cmd = '';

        if ($fdata['data']['queryFilter'] !== 'listall') {
            switch ($fdata['data']['type']) {
                case 'dsdtP':
                    $sqlBind = ['search' => '%'.$fdata['data']['queryFilter'].'%'];
                    $cmd = 'AND Comment LIKE :search';
                    break;
                case 'kextP':
                case 'ozkextP':
                    $sqlBind = [
                        'search' => '%'.$fdata['data']['queryFilter'].'%',
                        'search2' => '%'.$fdata['data']['queryFilter'].'%',
                        'search3' => '%'.$fdata['data']['queryFilter'].'%'
                    ];
                    $cmd = 'AND Comment LIKE :search OR Name LIKE :search2 OR MatchOS LIKE :search3';
                    break;
                case 'bootefiP':
                case 'kernelP':
                case 'ozkernelP':
                case 'chamkernelP':
                    $sqlBind = [
                        'search' => '%'.$fdata['data']['queryFilter'].'%',
                        'search2' => '%'.$fdata['data']['queryFilter'].'%'
                    ];
                    $cmd = 'AND Comment LIKE :search OR MatchOS LIKE :search2';
                    break;
                default:
                    break;
            }
        }

        if (!$isCount) {
            $sqlBindP2 = [
                'patchType' => $fdata['data']['type'],
                'limit' => $fdata['data']['perPagePatches'],
                'offset' => $fdata['data']['offset']
            ];
            $sqlBinds = array_merge($sqlBind, $sqlBindP2);

            $re = $db->fetch('patches_list',
                             'WHERE instr(patchType, :patchType) > 0 AND Status = "w" '.$cmd.' LIMIT :limit OFFSET :offset',
                             $sqlBinds, 'all');
        } else {
            $sqlBindP2 = ['patchType' => $fdata['data']['type']];
            $sqlBinds = array_merge($sqlBind, $sqlBindP2);
            $re = $db->rows('patches_list', 'WHERE instr(patchType, :patchType) > 0 '.$cmd, $sqlBinds);

            if ($re < 0)
                $re = 0;
        }
    }
        break;
    default:
        break;
}

$db->kill();

echo json_encode($re);
exit(0);

error:
    echo json_encode(false);
    exit(1);