<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
session_start();

require_once __DIR__.'/../CloudCloverEditor/cloverPlist.php';
require_once __DIR__.'/../CloudCloverEditor/ozmosisPlist.php';
require_once __DIR__.'/../CloudCloverEditor/chameleonPlist.php';
require_once __DIR__.'/../CloudCloverEditor/openCorePlist.php';
require_once __DIR__.'/../CloudCloverEditor/plistContainer.php';
require_once __DIR__.'/../user_fn.php';

if (!isset($_SESSION['plist-list']))
    exit(1);

$fileName = filter_input(INPUT_POST, 'filename', FILTER_SANITIZE_STRING);
$fileNameFinal = $fileName !== false && $fileName !== null ? $fileName:'cce_config';
$plistList = unserialize($_SESSION['plist-list']);
$plist = $plistList->getPlistObj('');

if (!$plist->export($fileNameFinal))
    exit(1);
