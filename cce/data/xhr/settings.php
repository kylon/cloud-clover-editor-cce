<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

session_start();

require_once __DIR__.'/../CloudCloverEditor/ozmosisPlist.php';
require_once __DIR__.'/../CloudCloverEditor/chameleonPlist.php';
require_once __DIR__.'/../CloudCloverEditor/openCorePlist.php';
require_once __DIR__.'/../SimpleDB.php';
require_once __DIR__.'/../utils.php';

$isValidSession = isset($_SESSION['cce-sett']) && isset($_SESSION['plist-list']);

if (!$isValidSession || !isValidToken())
    goto error;

$fdata = json_decode(file_get_contents('php://input'), true);

if ($fdata === null)
    goto error;

$wcmd = filter_var($fdata['type'], FILTER_SANITIZE_STRING);
$argC = count($fdata['data'] ?? []);
$re = false;

switch ($wcmd) {
    case 'setccetab': {
        if ($argC !== 1)
            break;

        $_SESSION['cce-tab'] = $fdata['data']['ccetab'];
        $re = true;
    }
        break;
    case 'ccesett': {
        if ($argC !== 3)
            break;

        switch ($fdata['data']['Setting']) {
            case 'ccebnk': {
                unset($_SESSION['mybnkcid']);

                if ($fdata['data']['BankMode'] === 'cceb') {
                    $re = true;
                    break;
                }

                if ($fdata['data']['BankMode'] !== 'myb' || ($fdata['data']['BankMode'] === 'myb' && strlen($fdata['data']['CID']) !== 12))
                    break;

                $db = new simpleDB('sqlite');

                if (!$db->isConnected())
                    break;

                $sqlBind['cid'] = $fdata['data']['CID'];
                $c = $db->rows('config_list', 'WHERE cid = :cid', $sqlBind);
                $db->kill();

                if ($c <= 0)
                    break;

                $_SESSION['mybnkcid'] = $fdata['data']['CID'];
                $re = true;
            }
                break;
            default:
                break;
        }
    }
        break;
    default:
        break;
}

echo json_encode($re);
exit(0);

error:
    echo json_encode(false);
    exit(1);