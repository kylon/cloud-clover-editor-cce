<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
session_start();

require_once __DIR__.'/../CloudCloverEditor/include/configUpgrader.php';
require_once __DIR__.'/../CloudCloverEditor/cloverPlist.php';
require_once __DIR__.'/../CloudCloverEditor/ozmosisPlist.php';
require_once __DIR__.'/../CloudCloverEditor/chameleonPlist.php';
require_once __DIR__.'/../CloudCloverEditor/openCorePlist.php';
require_once __DIR__.'/../CloudCloverEditor/settingsContainer.php';
require_once __DIR__.'/../CloudCloverEditor/plistContainer.php';
require_once __DIR__.'/../SimpleDB.php';
require_once __DIR__.'/../user_fn.php';

$isValidSession = isset($_SESSION['plist-list']) && isset($_SESSION['cce-sett']);
$ucmd = filter_input(INPUT_POST, 'ucmd', FILTER_SANITIZE_STRING);
$err = '';

/**
 * Helper function
 *
 * @param string $type
 * @param string $plist
 *
 * @return \CCE\abstractPlist
 */
function getPlistObject(string $type, string $plist): \CCE\abstractPlist {
    return match ($type) {
        'oz' => new \CCE\ozmosisPlist($plist),
        'chm' => new \CCE\chameleonPlist($plist),
        'oc' => new \CCE\openCorePlist($plist),
        default => new \CCE\cloverPlist($plist),
    };
}

switch ($ucmd) {
    case 'ocfg':
    case 'nfscfg': {
        try {
            $resetS = filter_has_var(INPUT_POST, 'resetse');
            $isBankFile = $ucmd === 'nfscfg';

            if (!$isBankFile && !isset($_FILES['config']))
                break;

            $plistList = unserialize($_SESSION['plist-list']);
            $settings = unserialize($_SESSION['cce-sett']);
            $cceMode = filter_has_var(INPUT_POST, 'manualmode') ? filter_input(INPUT_POST, 'manualmode', FILTER_SANITIZE_STRING):'';

            if ($isBankFile) {
                $bid = filter_input(INPUT_POST, 'bid', FILTER_VALIDATE_INT);
                $db = new simpleDB('sqlite');
                $sqlBind = ['id' => $bid];
                $db_config = $db->fetch('config_list', 'WHERE id = :id', $sqlBind);
                $cceMode = $db_config['type'];

                $db->kill();
            }

            if ($resetS) {
                unset($_SESSION);
                session_destroy();
                session_start();
                $settings->clear();
                $plistList->clear();
            }

            $fileCont = $isBankFile ? gzuncompress(substr($db_config['content'], 2)):file_get_contents($_FILES['config']['tmp_name']);
            $fileName = $isBankFile ? $db_config['name']:str_replace('.plist', '', $_FILES['config']['name']);
            $plist = null;

            if ($fileCont === false)
                throw new Exception();

            if ($cceMode == '') { // Try auto-detect
                if ($_FILES['config']['size'] < 190) { // empty?
                    $cceMode = 'oc';

                } else {
                    $try = [ // we can't do much, just try some common keys
                        'cce' => [ // clover
                            'KernelAndKextPatches','RtVariables','SystemParameters','<key>Board-ID</key>',
                            '<key>GUI</key>'
                        ],
                        'oz' => [ // Ozmosis
                            'Defaults:','<key>Preferences</key>','WholePrelinked','<key>BlockKextCaches</key>',
                            '<key>BooterToPatch</key>','<key>AudioFileName</key>','<key>AudioIoDevicePath</key>',
                            '<key>GenerateCpuStates</key>','<key>PatchOemTableOnly</key>'
                        ],
                        'chm' => [ // chameleon
                            'KernelBooter_kexts','GraphicsEnabler','SMbios','SMboard',
                            '<key>Kernel Flags</key>','<key>KernelPatches</key>', '<key>KextsPatches</key>'
                        ],
                        'oc' => [ // OpenCore
                            '<key>Kernel</key>','<key>Emulate</key>','<key>Quirks</key>','<key>PlatformInfo</key>',
                            '<key>UEFI</key>'
                        ]
                    ];
                    $match = false;

                    foreach ($try as $mode => $keySet) {
                        foreach ($keySet as $key) {
                            if (strstr($fileCont, $key) === false)
                                continue;

                            $cceMode = $mode;
                            $match = true;
                            break 2;
                        }
                    }

                    if (!$match) {
                        $err = 'unkcfg';
                        break; // switch
                    }
                }
            }

            $plist = getPlistObject($cceMode, $fileCont);

            $plistList->add($fileName, $plist, true);
            $settings->initSettings($plistList->getActiveIdx());
            $settings->set($plistList->getActiveIdx(), 'mode', $cceMode);

            $_SESSION['plist-list'] = serialize($plistList);
            $_SESSION['cce-sett'] = serialize($settings);
            $_SESSION['cce-tab'] = null;

        } catch (Throwable $e) {
            $err = 'parseer';
        }
    }
        break;
    case 'switchcfg': {
        if (!$isValidSession)
            break;

        try {
            $plistList = unserialize($_SESSION['plist-list']);
            $idx = filter_input(INPUT_POST, 'idx', FILTER_SANITIZE_STRING);

            $plistList->setActiveIdx($idx);

            $_SESSION['plist-list'] = serialize($plistList);
            $_SESSION['cce-tab'] = null;

        } catch (Throwable $e) {
            $err = 'switcher';
        }
    }
        break;
    case 'upgrade': {
        if (!$isValidSession)
            break;

        try {
            $plistList = unserialize($_SESSION['plist-list']);
            $settings = unserialize($_SESSION['cce-sett']);
            $textMode = $settings->get($plistList->getActiveIdx(), 'text');
            $plistType = $settings->get($plistList->getActiveIdx(), 'mode');
            $plist = $plistList->getPlistObj('');
            $plistUpgrader = \CCE\Upgrader\abstractUpgrader::getUpgraderObj($plistType, $plist, false);

            if ($textMode)
                $plist->parsePlist($plist->getPlistString());

            $plistUpgrader->checkUpgrade();
            $plistUpgrader->upgradePlist();

            if ($textMode)
                $plist->setPlistString();

            $plistList->update($plistList->getActiveIdx(), $plist);
            unset($plistUpgrader);

            $_SESSION['plist-list'] = serialize($plistList);

        } catch (Throwable $e) {
            $err = 'upgrer';
        }
    }
        break;
    case 'ccemode': {
        if (!$isValidSession)
            break;

        try {
            $plistList = unserialize($_SESSION['plist-list']);
            $settings = unserialize($_SESSION['cce-sett']);
            $mode = filter_input(INPUT_POST, 'cce_sett_mode', FILTER_SANITIZE_STRING);
            $plist = getPlistObject($mode, '');

            $settings->set($plistList->getActiveIdx(), 'mode', $mode);
            $settings->set($plistList->getActiveIdx(), 'text', false);

            $plistList->update($plistList->getActiveIdx(), $plist);

            $_SESSION['plist-list'] = serialize($plistList);
            $_SESSION['cce-sett'] = serialize($settings);
            $_SESSION['cce-tab'] = null;

        } catch (Throwable $e) {
            $err = 'modeer';
        }
    }
        break;
    case 'textmode': {
        if (!$isValidSession)
            break;

        try {
            $plistList = unserialize($_SESSION['plist-list']);
            $settings = unserialize($_SESSION['cce-sett']);
            $textModeSett = filter_input(INPUT_POST, 'cce_text_mode', FILTER_VALIDATE_BOOLEAN);
            $plist = $plistList->getPlistObj('');

            if ($textModeSett)
                $plist->setPlistString();
            else
                $plist->parsePlist($plist->getPlistString());

            $plistList->update($plistList->getActiveIdx(), $plist);
            $settings->set($plistList->getActiveIdx(), 'text', $textModeSett);

            $_SESSION['plist-list'] = serialize($plistList);
            $_SESSION['cce-sett'] = serialize($settings);
            $_SESSION['cce-tab'] = null;

        } catch (Throwable $e) {
            $err = 'txmode';
        }
    }
        break;
    case 'reset': {
        unset($_SESSION);
        session_destroy();
    }
        break;
    default:
        break;
}

header('Location: ../../index.php'.($err !== '' ? '?cceerr='.$err : ''), true, 301);
exit(0);
