<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

require_once __DIR__.'/data/editor_utils.php';
require_once __DIR__.'/data/utils.php';
require_once __DIR__.'/data/links.php';

global $link;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo head(); ?>

    <link type="text/css" rel="stylesheet" href="<?php echo $link['bootstrap']; ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo $link['icomoon']; ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo $link['cce']; ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo $link['light_theme']; ?>">
</head>
<body id="maint">
    <div class="container mb-4">
        <div class="row">
            <div class="col-12 text-center mt-5">
                <img src="../style/img/cce-logo.svg" class="img-fluid main-logo ccelogo float-anim" alt="CCE logo">
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center text-white">
                <h2 class="font-bold">Fatal Error :(</h2>
                <br>
                If the problem persists, please open an issue <a class="text-warning" href="https://bitbucket.org/kylon/cloud-clover-editor-cce/issues?status=new&status=open" target="_blank" rel="noopener">here</a> and attach the affected file.
                <br><br>
                If you reset CCE, all your unsaved files will be lost.
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-12 text-center">
                <a href="index.php" class="btn btn-outline-warning">Go back</a> <button type="submit" form="rst" class="btn btn-outline-warning">Reset CCE</button>
            </div>

            <form id="rst" name="reset" method="post" action="data/xhr/upload.php">
                <input type="hidden" name="ucmd" value="reset">
            </form>
        </div>
    </div>

    <footer class="cce-tab-footer maint-footer w-100 text-white">Cloud Clover Editor (CCE) - kylon</footer>
</body>
</html>

