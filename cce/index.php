<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
session_start();

require_once __DIR__.'/data/check_maintenance.php';
require_once __DIR__.'/data/CloudCloverEditor/include/cceUI/cceUI.php';
require_once __DIR__.'/data/editor_utils.php';
require_once __DIR__.'/data/cceTables_utils.php';
require_once __DIR__.'/data/utils.php';
require_once __DIR__.'/data/links.php';

set_exception_handler('fatalError');
initCCE();

global $config, $link;

// shared globals
$plistList = unserialize($_SESSION['plist-list']);
$settings = unserialize($_SESSION['cce-sett']);
$config = $plistList->getPlistObj('');
$curIdx = $plistList->getActiveIdx();
$cceMode = $settings->get($curIdx, 'mode');
$textMode = $settings->get($curIdx, 'text');
$cceUI = \CCE\cceUI::getUIInstance($cceMode);
$funcNamesList = $cceUI->getFuncNamesList();
$funcNamesIter = $funcNamesList->getIterator();
$text = new TextContainer($cceMode);
$upgAlertCls = '';
$upgrLog = [];

require_once __DIR__.'/data/CloudCloverEditor/include/'.\CCE\cceUI::getIncludeFile();
require_once CCE\CCECache::getInstance()->getCachePath().'/'.$text->getLanguage().'/'.$cceMode.'_data.php';

if ($_SESSION['cce-tab'] === null)
    $_SESSION['cce-tab'] = $funcNamesIter->current();

checkConfigUpgrades($cceMode, $upgrLog, $upgAlertCls);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo head(); ?>

        <link type="text/css" rel="stylesheet" href="<?php echo $link['bootstrap']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['simplesidebar']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['icomoon']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['psscrollbar']; ?>">
<?php if ($textMode) { ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $link['codemirror']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cmsimplescrollbar']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cmbase16-dark_theme']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cmdialog']; ?>">
<?php } ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cce']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['light_theme']; ?>">
    </head>
    <body>
        <?php echo drawFilesNavbar($curIdx, $plistList, $settings); ?>

        <div id="wrapper">
            <div class="sidebar-cont">
                <div id="sidebar-wrapper"<?php echo $textMode ? ' class="sidebar-wrap-textmode"':''; ?>>
                    <!-- OPTIONS -->
                    <div class="cce-opts text-center">
                        <button class="btn btn-opt btn-open" data-bs-toggle="modal" data-bs-target="#opencfg" data-ccemenu-click="opencfg-btn">
                            <i class="fa icon-folder-open2" title="<?php echo $text['open_cfg']; ?>"></i>
                        </button>
                        <button class="btn btn-opt download-btn" data-ccemenu-click="download-btn" data-bs-toggle="modal" data-bs-target="#saveas">
                            <i class="fa icon-download" title="<?php echo $text['download']; ?>"></i>
                        </button>
                        <button class="btn btn-opt" data-bs-toggle="modal" data-bs-target="#ccesett" data-ccemenu-click="open-settings">
                            <i class="fa icon-cog3" title="<?php echo $text['settings']; ?>"></i>
                        </button>
                        <button class="btn btn-opt" data-ccemenu-click="open-ccetools">
                            <i class="fa icon-wrench" title="<?php echo $text['tools']; ?>"></i>
                        </button>
                        <button class="btn btn-opt" data-bs-toggle="modal" data-bs-target="#cceinfo">
                            <i class="fa icon-info2" title="<?php echo $text['about']; ?>"></i>
                        </button>
                        <button class="btn btn-up-alert<?php echo $upgAlertCls; ?>" <?php echo $upgAlertCls ? 'data-bs-toggle="modal" data-bs-target="#confupgr"':''; ?>>
                            <i class="fa icon-notification" title="<?php echo $text['upgrade']; ?>"></i>
                        </button>

                        <form id="cceBankForm" name="nfscfg" method="post" action="data/xhr/upload.php">
                            <input type="hidden" name="ucmd" value="nfscfg">
                            <input type="hidden" name="bid" value="">
                        </form>

                        <form id="saveForm" method="post" action="data/xhr/save.php"></form>
                    </div>
<?php if (!$textMode) { ?>
                    <?php echo drawEditorNavbar($funcNamesIter, $cceMode); ?>
                    <?php echo drawCceToolsPanel(); ?>
                </div>

                <?php echo drawSidebarToggleBtn(); ?>
            </div>

            <div class="tab-content cce-content">
                <?php
                if ($config->hasError())
                    echo drawErrorPage();
                else
                    drawCCETabs($funcNamesList, $cceMode);
                ?>
            </div>
<?php } else { ?>
                    <div class="key-hints ps-2">
                        <p>Key shortcuts:</p>
                        <p>
                            <span class="ps-3">Ctrl+F: Find</span><br>
                            <span class="ps-3">Ctrl+G: Replace</span><br>
                            <span class="ps-3">Ctrl+S: Save</span>
                        </p>
                    </div>
                <?php echo drawCceToolsPanel(); ?>
                </div> <!-- sidebar-wrapper close -->

                <?php echo drawSidebarToggleBtn(); ?>
            </div>

            <p class="cce-bank-loadtx loading-texteditor mt-5"><i class="fa icon-spinner5 rotate360-inf"></i> <?php echo $text['loading..']; ?></p>
            <textarea id="texteditor" class="d-none"><?php echo $config->getPlistString(); ?></textarea>
<?php } ?>
            <footer class="cce-tab-footer"><?php echo $text['credit']; ?></footer>

            <!-- Notifications -->
            <?php bootstrapToastBase(); ?>
        </div>

        <!-- LOADING SCREEN -->
        <div id="loadingscr" class="text-center align-middle d-none">
            <div class="spinner-border text-primary" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>

        <!-- SAVE AS.. MODAL -->
        <div id="saveas" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['save_file_as']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center mb-3">
                                    <input type="text" form="saveForm" name="filename" value="" minlength="5" maxlength="40" placeholder="cce_config" class="saveas-text" aria-describedby="cfgNameHelp">
                                    <small id="cfgNameHelp" class="form-text text-muted text-start d-none"><?php echo $text['cce_bank_help']; ?></small>
                                </div>

                                <div class="text-start">
                                    <?php echo drawCheckbox('', '', '', '', 'save_to_bank', false, 'save-to-bank', '', false, false); ?>
                                </div>

                                <div class="bank-config-cid d-none text-center mb-4 mt-3">
                                    <label class="bank-config-cid-t text-center form-label"><?php echo $text['cid_title']; ?></label>
                                    <input type="text" class="bank-cid-text" minlength="12" maxlength="12" placeholder="<?php echo $text['ins_your_cid']; ?>">
                                    <span class="bank-cid-new d-none"></span>
                                </div>

                                <div class="bank-gencid d-none">
                                    <?php echo drawCheckbox('', '', '', '', 'gen_new_cid', false, 'gen-new-cid', false, false, false); ?>
                                </div>

                                <div class="d-none bank-config-lock-flag mt-3">
                                    <label class="bank-config-lock-flag-t text-center form-label"><?php echo $text['bank_edit_mode']; ?></label>
                                    <?php
                                    echo drawRadio('', 'id="bankLockPub"', '', 'n', 'public', 'configBankLockFlag', true);
                                    echo drawRadio('', 'id="bankLockPriv"', '', 'y', 'private', 'configBankLockFlag');
                                    echo drawRadio('', 'id="bankMyLock"', '', 'z', 'my_bank', 'configBankLockFlag');
                                    ?>
                                </div>

                                <div class="d-none bank-cfg-sett mt-3">
                                    <label class="bank-cfg-sett-t text-center form-label"><?php echo $text['settings']; ?></label>
                                    <?php
                                    if ($cceMode == 'cce')
                                        echo drawCheckbox('', '', '', '', 'cce_bank_skip_centry', false, 'skip-centry', '', false, false);

                                    echo drawCheckbox('', '', '', '', 'cce_bank_skip_serials', false, 'skip-sdata', '', false, false);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer p-0">
                        <button type="button" class="btn save-btn m-0" data-click="save-modal"><?php echo $text['save']; ?></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- INFO MODAL -->
        <div id="cceinfo" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['cce']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-lg-5 info-logo float-anim"><img src="../style/img/cce-logo-alt.svg" class="img-fluid"> </div>
                            <div class="col-12 col-lg-7">
                                <p class="text-center projhome mb-4">
                                    <a href="https://www.insanelymac.com/forum/topic/313284-cloud-clover-editor-cce-with-ozmosis-support/" target="_blank" rel="noopener">
                                        Project home page (news and issues)
                                    </a>
                                </p>

                                <p><?php echo $text['sources']; ?>:</p>
                                <ul>
                                    <li><a href="https://bitbucket.org/kylon/cloud-clover-editor-cce" target="_blank" rel="noopener">CCE</a></li>
                                    <li><a href="https://bitbucket.org/kylon/cfpropertylist-cce" target="_blank" rel="noopener">CFPropertyList-CCE</a></li>
                                </ul>

                                <p><?php echo $text['usefull_links']; ?>:</p>
                                <ul>
                                    <li><a href="https://bitbucket.org/kylon/cloud-clover-editor-cce/wiki/Home" target="_blank" rel="noopener">CCE Wiki</a></li>
                                    <li><a href="https://github.com/acidanthera/OpenCorePkg" target="_blank" rel="noopener">OpenCore Git</a></li>
                                    <li><a href="https://dortania.github.io/docs/" target="_blank" rel="noopener">OpenCore Documentation</a></li>
                                    <li><a href="https://github.com/CloverHackyColor/CloverBootloader" target="_blank" rel="noopener">Clover EFI Git</a></li>
                                    <li><a href="http://forge.voodooprojects.org/p/chameleon/source/tree/HEAD/branches/ErmaC" target="_blank" rel="noopener">Chameleon SVN</a></li>
                                </ul>

                                <p class="text-end font-italic"><?php echo $text['see_readme']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- SETTINGS MODAL -->
        <div id="ccesett" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['settings']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 text-start">
                                <p class="cce-sett-title"><?php echo $text['cce_sett_mode']; ?></p>
                                <?php
                                echo drawRadio('form-check-inline', 'data-sett="mode" form="ccemodef"', 'cce-sett', 'cce',
                                    'cloverefi', 'cce_sett_mode', $cceMode === 'cce');

                                echo drawRadio('form-check-inline', 'data-sett="mode" form="ccemodef"', 'cce-sett', 'oz',
                                    'ozmosis', 'cce_sett_mode', $cceMode === 'oz');

                                echo drawRadio('form-check-inline', 'data-sett="mode" form="ccemodef"', 'cce-sett', 'chm',
                                    'chameleon', 'cce_sett_mode', $cceMode === 'chm');

                                echo drawRadio('form-check-inline', 'data-sett="mode" form="ccemodef"', 'cce-sett', 'oc',
                                               'opencore', 'cce_sett_mode', $cceMode === 'oc');
                                ?>

                                <form id="ccemodef" method="post" action="data/xhr/upload.php" class="db-none">
                                    <input type="hidden" name="ucmd" value="ccemode">
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-start mt-4">
                                <p class="cce-sett-title">Text Editor Mode</p>
                                <?php
                                echo drawRadio('form-check-inline', 'data-sett="textmode" form="textmode"', 'cce-sett', true,
                                               'on', 'cce_text_mode', $textMode);

                                echo drawRadio('form-check-inline', 'data-sett="textmode" form="textmode"', 'cce-sett', false,
                                               'off', 'cce_text_mode', !$textMode);
                                ?>

                                <form id="textmode" method="post" action="data/xhr/upload.php" class="db-none">
                                    <input type="hidden" name="ucmd" value="textmode">
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-start mt-4">
                                <p class="cce-sett-title"><?php echo $text['cce_bank_view_mode']; ?></p>
                                <?php
                                echo drawRadio('form-check-inline', 'data-sett="ccebnk"', 'cce-sett', 'cceb',
                                                'cce_bank', 'bankViewMode', true);
                                echo drawRadio('form-check-inline', 'data-sett="ccebnk"', 'cce-sett', 'myb',
                                                'my_bank', 'bankViewMode');
                                ?>

                                <div class="row bankopts-mode-cid d-none">
                                    <div class="col-12 col-md-4">
                                        <div class="input-group mb-3 mt-3">
                                            <input type="text" class="form-control bnkVModeInpt btngroup-inpt-l-unfinished" placeholder="<?php echo $text['ins_your_cid']; ?>" minlength="12" maxlength="12">
                                            <button class="btn btn-outline-secondary btngroup-r-unfinished" type="button" data-click="bankopts-view-sett"><i class="fa icon-arrow-right3"></i></button>
                                        </div>
                                        <!-- col-md-8 here -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-12">
                                <p class="bank-conf-man-t text-center"><?php echo substr($curIdx, 0, -7); ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-lg-5">
                                <div class="bankopts-nocfg font-italic"><?php echo $text['no_options']; ?></div>

                                <div class="bankopts-cfg-form text-end d-none">
                                    <div class="text-center mb-3 text-start bankopts-lock-flag">
                                        <label class="bankopts-lock-flag-t text-start form-label"><?php echo $text['bank_edit_mode']; ?></label>
                                        <?php
                                        echo drawRadio('form-check-inline', 'id="optLockPub"', '',
                                                        'n', 'public', 'bankoptsLockFlag', true);
                                        echo drawRadio('form-check-inline', 'id="optLockPriv"', '',
                                                        'y', 'private', 'bankoptsLockFlag');
                                        echo drawRadio('form-check-inline', 'id="optMyLock"', '',
                                                        'z', 'my_bank', 'bankoptsLockFlag');
                                        ?>
                                    </div>

                                    <div class="text-center mb-3 bankopts-ren-cfg mt-4">
                                        <label for="bnkre" class="bankopts-ren-cfg-t text-start form-label"><?php echo $text['cce_bank_name_title']; ?></label>
                                        <input id="bnkre" type="text" class="bankopts-ren-text" value="" aria-describedby="confNameHelp" minlength="5" maxlength="40">
                                        <label class="bankopts-ren-text-err d-none"><?php echo $text['cce_bank_name_inuse']; ?></label>

                                        <small id="confNameHelp" class="form-text text-muted text-start">
                                            <?php echo $text['cce_bank_help']; ?>
                                        </small>
                                    </div>

                                    <div class="text-center mb-3 text-start mt-4">
                                        <?php echo drawCheckbox('', '', '', '', 'cce_bank_del_cfg', false, 'bankopts-del-cfg', '', false, false); ?>
                                    </div>

                                    <div class="input-group bankopts-cid-cfg mb-3 mt-3">
                                        <input type="text" class="form-control bnkSvCfgCid btngroup-inpt-l-unfinished" placeholder="<?php echo $text['ins_your_cid']; ?>" minlength="12" maxlength="12">
                                        <button class="btn btn-outline-secondary btngroup-r-unfinished" type="button" data-click="bankopts-sv-cfg"><i class="fa icon-arrow-right3"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php if (!$textMode) { ?>
        <!-- COPY TO MODAL -->
        <div id="copyto" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['copy_to']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center mb-3">
                            <label class="form-label" for="copytolist"><?php echo $text['sel_config']; ?></label>
                            <select id="copytolist" class="form-select copytoconfig-list" name="copytoconfig"></select>
                        </div>
                    </div>
                    <div class="modal-footer p-0">
                        <button type="button" class="btn copyto-btn m-0" data-source="">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- PATCHES DB MODAL -->
        <div id="patchesdb" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['patches_db_t']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-end">
                            <div class="col-12 col-sm-6 col-lg-4">
                                <input type="search" placeholder="<?php echo $text['search']; ?>" class="db-patch-search form-control" value="">
                                <label class="bg-secondary db-patch-search-label form-label"></label>
                            </div>
                        </div>
                        <div id="dbPatchesList" class="row mt-3">
                            <!-- Patches list (js) -->
                            <p class="cce-bank-loadtx">
                                <i class="fa icon-spinner5 rotate360-inf"></i> <?php echo $text['loading..']; ?>
                            </p>
                        </div>

                        <div class="row mt-4">
                            <div class="col-12">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination"><!-- js --></ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>
        <!-- OPEN MODAL -->
        <div id="opencfg" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['open_cfg']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <label class="btn btn-outline-secondary mb-0 btngroup-l-unfinished bankmode-open-modal">CCE Bank</label>
                                    <input type="search" class="bank-search form-control btngroup-inpt-l-unfinished" value="" placeholder="<?php echo $text['search']; ?>">
                                    <label class="btn btn-outline-secondary mb-0 btngroup-r-unfinished" for="cce-ocfg">
                                        <i class="fa icon-folder-open2"></i> <?php echo $text['browse']; ?>
                                    </label>
                                    <select name="manualmode" class="form-select selectgroup-unfinished" form="open-cfg" title="Bootloader detection">
                                        <option selected value="">Auto-Detect</option>
                                        <option value="cce">Clover EFI</option>
                                        <option value="oz">Ozmosis</option>
                                        <option value="chm">Chameleon</option>
                                        <option value="oc">OpenCore</option>
                                    </select>

                                    <form id="open-cfg" method="post" action="data/xhr/upload.php" enctype="multipart/form-data">
                                        <input id="cce-ocfg" name="config" type="file" accept=".plist" value="">
                                        <input type="hidden" name="ucmd" value="ocfg">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="cce-bank-container">
                                    <div class="row">
                                        <!-- CCE Bank container (js) -->
                                        <p class="cce-bank-loadtx">
                                            <i class="fa icon-spinner5 rotate360-inf"></i> <?php echo $text['loading..']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- CONFIG UPGRADE LOG MODAL -->
        <div id="confupgr" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['cce_config_upgrade_t']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <ul class="fw-bold fst-italic">
                                    <?php foreach ($upgrLog as $msg) { ?>
                                        <li><?php echo $msg; ?></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-end">
                                <form method="post" action="data/xhr/upload.php">
                                    <button type="submit" class="btn btn-upgrade">
                                        <i class="fa icon-angle-double-up"></i> <span><?php echo $text['upgrade']; ?></span>
                                    </button>
                                    <input type="hidden" name="ucmd" value="upgrade">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            if (filter_has_var(INPUT_GET, 'cceerr'))
                echo drawOpenPlistErrModal(filter_input(INPUT_GET, 'cceerr', FILTER_SANITIZE_STRING));

            echo includeJsScripts($cceMode, $textMode);
        ?>
    </body>
</html>
