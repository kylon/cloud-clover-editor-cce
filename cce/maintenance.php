<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

if (!file_exists(__DIR__.'/../maintenance')) {
    header('Location: index.php', true, 301);
    exit(0);
}

require_once __DIR__.'/data/editor_utils.php';
require_once __DIR__.'/data/utils.php';
require_once __DIR__.'/data/links.php';

global $link;

$text = new TextContainer('oc');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo head(); ?>

    <link type="text/css" rel="stylesheet" href="<?php echo $link['bootstrap']; ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo $link['icomoon']; ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo $link['cce']; ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo $link['light_theme']; ?>">
</head>
<body id="maint">
    <div class="container mb-4">
        <div class="row">
            <div class="col-12 text-center mt-5">
                <img src="../style/img/cce-logo.svg" class="img-fluid main-logo ccelogo float-anim" alt="CCE logo">
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <p class="font-bold mtext"><h2 class="text-white">Maintenance mode</h2></p>
            </div>
        </div>
    </div>

    <footer class="cce-tab-footer maint-footer w-100 text-white"><?php echo $text['credit']; ?></footer>
</body>
</html>

