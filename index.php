<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

if (version_compare(phpversion(), '7.3.0', '<')) {
    session_destroy();
    die('CCE requires PHP 7.3.0 or above, please upgrade!');

} else if (file_exists('maintenance')) {
    session_destroy();
    header('Location: cce/maintenance.php', true, 302);
    exit(0);

} else {
    header('Location: cce/index.php', true, 301);
    exit(0);
}
