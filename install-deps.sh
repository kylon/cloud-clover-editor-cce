#!/bin/bash
clear

npm=$(command -v npm)
composer=$(command -v composer)

if [ -z "$npm" ]; then
	echo -e "Please install npm or add it to your PATH\n";
	exit 1

elif [ -z "$composer" ]; then
	echo -e "Please install composer or add it to your PATH\n";
	exit 1;
fi

npm install --production
cd cce/data/modules/cfpropertylist-cce
composer install --no-dev
