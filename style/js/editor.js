/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

$(function() {
    const isTextMode = typeof CCETextEditor === "object";
    const sidebarEl = $('#sidebar-wrapper');
    const bankModeLabel = $('.bankmode-open-modal');
    const detectErModal = $('#detecterrm');
    const cceBankFormEl = $('#cceBankForm');
    const navbarConfigList = $('#config-list');
    const cceSettingsModal = $('#ccesett');
    const cceTools = sidebarEl.find('#ccetools');
    const saveAsModal = $('#saveas');
    const navScroll = new PerfectScrollbar('#sidebar-wrapper', {wheel:0.5});
    const topbarScroll = new PerfectScrollbar('#config-list', {
        wheelPropagation: true,
        suppressScrollY: true,
        useBothWheelAxes: true
    });

    if (detectErModal.length) {
        window.history.replaceState({}, document.title, window.location.href.replace('cceerr=', ''));
        detectErModal.modal('show');
    }

    // Bootstrap 5.0+ workaround
    $(document).on('click', '.form-check-label', function () {
        const _this = $(this);
        const input = _this.parent().find('input');

        $('.form-check-label[for="tempId"], .form-check-input[id="tempId"]').each(function () {
            $(this).removeAttr('for id');
        });

        if (!input.prop('disabled')) {
            _this.attr('for', 'tempId');
            input.attr('id', 'tempId');
        }
    });

    $(document).on('hidden.bs.toast', function () {
        Utils.showPendingNotification();
    });

    $(document).on('click', '.pin-nav-btn', function (e) {
        e.stopPropagation();
        EditorUtils.toggleSidebar(isTextMode, true);
    });

    $(document).on('click', '.sidebar-toggle-btn', function (e) {
        e.stopImmediatePropagation();

        if (window.innerWidth < 1200) // bootstrap xl
            EditorUtils.toggleSidebar(isTextMode);
    });

    $(document).on('click', 'html', function (e) {
        if (window.innerWidth < 1200 && EditorUtils.isSidebarPinned()) {
            e.stopImmediatePropagation();
            EditorUtils.toggleSidebar(isTextMode);
        }
    });

    // Open from file
    $(document).on('change', '#cce-ocfg', async function () {
        Utils.loadingScr(true);

        if (isTextMode)
            await CCETextEditor.writePlistInContainer();

        $(this).parent().trigger('submit');
    });

    // Open from CCE Bank
    $(document).on('click', '.bank-config-box', async function () {
        Utils.loadingScr(true);

        if (isTextMode)
            await CCETextEditor.writePlistInContainer();

        cceBankFormEl.find('input[name="bid"]').val($(this).attr('data-idx'));
        cceBankFormEl.trigger('submit');
    });

    // CCE Settings
    $(document).on('change', '.cce-sett', async function () {
        const _this = $(this);
        const sett = _this.attr('data-sett');

        switch (sett) {
            case 'textmode': {
                Utils.loadingScr(true);

                if (isTextMode)
                    await CCETextEditor.writePlistInContainer();

                _this.parent().parent().find('form').trigger('submit');
            }
                break;
            case 'mode': {
                Utils.loadingScr(true);
                _this.parent().parent().find('form').trigger('submit');
            }
                break;
            case 'ccebnk': {
                const bankOptsCid = cceSettingsModal.find('.bankopts-mode-cid');
                const bmode = _this.val();

                if (bmode === 'cceb') {
                    const fdata = { Setting: sett, BankMode: bmode, CID: '' };

                    bankOptsCid.addClass('d-none');

                    // delete session data
                    Utils.fetchReq(FetchURL.Settings, 'ccesett', fdata, res => {
                        if (res === false)
                            throw new Error('ccbe_sett_e');

                    }).catch((e) => {
                        Utils.showNotification(ToastType.Error, ToastMessage.SettingsE);
                        console.log(e);
                    });
                    break;
                }

                bankOptsCid.removeClass('d-none');
                // -> bankopts-view-sett
            }
                break;
            default:
                break;
        }
    });

    $(document).on('keyup', '.bank-search', function () {
        const val = $(this).val();

        EditorUtils.drawConfigs(val === '' ? 'listall':val);
    });

    // Save config / Save in CCE Bank
    $(document).on('keyup', '.saveas-text', function (e) {
        if (e.which === 13 && !saveAsModal.find('.save-to-bank').prop('checked'))
            saveAsModal.modal('hide');
    });

    $(document).on('change', '.save-to-bank', async function () {
        const _this = $(this);
        const confNameInpt = saveAsModal.find('.saveas-text');
        const fdata = { ConfigName: navbarConfigList.find('.fhandle-active .handle-name').text() };
        const configCidEl = saveAsModal.find('.bank-config-cid');
        const parent = configCidEl.parent();
        let ret = '';

        if (!_this.prop('checked')) {
            EditorUtils.resetSaveToBankInputs();
            return false;
        }

        confNameInpt.val('').attr('placeholder', 'Configuration name');
        saveAsModal.find('#cfgNameHelp').removeClass('d-none');

        await Utils.fetchReq(FetchURL.CCEBank, 'chksvtobnk', fdata, function (res) {
            ret = res;

            if (res === false)
                throw new Error('chk_bank_e');

        }).catch((e) => {
            Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
            console.log(e);
        });

        if (ret === false)
            return false;

        if (ret === -1) { // Not found in CCE Bank
            configCidEl.removeClass('d-none');
            parent.find('.bank-gencid').removeClass('d-none');
            parent.find('.bank-config-lock-flag').removeClass('d-none');

        } else {
            await Utils.fetchReq(FetchURL.CCEBank, 'getbnkcfglock', fdata, function (res) {
                if (res === false)
                    throw new Error('getlock_bnk_e');

                confNameInpt.val(fdata.ConfigName).prop('disabled', true);
                _this.addClass('updmode');

                if (res !== 'n') // locked config
                    configCidEl.val('').removeClass('d-none').find('.bank-cid-text').addClass('material-text-inp-err');

            }).catch((e) => {
                Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
                console.log(e);
            });
        }

        parent.find('.bank-cfg-sett').removeClass('d-none');
    });

    $(document).on('change', '.gen-new-cid', async function () {
        const _this = $(this);
        const configCidEl = saveAsModal.find('.bank-config-cid').first();

        if (_this.prop('checked')) {
            await Utils.fetchReq(FetchURL.CCEBank, 'genbnkcid', null, function (res) {
                if (res === false)
                    throw new Error('gen_bnk_e');

                configCidEl.find('.bank-cid-text').addClass('d-none').val('');
                configCidEl.find('.bank-cid-new').text(res).removeClass('d-none');

            }).catch((e) => {
                Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
                _this.prop('checked', false);
                console.log(e);
            });

        } else {
            configCidEl.find('.bank-cid-text').removeClass('d-none');
            configCidEl.find('.bank-cid-new').text('').addClass('d-none');
        }
    });

    $(document).on('change', '.bankopts-del-cfg', function () {
        const disabled = $(this).prop('checked');

        cceSettingsModal.find('input[name="bankoptsLockFlag"]').prop('disabled', disabled);
        cceSettingsModal.find('.bankopts-ren-text').prop('disabled', disabled).toggleClass('color-disabled');
    });

    // CCE Tools - Coder
    $(document).on('click', '#encdec-tool', function () {
        const fromT = cceTools.find('#from-encdec').val();
        const toT = cceTools.find('#to-encdec').val();
        const data = cceTools.find('#encdec-val').val().trim();
        const resElm = cceTools.find('.encdec-tool-res');

        if (!data)
            return;

        try {
            resElm.text(Coder.Codify(data, fromT, toT));

        } catch (e) {
            console.log('CCE Coder error: ' + e.message);
            resElm.text('Invalid input data');
        }
    });

    $(document).on('click', '[data-ccemenu-click]', async function () {
        const _this = $(this);

        switch (_this.attr('data-ccemenu-click')) {
            case 'opencfg-btn': {
                const bankViewMode = await EditorUtils.getBankViewMode();

                bankModeLabel.text(bankViewMode === 'myb' ? 'My Bank':'CCE Bank');
                EditorUtils.drawConfigs('listall');
            }
                break;
            case 'download-btn': {
                const saveToBnkEl = saveAsModal.find('.save-to-bank');
                const confName = navbarConfigList.find('.fhandle-active .handle-name').text();

                if (isTextMode) {
                    Utils.loadingScr(true);
                    await CCETextEditor.writePlistInContainer();
                    CCETextEditor.enableAutoSave();
                    Utils.loadingScr(false);
                }

                saveAsModal.find('.saveas-text').val(confName);

                if (saveToBnkEl.prop('checked'))
                    saveToBnkEl.prop('checked', false).trigger('change');
            }
                break;
            case 'open-settings': {
                const bnkFormEl = cceSettingsModal.find('.bankopts-cfg-form').first();
                const delChkEl = bnkFormEl.find('.bankopts-del-cfg');
                const bankVieMode = await EditorUtils.getBankViewMode();
                let isBankCfg = false;

                if (bankVieMode === false)
                    break;

                const bnkViewModeRadio = cceSettingsModal.find('input[name="bankViewMode"][value="'+bankVieMode+'"]');

                bnkViewModeRadio.prop('checked', true).parent().parent().find('.bnkVModeInpt').removeClass('material-text-inp-err');

                if (bankVieMode === 'myb')
                    cceSettingsModal.find('.bankopts-mode-cid').removeClass('d-none');
                else
                    cceSettingsModal.find('.bankopts-mode-cid').addClass('d-none');

                await Utils.fetchReq(FetchURL.CCEBank, 'chkisbnkcfg', null, res => {
                    isBankCfg = res;

                    if (res === false)
                        throw new Error('isbcfg_e');

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
                    console.log(e);
                });

                if (isBankCfg === false || isBankCfg <= 0) {
                    cceSettingsModal.find('.bankopts-nocfg').removeClass('d-none');
                    bnkFormEl.addClass('d-none');
                    break;
                }

                await Utils.fetchReq(FetchURL.CCEBank, 'getbankoptscfgdata', null, res => {
                    if (res === false)
                        throw new Error('get_bopts_e');

                    cceSettingsModal.find('.bankopts-nocfg').addClass('d-none');
                    bnkFormEl.removeClass('d-none');
                    bnkFormEl.find('input[name="bankoptsLockFlag"][value="'+res['locked']+'"]').prop('checked', true);
                    bnkFormEl.find('.bankopts-ren-text').val(res['name']);
                    bnkFormEl.find('.bankopts-ren-text-err').addClass('d-none');
                    bnkFormEl.find('.material-text-inp-err').removeClass('material-text-inp-err');

                    if (delChkEl.prop('checked'))
                        delChkEl.prop('checked', false).trigger('change');

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
                    console.log(e);
                });
            }
                break;
            case 'open-ccetools': {
                const tabsMenu = sidebarEl.find(isTextMode ? '.key-hints':'.sidebar-nav');

                tabsMenu.toggleClass('flip-vertical-left');
                cceTools.toggleClass('flip-vertical-right');

                const timeout = setTimeout($.proxy(function () {
                    clearTimeout(timeout);
                    tabsMenu.toggleClass('d-none');
                    cceTools.toggleClass('d-none');
                    _this.toggleClass('text-white');
                    navScroll.update();
                }, this), 220);
            }
                break;
        }
    });

    $(document).on('click', '[data-click]', async function (e) {
        const _this = $(this);

        switch (_this.attr('data-click')) {
            case 'config-add': {
                e.stopPropagation();

                const files = navbarConfigList.find('.file-handle');
                const newFile = files.first().clone();

                Utils.loadingScr(true);

                await Utils.fetchReq(FetchURL.Editor, 'sncfg', null, function (res) {
                    if (res === false)
                        throw new Error('ncfg_idx_e');

                    newFile.attr('title', res).attr('data-idx', res).removeClass('fhandle-active');
                    newFile.find('.handle-name').text(res.substring(0, res.length-7));
                    newFile.find('i').first().attr('class', 'fa icon-file-opencore');
                    newFile.find('[name="idx"]').val(res);
                    newFile.insertAfter(files.last());
                    topbarScroll.update();

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.NewFileE);
                    console.log(e);
                });

                Utils.loadingScr(false);
            }
                break;
            case 'close-cfg': {
                e.stopPropagation();

                const el = _this.parent();
                const listLen = el.parent().find('.file-handle').length;
                const fdata = { RemovedIdx: el.attr('data-idx'), NextIdx: '' };

                Utils.loadingScr(true);

                if (el.hasClass('fhandle-active') && listLen > 1) {
                    let tmp = el.prev();

                    if (tmp.length === 0)
                        tmp = el.next();

                    fdata.NextIdx = tmp.attr('data-idx');

                } else if (listLen === 1) {
                    fdata.NextIdx = 'x';
                }

                Utils.fetchReq(FetchURL.Editor, 'dscfg', fdata, res => {
                    if (res === false)
                        throw new Error('del_e');

                    if (fdata.NextIdx !== '') {
                        window.location.reload();
                        return;
                    }

                    // inactive config, just remove
                    el.remove();
                    topbarScroll.update();
                    Utils.loadingScr(false);

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.RemoveFileE);
                    console.log(e);
                });
            }
                break;
            case 'switch-cfg': {
                e.stopPropagation();

                if (_this.hasClass('fhandle-active'))
                    break;

                Utils.loadingScr(true);

                if (isTextMode)
                    await CCETextEditor.writePlistInContainer();

                _this.find('.session-switch-form').trigger('submit');
            }
                break;
            case 'save-modal': {
                const saveToBnkEl = saveAsModal.find('.save-to-bank');
                const saveAsTextEl = saveAsModal.find('.saveas-text');

                if (!saveToBnkEl.prop('checked')) {
                    saveAsModal.modal('hide');
                    $('#saveForm').trigger('submit');
                    saveAsTextEl.val('');
                    break;
                }

                const cmd = saveToBnkEl.hasClass('updmode') ? 'upd':'new';
                const fdata = {
                    CID: '',
                    ConfigName: Utils.sanitizeString(saveAsTextEl.val()),
                    LockFlag: 'n',
                    IsNewCID: true,
                    SkipCentry: saveAsModal?.find('.skip-centry').prop('checked') ?? false,
                    SkipSdata: saveAsModal?.find('.skip-sdata').prop('checked') ?? false
                };

                if (cmd === 'new') {
                    const cidEl = saveAsModal.find('.bank-cid-text').first();
                    const isNewCid = saveAsModal.find('.gen-new-cid').prop('checked');
                    const cid = isNewCid ? saveAsModal.find('.bank-cid-new').text() : cidEl.val();
                    const lockFlag = saveAsModal.find('input[name="configBankLockFlag"]:checked').val();
                    let isValidName = false;

                    await Utils.fetchReq(FetchURL.CCEBank, 'chkbnkname', {ConfigName: fdata.ConfigName}, res => {
                        isValidName = res;

                        if (res === false)
                            throw new Error('chk_bnk_e');

                    }).catch((e) => {
                        Utils.showNotification(ToastType.Error, ToastMessage.BankInvalidNameE);
                        console.log(e);
                    });

                    if (isValidName !== true || fdata.ConfigName === '') {
                        saveAsTextEl.addClass('material-text-inp-err').trigger('focus');
                        Utils.showNotification(ToastType.Error, ToastMessage.BankInvalidNameE);
                        break;
                    }

                    saveAsTextEl.removeClass('material-text-inp-err');

                    if (cid === '' || cid.length !== 12) {
                        cidEl.addClass('material-text-inp-err').trigger('focus');
                        break;
                    }

                    cidEl.removeClass('material-text-inp-err');

                    fdata.CID = cid;
                    fdata.LockFlag = lockFlag;
                    fdata.IsNewCID = isNewCid;

                } else {
                    const cidEl = saveAsModal.find('.bank-config-cid').first();
                    const isPublic = cidEl.hasClass('d-none');
                    const cid = isPublic ? 'pub' : cidEl.find('.bank-cid-text').val();
                    const lockFlag = isPublic ? 'n':'y';
                    const ret = isPublic ? true : (cid !== '' && cid.length === 12);

                    if (!ret)
                        break;

                    fdata.CID = 'u_' + cid;
                    fdata.LockFlag = lockFlag;
                    fdata.IsNewCID = false;
                }

                Utils.loadingScr(true);

                await Utils.fetchReq(FetchURL.CCEBank, 'svtobnk', fdata, res => {
                    if (res === false)
                        throw new Error('save_bnk_e');

                    if (cmd === 'new') {
                        const name = res.substring(0, res.length-7);

                        navbarConfigList.find('.fhandle-active').attr('data-idx', res).attr('title', res).find('.handle-name').text(name);
                        cceSettingsModal.find('.bank-conf-man-t').text(name);
                    }

                    Utils.showNotification(ToastType.Success, ToastMessage.Saving);

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
                    console.log(e);
                });

                Utils.loadingScr(false);
                saveAsModal.modal('hide');
                EditorUtils.resetSaveToBankInputs();
                saveAsTextEl.val('');
            }
                break;
            case 'bankopts-view-sett': {
                const viewMode = cceSettingsModal.find('input[name="bankViewMode"]:checked').val();
                const cidEl = _this.parent().parent().find('.bnkVModeInpt');
                const fdata = { Setting: 'ccebnk', BankMode: viewMode, CID: cidEl.val() };

                if (viewMode === 'myb' && (fdata.CID === '' || fdata.CID.length !== 12)) {
                    cidEl.addClass('material-text-inp-err').focus();
                    break;
                }

                await Utils.fetchReq(FetchURL.Settings, 'ccesett', fdata, res => {
                    if (res === false)
                        throw new Error('sett_vmode_e');

                    Utils.showNotification(ToastType.Success, ToastMessage.MyBankMode);
                    cidEl.removeClass('material-text-inp-err');

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
                    cidEl.addClass('material-text-inp-err');
                    console.log(e);
                });

                cidEl.val('');
                EditorUtils.drawConfigs('listall'); // refresh
            }
                break;
            case 'bankopts-sv-cfg': {
                const bankOptsEl = cceSettingsModal.find('.bankopts-cfg-form').first();
                const bnkRenTxEl = bankOptsEl.find('#bnkre');
                const bnkRenTxInUse = bankOptsEl.find('.bankopts-ren-text-err');
                const cidEl = _this.parent().parent().find('.bnkSvCfgCid');
                const isDeleteCfg = bankOptsEl.find('.bankopts-del-cfg').prop('checked');
                const cmd = isDeleteCfg ? 'delcfgfrombnk' : 'updbnkcfg';
                const fdata = {
                    CID: cidEl.val(),
                    ConfigName: Utils.sanitizeString(bnkRenTxEl.val()),
                    LockFlag: bankOptsEl.find('input[name="bankoptsLockFlag"]:checked').val()
                };
                let isValidName = false;

                Utils.loadingScr(true);
                cidEl.removeClass('material-text-inp-err');
                bnkRenTxEl.removeClass('material-text-inp-err');
                bnkRenTxInUse.addClass('d-none');

                await Utils.fetchReq(FetchURL.CCEBank, 'chkbnkname', {ConfigName: fdata.ConfigName}, res => {
                    const oldFileName = cceSettingsModal.find('.bank-conf-man-t').text();

                    isValidName = oldFileName === fdata.ConfigName ? true:res;

                    if (isValidName !== true && res === false)
                        throw new Error('ren_bnk_e');

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.BankInvalidNameE);
                    console.log(e);
                });

                if (isValidName !== true || fdata.ConfigName === '') {
                    bnkRenTxEl.addClass('material-text-inp-err').trigger('focus');
                    Utils.showNotification(ToastType.Error, ToastMessage.BankInvalidNameE);

                    if (fdata.ConfigName !== '')
                        bnkRenTxInUse.removeClass('d-none');

                    Utils.loadingScr(false);
                    break;

                } else if (fdata.CID === '' || fdata.CID.length !== 12) {
                    Utils.loadingScr(false);
                    cidEl.addClass('material-text-inp-err').trigger('focus');
                    break;
                }

                await Utils.fetchReq(FetchURL.CCEBank, cmd, fdata, res => {
                    if (res === false)
                        throw new Error(cmd.substring(0, 3)+'_bnk_e');

                    if (typeof res === 'string' && !isDeleteCfg) {
                        const nName = res.substring(0, res.length-7);

                        navbarConfigList.find('.fhandle-active').attr('data-idx', res).attr('title', res).find('.handle-name').text(nName);
                        cceSettingsModal.find('.bank-conf-man-t').text(nName)

                    } else if (res && isDeleteCfg) {
                        bankOptsEl.addClass('d-none').parent().find('.bankopts-nocfg').removeClass('d-none');
                    }

                    Utils.showNotification(ToastType.Success, ToastMessage.Saving);
                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
                    console.log(e);
                });

                cidEl.val('');
                Utils.loadingScr(false);
                EditorUtils.drawConfigs('listall'); // refresh
            }
                break;
            default:
                break;
        }
    });
});