/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const GUIBaseEventHandler = (() => {
    const base = {};

    base.doTriggerEv = evdata => {
        switch (evdata.name) {
            case 'itmLiveTitle': { // fancy panels
                const modalTitleEl = $('.modal.show').find('.modal-title');
                const iconTitleEl = evdata.shownModalTriggerBtn.find('.single-item-title');

                modalTitleEl.attr('title', evdata.fetchData.Value).html(evdata.fetchData.Value);
                iconTitleEl.attr('title', evdata.fetchData.Value).html(evdata.fetchData.Value);
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doSortUpdate = evdata => {
        switch (evdata.name) {
            case 'panel-ocEntry':
            case 'panel-ocTools':
            case 'panel-ocMemDevs':
            case 'panel-ocKernAdd':
            case 'panel-cToolsE':
            case 'panel-cLegE': {
                const delBtnEl = evdata.itemRef.find('.del-cEntry-btn');
                const panel = $('.'+evdata.name);
                const factoryEl = panel.find('.single-item-group.d-none').first().detach();
                const elemID = evdata.name.split('-')[1];
                let idx = 0;

                evdata.fetchData.Path = delBtnEl.attr('data-path');
                evdata.fetchData.OldKey = delBtnEl.attr('data-index');

                panel.find('.single-'+elemID).each(function () {
                    const _this = $(this);
                    const modalentry = _this.next();
                    const iconEl = _this.find('.single-item-icon > i');
                    const target = iconEl.attr('data-bs-target').split('-')[0];

                    iconEl.attr('data-bs-target', target + '-' + idx);
                    _this.find('[data-subid]').attr('data-subid', idx);
                    _this.find('[data-id="cp-'+elemID+'"], [data-id="del-'+elemID+'"]').attr('data-index', idx);

                    modalentry.find('[data-subid]').each(function () {
                        $(this).attr('data-subid', idx);
                    });

                    modalentry.find('[data-path]').each(function () {
                        const _this = $(this);
                        const path = _this.attr('data-path').split('/');

                        path[path.length - 1] = idx.toString();

                        _this.attr('data-path', path.join('/'));
                    });

                    modalentry.attr('class', 'modal fade modal'+elemID+'-' + idx);
                    ++idx;
                });

                // factory element must be first
                panel.prepend(factoryEl);

                evdata.fetchData.NewKey = delBtnEl.attr('data-index');
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doComboChange = evdata => {
        return true;
    }

    base.doComboInputChange = evdata => {
        return true;
    }

    base.doCCESelChange = async evdata => {
        switch (evdata.name) {
            case 'b-args': {
                const argsCont = $('.b-args-tx');

                evdata.fetchData.Path = argsCont.attr('data-path');
                evdata.fetchData.Field = argsCont.attr('data-field');

                if (evdata.fetchData.Value === 'del') {
                    evdata.retData.cmd = 'unset';
                    evdata.fetchData.Value = '';

                    argsCont.val('');
                    break;
                }

                const argVal = argsCont.val();
                const current = argVal.split(' ');
                const space = argVal === '' ? '':' ';

                for (let ar=0, len=current.length; ar<len; ++ar) {
                    const newVal = evdata.fetchData.Value.substring(0, 4);
                    const curVal = current[ar].substring(0, 4);

                    if (current[ar] === evdata.fetchData.Value) // this arg exists, no config write needed
                        return false;

                    if (newVal === curVal && (newVal === 'dark' || newVal === 'arch' || newVal === 'npci')) {
                        current.splice(ar, 1);
                        break;
                    }
                }

                evdata.fetchData.Value = current.join(' ') + space + evdata.fetchData.Value;

                argsCont.val(evdata.fetchData.Value);
            }
                break;
            case 'matchoses': {
                const textarea = evdata.objRef.parent().parent().parent().parent().find('.matchos-tx');

                evdata.fetchData.Path = textarea.attr('data-path');
                evdata.fetchData.Field = textarea.attr('data-field');

                switch (evdata.fetchData.Value) {
                    case 'del': {
                        evdata.retData.cmd = 'unset';
                        evdata.fetchData.Value = '';

                        textarea.val('');
                    }
                        break;
                    case 'All': {
                        textarea.val(evdata.fetchData.Value);
                    }
                        break;
                    default: {
                        const current = textarea.val().split(',');
                        const selected = evdata.fetchData.Value.split('.');
                        const osBaseSelected = selected[0]+'.'+selected[1];
                        const isAllOSVer = selected[2] === 'x';
                        const toRemove = [];

                        for (let ar=0, len=current.length; ar<len; ++ar) {
                            const currentLoop = current[ar].split('.');
                            const osBaseCurrent = currentLoop[0]+'.'+currentLoop[1];

                            if (current[ar] === evdata.fetchData.Value || (osBaseCurrent === osBaseSelected && currentLoop[2] === 'x'))
                                return false;

                            if (current[ar] === 'All' || (isAllOSVer && osBaseCurrent === osBaseSelected && current[ar] !== evdata.fetchData.Value))
                                    toRemove.push(ar);
                        }

                        while(toRemove.length)
                            current.splice(toRemove.pop(), 1);

                        current.push(evdata.fetchData.Value);

                        evdata.fetchData.Value = current.filter(el => { return el !== ''; }).join(',');

                        textarea.val(evdata.fetchData.Value);
                    }
                        break;
                }
            }
                break;
            case 'smbios-sel': {
                SMBIOSUtils.switchSMBIOS(evdata.fetchData.Value);
                return false;
            }
            case 'datatype': {
                evdata.fetchData.DataType = evdata.fetchData.Value;
                evdata.fetchData.Value = '';
                evdata.retData.cmd = 'setdatatype';
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doCCECheckboxChange = evdata => {
        switch (evdata.name) {
            case 'btrf':
            case 'csrf': {
                const elem = $('.'+evdata.name+'Conf');
                let flag = '0x' + FlagsUtils.genSimpleFlag(evdata.name).toString(16);

                evdata.retData.cmd = 'setval';
                evdata.fetchData.Path = elem.attr('data-path');
                evdata.fetchData.Field = elem.attr('data-field');
                evdata.fetchData.Value = flag;

                $('.'+evdata.name+'-fflag').text(flag);
                elem.val(flag);
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doCCERadioChange = evdata => {
        return true;
    }

    base.doTristateCheckboxChange = evdata => {
        return true;
    }

    base.doTDClickEvt = async evdata => {
        switch (evdata.name) {
            case 'ocpPropS': // opencore device props
            case 'ocnaPropS': // opencore nvram add
            case 'pPropS': { // clover device props
                const path = evdata.parentObjRef.attr('data-path') + '/' + evdata.parentObjRef.attr('data-index');
                const pdata = path.charAt(path.length - 1) !== '/' ? await Utils.getPropertiesData('getpprops', path) : [];
                const event = evdata.name.toLowerCase();
                const rows = [];

                rows.push(evdata.factoryObjRef);

                for (let i=0, l=pdata?.length ?? 0; i<l; ++i) {
                    const row = Utils.createTableRow(evdata.tableObjRef, path);

                    row.attr('data-index', pdata[i]['key']);
                    row.find('[data-inlineeditEv="'+event+'"]').attr('title', pdata[i]['key']).text(pdata[i]['key']);
                    row.find('[data-inlineeditEv="'+event+'Val"]').attr('data-field', pdata[i]['key']).attr('title', pdata[i]['val']).text(pdata[i]['val']);
                    row.find('option[value="'+pdata[i]['type']+'"]').prop('selected', true);
                    rows.push(row);
                }

                evdata.tableObjRef.html('').append(rows);
            }
                break;
            case 'socbPropS': // opencore device props
            case 'socnbPropS': // opencore nvram delete
            case 'soclsPropS': { // opencore nvram legacy
                const path = evdata.parentObjRef.attr('data-path') + '/' + evdata.parentObjRef.attr('data-index');
                const pdata = path.charAt(path.length - 1) !== '/' ? await Utils.getPropertiesData('getsprops', path) : [];
                const rows = [];

                rows.push(evdata.factoryObjRef);

                for (let i=0,l=pdata?.length ?? 0; i<l; ++i) {
                    const row = Utils.createTableRow(evdata.tableObjRef, path);

                    row.attr('data-index', i).find('.inline-text').attr('title', pdata[i]).attr('data-field', i).text(pdata[i]);
                    rows.push(row);
                }

                evdata.tableObjRef.html('').append(rows);
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doEditBtnClick = async evdata => {
        return true;
    }

    base.doInlineEditEv = async evdata => {
        switch (evdata.name) {
            case 'socbpropm':
            case 'socnbpropm':
            case 'soclspropm':
            case 'ppropm':
            case 'pprops':
            case 'ocnapropm':
            case 'ocnaprops':
            case 'ocppropm':
            case 'ocpprops': {
                const oldKey = evdata.dataIndex.replace(new RegExp(/\%/, 'g'), '/');
                const dataTSel = evdata.trObjRef.find('select');

                if (evdata.retData.cmd === 'unset') {
                    evdata.retData.inlineValue = oldKey;
                    evdata.retData.cmd = '';

                } else if (oldKey !== '' && oldKey === evdata.fetchData.Value) { // no changes
                    evdata.retData.cmd = '';

                } else if (oldKey !== '') {
                    evdata.retData.fetchDataUpdKey = new FetchReqDataUpdKey(evdata.trObjRef.attr('data-path'), oldKey, evdata.fetchData.Value);
                    evdata.retData.cmd = 'updkey';

                    if (evdata.name.includes('propm')) { // update paths in slave
                        let type = evdata.name.split('propm')[0];

                        $('.'+type+'PropS-tb-cont').find('[data-path]').each(function () {
                            $(this).attr('data-path', evdata.dataPath + '/' + evdata.retData.fetchDataUpdKey.NewKey);
                        });
                    }

                } else if (evdata.name.includes('propm')) {
                    if (evdata.name.charAt(0) === 's') {
                        // ArbitrarySingle tables, do nothing
                        evdata.retData.cmd = '';

                    } else { // refresh slave table data
                        evdata.fetchData.Path += '/' + evdata.fetchData.Value;
                        evdata.fetchData.Field = 'newkey';
                        evdata.fetchData.Value = '';
                        evdata.retData.triggerParentClick = true;
                    }

                } else {
                    evdata.fetchData.Field = evdata.fetchData.Value;
                    evdata.fetchData.Value = '';
                }

                evdata.trObjRef.attr('data-index', evdata.retData.inlineValue.replace(new RegExp(/\//, 'g'), '%'));
                evdata.tdObjRef.parent().find(':nth-child(2)').attr('data-field', evdata.retData.inlineValue); // pprops only

                if (dataTSel.length)
                    dataTSel.attr('data-field', evdata.retData.inlineValue);
            }
                break;
            case 'ppropsVal':
            case 'ocnapropsVal':
            case 'ocppropsVal': {
                if (evdata.dataIndex !== '') // break if we have a key
                    break;

                // set a default key if we set a value with no key
                const tblEl = $('.'+evdata.name.split('props')[0]+'PropS-tb-cont tr');
                const dataTSel = evdata.trObjRef.find('select');
                const idx = tblEl.length-1;
                let tdEls = null;

                // workaround
                if (idx === 0 || (idx === 1 && $(tblEl[1]).attr('data-index') === '')) {
                    const pathA = evdata.fetchData.Path.split('/');
                    const fdata = new FetchReqData(pathA[0]+'/'+pathA[1], pathA[2].replace(new RegExp(/\%/, 'g'), '/'), '');

                    await Utils.writeDataFetch('unset', fdata, 'wk_prop_e');
                }

                evdata.fetchData.Field = 'new'+idx;
                tdEls = evdata.tdObjRef.parent().find('td');

                tdEls.first().attr('title', evdata.fetchData.Field).text(evdata.fetchData.Field);
                tdEls.eq(1).attr('data-field', evdata.fetchData.Field);
                evdata.trObjRef.attr('data-index', evdata.fetchData.Field);

                if (dataTSel.length && evdata.fetchData.Value === '')
                    dataTSel.find('option').first().prop('selected', true);
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doDelTbButton = async evdata => {
        switch (evdata.name) {
            case 'pPropM':
            case 'ocpPropM':
            case 'socbPropM':
            case 'ocnaPropM':
            case 'socnbPropM':
            case 'soclsPropM':
            case 'cPropM': {
                const type = evdata.name.split('PropM')[0];
                const tableEl = $('.'+type+'PropS-tb-cont');
                const emptyEl = tableEl.find('tr').first().clone();

                tableEl.html('').append(emptyEl);
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doAddTbButton = evdata => {
        switch (evdata.name) {
            case 'ocpPropS':
            case 'socbPropS':
            case 'ocnaPropS':
            case 'socnbPropS':
            case 'soclsPropS':
            case 'pPropS':
            case 'cPropS': {
                const master = evdata.name.substr(0, evdata.name.length-1) + 'M';
                const masterEl = $('.'+master+'-tb-cont').find('.entry-active').first();
                let path = masterEl.attr('data-path') + '/' + masterEl.attr('data-index');
                let isEmptyMaster = true;

                masterEl.find('.inline-text').each(function () {
                    if ($(this).text() !== '') {
                        isEmptyMaster = false;
                        return false;
                    }
                });

                if (isEmptyMaster || !masterEl.length)
                    return false;

                if (evdata.name === 'cPropS')
                    path = masterEl.attr('data-path') + '/' + masterEl.attr('data-index') + '/CustomProperties';

                evdata.elemToAddRef.attr('data-path', path);
            }
                break;
            default:
                break;
        }

        return true;
    }

    base.doMacserialEvent = evdata => {
        return true;
    }

    Object.freeze(base);
    return base;
})();