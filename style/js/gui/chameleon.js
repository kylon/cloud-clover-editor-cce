/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const GUIEventHandler = (() => {
    const chameleon = Object.assign({}, GUIBaseEventHandler);
    const _kextPatchesModal = $('.modalchamKextP--1');

    chameleon.doTriggerEv = evdata => {
        switch (evdata.name) {
            case 'chmKxCmm': { // kext patch comment
                const pathAr = evdata.fetchData.Path.split('/');
                const target = $('.chamkextP-tb-cont').find('td[data-field="'+pathAr[1]+'"]')[pathAr[2]];

                $(target).parent().find('.comment').text(evdata.fetchData.Value);
            }
                break;
            default:
                return GUIBaseEventHandler.doTriggerEv(evdata);
        }

        return true;
    }

    chameleon.doComboChange = evdata => {
        switch (evdata.fetchData.Field) {
            case 'CsrActiveConfig':
                FlagsUtils.updateGUIFlagBits(evdata.objRef.attr('id'), BigInt(parseInt(evdata.fetchData.Value, 16)), true);
                break;
            default:
                return GUIBaseEventHandler.doComboChange(evdata);
        }

        return true;
    }

    chameleon.doEditBtnClick = async evdata => {
        switch (evdata.name) {
            case 'chamkextP': {
                const field = evdata.selectedObjRef.find('[data-field]').attr('data-field');
                const path = evdata.selectedObjRef.attr('data-path') + '/' + field + '/' + evdata.selectedObjRef.attr('data-index');
                const fdata = new FetchReqDataGet(path);

                evdata.retData.modalElm = _kextPatchesModal;

                Utils.loadingScr(true);

                await Utils.fetchReq(FetchURL.GUIEditor, 'getchamKxPatchdata', fdata, res => {
                    if (res === false)
                        throw new Error('chamkx_e');

                    const title = evdata.selectedObjRef.find('.inline-text').text();
                    const findType = Utils.getPropVal(res, 'Find_type');
                    const replType = Utils.getPropVal(res, 'Replace_type');

                    _kextPatchesModal.find('.modal-title').attr('title', title).text(title);
                    _kextPatchesModal.find('[data-field="Comment"]').val(Utils.getPropVal(res, 'Comment'));
                    _kextPatchesModal.find('textarea[data-field="Find"]').attr('data-comboselect-opt', findType).val(Utils.getPropVal(res, 'Find'));
                    _kextPatchesModal.find('select[data-field="Find"] > option[value="'+findType+'"]').prop('selected', true);
                    _kextPatchesModal.find('textarea[data-field="Replace"]').attr('data-comboselect-opt', replType).val(Utils.getPropVal(res, 'Replace'));
                    _kextPatchesModal.find('select[data-field="Replace"] > option[value="'+replType+'"]').prop('selected', true);
                    _kextPatchesModal.find('[data-field="MatchOS"]').val(Utils.getPropVal(res, 'MatchOS'));
                    _kextPatchesModal.find('[data-field="MathBuild"]').val(Utils.getPropVal(res, 'MatchBuild'));
                    _kextPatchesModal.find('[data-sel="matchoses"]').val(''); // reset matchos select

                    _kextPatchesModal.find('[data-path]').each(function() {
                        $(this).attr('data-path', path);
                    });
                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.ChamKPathesE);
                    console.log(e);
                });

                Utils.loadingScr(false);
            }
                break;
            default:
                return await GUIBaseEventHandler.doEditBtnClick(evdata);
        }

        return true;
    }

    chameleon.doInlineEditEv = async evdata => {
        switch (evdata.name) {
            case 'chmkxkey': {
                const table = evdata.trObjRef.parent();

                if (evdata.retData.cmd === 'unset') {
                    evdata.retData.cmd = '';
                    evdata.retData.inlineValue = evdata.fetchData.Field;

                } else if (evdata.fetchData.Field === '') { // this is new
                    const index = table.find('[data-field="' + evdata.fetchData.Value + '"]').length;
                    const nPath = evdata.trObjRef.attr('data-path') + '/' + evdata.fetchData.Value;

                    evdata.trObjRef.attr('data-index', index);
                    evdata.tdObjRef.attr('data-field', evdata.fetchData.Value);

                    // create empty element
                    evdata.fetchData.Path = nPath + '/' + index;
                    evdata.fetchData.Field = 'Comment';
                    evdata.fetchData.Value = 'new';

                } else {
                    evdata.retData.fetchDataUpdKey = new FetchReqDataUpdKey(evdata.dataPath, evdata.fetchData.Field, evdata.fetchData.Value);
                    evdata.retData.cmd = 'updkey';

                    table.find('[data-field="' + evdata.retData.fetchDataUpdKey.OldKey + '"]').each(function () {
                        const newk = evdata.retData.fetchDataUpdKey.NewKey;

                        $(this).attr('data-field', newk).attr('title', newk).text(newk);
                    });
                }
            }
                break;
            default:
                return await GUIBaseEventHandler.doInlineEditEv(evdata);
        }

        return true;
    }

    chameleon.doDelTbButton = async evdata => {
        switch (evdata.name) {
            case 'chamkextP': {
                // remove empty elements
                for (let i=evdata.delPathsList.length - 1; i>=0; --i) {
                    if (evdata.delPathsList[i].charAt(evdata.delPathsList[i].length - 1) === '/')
                        evdata.delPathsList.splice(i, 1);
                }

                if (evdata.delPathsList.length === 0)
                    return false;

                const paths = Array.from(new Set(evdata.delPathsList));

                evdata.delPathsList.reverse();
                Utils.loadingScr(true);

                for (let i=0, len=evdata.delPathsList.length; i<len; ++i) {
                    const fdata = new FetchReqData(evdata.delPathsList[i], evdata.fetchData.Field.pop(), '');

                    await Utils.writeDataFetch('unset', fdata, 'chamkx_e', true);
                }

                paths.forEach(function (p) {
                    const field = p.split('/')[1];
                    let j = 0;

                    evdata.tableObjRef.find('td[data-field="'+field+'"]').each(function () {
                        $(this).parent().attr('data-index', j++);
                    });
                });

                Utils.loadingScr(false);
                return false;
            }
            default:
                return await GUIBaseEventHandler.doDelTbButton(evdata);
        }

        return true;
    }

    chameleon.doAddTbButton = evdata => {
        switch (evdata.name) {
            case 'chamkextP': {
                evdata.elemToAddRef.attr('data-index', '0'); // new elem, so reset index to 0
            }
                break;
            default:
                return GUIBaseEventHandler.doAddTbButton(evdata);
        }

        return true;
    }

    chameleon.doMacserialEvent = evdata => {
        SMBIOSUtils.setMLB(evdata.mlbSerial, 'chm');
        return true;
    }

    Object.freeze(chameleon);
    return chameleon;
})();