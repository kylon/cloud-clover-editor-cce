/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const GUIEventHandler = (() => {
    const clover = Object.assign({}, GUIBaseEventHandler);

    clover.doSortUpdate = evdata => {
        switch (evdata.name) {
            case 'panel-cEntry': {
                const delBtnEl = evdata.itemRef.find('.del-cEntry-btn');
                const panel = $('.'+evdata.name);
                const factoryEl = panel.find('.single-item-group.d-none').first().detach();
                const elemID = evdata.name.split('-')[1];
                let idx = 0;

                evdata.fetchData.Path = delBtnEl.attr('data-path');
                evdata.fetchData.OldKey = delBtnEl.attr('data-index');

                panel.find('.single-'+elemID).each(function () {
                    const _this = $(this);
                    const modalentry = _this.next();
                    const iconEl = _this.find('.single-item-icon > i');
                    const target = iconEl.attr('data-bs-target').split('-')[0];

                    iconEl.attr('data-bs-target', target + '-' + idx);
                    _this.find('[data-subid]').attr('data-subid', idx);
                    _this.find('[data-id="cp-'+elemID+'"], [data-id="del-'+elemID+'"]').attr('data-index', idx);

                    modalentry.find('[data-subid]').each(function () {
                        $(this).attr('data-subid', idx);
                    });

                    modalentry.find('[data-path]').each(function () {
                        const _this = $(this);
                        const path = _this.attr('data-path').split('/');
                        const pos = _this.hasClass('subcen-tr') ? 2:1;

                        path[path.length - pos] = idx.toString();

                        _this.attr('data-path', path.join('/'));
                    });

                    modalentry.attr('class', 'modal fade modal'+elemID+'-' + idx);
                    ++idx;
                });

                // factory element must be first
                panel.prepend(factoryEl);

                evdata.fetchData.NewKey = delBtnEl.attr('data-index');
            }
                break;
            case 'ssdtOr': {
                evdata.fetchData.Path = evdata.itemRef.attr('data-path');
                evdata.fetchData.OldKey = evdata.itemRef.attr('data-index');
                let idx = -1;

                evdata.itemRef.parent().find('tr').each(function () {
                    $(this).attr('data-index', idx).find('.inline-text').attr('data-field', idx);
                    ++idx;
                });

                evdata.fetchData.NewKey = evdata.itemRef.attr('data-index');
            }
                break;
            default:
                return GUIBaseEventHandler.doSortUpdate(evdata);
        }

        return true;
    }

    clover.doComboChange = evdata => {
        switch (evdata.fetchData.Field) {
            case 'CsrActiveConfig':
            case 'BooterConfig':
            case 'PlatformFeature': {
                FlagsUtils.updateGUIFlagBits(evdata.objRef.attr('id'), BigInt(parseInt(evdata.fetchData.Value, 16)), true, evdata.objRef.attr('data-flag-bits'));
            }
                break;
            case 'IntelMaxValue': {
                const maxBacklightChk = $('.tristate-checkbox[data-field="SetIntelMaxBacklight"]');

                if (maxBacklightChk.getTristateCheckboxState() !== true)
                    maxBacklightChk.tristateCheckbox('checked', true);
            }
                break;
            default:
                return GUIBaseEventHandler.doComboChange(evdata);
        }

        return true;
    }

    clover.doComboInputChange = evdata => {
        switch (evdata.fetchData.Field) {
            case 'IntelMaxValue': {
                const maxBacklightChk = $('.tristate-checkbox[data-field="SetIntelMaxBacklight"]');
                const state = evdata.fetchData.Value === '' ? 'unchecked':'checked';

                maxBacklightChk.tristateCheckbox(state, true);
            }
                break;
            default:
                return GUIBaseEventHandler.doComboInputChange(evdata);
        }

        return true;
    }

    clover.doCCESelChange = async evdata => {
        switch (evdata.name) {
            case 'enbl-scan': {
                const scanKernEl = $('#scker');
                const scanLegEl = $('#scleg');
                const isCustom = evdata.fetchData.Value === 'custom';

                $('.scanOp').each(function () {
                    const _this = $(this);

                    if (_this.hasClass('tristate-checkbox'))
                        _this.tristateCheckbox('unchecked');
                    else if (_this.hasClass('cce-sel'))
                        _this.val('');

                    _this.prop('disabled', !isCustom);

                    if (!isCustom) {
                        scanKernEl.parent().find('label').addClass('color-disabled');
                        scanLegEl.parent().find('label').addClass('color-disabled');
                    }
                });

                if (!isCustom)
                    break;

                await Utils.fetchReq(FetchURL.GUIEditor, 'unset', evdata.fetchData, res => {
                    if (res === false)
                        throw new Error('cust_scan_e');

                    scanKernEl.parent().find('label').removeClass('color-disabled');
                    scanLegEl.parent().find('label').removeClass('color-disabled');
                    scanKernEl.find('option[value="No"]').prop('selected', true).trigger('change');
                    scanLegEl.find('option[value="No"]').prop('selected', true).trigger('change');
                    Utils.showNotification(ToastType.Success, ToastMessage.Saving);

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.UnknownFetchE);
                    console.log(e);
                });

                return false;
            }
            default:
                return await GUIBaseEventHandler.doCCESelChange(evdata);
        }

        return true;
    }

    clover.doCCECheckboxChange = evdata => {
        switch (evdata.name) {
            case 'fullTtl': { // fulltitle mess
                const isCentryFtitle = evdata.objRef.hasClass('centry-ftitle');
                const ftitleO = !isCentryFtitle ? evdata.dataObjRef.find('.ftitle') : $('.'+evdata.objRef.attr('data-ftarget')+'-title[data-subid="'+evdata.objRef.attr('data-subid')+'"]');
                const ftitle = !isCentryFtitle ? ftitleO.text():ftitleO.val();
                const titleType = evdata.fetchData.Value ? 'Title':'FullTitle';
                const field = evdata.fetchData.Value ? 'FullTitle':'Title';
                const fdata = new FetchReqData(evdata.fetchData.Path, titleType, ftitle);

                evdata.fetchData.Field = field;
                evdata.fetchData.Value = ftitle;
                evdata.retData.cmd = 'setval';

                ftitleO.attr('data-field', field);

                Utils.writeDataFetch('unset', fdata, 'fulltlt_e');
            }
                break;
            case 'ocplf': {
                const elem = $('.'+evdata.name+'Conf');
                let flag = '0x' + FlagsUtils.genSimpleFlag(evdata.name).toString(16);

                evdata.retData.cmd = 'setval';
                evdata.fetchData.Path = elem.attr('data-path');
                evdata.fetchData.Field = elem.attr('data-field');
                evdata.fetchData.Value = flag;

                $('.'+evdata.name+'-fflag').text(flag);
                elem.val(flag);
            }
                break;
            default:
                return GUIBaseEventHandler.doCCECheckboxChange(evdata);
        }

        return true;
    }

    clover.doTristateCheckboxChange = evdata => {
        switch (evdata.name) {
            case 'dsdtf': {
                const fixmaskEl = $('#manual_fixmask');
                let atLeastOneChecked = false;

                evdata.parparentObjRef.find('.tristate-checkbox').each(function () {
                    const state = $(this).getTristateCheckboxState();

                    if (state !== true && state !== false)
                        return true;

                    atLeastOneChecked = true;
                    return false;
                });

                if (!atLeastOneChecked && fixmaskEl.prop('disabled'))
                    fixmaskEl.prop('disabled', false).parent().find('label').removeClass('color-disabled');
                else if (atLeastOneChecked && !fixmaskEl.prop('disabled'))
                    fixmaskEl.prop('disabled', true).parent().find('label').addClass('color-disabled');
            }
                break;
            case 'ssdtg': {
                $('.ssdtGenOp').each(function () {
                    $(this).prop('disabled', evdata.fetchData.Value || evdata.isIndeterminate).tristateCheckbox('unchecked');
                });
            }
                break;
            case 'HWPEnable': {
                const hwpElem = $('.hwpval');

                if (evdata.fetchData.Value) {
                    hwpElem.prop('disabled', false).parent().find('label').removeClass('color-disabled');
                    break;
                }

                hwpElem.prop('disabled', true).parent().find('label').addClass('color-disabled');
            }
                break;
            case 'intlmaxbcklght': {
                if (!evdata.fetchData.Value)
                    $('[data-field="IntelMaxValue"]').val('').trigger($.Event('combo-change', {which:8}));
            }
                break;
            case 'injgfx': {
                $('.single_inj').find('.tristate-checkbox').each(function () {
                    const _this = $(this);

                    _this.prop('disabled', evdata.fetchData.Value || evdata.isIndeterminate);

                    if (evdata.fetchData.Value)
                        _this.tristateCheckbox('unchecked');
                });
            }
                break;
            default:
                return GUIBaseEventHandler.doTristateCheckboxChange(evdata);
        }

        return true;
    }

    clover.doTDClickEvt = async evdata => {
        switch (evdata.name) {
            case 'cPropS': { // clover arbitrary device props
                const path = evdata.parentObjRef.attr('data-path') + '/' + evdata.parentObjRef.attr('data-index') + '/CustomProperties';
                const pdata = await Utils.getPropertiesData('getcprops', path);
                const rows = [];

                rows.push(evdata.factoryObjRef);

                for (let i=0, l=pdata.length; i<l; ++i) {
                    const key = pdata[i]['Key'] ?? '';
                    const chkbState = pdata[i]['Disabled'] ? 'checked':(pdata[i]['Disabled'] === false ? 'indeterminate':'unchecked');
                    const val = pdata[i]['Value'] ?? '';
                    const row = Utils.createTableRow(evdata.tableObjRef, path);

                    row.attr('data-index', i);
                    row.find('[data-field="Key"]').attr('title', key).text(key);
                    row.find('.inline-text[data-field="Value"]').attr('title', val).text(val);
                    row.find('option[value="'+pdata[i]['Value_type']+'"]').prop('selected', true);
                    row.find('[data-field="Disabled"]').tristateCheckbox(chkbState, true);
                    rows.push(row);
                }

                evdata.tableObjRef.html('').append(rows);
            }
                break;
            default:
                return await GUIBaseEventHandler.doTDClickEvt(evdata);
        }

        return true;
    }

    clover.doInlineEditEv = async evdata => {
        switch (evdata.name) {
            case 'rendev': {
                const oldKey = evdata.fetchData.Field.replace(new RegExp(/\%/, 'g'), '/');
                const tr = evdata.tdObjRef.parent();

                if (evdata.retData.cmd === 'unset') {
                    evdata.retData.inlineValue = oldKey;
                    evdata.retData.cmd = '';

                } else if (oldKey !== '' && oldKey === evdata.fetchData.Value) { // no changes
                    evdata.retData.cmd = '';

                } else if (oldKey !== '') {
                    const path = evdata.trObjRef.attr('data-path') + '/' + evdata.dataIndex;

                    evdata.retData.fetchDataUpdKey = new FetchReqDataUpdKey(path, oldKey, evdata.fetchData.Value);
                    evdata.retData.cmd = 'updkey';

                } else {
                    evdata.fetchData.Field = evdata.fetchData.Value;
                    evdata.fetchData.Value = '';
                }

                tr.find(':first-child').attr('data-field', evdata.retData.inlineValue.replace(new RegExp(/\//, 'g'), '%'));
                tr.find(':nth-child(2)').attr('data-field', evdata.retData.inlineValue);
            }
                break;
            case 'rendevVal': {
                if (evdata.fetchData.Field !== "")
                    break;

                const tableEl = $('.renDevs-tb-cont tr');
                const idx = tableEl.length-1;
                let tdEls = null;

                evdata.fetchData.Field = 'new'+idx;
                tdEls = evdata.tdObjRef.parent().find('td');

                tdEls.first().attr('title', evdata.fetchData.Field).attr('data-field', evdata.fetchData.Field).text(evdata.fetchData.Field);
                tdEls.eq(1).attr('data-field', evdata.fetchData.Field);

            }
                break;
            default:
                return await GUIBaseEventHandler.doInlineEditEv(evdata);
        }

        return true;
    }

    clover.doMacserialEvent = evdata => {
        SMBIOSUtils.setMLB(evdata.mlbSerial, 'clv');
        return true;
    }

    Object.freeze(clover);
    return clover;
})();