/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const GUIEventHandler = (() => {
    const openCore = Object.assign({}, GUIBaseEventHandler);
    const _consoleAttrModal = $('.consattrs-modal');
    const _consoleAttrInpt = $('#occat');
    const _miscPwdInpt = $('.ocpwd');
    const _platinfNoAuto = $('.advplatinf');
    const _platinfAutoMsg = $('.autoplatinfmsg');

    openCore.doComboChange = evdata => {
        switch (evdata.fetchData.Field) {
            case 'ScanPolicy':
            case 'ExposeSensitiveData':
            case 'PickerAttributes':
            case 'DisplayLevel':
            case 'HaltLevel':
            case 'Target':
            case 'PlatformFeature': {
                FlagsUtils.updateGUIFlagBits(evdata.objRef.attr('id'), BigInt(evdata.fetchData.Value), false, evdata.objRef.attr('data-flag-bits'));
            }
                break;
            default:
                return GUIBaseEventHandler.doComboChange(evdata);
        }

        return true;
    }

    openCore.doCCECheckboxChange = evdata => {
        switch (evdata.name) {
            case 'ocscanplf': {
                const bit = evdata.objRef.val();

                switch (bit) {
                    case '1':
                    case '2': {
                        if (evdata.fetchData.Value)
                            break;

                        const bits = evdata.objRef.parent().parent().find('.'+evdata.name+'-flag');
                        const toUnset = bit === '1' ? '_FS':'_DEVICE';

                        for (let i=2, l=bits.length; i<l; ++i) {
                            if (!bits[i].querySelector('label').textContent.includes(toUnset))
                                continue;

                            bits[i].querySelector('input').checked = false;
                        }
                    }
                        break;
                    default: {
                        if (!evdata.fetchData.Value)
                            break;

                        const parent = evdata.objRef.parent();

                        if (parent.find('label').text().includes('_FS'))
                            parent.parent().find('input[value="1"]').prop('checked', true);
                        else
                            parent.parent().find('input[value="2"]').prop('checked', true);
                    }
                        break;
                }
            }
            case 'ocexsdf':
            case 'ocpka':
            case 'ocdlv':
            case 'ochlv':
            case 'ocdbgt':
            case 'ocplf': {
                const inpt = $('.'+evdata.name+'Conf');
                const flag = FlagsUtils.genSimpleFlag(evdata.name);

                evdata.retData.cmd = 'setval';
                evdata.fetchData.Path = inpt.attr('data-path');
                evdata.fetchData.Field = inpt.attr('data-field');
                evdata.fetchData.Value = flag;

                $('.'+evdata.name+'-fflag').text(flag);
                inpt.val(flag);
            }
                break;
            default:
                return GUIBaseEventHandler.doCCECheckboxChange(evdata);
        }

        return true;
    }

    openCore.doTristateCheckboxChange = evdata => {
        if (evdata.cstate === undefined) { // oc does not support unset, skip this state
            evdata.objRef.setTristateCheckboxState('checked');
            evdata.fetchCmd.override = 'setval';
            evdata.fetchData.Value = true;
        }

        switch (evdata.name) {
            case 'ocenablepwd': {
                const label = _miscPwdInpt.parent().find('label');

                _miscPwdInpt.prop('disabled', !evdata.fetchData.Value);

                if (evdata.fetchData.Value)
                    label.removeClass('color-disabled');
                else
                    label.addClass('color-disabled');
            }
                break;
            case 'ocautoplatinf': {
                if (evdata.fetchData.Value) {
                    _platinfNoAuto.addClass('d-none');
                    _platinfAutoMsg.removeClass('d-none');

                } else {
                    _platinfNoAuto.removeClass('d-none');
                    _platinfAutoMsg.addClass('d-none');
                }
            }
                break;
            default:
                return GUIBaseEventHandler.doTristateCheckboxChange(evdata);
        }

        return true;
    }

    openCore.doCCERadioChange = evdata => {
        switch (evdata.name) {
            case 'occattr': {
                const vals = _consoleAttrModal.find('input[type="radio"]:checked');
                const v1 = parseInt(vals[0].value, 10);
                const v2 = parseInt(vals[1].value, 10);

                evdata.fetchData.Value = v1 + v2;

                _consoleAttrInpt.val(evdata.fetchData.Value);
            }
                break;
            default:
                return GUIBaseEventHandler.doCCERadioChange(evdata);
        }

        return true;
    }

    openCore.doCCESelChange = async evdata => {
        switch (evdata.name) {
            case 'smbios-sel': {
                SMBIOSUtils.switchSMBIOSOC(evdata.fetchData.Value);
                return false;
            }
            default:
                return GUIBaseEventHandler.doCCESelChange(evdata);
        }

        return true;
    }

    openCore.doMacserialEvent = evdata => {
        SMBIOSUtils.setMLB(evdata.mlbSerial, 'oc');
        return true;
    }

    Object.freeze(openCore);
    return openCore;
})();