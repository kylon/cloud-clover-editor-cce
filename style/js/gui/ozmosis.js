/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const GUIEventHandler = (() => {
    const ozmosis = Object.assign({}, GUIBaseEventHandler);

    ozmosis.doTriggerEv = evdata => {
        switch (evdata.name) {
            case 'ozMacAdr': {
                const regex = /([a-f0-9]{2})([a-f0-9]{2})/i;
                let str = evdata.fetchData.Value.replace(/[^a-f0-9]/ig, '');

                while (regex.test(str))
                    str = str.replace(regex, '$1'+':'+'$2');

                evdata.fetchData.Value = str.slice(0, 17); // new val

                evdata.objRef.val(evdata.fetchData.Value);

                if (evdata.fetchData.Value.length !== 17 && evdata.fetchData.Value !== '')
                    return false;
            }
                break;
            case 'ozTemplateTx': {
                if ($('select[data-sel="oz-disk-type"]').val() === '') {
                    evdata.objRef.val('');
                    return false;
                }
            }
                break;
            default:
                return GUIBaseEventHandler.doTriggerEv(evdata);
        }

        return true;
    }

    ozmosis.doComboChange = evdata => {
        switch (evdata.fetchData.Field) {
            case 'AcpiLoaderMode':
            case 'csr-active-config':
                FlagsUtils.updateGUIFlagBits(evdata.objRef.attr('id'), BigInt(parseInt(evdata.fetchData.Value, 16)), true);
                break;
            default:
                return GUIBaseEventHandler.doComboChange(evdata);
        }

        return true;
    }

    ozmosis.doCCESelChange = async evdata => {
        switch (evdata.objRef.attr('data-sel')) {
            case 'oz-disk-type': { // Ozmosis Template - select disk type
                const ozTxBox = $('.oz-templates-tx');
                const ozTemplType = $('.oztempltype');

                if (evdata.fetchData.Value === '') {
                    ozTxBox.val('').prop('disabled', true);
                    ozTemplType.prop('disabled', true);
                    return false;
                }

                ozTxBox.prop('disabled', false);
                ozTemplType.prop('disabled', false);

                evdata.fetchData.Path = ozTxBox.attr('data-path');
                evdata.fetchData.Field = evdata.fetchData.Value;
                evdata.fetchData.Value = '';

                await Utils.fetchReq(FetchURL.GUIEditor, 'getoztempldata', evdata.fetchData, res => {
                    if (res === false)
                        throw new Error('oztmpl_e');

                    ozTxBox.val(res).attr('data-field', evdata.fetchData.Field);

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.OzTemplateDataE);
                    console.log(e);
                });

                return false;
            }
            case 'oz-templ-type': { // Ozmosis Template - select template type
                const argCont = $('.oz-templates-tx');
                const argVal = argCont.val();
                const space = argVal === '' ? '':' ';

                evdata.objRef.prop('selectedIndex', 0);

                if ($('select[data-sel="oz-disk-type"]').val() === '')
                    return false;

                evdata.fetchData.Path = argCont.attr('data-path');
                evdata.fetchData.Field = argCont.attr('data-field');

                if (evdata.fetchData.Value === 'del') {
                    evdata.retData.cmd = 'unset';
                    evdata.fetchData.Value = '';

                    argCont.val('');
                    break;
                }

                evdata.fetchData.Value = argVal + space + evdata.fetchData.Value;

                argCont.val(evdata.fetchData.Value);
            }
                break;
            default:
                return await GUIBaseEventHandler.doCCESelChange(evdata);
        }

        return true;
    }

    ozmosis.doCCECheckboxChange = evdata => {
        switch (evdata.name) {
            case 'acpif': {
                const bits = evdata.objRef.parent().parent().find('.'+evdata.name+'-flag');
                const bit = evdata.objRef.val();

                switch (bit) {
                    case '0':
                    case '1': {
                        if (bit === '1' && evdata.fetchData.Value) {
                            bits[0].querySelector('input').checked = false;
                            break;
                        }

                        for (let i=1,l=bits.length; i<l; ++i)
                            bits[i].querySelector('input').checked = false;

                        bits[0].querySelector('input').checked = true;
                    }
                        break;
                    default: {
                        if (!evdata.fetchData.Value)
                            break;

                        bits.find('input[value="0"]').prop('checked', false);
                        bits.find('input[value="1"]').prop('checked', true);
                    }
                        break;
                }

                const inpt = $('.'+evdata.name+'Conf');
                const flag = '0x' + FlagsUtils.genSimpleFlag(evdata.name).toString(16);

                evdata.retData.cmd = 'setval';
                evdata.fetchData.Path = inpt.attr('data-path');
                evdata.fetchData.Field = inpt.attr('data-field');
                evdata.fetchData.Value = flag;

                $('.'+evdata.name+'-fflag').text(flag);
                inpt.val(flag);
            }
                break;
            default:
                return GUIBaseEventHandler.doCCECheckboxChange(evdata);
        }

        return true;
    }

    ozmosis.doTristateCheckboxChange = evdata => {
        switch (evdata.name) {
            case 'DisableIntelInjection': {
                $('.ozintel-inj').each(async function () {
                    const _this = $(this);
                    const labelEl = _this.parent().parent().find('label');

                    _this.prop('disabled', evdata.fetchData.Value).val('');

                    if (!evdata.fetchData.Value) {
                        _this.removeClass('jcb-disabled');
                        labelEl.removeClass('color-disabled');
                        return true;
                    }

                    const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-field'), '');

                    Utils.fetchReq(FetchURL.GUIEditor, 'unset', fdata, res => {
                        if (res === false)
                            throw new Error('dis_int_inj_e');

                        _this.addClass('jcb-disabled');
                        labelEl.addClass('color-disabled');

                    }).catch((e) => {
                        Utils.showNotification(ToastType.Error, ToastMessage.UnknownFetchE);
                        console.log(e);
                    });
                });
            }
                break;
            case 'DisableAtiInjection': {
                const atiInjEl = $('.ozati-inj');
                const atiInjLabel = atiInjEl.parent().parent().find('label');

                atiInjEl.prop('disabled', evdata.fetchData.Value).val('');

                if (!evdata.fetchData.Value) {
                    atiInjEl.removeClass('jcb-disabled');
                    atiInjLabel.removeClass('color-disabled');
                    break;
                }

                const fdata = new FetchReqData(atiInjEl.attr('data-path'), atiInjEl.attr('data-field'), '');

                Utils.fetchReq(FetchURL.GUIEditor, 'unset', fdata, res => {
                    if (res === false)
                        throw new Error('dis_ati_inj_e');

                    atiInjEl.addClass('jcb-disabled');
                    atiInjLabel.addClass('color-disabled');

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.UnknownFetchE);
                    console.log(e);
                });
            }
                break;
            case 'DisableVoodooHda':
            case 'EnableVoodooHdaInternalSpdif': {
                const toDisable = evdata.name.charAt(0) === 'D' ? 'EnableVoodooHdaInternalSpdif':'DisableVoodooHda';
                const checkb = $('.tristate-checkbox[data-field="'+toDisable+'"]');

                if (!evdata.fetchData.Value) {
                    checkb.prop('disabled', false);
                    break;
                }

                const fdata = new FetchReqData(checkb.parent().attr('data-path'), checkb.attr('data-field'), '');

                Utils.fetchReq(FetchURL.GUIEditor, 'unset', fdata, res => {
                    if (res === false)
                        throw new Error('vd_hda_e');

                    checkb.tristateCheckbox('unchecked').prop('disabled', true);

                }).catch((e) => {
                    Utils.showNotification(ToastType.Error, ToastMessage.UnknownFetchE);
                    console.log(e);
                });
            }
                break;
            default:
                return GUIBaseEventHandler.doTristateCheckboxChange(evdata);
        }

        return true;
    }

    ozmosis.doMacserialEvent = evdata => {
        SMBIOSUtils.setMLB(evdata.mlbSerial, 'oz');
        return true;
    }

    Object.freeze(ozmosis);
    return ozmosis;
})();