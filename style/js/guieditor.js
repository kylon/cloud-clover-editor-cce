/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

$(function() {
    const dbPatchesElem = $('#patchesdb');
    const macserialEl = $('.macserialbtn');
    const smbiosProductName = $('#smpdn');
    const ocSmbiosGenericProductName = $('#ocgsysprodn');
    let curShownModalTriggerBtn = null;
    let prevDataTypeSelect = '';

    /*
        GUI Editor Init
     */
    Utils.loadingScr(true);

    SMBIOSUtils.populateSMBIOSValues(); // init smbios list
    $('.cce-combo').combobox(); // Init jcombobox
    $('.tristate-checkbox').tristateCheckbox();
    $('.comboselect-txarea, .comboselect-input').comboSelect();

    // init sortables - fancy panels
    $('.fancypanel-sortable').each(function() {
        const _this = $(this);
        const id = _this.attr('data-sid').substring(6);

        _this.sortable({
            items: '.single-item-group',
            handle: '.'+id+'-shandle',
            cursor: "move",
            containment: "parent",
            tolerance: "pointer",
            axis: 'x',
            start: (event, ui) => EditorUtils.sortingStartFn(event, ui),
            stop: () => EditorUtils.sortingStopFn()
        });
    });

    // Init sortables (ssdt order)
    $('.ssdtOr .table-cont').sortable({
        items: 'tr',
        handle: '.ssdt-sortable',
        axis: 'y',
        containment: "parent",
        tolerance: "pointer",
        cursor: "move",
        start: function(event, ui) {
            const el = ui.item.parent();

            el.find('.ssdt-sortable').addClass('right');
            el.find('.inline-text').addClass('left');
            el.attr('data-sid','ssdtOr');
        },
        stop: function(event, ui) {
            const el = ui.item.parent();

            el.find('.ssdt-sortable').removeClass('right');
            el.find('.inline-text').removeClass('left');
            el.removeAttr('data-sid');
        }
    });

    Utils.loadingScr(false);
    /*
        END Init
     */

    // CCE Navbar
    $(document).on('click', '.sidebar-nav li', function () {
        const _this = $(this);
        const tabs = $('.tab-content');
        const el = _this.find('a');
        const selected = el.attr('href').substring(1);

        _this.parent().find('.active').removeClass('active');
        el.addClass('active');

        tabs.find('.active').removeClass('active');
        tabs.find('#'+selected).addClass('active');

        Utils.fetchReq(FetchURL.Settings, 'setccetab', {ccetab: selected}, res => {
            if (res === false)
                throw new Error('set_tab_e');
        });
    });

    $(document).on('click', 'html', function () {
        const entriesSelected = $('.entry-selected');

        if (entriesSelected.length) {
            entriesSelected.each(function () {
                $(this).removeClass('entry-selected');
            });
        }
    });

    // Save trigger button element on shown event to update its data
    $(document).on('shown.bs.modal', '.modal', function (e) {
        curShownModalTriggerBtn = $(e.relatedTarget).parent().parent();
    });

    $(document).on('click', '.btn-wiptxtmode, .btn-switchfatxm', function () {
        $('.cce-sett[name="cce_text_mode"]:first').prop('checked', true).trigger('change');
    });

    // CCE text inputs
    $(document).on('keyup paste', '.cce-text', function (e) {
        if (e.type !== 'paste' && !Utils.isValidKey(e.which))
            return false;

        const _this = $(this);
        const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-field'), _this.val(), _this.attr('data-vtype') ?? false);
        const hasDataType = _this.hasAnyClass(['comboselect-txarea', 'comboselect-input']);
        const cmd = fdata.Value !== '' ? 'setval':'unset';
        const notify = e.notf ?? true;

        if (e.disableSpecialEv !== true) {
            const ret = GUIEventHandler.doTriggerEv({
                objRef: _this,
                name: _this.attr('data-triggerEv'),
                fetchData: fdata,
                shownModalTriggerBtn: curShownModalTriggerBtn
            });

            if (ret !== true)
                return false;
        }

        if (hasDataType)
            fdata.DataType = _this.parent().find('option:checked').val();

        Utils.writeDataFetch(cmd, fdata, 'data_write_e', notify);
    });

    // CCE number input
    $(document).on('keyup change', '.cce-numb', function (e) {
        if (e.type !== 'change' && !Utils.isValidNumbKey(e.which))
            return false;

        const _this = $(this);
        const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-field'), _this.val());
        const cmd = fdata.Value === '' ? 'unset':'setval';

        Utils.writeDataFetch(cmd, fdata, 'data_write_num_e', true);
    });

    // MacSerial
    macserialEl.each(function () {
        const _this = $(this);

       _this.on('click', function (e) {
           const productName = ocSmbiosGenericProductName?.length ? ocSmbiosGenericProductName.val() : smbiosProductName.val();
           const selected = productName.replace(' ', '').replace(',', '').toLowerCase();
           const macserialData = {
               targetEl: null,
               pname: '',
               mcode: '',
               output: '',
               success: false,
               syncAllFields: e.syncall || _this.parent().find('input').attr('data-path').includes('PlatformInfo')
           };

           if (Utils.isEmptyValue(productName))
               return false;

           _this.prop('disabled', true).find('i').addClass('rotate360-inf');

           macserialData.pname = productName;
           macserialData.mcode = smbiosdata[selected]['modelCode'];
           macserialData.targetEl = _this;

           runMacSerial(macserialData);

           const serialTimer = setTimeout(() => {
               if (_this.find('i').hasClass('rotate360-inf')) {
                   _this.prop('disabled', false).find('i').removeClass('rotate360-inf');
                   Utils.showNotification(ToastType.Error, ToastMessage.macserialCmdE);
                   clearTimeout(serialTimer);
               }
           }, 8 * 1000);
       });
    });

    $(document).on('macserial_evt', (e, macserialdata) => {
        const _this = macserialdata.targetEl;

        if (_this == null || macserialdata.success !== true) {
            _this.prop('disabled', false).find('i').removeClass('rotate360-inf');
            Utils.showNotification(ToastType.Error, ToastMessage.macserialCmdE);
            return false;
        }

        const snArr = macserialdata.output.split('|').filter(el => {
            return typeof el === 'string' && el.trim() !== '';
        });
        const len = snArr.length ?? 0;

        if (len <= 0) {
            _this.prop('disabled', false).find('i').removeClass('rotate360-inf');
            Utils.showNotification(ToastType.Error, ToastMessage.macserialUknownE);
            return false;
        }

        const inputList = macserialdata.syncAllFields ? $('.macserialres') : _this.parent().parent().find('.macserialres');
        const serialNumb = snArr[0].trim();
        const mlb = snArr[1].trim();

        inputList.each(function () {
            const _this = $(this);
            const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-field'), serialNumb);

            _this.val(serialNumb);

            Utils.fetchReq(FetchURL.GUIEditor, 'setval', fdata, res => {
                if (res === false)
                    throw new Error('macserial_e');

                _this.parent().find('.macserialbtn').prop('disabled', false).find('i').removeClass('rotate360-inf');
                Utils.showNotification(ToastType.Success, ToastMessage.Saving);

            }).catch((e) => {
                Utils.showNotification(ToastType.Error, ToastMessage.UnknownFetchE);
                console.log(e);
            });
        });

        GUIEventHandler.doMacserialEvent({
            mlbSerial: mlb
        });
    });

    // Update SMBIOS data button
    $(document).on('click', '.trigUpdSmbios', function () {
        const mac = $(this).parent().parent().find('#smsel > option:checked').val();
        const fdata = new FetchReqData('', '', mac);

        // reuse select event
        GUIEventHandler.doCCESelChange({
            name: 'smbios-sel',
            fetchData: fdata
        });
    });

    $(document).on('sortupdate', '.ui-sortable, .ssdtOr .table-cont', function (e, ui) {
        const _this = $(this);
        const fdata = new FetchReqDataUpdKey();

        const ret = GUIEventHandler.doSortUpdate({
            objRef: _this,
            itemRef: ui.item,
            name: _this.attr('data-sid'),
            fetchData: fdata
        });

        if (ret !== true)
            return false;

        Utils.writeDataFetch('sortval', fdata, 'sort_e', true);
    });

    // Copy button - fancy panel
    $(document).on('click', '.copycEntry-btn', function(e) {
        e.preventDefault();
        e.stopPropagation();

        const modalElm = Utils.prepareCopyToModal();

        $('.centry-selected').removeClass('centry-selected');
        $(this).addClass('centry-selected');

        modalElm.find('.copyto-btn').attr('data-source', 'centry');
        modalElm.modal('show');
    });

    // Copy button - table
    $(document).on('click', '.cpTbBtn', function(e) {
        e.stopImmediatePropagation();

        const _this = $(this);
        const tableEl = _this.parent().parent().parent();
        const btnCls = _this.attr('data-id').substring(3);

        if (tableEl.find('.entry-selected').length === 0)
            return false;

        const modalElm = Utils.prepareCopyToModal();

        modalElm.find('.copyto-btn').attr('data-source', btnCls);
        modalElm.modal('show');
    });

    $(document).on('click', '.copyto-btn', function () {
        const _this = $(this);
        const source = _this.attr('data-source');
        const indexes = [_this.parent().parent().find('.copytoconfig-list').val()];
        const elems = source === 'centry' ? $('.centry-selected'):$('.'+source+'-tb-cont tr.entry-selected');
        const fdata = new FetchReqDataCopy(elems.first().attr('data-path'));

        elems.each(function () {
            indexes.push($(this).attr('data-index'));
        });

        fdata.Data = indexes;

        Utils.fetchReq(FetchURL.GUIEditor, 'copy', fdata, res => {
            if (res === false)
                throw new Error('cp_e');

            Utils.showNotification(ToastType.Success, ToastMessage.Saving);

        }).catch((e) => {
            Utils.showNotification(ToastType.Error, ToastMessage.CopyValueE);
            console.log(e);
        });

        $('#copyto').modal('hide');
    });

    // Combobox - select
    $(document).on('combo-change', '.combobox-opt', function (e) {
        const _this = $(this);
        const notify = e.notf ?? true;
        const ccecombo = _this.parent().parent().find('.cce-combo');
        const fdata = new FetchReqData(ccecombo.attr('data-path'), ccecombo.attr('data-field'), _this.attr('data-value'), ccecombo.attr('data-vtype') ?? false);

        const ret = GUIEventHandler.doComboChange({
            objRef: ccecombo,
            fetchData: fdata
        });

        if (ret !== true)
            return false;

        Utils.writeDataFetch('setval', fdata, 'combo_write_e', notify);
    });

    // Combobox - input
    $(document).on('combo-input-change', '.combobox-input', function (e) {
        if (!Utils.isValidKey(e.which))
            return false;

        const _this = $(this);
        const notify = e.notf ?? true;
        const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-field'), _this.val(), _this.attr('data-vtype') ?? false);
        const cmd = fdata.Value === '' ? 'unset':'setval';

        const ret = GUIEventHandler.doComboInputChange({
            fetchData: fdata
        });

        if (ret !== true)
            return false;

        Utils.writeDataFetch(cmd, fdata, 'combo_write_e', notify);
    });

    // Combobox - special flag input event
    $(document).on('combo-flag-change', '.cce-combo', function (e) {
        const _this = $(this);
        const notify = e.notf ?? true;
        const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-field'), _this.val(), _this.attr('data-vtype') ?? false);

        const ret = GUIEventHandler.doComboChange({
            objRef: _this,
            fetchData: fdata
        });

        if (ret !== true)
            return false;

        Utils.writeDataFetch('setval', fdata, 'combo_write_e', notify);
    });

    $(document).on('focus', '[data-sel="datatype"]', function () {
        prevDataTypeSelect = $(this).val();
    });

    $(document).on('change', '.cce-sel', async function (e) {
        const _this = $(this);
        const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-field'), _this.val());
        const parent = _this.parent().parent();
        const dataSelect = _this.attr('data-sel');
        const index = parent.attr('data-index');
        const tbPidx = fdata.Field === index ? '' : '/'+index;
        const retdata = { cmd: fdata.Value === '' ? 'unset':'setval' };
        const notify = e.notf ?? true;

        if (_this.hasClass('select-inTable'))
            fdata.Path = parent.attr('data-path') + tbPidx;

        const ret = await GUIEventHandler.doCCESelChange({
            objRef: _this,
            fetchData: fdata,
            name: dataSelect,
            retData: retdata
        });

        if (ret !== true)
            return false;

        Utils.fetchReq(FetchURL.GUIEditor, retdata.cmd, fdata, res => {
            if (res === false) {
                if (dataSelect === 'datatype') {
                    _this.val(prevDataTypeSelect);
                    Utils.showNotification(ToastType.Error, ToastMessage.InvalidDataType);
                }

                return false;
            }

            prevDataTypeSelect = _this.val();

            if (notify)
                Utils.showNotification(ToastType.Success, ToastMessage.Saving);

        }).catch(() => { Utils.showNotification(ToastType.Error, ToastMessage.UnknownFetchE); });
    });

    $(document).on('change', '.cce-checkbox', function () {
        const _this = $(this);
        const parent = _this.parent();
        const parParent = parent.parent();
        const dataEl = parParent.prop('tagName') === 'TD' ? parParent.parent():parent;
        const index = dataEl.attr('data-index') ?? '';
        const fpath = index !== '' ?  dataEl.attr('data-path') + '/' + index : dataEl.attr('data-path');
        const fdata = new FetchReqData(fpath, _this.attr('data-field'), _this.prop('checked'));
        const retdata = { cmd: fdata.Value ? 'setval':'unset' };

        const ret = GUIEventHandler.doCCECheckboxChange({
            objRef: _this,
            dataObjRef: dataEl,
            name: _this.attr('data-change'),
            fetchData: fdata,
            retData: retdata
        });

        if (ret !== true)
            return false;

        Utils.writeDataFetch(retdata.cmd, fdata, 'chkb_e', true);
    });

    $(document).on('change', '.cce-radio', function () {
        const _this = $(this);
        const inpt = _this.find('input');
        const fdata = new FetchReqData(_this.attr('data-path'), inpt.attr('data-field'), '');

        const ret = GUIEventHandler.doCCERadioChange({
            name: inpt.attr('data-change'),
            fetchData: fdata
        });

        if (ret !== true)
            return false;

        Utils.writeDataFetch('setval', fdata, 'crd_e', true);
    });

    $(document).on('checkbox-state-change', '.tristate-checkbox', function () {
        const _this = $(this);
        const state = _this.getTristateCheckboxState();
        const parent = _this.parent();
        const parParent = parent.parent();
        const dataEl = parParent.prop('tagName') === 'TD' ? parParent.parent():parent;
        const indeterminate = state === false;
        const index = dataEl.attr('data-index') ?? '';
        const fpath = index !== '' ?  dataEl.attr('data-path') + '/' + index : dataEl.attr('data-path');
        const fdata = new FetchReqData(fpath, _this.attr('data-field'), state === true);
        const cmd = {
            command: !fdata.Value && !indeterminate ? 'unset' : 'setval',
            override: null
        };

        const ret = GUIEventHandler.doTristateCheckboxChange({
            objRef: _this,
            cstate: state,
            parparentObjRef: parParent,
            name: _this.attr('data-change'),
            isIndeterminate: indeterminate,
            fetchData: fdata,
            fetchCmd: cmd
        });

        if (ret !== true)
            return false;

        Utils.writeDataFetch(cmd.override ?? cmd.command, fdata, 'trichkb_e', true);
    });

    // Do not loose selected entries
    $(document).on('click', '.copytoconfig-list', function (e) {
        e.stopImmediatePropagation();
    });

    $(document).on('click tap', 'tr[data-path] td', async function (e) {
        const targetEl = $(e.target);
        const isInline = targetEl.isInlineEditActive() ?? false;

        if (isInline || targetEl.hasAnyClass(['form-check-label', 'form-check-input', 'cce-sel']))
            return false

        e.stopImmediatePropagation();
        e.preventDefault();

        const parent = $(this).parent();
        const parParent = parent.parent();
        const tryMulSel = parParent.find('.entry-selected').length;
        const propMatches = parent.attr('class').match(/(?:^|\s)(\w+)propm(?:\s|$)/);

        if ((e.type !== 'tap' && !e.ctrlKey) || tryMulSel === 0 || (e.type === 'tap' && tryMulSel === 0)) {
            $('.entry-selected').each(function () {
                $(this).removeClass('entry-selected');
            });
        }

        parent.toggleClass('entry-selected');

        if (document.selection)
            document.selection.empty();
        else if (window.getSelection)
            window.getSelection().removeAllRanges();

        if (propMatches === null || tryMulSel > 1)
            return false;

        parParent.find('.entry-active').removeClass('entry-active');
        parent.addClass('entry-active');

        const type = propMatches[1] + 'PropS';
        const tableEl = $('.' + type + '-tb-cont');

        const ret = await GUIEventHandler.doTDClickEvt({
            tableObjRef: tableEl,
            factoryObjRef: tableEl.find('tr').first().clone(),
            parentObjRef: parent,
            name: type
        });

        if (ret !== true)
            return false;
    });

    $(document).on('click', '.editTbBtn', async function () {
        const _this = $(this);
        const tid = _this.attr('data-id').substring(5);
        const selectedElm = _this.parent().parent().parent().find('.' + tid + ' .entry-selected');
        const retdata = { modalElm: null };

        if (selectedElm.length > 1 || !selectedElm.parent().parent().hasClass(tid) || selectedElm.first().text() === '')
            return false;

        const ret = await GUIEventHandler.doEditBtnClick({
            selectedObjRef: selectedElm,
            retData: retdata,
            name: tid
        });

        if (ret !== true)
            return false;

        retdata.modalElm.modal('show');
    });

    $(document).on('click', '.del-cEntry-btn', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();

        const _this = $(this);
        const fdata = new FetchReqData(_this.attr('data-path'), _this.attr('data-index'), '');
        const tid = _this.attr('data-id').substring(4);
        const parent = _this.parent();
        let i = -1;

        parent.addClass('flip-out');

        const timeout = setTimeout($.proxy(function () {
            const toRemove = parent.parent();
            const panel = toRemove.parent();

            clearTimeout(timeout);
            toRemove.remove();

            panel.find('.single-'+tid).each(function () {
                if (i < fdata.Field) {
                    ++i;
                    return true;
                }

                const _this = $(this);
                const modalEntry = _this.next();
                const iconEl = _this.find('.single-item-icon > i');
                const target = iconEl.attr('data-bs-target').split('-')[0];

                iconEl.attr('data-bs-target', target+'-'+i);
                _this.find('[data-index]').attr('data-index', i);
                _this.parent().find('[data-subid]').attr('data-subid', i);

                modalEntry.find('[data-path]').each(function () {
                    const _this = $(this);
                    const path = _this.attr('data-path').split('/');
                    const subtract = _this.hasClass('subcen-tr') ? 2:1;

                    path[path.length - subtract] = i;

                    _this.attr('data-path', path.join('/'));
                });

                modalEntry.attr('class', 'modal fade modal'+tid+'-'+i);
                ++i;
            });
        }, this), 300);

        Utils.writeDataFetch('unset', fdata, 'dcentry_e', true);
    });

    $(document).on('click', '.delTbBtn', async function () {
        const _this = $(this);
        const tid = _this.attr('data-id').substring(4);
        const tableEl = _this.parent().parent().parent().find('.'+tid+'-tb-cont').first();
        const selected = tableEl.find('.entry-selected');
        const fdata = new FetchReqData('', '', '');
        const delPaths = [];
        const indexes = [];
        const i = -1;

        if (!selected.parent().parent().hasClass(tid))
            return false;

        selected.each(function () {
            const _this = $(this);

            if (fdata.Path === '')
                fdata.Path = _this.attr('data-path');

            if (tid === 'chamkextP')
                delPaths.push(_this.attr('data-path')+'/'+_this.find('[data-inlineeditEv]').attr('data-field'));

            indexes.push(_this.attr('data-index').replace(new RegExp(/\%/, 'g'), '/'));
            _this.remove();
        });

        fdata.Field = indexes;

        const elems = tableEl.find('tr[data-path]');
        const tEl = elems.last();
        const idx = tEl.attr('data-index') ?? '';
        const hasIdx = idx !== '';
        const isFieldIdx = hasIdx && idx === tEl.find('[data-field]').attr('data-field');

        const ret = await GUIEventHandler.doDelTbButton({
            name: tid,
            fetchData: fdata,
            delPathsList: delPaths,
            tableObjRef: tableEl,
            elementsList: elems
        });

        if (ret !== true)
            return false;

        if (hasIdx && !isNaN(idx)) {
            let j = i;

            elems.each(function() {
                $(this).attr('data-index', j++);
            });

            if (isFieldIdx) {
                j = i;
                elems.find('.inline-text').each(function() {
                    $(this).attr('data-field', j++);
                });

                j = i;
                elems.find('.cce-sel').each(function () {
                    $(this).attr('data-field', j++);
                });
            }
        }

        Utils.writeDataFetch('unset', fdata, 'dtb_btn_e', true);
    });

    $(document).on('click', '.addTbBtn', function () {
        const _this = $(this);
        const tid = _this.attr('data-id').substring(4);
        const tableEl = _this.parent().parent().parent().find('.'+tid+'-tb-cont');
        const newElem = Utils.createTableRow(tableEl);

        const ret = GUIEventHandler.doAddTbButton({
            name: tid,
            elemToAddRef: newElem
        });

        if (ret === false)
            return false;

        tableEl.append(newElem);
    });

    $(document).on('click', '.addcEntryBtn', function () {
        const tid = $(this).attr('data-id').substring(4);
        const panelEl = $('.panel-'+tid);
        const groups = panelEl.find('.single-item-group');
        const newElem = groups.first().clone(true);
        const modal = newElem.find('.modal').first();
        const pathElems = modal.find('[data-path]');
        const newIdx = groups.length - 1;
        const cls = 'modal'+tid+'-'+newIdx;
        const path = pathElems.first().attr('data-path');
        const npath = path.substr(0, path.length-2) + newIdx;
        const eventField = modal.find('[data-triggerev="itmLiveTitle"]').attr('data-field');

        newElem.find('[data-subid]').attr('data-subid', newIdx);
        newElem.find('.single-item').find('[data-index]').attr('data-index', newIdx);
        newElem.find('.single-item-icon > i').attr('data-bs-target', '.'+cls);
        modal.removeClass('modal'+tid+'--1').addClass(cls);
        modal.find('.subcen-tr').attr('data-path', npath+'/SubEntries'); // clover custom entries
        pathElems.attr('data-path', npath);

        newElem.removeClass('d-none');
        panelEl.append(newElem);
        newElem.find('[data-field="' + eventField + '"]').trigger($.Event('keyup', {which:8, disableSpecialEv:true}));
    });

    $(document).on('click', '.dbTbBtn', async function () {
        const _this = $(this);
        const tid = _this.attr('data-id').substring(3);
        const tableEl = _this.parent().parent().parent();
        let searchLabel = 'Comment';
        let ret;

        if (tableEl.find('[data-field="Name"]').length)
            searchLabel += ', Name';

        if (tableEl.find('[data-field="MatchOS"]').length)
            searchLabel += ', MatchOS';

        ret = await Utils.initPagingAndPatchesList(tid, 'listall');
        if (ret === false)
            return false;

        dbPatchesElem.find('.db-patch-search').attr('data-id', tid).val('').parent().find('label').text(searchLabel);
        dbPatchesElem.modal('show');
    });

    $(document).on('keyup', '.db-patch-search', async function () {
        const _this = $(this);
        const val = _this.val();
        const filter = val === '' ? 'listall':val;

        await Utils.initPagingAndPatchesList(_this.attr('data-id'), filter);
    });

    $(document).on('click', '.btnDbPatchImport', function () {
        const _this = $(this);
        const patchData = _this.parent().parent().find('#dbPatchData span');
        const type = _this.attr('data-type');

        $.when($('[data-id="add-'+type+'"]').trigger('click')).done(function () {
            const isTable = $(this).hasClass('addTbBtn');
            const tableEl = isTable ? $('.'+type+'-tb-cont tr').last() : $('.panel-'+type+' .modal').last();

            patchData.each(function () {
                const _this = $(this);
                const field = _this.attr('data-field');
                const val = _this.text();

                if (val === '' || val === 'false') {
                    return true;

                } else if (field.includes('_type')) {
                    tableEl.find('select[data-field="' + field.split('_')[0] + '"] > option[value="' + val + '"]').prop('selected', true).change();

                } else if (field.charAt(0) === 'I') { // InfoPlistPatch
                    $('.panel-'+type+' .modal').last().find('[data-field="InfoPlistPatch"]').tristateCheckbox('checked', true);

                } else if (isTable) {
                    const path = tableEl.attr('data-path')+'/'+tableEl.attr('data-index');
                    const fdata = new FetchReqData(path, field, val);

                    Utils.fetchReq(FetchURL.GUIEditor, 'setval', fdata, res => {
                        if (res === false)
                            throw new Error('db_imp_e');

                        // we now have an input and its data type element, both with the same field
                        tableEl.find('[data-field="'+field+'"]').each(function () {
                            const _this = $(this);

                            if (_this.attr('data-sel') !== undefined) // skip datatype elements
                                return true;

                            _this.attr('title', val).text(val);
                        });

                        Utils.showNotification(ToastType.Success, ToastMessage.Saving);

                    }).catch((e) => {
                        Utils.showNotification(ToastType.Error, ToastMessage.PatchDBE);
                        console.log(e);
                    });

                } else { // fancy panel
                    // we now have an input and its data type element, both with the same field
                    tableEl.find('[data-field="'+field+'"]').each(function () {
                        const _this = $(this);

                        if (_this.hasClass('seltx-select')) // skip comboselect elements
                            return true;

                        _this.val(val).trigger($.Event('keyup', {which:8, disableSpecialEv:true}));
                    });
                }
            });

            // set title and icon text on fancy panels
            if (!isTable) {
                const title = Utils.sanitizeString(tableEl.find('[data-triggerEv]').val());
                const iconTextEl = tableEl.parent().find('.single-item').last().find('.single-item-title');
                const modalTitleEl = tableEl.find('.modal-title');

                iconTextEl.attr('title', title).text(title);
                modalTitleEl.attr('title', title).text(title);
            }
        });
    });

    $(document).on('inline-edit-exit', '', async function (e) {
        Utils.loadingScr(true);

        const tdElem = $(e.target);
        const trElem = tdElem.parent();
        const index = trElem.attr('data-index');
        const field = tdElem.attr('data-field');
        const pidx = index !== field && index !== undefined ? '/'+index:'';
        const path = trElem.attr('data-path');
        const fdata = new FetchReqData(path + pidx, field, e.ivalue);
        const retdata = {
            cmd: fdata.Value !== '' ? 'setval':'unset',
            triggerParentClick: false,
            inlineValue: fdata.Value,
            fetchDataUpdKey: null
        };

        await GUIEventHandler.doInlineEditEv({
            name: tdElem.attr('data-inlineeditEv'),
            retData: retdata,
            fetchData: fdata,
            dataIndex: index,
            tdObjRef: tdElem,
            trObjRef: trElem,
            dataPath: path
        });

        const svalue = Utils.sanitizeString(retdata.inlineValue);

        tdElem.text(svalue).attr('title', svalue);

        if (retdata.cmd === '') {
            Utils.loadingScr(false);
            return true;
        }

        if (svalue !== '' && retdata.fetchDataUpdKey === null) {
            const dataTypeEl = trElem.find('select[data-field="' + fdata.Field + '"]');

            fdata.DataType = tdElem.attr('data-vtype') ?? false;

            if (dataTypeEl.length)
                fdata.DataType = dataTypeEl.find('option:checked').val();
        }

        await Utils.writeDataFetch(retdata.cmd, retdata.fetchDataUpdKey ?? fdata, 'inl_ed_e', true);
        Utils.loadingScr(false);

        if (retdata.triggerParentClick)
            tdElem.trigger('click');
    });

    $.fn.hasAnyClass = function(classes) {
        for (let i=0, l=classes.length; i<l; ++i) {
            if (this.hasClass(classes[i]))
                return true;
        }

        return false;
    };
});
