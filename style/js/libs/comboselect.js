/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 *
 * rev 1.4
 */
'use strict';

(function ($) {
    $.fn.comboSelect = function () {
        return this.each(function () {
            const _this = $(this);
            const options = _this.attr('data-comboselect').split(';');
            const selected = _this.attr('data-comboselect-opt');
            const isTextarea = _this.hasClass('comboselect-txarea');
            const path = _this.attr('data-path');
            const field = _this.attr('data-field');
            let selectEl = '<select class="cce-sel form-select seltx-select border-bottom-0" data-sel="datatype" data-path="'+path+'" data-field="'+field+'">';

            _this.removeAttr('data-select');

            if (isTextarea)
                _this.addClass('seltx-textarea');
            else
                _this.addClass('seltx-input');

            for (let i=0, len=options.length; i<len; ++i) {
                const csel = options[i] === selected || i === 0 ? 'selected':'';

                selectEl += '<option value="'+options[i]+'" '+csel+'>'+options[i]+'</div>';
            }

            selectEl += '</select>';

            if (isTextarea) {
                const elem = document.createElement('div');
                const parent = _this.parent();

                $(elem).append(selectEl).append(_this).appendTo(parent);

            } else {
                const isCombo = _this.hasClass('cce-combo');
                const parent = _this.parent();
                const pparent = isCombo ? parent.parent():_this.parent();
                const sel = $(selectEl).removeClass('border-bottom-0');

                if (isCombo)
                    parent.addClass('w-100');

                $('<div class="input-group no-flexwrap"></div>').append( isCombo ? pparent.find('.combobox'):_this).append(sel).appendTo(pparent);
            }
        });
    };
}(jQuery));
