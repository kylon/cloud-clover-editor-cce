/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 *
 * rev 1.0.1
 */
'use strict';

(function ($) {
    let activeInline = false;

    $.fn.isInlineEditActive = function () {
        return activeInline;
    }

    $(document).on('click', 'html', function (e) {
        if (activeInline && !e.target.classList.contains('inline-input')) {
            const inpt = $('.inline-input');

            $(this).trigger( $.extend({}, e, { type: 'inline-edit-exit', target: inpt.parent(), ivalue: inpt.val() }) );
            closeActiveInline();
        }
    });

    $(document).on('dblclick taphold', '.inline-text, .inline-numb', function (e) {
        e.stopPropagation();

        if (!e.target.classList.contains('inline-input')) {
            closeActiveInline();
            toInlineEditInput($(this));
        }
    });

    $(document).on('keydown', '.inline-input', function (e) {
        if (e.which === 13) {
            const inpt = $(e.target);

            $(this).trigger( $.extend({}, e, { type: 'inline-edit-exit', target: inpt.parent(), ivalue: inpt.val() }) );
            closeActiveInline();
        }
    });

    function closeActiveInline() {
        if (!activeInline)
            return;

        $('.inline-input').each(function () {
            const _this = $(this);
            const val = Utils.sanitizeString(_this.val());

            _this.parent().css('padding', '').text(val).attr('title', val);
        });

        activeInline = false;
    }

    function toInlineEditInput(input) {
        const itype = input.hasClass('inline-text') ? 'text':'number';

        input.css('padding', '0px').html(DOMPurify.sanitize('<input type="' + itype + '" class="inline-input" value="' + input.text() + '">'));
        input.find('.inline-input').trigger('focus');

        activeInline = true;
    }
}(jQuery));
