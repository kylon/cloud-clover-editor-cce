/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 *
 * rev 1.4.5
 */
'use strict';

(function ($) {
    $.fn.combobox = function () {
        return this.each(function () {
            const _this = $(this);
            const options = _this.attr('data-combo').split(';');
            let optionsCode = '<img src="../style/img/select-arrow.png" class="combobox-arrow" alt="show options">' +
                              '<div class="combobox-opt-container combobox-hide">';

            _this.removeAttr('data-combo');
            _this.addClass('combobox-input').addClass('rounded');
            _this.appendTo(
                $('<div class="combobox"></div>').insertBefore(_this)
            );

            for (let i=0, len=options.length; i<len; ++i) {
                const curOpt = options[i].split(':');
                let val = curOpt[0];

                if (val === '')
                    continue;

                if (curOpt.length === 2) // option type: Desc:Val
                    val = curOpt[1];

                optionsCode += '<div class="combobox-opt" data-value="'+val+'">'+curOpt[0]+'</div>';
            }
            optionsCode += '</div>';

            $(optionsCode).insertAfter(_this);
        });
    };

    $(document).on('click','html',function () {
        closeAll();
    });

    $(document).on('click','.combobox-arrow',function (e) {
        e.stopPropagation();

        const parent = $(this).parent();
        const el = parent.find('.combobox-opt-container');
        const comboEl = parent.find('.combobox-input');

        closeAll(el);

        if (comboEl.hasClass('jcb-disabled'))
            return true;

        if (el.hasClass('combobox-hide')) {
            el.removeClass('combobox-hide');
            comboEl.removeClass('rounded');
            comboEl.addClass('rounded-top');
            parent.find('img').addClass('combobox-arrow-open');

        } else {
            el.addClass('combobox-hide');
            comboEl.removeClass('rounded-top');
            comboEl.addClass('rounded');
            parent.find('img').removeClass('combobox-arrow-open');
        }
    });

    $(document).on('click','.combobox-opt',function(e) {
        const _this = $(this);

        _this.parent().parent().find('.combobox-input').val(_this.attr('data-value'));

        return _this.trigger( $.extend({}, e, { type: 'combo-change' }) );
    });

    $(document).on('keyup','.combobox-input',function(e) {
        return $(this).trigger( $.extend({}, e, { type: 'combo-input-change' }) );
    });

    function closeAll(exclude) {
        const visible = $('.combobox-opt-container').not('.combobox-hide');

        visible.each(function () {
            const _this = $(this);

            if (exclude === null || !_this.is(exclude)) {
                _this.addClass('combobox-hide');
                _this.parent().find('img').removeClass('combobox-arrow-open');
            }
        });
    }
}(jQuery));
