/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 *
 * rev 1.1.2
 */
'use strict';

(function ($) {
    $.fn.tristateCheckbox = function (cmd, update) {
        const _this = $(this);

        switch (cmd) {
            case 'unchecked':
            case 'checked':
            case 'indeterminate': {
                _this.attr('data-state', cmd);
                setState(_this);

                if (update === true)
                    _this.trigger('checkbox-state-change');
            }
                break;
            default: {
                return this.each(function () {
                    setState($(this));
                });
            }
                break;
        }

        return _this;
    };

    $.fn.getTristateCheckboxState = function () {
        const state = $(this).attr('data-state');

        return state === 'checked' ? true : (state === 'indeterminate' ? false : undefined);
    };

    $.fn.setTristateCheckboxState = function (state) {
        const _this = $(this);

        _this.attr('data-state', state);
        setState(_this);
    }

    function setState(_this) {
        const state = _this.attr('data-state');

        _this.prop('indeterminate', false);
        _this.prop('checked', false);

        switch (state) {
            case 'checked':
                _this.prop('checked', true);
                break;
            case 'indeterminate':
                _this.prop('indeterminate', true);
                break;
            case 'unchecked':
            default:
                break;
        }
    }

    $(document).on('change', '.tristate-checkbox', function (e) {
        const _this = $(this);
        const state = _this.attr('data-state'); // get old state

        switch (state) { // set new state
            case 'unchecked':
                _this.attr('data-state', 'checked');
                setState(_this);
                break;
            case 'checked':
                _this.attr('data-state', 'indeterminate');
                setState(_this);
                break;
            case 'indeterminate':
                _this.attr('data-state', 'unchecked');
                setState(_this);
                break;
            default:
                break;
        }

        return _this.trigger( $.extend({}, e, { type: 'checkbox-state-change' }) );
    });
}(jQuery));
