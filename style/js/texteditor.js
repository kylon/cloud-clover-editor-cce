/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const CCETextEditor = (() => {
    const _saveWorker = new Worker(Server.BasePath + '../style/js/workers/textM-autosave.js');
    const _isDayTheme = _isDay(new Date().getHours());
    const _editor = CodeMirror.fromTextArea(document.getElementById('texteditor'), {
        mode: "xml",
        lineNumbers: true,
        undoDepth: 50,
        autofocus: true,
        dragDrop: false,
        spellcheck: false,
        autocorrect: false,
        autocapitalize: false,
        styleActiveLine: true,
        autoCloseTags: true,
        scrollbarStyle: "overlay",
        extraKeys: {"Ctrl-F": "findPersistent"},
        theme: _isDayTheme ? 'default':'base16-dark',
        onLoad: $('.loading-texteditor').remove()
    });
    const txEditor = {};

    function _isDay(hour) {
        return hour >= 7 && hour < 22;
    }

    async function _updatePlistInContainer(plistString, notify = false) {
        await Utils.fetchReq(FetchURL.TextEditor, 'updplststr', {plist: plistString}, res => {
            if (res === false)
                throw new Error('atf_e');

            if (notify)
                Utils.showNotification(ToastType.Success, ToastMessage.Saving);

        }).catch((e) => {
            Utils.showNotification(ToastType.Error, ToastMessage.AutoSaveE);
            console.log(e);
        });
    }

    function _init() {
        $('#wrapper').addClass('h-100');
        _editor.setSize('100%', '100%');
        _saveWorker.postMessage(['start']);

        if (_isDayTheme)
            $('.cce-tab-footer').removeClass('cce-tab-footer-dark');
        else
            $('.cce-tab-footer').addClass('cce-tab-footer-dark');

        _saveWorker.onmessage = async function(e) {
            switch (e.data) { // cmd
                case 'atf':
                    await _updatePlistInContainer(_editor.getValue(), true);
                    break;
                case 'kill':
                    _saveWorker.terminate();
                    break;
                default:
                    break;
            }
        };

        _editor.on('change', function () {
            _saveWorker.postMessage(['setATF', true]);
        });

        $(document).on('keydown', async function (e) {
            if (!e.ctrlKey)
                return true;

            switch (e.code) {
                case 'KeyS': {
                    e.preventDefault();

                    Utils.loadingScr(true);
                    _saveWorker.postMessage(['setATF', false]);
                    await _updatePlistInContainer(_editor.getValue(), true);
                    Utils.loadingScr(false);
                    _saveWorker.postMessage(['setATF', true]);
                }
                    break;
                case 'KeyF': {
                    e.preventDefault();

                    $('.CodeMirror-dialog-top').remove(); // workaround csp
                    CodeMirror.commands.findPersistent(_editor);
                }
                    break;
                case 'KeyG': {
                    e.preventDefault();

                    $('.CodeMirror-dialog-top').remove(); // workaround csp
                    CodeMirror.commands.replace(_editor);
                }
                    break;
                default:
                    break;
            }
        });

        $(window).on('resize', function () {
            _editor.refresh();
            _editor.setSize('100%', '100%');
        });
    }

    txEditor.writePlistInContainer = async () => {
        _saveWorker.postMessage(['stop']);
        await _updatePlistInContainer(_editor.getValue());
    }

    txEditor.enableAutoSave = () => {
        _saveWorker.postMessage(['setATF', true]);
    }

    Object.freeze(txEditor);
    _init();

    return txEditor;
})();