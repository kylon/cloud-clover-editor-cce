/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const Coder = (() => {
    const fnMap = {
        'b64hex': _b64hex,
        'b64ascii': _b64ascii,
        'hexb64': _hexb64,
        'hexascii': _hexascii,
        'asciihex': _asciihex,
        'asciib64': _asciib64
    };
    const obj = {};

    function _b64hex(b64) {
        const str = atob(b64);
        let ret = '';

        for (let i=0, l=str.length; i<l; ++i)
            ret += str.charCodeAt(i).toString(16).padStart(2, '0');

        return ret.toUpperCase();
    }

    function _b64ascii(b64) {
        return _hexascii(_b64hex(b64));
    }

    function _hexb64(hex) {
        return btoa(_hexascii(hex, false));
    }

    function _hexascii(hex, strict = true) {
        return hex.match(/\w{2}/g).map(a => {
            const code = parseInt(a, 16);

            return (code > 32 && code < 127) || !strict ? String.fromCharCode(code) : '';
        }).join('');
    }

    function _asciihex(ascii) {
        let ret = '';

        for (let i=0,l=ascii.length; i<l; ++i)
            ret += ascii.charCodeAt(i).toString(16);

        return ret.toUpperCase();
    }

    function _asciib64(ascii) {
        return _hexb64(_asciihex(ascii));
    }

    function _isHex(v) {
        return /(0[xX])?[0-9a-fA-F]/.test(v);
    }

    obj.Codify = (data, from, to) => {
        if (from === to)
            return data;
        else if (from === 'hex' && !_isHex(data))
            throw new Error('invalid hex input');

        return fnMap[from+to](data);
    }

    Object.freeze(obj);
    return obj;
})();