/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const EditorUtils = (() => {
    const _bankContainer = $('.cce-bank-container');
    const _footer = $('.cce-tab-footer');
    const _sidebar = $('.sidebar-cont');
    const _saveAsModal = $('#saveas');
    const _configList = $('#config-list');
    const edUtil = {};

    edUtil.sortingStartFn = (evt, sortUi) => {
        const itm = sortUi.item;
        const elemID = itm.parent().attr('data-sid').split('-')[1];

        itm.addClass('noshake');

        $('.single-'+elemID).each(function () {
            const _this = $(this);

            if (!_this.hasClass('noshake'))
                _this.addClass('inf-shake');

            _this.removeClass('flip-in');
        });
    }

    edUtil.sortingStopFn = () => {
        $('.inf-shake').each(function () {
            $(this).removeClass('inf-shake noshake').addClass('flip-in');
        });
    }

    edUtil.isSidebarPinned = () => {
        return _sidebar.find('.pinned-sidebar').length > 0;
    }

    edUtil.toggleSidebar = (isTextMode, pin = false) => {
        const content = isTextMode ? $('.CodeMirror'):$('.cce-content');
        const toggleBtn = _sidebar.find('.sidebar-toggle-btn');

        _sidebar.find('#sidebar-wrapper').toggleClass('pinned-sidebar');
        content.toggleClass('pinned-sidebar-content');
        _footer.toggleClass('pinned-sidebar-cce-footer');
        toggleBtn.toggleClass('pinned-sidebar-toggle-btn').find('.pin-arrow').toggleClass('icon-angle-right').toggleClass('icon-angle-left2');

        if (pin)
            toggleBtn.toggleClass('text-white');
    }

    edUtil.resetSaveToBankInputs = () => {
        const cidTxtEl = _saveAsModal.find('.bank-cid-text');
        const cfgNameEl = _saveAsModal.find('.saveas-text');
        const genNewCidEl = _saveAsModal.find('.gen-new-cid');
        const confName = _configList.find('.fhandle-active .handle-name').text();

        _saveAsModal.find('.bank-config-cid, .bank-gencid, .bank-config-lock-flag, .bank-cfg-sett, #cfgNameHelp').addClass('d-none');
        _saveAsModal.find('input[name="configBankLockFlag"][value="n"]').prop('checked', true);
        cfgNameEl.off('keyup').removeClass('material-text-inp-err').prop('disabled', false).attr('placeholder', 'cce_config').val(confName);
        _saveAsModal.find('.save-to-bank').prop('checked', false).removeClass('updmode');
        _saveAsModal.find('.skip-centry, .skip-sdata').prop('checked', false);
        cidTxtEl.removeClass('material-text-inp-err').val('');

        if (genNewCidEl.prop('checked')) {
            _saveAsModal.find('.bank-cid-new').addClass('d-none').text('');
            genNewCidEl.prop('checked', false);
            cidTxtEl.removeClass('d-none');
        }
    }

    edUtil.getBankViewMode = async () => {
        let viewMode = 'cceb';

        await Utils.fetchReq(FetchURL.CCEBank, 'getbnkviewmode', null, res => {
            if (res === false)
                throw new Error('get_viewm_e');

            viewMode = res;

        }).catch((e) => {
            Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);
            console.log(e);
        });

        return viewMode;
    }

    /**
     * CCE Bank - draw configs
     *
     * @param filter
     */
    edUtil.drawConfigs = (filter) => {
        const fdata = {queryFilter: filter};

        Utils.fetchReq(FetchURL.CCEBank, 'bsearch', fdata, function (res) {
            const invalid = res === false || res === null;
            const len = invalid ? 0 : res.length;
            let content = '';

            if (invalid) // dont return now so that loading screen will be removed
                Utils.showNotification(ToastType.Error, ToastMessage.BankDBE);

            for (let i = 0; i < len; ++i) {
                const lockFlag = res[i]['locked'] === 'y' ? '-locked' : '';
                let icon = 'clover-efi';
                let fix = '';

                switch (res[i]['type']) {
                    case 'oz':
                        icon = 'ozmosis';
                        break;
                    case 'chm':
                        icon = 'chameleon';
                        fix = ' bank-label-fix';
                        break;
                    case 'oc':
                        icon = 'opencore';
                        fix = ' bank-label-fix';
                        break;
                    default:
                        break;
                }

                content +=
                    '<div class="col-6 col-sm-4 col-lg-2">'+
                    '<div class="bank-config-box" data-idx="' + res[i]['id'] + '" title="' + res[i]['name'] + '">'+
                    '<i class="fa icon-file-' + icon + lockFlag + ' bank-config-icon"></i>'+
                    '<label class="bank-cfg-title truncate-100'+fix+'">' + res[i]['name'] + '</label>'+
                    '</div></div>';
            }

            _bankContainer.find('.row').html(DOMPurify.sanitize(content));
        }).catch(() => { Utils.showNotification(ToastType.Error, ToastMessage.BankDBE); });
    }

    Object.freeze(edUtil);
    return edUtil;
})();