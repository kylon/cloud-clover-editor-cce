/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const FlagsUtils = (() => {
    const _modalsMap = {
        'acpif':        $('.acpif-modal'),
        'csrf':         $('.csrf-modal'),
        'btrf':         $('.btrf-modal'),
        'ocpka':        $('.ocpka-modal'),
        'ocdlv':        $('.ocdlv-modal'),
        'ocdbgt':       $('.ocdbgt-modal'),
        'ocscanplf':    $('.ocscanplf-modal'),
        'ocexsdf':      $('.ocexsdf-modal'),
        'ochlv':        $('.ochlv-modal'),
        'ocplf':        $('.ocplf-modal')
    };
    const flagsUtil = {};

    function _getUBigInt(n, bits) {
        if (bits === undefined)
            return n;

        return BigInt.asUintN(parseInt(bits, 10), n);
    }

    function _hasFlag(flag, bit) {
        return (flag & bit) === bit;
    }

    function _getSimpleFlagBits(flag, id, bitsN) {
        let flagsElems = null;
        let flagsC = 0;
        const bits = [];

        if (flag === 0n)
            return [0];

        flagsElems = _modalsMap[id]?.find('.'+id+'-flag-val');
        flagsC = flagsElems?.length ?? 0;

        for (let i=0; i<flagsC; ++i) {
            const v = _getUBigInt(BigInt(1 << i), bitsN);

            if (_hasFlag(flag, v))
                bits.push(v);
        }

        return bits;
    }

    function _getAdvFlagBits(flag, id, bitsN) {
        const bits = [];

        if (flag === 0n)
            return [0];

        _modalsMap[id]?.find('.'+id+'-flag-val').each(function () {
            const v = _getUBigInt(BigInt(parseInt($(this).val(), 10)), bitsN);

            if (v !== 0n && _hasFlag(flag, v))
                bits.push(v);
        });

        return bits;
    }

    /**
     * Update selected flag bits in CCE UI
     *
     * @param id string
     * @param flag string
     * @param asHex bool
     * @param bitsN string|int|undefined
     */
    flagsUtil.updateGUIFlagBits = (id, flag, asHex = false, bitsN = undefined) => {
        let bits;

        switch (id) {
            case 'acpif':
            case 'ocdlv':
            case 'ocscanplf':
            case 'ochlv':
                bits = _getAdvFlagBits(flag, id, bitsN).sort();
                break;
            case 'csrf':
            case 'btrf':
            case 'ocdbgt':
            case 'ocexsdf':
            case 'ocpka':
            case 'ocplf':
                bits = _getSimpleFlagBits(flag, id, bitsN).sort();
                break;
            default:
                return false;
        }

        if (asHex)
            flag = '0x' + flag.toString(16);

        _modalsMap[id].find('.'+id+'-flag-val').each(function () {
            const _this = $(this);
            let state = false;

            for (let i=0, len=bits.length; i<len; ++i) {
                if (_this.val() !== bits[i].toString())
                    continue;

                state = true;
                bits.splice(i,1);
                break;
            }

            _this.prop('checked', state);
        });

        _modalsMap[id].find('.'+id+'-fflag').text(flag);
    }

    /**
     * Generate flag from selected bits in CCE UI
     *
     * @param id string
     * @param bitsN string|int|undefined
     *
     * @returns {BigInt}
     */
    flagsUtil.genSimpleFlag = (id, bitsN = undefined) => {
        let flag = 0n;

        _modalsMap[id]?.find('.'+id+'-flag-val:checked')?.each(function () {
            flag |= _getUBigInt(BigInt(parseInt($(this).val(), 10)), bitsN);
        });

        return flag;
    }

    Object.freeze(flagsUtil);
    return flagsUtil;
})();