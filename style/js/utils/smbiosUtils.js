/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

const SMBIOSUtils = (() => {
    const _smbiosTab = $('#smbios, #platform_info');
    const _smbiosSelectEl = _smbiosTab.find('#smsel');
    const _smbiosDeviceDetails = _smbiosTab.find('.smbios-info').first();
    const smbUtil = {};

    /**
     * Update SMBIOS info modal
     *
     * @param prodName
     * @param mac
     */
    function _updateSMBIOSDeviceDetails(prodName, mac) {
        if (mac === '' || mac === undefined)
            return;

        let icon = 'question2';

        if (prodName.includes('book'))
            icon = 'laptop';
        else if (prodName.includes('serve') || prodName.includes('mini'))
            icon = 'hdd-o';
        else if (prodName.includes('imac'))
            icon = 'display';
        else if (prodName.includes('pro'))
            icon = 'computer-tower';

        let cpuList = mac['specs']['cpu'];
        let gpuList = mac['specs']['gpu'];
        let cpuHtml = '';
        let gpuHtml = '';

        for (let i=0, l=cpuList.length; i<l; ++i)
            cpuHtml += cpuList[i] + '<br>';

        for (let i=0, l=gpuList.length; i<l; ++i)
            gpuHtml += gpuList[i] + '<br>';

        _smbiosDeviceDetails.find('.smbiosmodel-icon').removeAttr('class').addClass('smbiosmodel-icon fa icon-'+icon);
        _smbiosDeviceDetails.find('.cpu').html(DOMPurify.sanitize(cpuHtml));
        _smbiosDeviceDetails.find('.ram').text(mac['specs']['ram'] !== '' ? mac['specs']['ram']:'N/A');
        _smbiosDeviceDetails.find('.gpu').html(DOMPurify.sanitize(gpuHtml));
        _smbiosDeviceDetails.find('.smcr').text(mac['smcVersion'] !== '' ? mac['smcVersion']:'N/A');
        _smbiosDeviceDetails.find('.mid').text(mac['modelCode'] !== '' ? mac['modelCode']:'N/A');
        _smbiosDeviceDetails.find('.yr').text(mac['year']);
        _smbiosDeviceDetails.find('.minosv').text(mac['minOSVersion']);
        _smbiosDeviceDetails.find('.maxosv').text(mac['maxOSVersion']);
        _smbiosDeviceDetails.parent().find('.modal-title').text(mac['productName']);
    }

    smbUtil.populateSMBIOSValues = () => {
        const selected = _smbiosSelectEl.attr('data-usrv');
        const frag = new DocumentFragment();

        for (const smb in smbiosdata) {
            const node = document.createElement('option');

            node.value = smb;
            node.text = smbiosdata[smb]['prettyName'];

            if (smb === selected)
                node.selected = true;

            frag.appendChild(node)
        }

        _smbiosSelectEl.append(frag);
        _updateSMBIOSDeviceDetails(selected, smbiosdata[selected]);
    }

    /**
     * Update SMBIOS values on screen when a Mac SMBIOS is selected
     *
     * @param prodName string
     */
    smbUtil.switchSMBIOS = prodName => {
        if (Utils.isEmptyValue(prodName))
            return;

        const eventArgs = { which:8, notf:false };
        const mac = smbiosdata[prodName];

        _smbiosTab.find('#smpdn').val(mac['productName']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smpdf').val(mac['family']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smbrd').val(mac['biosReleaseDate']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smbv').val(mac['biosLegacyVersion']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smbid').val(mac['boardID']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smbvr').val(mac['boardVersion']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smbt').val(mac['boardType']).trigger($.Event('change', {notf:false}));
        _smbiosTab.find('#smcht').val(mac['chassisType']).trigger($.Event('change', {notf:false}));
        _smbiosTab.find('#smchmver').val(mac['chassisVer']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smcha').val(mac['chassisAsset']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smcl').val(mac['locationInChassis']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smver').val(mac['version']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smbver').val(mac['version']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#efiver').val(mac['biosVersion']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smff').val(mac['fwFeatures']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smffm').val(mac['fwFeaturesMask']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#extsmff')?.val(mac['exFwFeatures']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#extsmffm')?.val(mac['exFwFeaturesMask']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocplf').val(mac['platformFeature']).trigger($.Event('combo-flag-change', {notf:false}));
        _smbiosTab.find('.macserialbtn').trigger('click');

        _updateSMBIOSDeviceDetails(prodName, mac);
    }

    smbUtil.switchSMBIOSOC = prodName => {
        if (Utils.isEmptyValue(prodName))
            return;

        const eventArgs = { which:8, notf:false };
        const mac = smbiosdata[prodName];

        _smbiosTab.find('#ocgncput').val(mac['processorType']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocgsysprodn').val(mac['productName']).trigger($.Event('keyup', eventArgs));

        if (_smbiosTab.find('.autosmb').getTristateCheckboxState() === true) {
            $(_smbiosTab.find('.macserialbtn')[0]).trigger($.Event('click'));
            _updateSMBIOSDeviceDetails(prodName, mac);
            return;
        }

        _smbiosTab.find('#ocnvrambid').val(mac['boardID']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#smpdn').val(mac['productName']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocdhbdprod').val(mac['boardID']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocdhboardrev').val(mac['boardRevisionEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocdhsyspdnm').val(mac['productName']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocdhsmcrev').val(mac['smcRevisionEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocdhsmcbrch').val(mac['smcBranchEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocdhsmcplat').val(mac['smcPlatformEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocmemffact').val(mac['memFormFactor']).trigger($.Event('change', {notf:false}));
        _smbiosTab.find('#ocnvfwf').val(mac['fwFeaturesEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocnvfwfm').val(mac['fwFeaturesMaskEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocbiosv').val(mac['biosVersion']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocbiosreld').val(mac['biosReleaseDate']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocsysprodn').val(mac['productName']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocsysver').val(mac['version']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocsyssku').val(mac['systemSkuNo']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocsysfaml').val(mac['family']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocboardprod').val(mac['boardID']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocboardv').val(mac['boardVersion']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocboardasst').val(mac['boardAssetTag']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocboardtp').val(mac['boardType']).trigger($.Event('change', {notf:false}));
        _smbiosTab.find('#ocboardlocchas').val(mac['locationInChassis']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#occhasstp').val(parseInt(mac['chassisType'], 16)).trigger($.Event('change', {notf:false}));
        _smbiosTab.find('#occhassver').val(mac['chassisVer']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#occhassasst').val(mac['chassisAsset']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocsmcver').val(mac['smcVersionEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocplf').val(parseInt(mac['platformFeature'], 16)).trigger($.Event('combo-flag-change', {notf:false}));
        _smbiosTab.find('#ocproctp').val(mac['processorType']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocfwf').val(mac['fwFeaturesEnc']).trigger($.Event('keyup', eventArgs));
        _smbiosTab.find('#ocfwfm').val(mac['fwFeaturesMaskEnc']).trigger($.Event('keyup', eventArgs));
        $(_smbiosTab.find('.macserialbtn')[0]).trigger($.Event('click', {syncall:true}));

        _updateSMBIOSDeviceDetails(prodName, mac);
    }

    smbUtil.setMLB = (mlb, mode) => {
        const isAutomatic = mode === 'oc' && _smbiosTab.find('.autosmb').getTristateCheckboxState() === true;
        const mlbInpt = $('.mlbfield');

        mlbInpt.each(function () {
            const _this = $(this);
            const path = _this.attr('data-path');

            if (isAutomatic && !path.includes('Generic'))
                return true;

            const fdataMlb = new FetchReqData(path, _this.attr('data-field'), mlb);

            _this.val(mlb);
            Utils.writeDataFetch('setval', fdataMlb, 'emlbset');
        });
    }

    Object.freeze(smbUtil);
    return smbUtil;
})();
