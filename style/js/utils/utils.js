/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

class FetchReqData {
    /**
     * @param path string
     * @param field string|array
     * @param value mixed
     * @param dataType string
     */
    constructor(path, field, value, dataType = false) {
        this.Path = path;
        this.Field = field;
        this.Value = value;
        this.DataType = dataType;

        Object.seal(this);
    }
}

class FetchReqDataUpdKey {
    /**
     * @param path string
     * @param oldKey mixed
     * @param newKey mixed
     */
    constructor(path = null, oldKey = null, newKey = null) {
        this.Path = path;
        this.OldKey = oldKey;
        this.NewKey = newKey;

        Object.seal(this);
    }
}

class FetchReqDataGet {
    /**
     * @param path string
     */
    constructor(path) {
        this.Path = path;

        Object.seal(this);
    }
}

class FetchReqDataCopy {
    /**
     * @param path string
     * @param data array
     */
    constructor(path, data = []) {
        this.Path = path;
        this.Data = data;

        Object.seal(this);
    }
}

const Server = (() => {
    function getCCEBasePath() {
        const pathAr = window.location.pathname.split('/');
        let path = '';

        for (let i=0,l=pathAr.length; i<l; ++i) {
            path += pathAr[i] + '/';

            if (pathAr[i] === 'cce')
                break;
        }

        return path;
    }

    const srv = {
        BasePath: getCCEBasePath()
    };

    Object.freeze(srv);
    return srv;
})();

const FetchURL = (() => {
    const url = {
        Editor: Server.BasePath + 'data/xhr/editor.php',
        GUIEditor: Server.BasePath + 'data/xhr/guieditor.php',
        PatchesDB: Server.BasePath + 'data/xhr/patchesdb.php',
        CCEBank: Server.BasePath + 'data/xhr/ccebank.php',
        TextEditor: Server.BasePath + 'data/xhr/texteditor.php',
        Settings: Server.BasePath + 'data/xhr/settings.php'
    };

    Object.freeze(url);
    return url;
})();

const ToastType = (() => {
    const type = {
        Success: 0,
        Error: 1
    };

    Object.freeze(type);
    return type;
})();

const ToastMessage = (() => {
    const msg = {
        // Error
        UnknownE: 'Unknown error',
        UnknownFetchE: 'CCE: Unknown fetch error',
        AutoSaveE: 'AutoSave error',
        SettingsE: 'CCE Settings error',
        BankDBE: 'CCE Bank: Database error',
        PatchDBE: 'Patches DB: Database error',
        BankInvalidNameE: 'CCE Bank: Invalid file name',
        NewFileE: 'New file index error',
        RemoveFileE: 'Could not remove file',
        ChamKPathesE: 'Chameleon: Unable to retrieve patch data',
        OzTemplateDataE: 'Ozmosis: Unable to retrieve template data',
        PropsDataE: 'Unable to retrieve property data',
        macserialCmdE: 'macserial: Failed to execute command',
        macserialUknownE: 'macserial: Unknown error',
        CopyValueE: 'Could not copy values to target file',
        InvalidDataType: 'Invalid type for this value',

        // Success
        MyBankMode: 'CCE Bank: switched to MyBank',
        Saving: 'Saving..'
    };

    Object.freeze(msg);
    return msg;
})();

const Utils = (() => {
    const _toastNotf = $('#ccetoast');
    const _dbPatchesListElm = $('#dbPatchesList');
    const _navbarConfigList = $('#config-list');
    const _loadingScr = $('#loadingscr');
    const _copyToModal = $('#copyto');
    const _htmlBodyElem = $('body');
    const _perPagePatches = 8;
    const _notfQue = [];
    const util = {};

    function _jsonStringifyAdv(key, val) {
        if (typeof val === 'bigint')
            return val.toString(10);

        return val;
    }

    function _drawPatchesCards (type, dbPatchesList, patchesCount) {
        const fields = [ //TODO Opencore
            'Comment', 'Name', 'InfoPlistPatch', 'Find', 'Find_type', 'Replace', 'Replace_type',
            'MaskFind', 'MaskFind_type', 'MaskReplace', 'MaskReplace_type', 'TgtBridge', 'TgtBridge_type',
            'MatchOS', 'MatchBuild'
        ];
        const hasMatch = ['kextP', 'kernelP', 'chamkernelP', 'ozkextP', 'ozkernelP', 'ozBtrP'];
        let cont = '';

        if (!patchesCount)
            cont += '<div class="col-12 text-center"><i class="fa icon-warning3 color-disabled mt-4 empty-patch-db"></i></div>';

        for (let i=0, limit=dbPatchesList.length; i<limit && i<_perPagePatches; ++i) {
            cont += '<div class="col-12 col-lg-6 mb-3">' +
                    '<div class="card mx-auto bg-dark text-white">' +
                    '<div class="card-body">' +
                    '<p class="card-text">' + dbPatchesList[i]['Comment'] + '</p>';

            for (let j=0,l=hasMatch.length; j<l; ++j) {
                if (type !== hasMatch[j])
                    continue;

                const matchOs = util.getPropVal(dbPatchesList[i], 'MatchOS');
                const matchBuild = util.getPropVal(dbPatchesList[i], 'MatchBuild');

                cont += '<p>MatchOS: ' + matchOs + '</p><p>MatchBuild: ' + matchBuild + '</p>';
                break;
            }

            cont += '<div class="card-link-cont text-end">' +
                    '<span class="card-link btnDbPatchImport" data-type="' + type + '">' +
                    '<i class="fa icon-plus-circle2"></i> Import' +
                    '</span></div>' +
                    '<div id="dbPatchData" class="d-none">';

            for (let j=0,l=fields.length; j<l; ++j) {
                let val = util.getPropVal(dbPatchesList[i], fields[j]);

                if (fields[j] === 'InfoPlistPatch' && val === '')
                    val = 'false';

                cont += '<span data-field="' + fields[j] + '">' + val + '</span>';
            }

            cont += '</div></div></div></div>';
        }

        _dbPatchesListElm.html(DOMPurify.sanitize(cont));
    }

    /**
     * Credits jbo5112 (stackoverflow)
     *
     * @param text
     *
     * @return {void | string | *}
     */
    util.sanitizeString = text => {
        const map = {
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&apos;',
        };

        return text.replace(/[<>"']/g, function(c) { return map[c]; }).trim();
    }

    util.isEmptyValue = value => { return (value === '' || value === undefined || value === null) };

    util.isValidKey = key => { return (key <= 8 || key === 32 || (key >= 46 && key <= 90) || key > 145 ); }

    util.isValidNumbKey = key => { return (key >= 48 && key <= 57 || key === 8 || key === 13 || key === 46); }

    util.showPendingNotification = () => {
            const notf = _notfQue.shift();

            if (notf === undefined)
                return;

            util.showNotification(notf[0], notf[1]);
    }

    /**
     * Show an error/success notification
     *
     * @param toastType
     * @param message
     */
    util.showNotification = (toastType, message = ToastMessage.UnknownE) => {
        if (_toastNotf.hasClass('show') || _toastNotf.hasClass('shown')) {
            if (toastType === ToastType.Error)
                _notfQue.push([ToastType.Error, message]);
            else if (_notfQue.length > 0 && _notfQue[_notfQue.length - 1][0] !== ToastType.Success)
                _notfQue.push([ToastType.Success, message]);

            return;
        }

        const toastHeader = _toastNotf.find('.toast-header');

        if (toastType === ToastType.Success)
            toastHeader.removeClass('bg-danger').addClass('bg-success');
        else
            toastHeader.removeClass('bg-success').addClass('bg-danger');

        _toastNotf.find('.toast-body').text(message);
        _toastNotf.toast('show');
    }

    util.fetchReq = async (url, type, fetchData, callback, tmout = 15, fileFetch = false, fdata = null) => {
        const init = () => {
            const head = {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'tok': document.head.querySelector('meta[name="csrf-token"]').getAttribute('content')
                }
            };

            if (fileFetch) {
                const formData = new FormData();

                for (const key in fdata) {
                    if (!fdata.hasOwnProperty(key))
                        continue;

                    formData.append(key, fdata[key]);
                }
                head.body = formData;

            } else {
                head.headers['Content-Type'] = 'application/json';
                head.body = JSON.stringify({
                    type: type,
                    data: fetchData
                }, function (k, v) {
                    return _jsonStringifyAdv(k, v);
                });
            }

            return head;
        };

        const timeout = new Promise((resolve, reject) => {
            const tm = setTimeout(() => {
                clearTimeout(tm);
                reject(new Error('Connection timeout'));
            }, tmout * 1000);
        });

        const fetchP = fetch(url, init()).then(res => {
            if (res.ok && res.status === 200) {
                if (callback !== null)
                    return res.json().then(res => callback(res));
                else
                    return res.json().catch(e => console.log(e));
            }
        });

        return Promise.race([timeout, fetchP]);
    }

    /**
     * FetchReq wrapper for simple requests
     *
     * @param cmd string
     * @param data FetchReqData|FetchReqDataSort
     * @param erType string
     * @param toastMessage ToastMessage
     * @param notify bool
     *
     * @returns {Promise<void>}
     */
    util.writeDataFetch = async (cmd, data, erType, notify = false, toastMessage = ToastMessage.UnknownFetchE) => {
        await util.fetchReq(FetchURL.GUIEditor, cmd, data, res => {
            if (res === false)
                throw new Error(erType);

            if (notify)
                util.showNotification(ToastType.Success, ToastMessage.Saving);

        }).catch((e) => {
            util.showNotification(ToastType.Error, toastMessage);
            console.log(e);
        });
    }

    util.createTableRow = (tableEl, path = null) => {
        const trList = tableEl.find('tr');
        const newIdx = trList.length - 1;
        const row = trList.first().clone();
        const index = row.attr('data-index') ?? '';

        if (path !== null)
            row.attr('data-path', path);

        if (index !== '')
            row.attr('data-index', newIdx);

        row.removeClass('d-none');

        row.find('[data-field]').each(function () {
            const _this = $(this);

            if (_this.attr('data-field') === '-1')
                _this.attr('data-field', newIdx);
        });

        return row;
    }

    /**
     * Show/hide a loading screen to lock user inputs
     *
     * @param enable
     */
    util.loadingScr = enable => {
        if (enable)
            _loadingScr.removeClass('d-none');
        else
            _loadingScr.addClass('d-none');

        _htmlBodyElem.css('overflow', enable ? 'hidden':'');
    }

    /**
     * Initialize pagination element and patches list
     *
     * @param ptype string
     * @param filter string
     */
    util.initPagingAndPatchesList = async (ptype, filter) => {
        const fdata = { type: ptype, queryFilter: filter };
        let dbPatchesCount = 0;

        await Utils.fetchReq(FetchURL.PatchesDB, 'getPatchesListCount', fdata, res => {
            dbPatchesCount = res;

            if (res === false)
                throw new Error('db_tb_btn_e');

        }).catch((e) => {
            Utils.showNotification(ToastType.Error, ToastMessage.PatchDBE);
            console.log(e);
        });

        if (dbPatchesCount === false)
            return false;

        $(".pagination").paging(dbPatchesCount, {
            format: '[ nncnn ]',
            perpage: _perPagePatches,
            page: 1,
            stepwidth: 0,
            circular: false,
            onSelect: async function(page) {
                const start = (_perPagePatches * page) - _perPagePatches;
                const fdata = { type: ptype, queryFilter: filter, perPagePatches: _perPagePatches, offset: start };
                let dbPatchesList = false;

                await util.fetchReq(FetchURL.PatchesDB, 'getPatchesList', fdata, res => {
                    dbPatchesList = res;

                    if (res === false)
                        throw new Error('init_dbp_e');

                }).catch((e) => {
                    util.showNotification(ToastType.Error, ToastMessage.PatchDBE);
                    console.log(e);
                });

                if (dbPatchesList === false)
                    return;

                _drawPatchesCards(ptype, dbPatchesList, dbPatchesCount);
            },
            onFormat: function(type) {
                let linkHtml, pageVal = '', activeClass = '', isActive = false;

                switch (type) {
                    case 'block': // n and c
                        pageVal = this.value;
                        isActive = this.page === this.value;
                        activeClass = isActive ? ' active':'';
                        break;
                    case 'next': // >
                        pageVal  = '&gt;';
                        break;
                    case 'prev': // <
                        pageVal  = '&lt;';
                        break;
                    case 'first': // [
                        pageVal  = 'First';
                        break;
                    case 'last': // ]
                        pageVal  = 'Last';
                        break;
                    default:
                        break;
                }

                linkHtml = isActive ? '<span class="page-link">'+pageVal+'</span>' : '<a class="page-link" href="#">'+pageVal+'</a>';

                return '<li class="page-item' + activeClass + '">' + linkHtml + '</li>';
            }
        });

        return true;
    }

    /**
     * Get the value of the property
     *
     * @param arr array
     * @param key string
     */
    util.getPropVal = (arr, key) => {
        if (arr == null || !(key in arr) || arr[key] == null)
            return '';

        if (typeof arr[key] === 'string') {
            if (arr[key] === 'true')
                return true;
            else if (arr[key] === 'false')
                return false;
            else
                return arr[key];
        }

        return arr[key];
    }

    util.prepareCopyToModal = () => {
        const fileType = _navbarConfigList.find('.fhandle-active > i').first().attr('class').split(' ')[1];
        const frag = new DocumentFragment();

        frag.append(document.createElement('option'))

        // populate configs list
        _navbarConfigList.find('.'+fileType).each(function () {
            const _this = $(this).parent();

            if (_this.hasClass('fhandle-active'))
                return true;

            const idx = _this.attr('data-idx');
            const node = document.createElement('option');

            node.text = idx;
            node.value = idx;

            frag.append(node);
        });

        _copyToModal.find('.copytoconfig-list').html('').append(frag);

        return _copyToModal;
    }

    util.getPropertiesData = async (cmd, path) => {
        let pdata = false;

        await Utils.fetchReq(FetchURL.GUIEditor, cmd, new FetchReqDataGet(path), res => {
            pdata = res;

            if (res === false)
                throw new Error('trpath_e');

        }).catch((e) => {
            Utils.showNotification(ToastType.Error, ToastMessage.PropsDataE);
            console.log(e);
        });

        return pdata;
    }

    Object.freeze(util);
    return util;
})();