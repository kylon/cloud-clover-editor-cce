/*!
 * Cloud Clover Editor
 * Copyright (C) kylon - 2016-2022
 * Licensed under GPLv3 license
 */
'use strict';

let autosaveFlag = false;
let autosaveInt = null;

onmessage = function(e) {
    switch (e.data[0]) { // cmd
        case 'setATF': {
            autosaveFlag = e.data[1];
        }
            break;
        case 'start': {
            autosaveInt = setInterval(function() {
                if (!autosaveFlag)
                    return;

                autosaveFlag = false;

                postMessage('atf');
            }, 10*1000);
        }
            break;
        case 'stop': {
            clearInterval(autosaveInt);
            autosaveInt = null;
            postMessage('kill');
        }
            break;
        default:
            break;
    }
};

