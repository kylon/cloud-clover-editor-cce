## CCE TOOLS

### gen_smbios.py
Generate smbios-data.js from OpenCorePkg data.

1. Place **gen_smbios.py** in OpenCorePkg source root folder
2. Open a terminal and run the script
3. Place the generated **smbios-data.js** in **style/js/data**