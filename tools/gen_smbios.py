#!/usr/bin/env python3

from pathlib import Path
import datetime
import yaml
import re
import os
import base64
import struct

APPLE_SMBIOS_SMC_VERSION_SIZE = 16  # from OpenCore
OC_SMBIOS_PATH = "AppleModels/DataBase/"
OUTPUT_FILE = "smbios-data.js"

def genArrIndex(productName):
    return productName.replace(",", "").lower()

def genEmptySmcVer():
    return ['0x00'] * APPLE_SMBIOS_SMC_VERSION_SIZE

# TEMP?
def getSmcVersionStr(smcRev):
    r = ""
    i = 0

    smcRev.pop(3)

    for b in smcRev:
        r += str(hex(int(b, 16))).replace("0x", "")

        if i == 0:
            r += "."
            i += 1

    return r

def smcVersionToByteArr(verStr):
    bar = bytearray(verStr.upper(), 'ascii')

    while len(bar) < APPLE_SMBIOS_SMC_VERSION_SIZE:
        bar.append(0x00)

    return bar

def fwFeaturesToByteArr(hexStr):
    return bytearray(struct.pack('<Q', int(hexStr, 16)))

def getValue(data, key):
    return data[key][0] if isinstance(data[key], list) else data[key]

def encodeData(data):
    if type(data) is list:
        for i, v in enumerate(data):
            data[i] = int(data[i], 16)

        return base64.b64encode(bytearray(data)).decode('utf-8')

    elif type(data) is bytearray:
        return base64.b64encode(data).decode('utf-8')

def getProcessorType(data):
    if 'ProcessorType' in data:
        return getValue(data, 'ProcessorType')
    else:
        return '0'

def generatePrettyName(productName, cpuCodenameList):
    codnames = list(set(cpuCodenameList))

    return productName + " - " + "/".join(codnames).replace("'", "\\'")

def formatListToJsArray(cpuList, nline):
    ret = '['

    for cpu in cpuList:
        ret += f"{nline}\t\t'{cpu}',"

    return ret + f"{nline}\t]"

def getModelSortKey(name):
    r = re.search('[a-zA-Z]+(\d+).yaml', name)
    n = int(r.group(1) if r else 0)

    if "IMP" in name:
        return 1000 + n
    elif "IM" in name:
        return 2000 + n
    elif "MBP" in name:
        return 3000 + n
    elif "MBA" in name:
        return 4000 + n
    elif "MB" in name:
        return 5000 + n
    elif "MM" in name:
        return 6000 + n
    elif "MP" in name:
        return 7000 + n
    elif "XS" in name:
        return 8000 + n

def generateSMBIOSFile():
    files = sorted(Path(OC_SMBIOS_PATH).glob('**/*.yaml'), key=lambda file: getModelSortKey(file.name))
    now = datetime.datetime.now()
    out = open(OUTPUT_FILE, 'w')
    nline = "\n\t\t"

    print(
        "/*!\n"
        "* Cloud Clover Editor\n"
        f"* Copyright (C) kylon - 2016-{now.year}\n"
        "* Licensed under GPLv3 license\n"
        "*\n"
        "* AUTO-GENERATED FILE - DO NOT EDIT!\n"
        "*/\n"
        "'use strict';\n\n"
        "const smbiosdata = {", file=out)

    for mac in files:
        with open(str(mac)) as m:
            data = yaml.load(m, Loader=yaml.BaseLoader)
            model = genArrIndex(data['SystemProductName'])
            prettyName = generatePrettyName(data['SystemProductName'], data['Specifications']['CPUCodename'])
            boardId = getValue(data, 'BoardProduct')
            chassVer = getValue(data, 'ChassisVersion')
            procType = getProcessorType(data)
            maxOsVer = data['MaximumOSVersion'] if data['MaximumOSVersion'] != 'null' else 'Latest'
            cpuList = formatListToJsArray(data['Specifications']['CPU'], nline)
            gpuList = formatListToJsArray(data['Specifications']['GPU'], nline)
            smcVerStr = getSmcVersionStr(data['SmcRevision']) if 'SmcRevision' in data else ''
            boardRevEnc = encodeData([data['BoardRevision']]) if 'BoardRevision' in data else '0'
            smcRevEnc = encodeData(data['SmcRevision']) if 'SmcRevision' in data else ''
            smcVerEnc = encodeData(smcVersionToByteArr(smcVerStr)) if 'SmcRevision' in data else encodeData(genEmptySmcVer())
            smcBranchEnc = encodeData(data['SmcBranch']) if 'SmcBranch' in data else ''
            smcPlatEnc = encodeData(data['SmcPlatform']) if 'SmcPlatform' in data else ''
            fwFeaturseEnc = encodeData(fwFeaturesToByteArr(data['FirmwareFeatures'])) if 'FirmwareFeatures' in data else ''
            fwFeatMaskEnc = encodeData(fwFeaturesToByteArr(data['FirmwareFeaturesMask'])) if 'FirmwareFeaturesMask' in data else ''

            print(
                f"\t{model}: {{{nline}"
                f"prettyName: '{prettyName}',{nline}"
                f"productName: '{data['SystemProductName']}',{nline}"
                f"biosReleaseDate: '{data['BIOSReleaseDate']}',{nline}"
                f"biosVendor: '{data['BIOSVendor']}',{nline}"
                f"biosVersion: '{data['BIOSVersion']}',{nline}"
                f"biosLegacyVersion: '{data['BIOSLegacyVersion']}',{nline}"
                f"boardID: '{boardId}',{nline}"
                f"boardType: '{int(data['BoardType'], 16)}',{nline}"
                f"boardManufacturer: '{data['BoardManufacturer']}',{nline}"
                f"boardVersion: '{data['BoardVersion']}',{nline}"
                f"boardRevision: '{int(data.get('BoardRevision', '0'), 16)}',{nline}"
                f"boardAssetTag: '{data.get('BoardAssetTag', '')}',{nline}"
                f"chassisType: '{data['ChassisType']}',{nline}"
                f"chassisManufacturer: '{data['ChassisManufacturer']}',{nline}"
                f"chassisAsset: '{data['ChassisAssetTag']}',{nline}"
                f"chassisVer: '{chassVer}',{nline}"
                f"locationInChassis: '{data['BoardLocationInChassis']}',{nline}"
                f"smcVersion: '{smcVerStr}',{nline}"
                f"fwFeatures: '{data.get('FirmwareFeatures', '')}',{nline}"
                f"fwFeaturesMask: '{data.get('FirmwareFeaturesMask', '')}',{nline}"
                f"exFwFeatures: '{data.get('ExtendedFirmwareFeatures', '')}',{nline}"
                f"exFwFeaturesMask: '{data.get('ExtendedFirmwareFeaturesMask', '')}',{nline}"
                f"platformFeature: '{data.get('PlatformFeature', '0xFFFFFFFF')}',{nline}"
                f"smcRevisionEnc: '{smcRevEnc}',{nline}"
                f"smcVersionEnc: '{smcVerEnc}',{nline}"
                f"smcBranchEnc: '{smcBranchEnc}',{nline}"
                f"smcPlatformEnc: '{smcPlatEnc}',{nline}"
                f"boardRevisionEnc: '{boardRevEnc}',{nline}"
                f"fwFeaturesEnc: '{fwFeaturseEnc}',{nline}"
                f"fwFeaturesMaskEnc: '{fwFeatMaskEnc}',{nline}"
                f"family: '{data['SystemFamily']}',{nline}"
                f"manufacturer: '{data['SystemManufacturer']}',{nline}"
                f"memFormFactor: '{int(data.get('MemoryFormFactor', '0x2'), 16)}',{nline}"
                f"processorType: '{int(procType, 16)}',{nline}"
                f"systemSkuNo: '{data['SystemSKUNumber']}',{nline}"
                f"modelCode: '{data['AppleModelCode'][0]}',{nline}"
                f"year: '{data['AppleModelYear'][0]}',{nline}"
                f"minOSVersion: '{data['MinimumOSVersion']}',{nline}"
                f"maxOSVersion: '{maxOsVer}',{nline}"
                f"version: '{data['SystemVersion']}',{nline}"
                f"specs: {{{nline}\t"
                f"cpu: {cpuList},{nline}\t"
                f"gpu: {gpuList},{nline}\t"
                f"ram: '{data['Specifications']['RAM'][0]}',{nline}"
                '}\n\t},', file=out)

    print("};", file=out)
    out.close()

def main():
    if not os.path.isdir(OC_SMBIOS_PATH):
        print("\n" + OC_SMBIOS_PATH + ': folder not found\n\nPlace the script in OpenCorePkg source root folder\n')
        exit(1)

    print('Generating smbios-data.js..')
    generateSMBIOSFile()
    print('done')

# run
main()
